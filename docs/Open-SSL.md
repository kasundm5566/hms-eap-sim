# Install Open SSL

### Location
download open ssl from [here] (https://www.openssl.org/source/)

### Version
openssl-1.1.0 (stable)

Ubuntu 16.04 LTS

### Installation

1. unzip openssl-1.1.0c.tar.gz `unzip openssl-1.1.0c.tar.gz`
1. goto extracted location `cd openssl-1.1.0c`
1. run `./config shared --openssldir=/usr/local`
1. make `make`
1. install `make install`