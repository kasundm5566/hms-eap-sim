# INSTALLATION GUIDE

## INSTALLATION

1. Currently, nginx packages are available for the following distributions and versions:

RHEL/CentOS:

Version	Supported Platforms
5.x	x86_64, i386
6.x	x86_64, i386
7.x	x86_64, ppc64le
Debian:

Version	Codename	Supported Platforms
7.x	wheezy	x86_64, i386
8.x	jessie	x86_64, i386
Ubuntu:

Version	Codename	Supported Platforms
12.04	precise	x86_64, i386
14.04	trusty	x86_64, i386, aarch64/arm64
16.04	xenial	x86_64, i386, ppc64el
16.10	yakkety	x86_64, i386
SLES:

Version	Supported Platforms
12	x86_64
To enable automatic updates of Linux packages set up the yum repository for the RHEL/CentOS distributions, the apt repository for the Debian/Ubuntu distributions, or the zypper repository for SLES.

2. Pre-Built Packages for Stable version

To set up the yum repository for RHEL/CentOS, create the file named /etc/yum.repos.d/nginx.repo with the following contents:

[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/OS/OSRELEASE/$basearch/
gpgcheck=0
enabled=1
Replace “OS” with “rhel” or “centos”, depending on the distribution used, and “OSRELEASE” with “5”, “6”, or “7”, for 5.x, 6.x, or 7.x versions, respectively.

For Debian/Ubuntu, in order to authenticate the nginx repository signature and to eliminate warnings about missing PGP key during installation of the nginx package, it is necessary to add the key used to sign the nginx packages and repository to the apt program keyring. Please download this key from our web site, and add it to the apt program keyring with the following command:

sudo apt-key add nginx_signing.key
For Debian replace codename with Debian distribution codename, and append the following to the end of the /etc/apt/sources.list file:

deb http://nginx.org/packages/debian/ codename nginx
deb-src http://nginx.org/packages/debian/ codename nginx
For Ubuntu replace codename with Ubuntu distribution codename, and append the following to the end of the /etc/apt/sources.list file:

deb http://nginx.org/packages/ubuntu/ codename nginx
deb-src http://nginx.org/packages/ubuntu/ codename nginx
For Debian/Ubuntu then run the following commands:

apt-get update
apt-get install nginx
For SLES run the following command:

zypper addrepo -G -t yum -c 'http://nginx.org/packages/sles/12' nginx


## Starting, Stopping, and Reloading Configuration

1. To start nginx, run the executable file. Once nginx is started, it can be controlled by invoking the executable with the -s parameter. Use the following syntax:
   
   nginx -s signal
   Where signal may be one of the following:
   
   stop — fast shutdown
   quit — graceful shutdown
   reload — reloading the configuration file
   reopen — reopening the log files
   
## Configurations

1. Add following into nginx.conf file to forward requests to multiple servers.

stream {
    upstream dns_upstreams {
        server 127.0.0.1:1822;
        server 127.0.0.1:1823;
    }

    server {
        listen 1812 udp;
        proxy_pass dns_upstreams;
    }
}

Note: 
1. 1812 is the port which external systems send the requests and 
   1822 & 1823 are the internal eap-sim server instances' ports.
2. Here we use normal round-robin method for load balancing. 
   To change the load balancing methods please refer 'https://nginx.org/en/docs/http/load_balancing.html'  


References:
https://nginx.org/en/docs/
http://nginx.org/en/linux_packages.html#stable
https://nginx.org/en/docs/beginners_guide.html
https://nginx.org/en/docs/http/load_balancing.html