#installation guide (hazelcast-3.8)
Hazelcast is provider for distributed data structures such as map, list, set ... etc

### Location

download hazelcast from [3.8] (https://hazelcast.org/download/)


### Version

hazelcast 3.8
Ubuntu 16.04 LTS

### Installation

1. unzip hazelcast-3.8.zip `unzip hazelcast-3.8.zip`
1. goto `/path/to/hazelcast-3.8/bin`
1. update 'interface' and 'member' `hazelcast.xml`

   `          <multicast enabled="true">
                  <multicast-group>224.2.2.3</multicast-group>
                  <multicast-port>54327</multicast-port>
              </multicast>
              <tcp-ip enabled="false">
                  <interface>127.0.0.1</interface>
                  <member-list>
                      <member>127.0.0.1</member>
                  </member-list>
              </tcp-ip>`


1. start hazelcast
` ./start.sh`