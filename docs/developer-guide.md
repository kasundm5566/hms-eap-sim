# Eap Authentication Server Developer Guide

This document will describe the module structure and dependencies used by the project. Please refer to the installation-guide.md for information on required software and building the application.


## Module Structure

<pre>
├── auth-simulator
├── core
├── eapsim-server
├── fips186jna
├── fips186lib
├── ldap-connector
├── logs
├── mapgw-simulator
├── project
├── reporting
├── sbt
├── sbt-launch.jar
├── sigtran-gw-connector
├── sigtran-gw-sim
</pre>

* core - Contains all authentication logic
* sigtran-gw-connector - Handle the connectivity between our app and Sigtran Gateway
* ldap-connector - Handle connectivity to LDAP server
* reporting - Report design and reporting web application
* fips186lib - Contains the C Library for FIPS 186-2 pSuedo Random Number Generation
* fips186jna - JNA integration with fips186lib C Library
* eapsim-server - Server module, integrates all other modules.

#### fips186lib

Eap-Server uses a native C library for the calculation of fips-186 psedo-random number. Refer the readme.md file in that module for information on how to build that module. Pre-built `libfips186.so` is available in `eapsim-server/native_lib/` and that file is the one being used when eap-server is build. If you do any changes to C code, you'll have to build the `libfips186.so` file and copy that to `eapsim-server/native_lib/`. 

### Authentication Logic

Refer `core/readme.md` for information on authentication logic and key generation.


## Dependent Modules

### Radius 

For Radius connectivity and EAP message decoding we use two libraries from [radius-connector module](http://gitweb.hsenidmobile.com/hsenid-component/radius-connector.git)
 
