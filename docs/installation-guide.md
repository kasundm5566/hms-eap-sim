# INSTALLATION GUIDE

This guide intended to use by onsite and DA

## Software

* JDK 1.7.0_80
* Mysql 5.7.12
* Maven >= 3.3.9
* GCC and MAKE utilities(optional)

## Environment Setup

* Linux kernel with SCTP support
* install tomcat :todo [update version here]

## Installation

### eap-server

1. unzip `eapsim-server-bin.tar.gz`
1. start server

<pre>
   ./eapsim-server start
</pre>

### Sigtran Gateway

1. DA can use `modules/sigtran-gw-sim` as sigtran gateway
1. onsite should use `sigtran-gateway.zip`
1. unzip `unzip sigtran-gateway.zip`
1. start gateway

<pre>
cd sigtran-gateway/bin
./sigtran-gateway start
</pre>

### LDAP
Refer `docs/open-ldap-installation.md`

### SPML Simulator
1. checkout spml-connector, 1.0.10 from  `git@gitlablive.hsenidmobile.com:hsenid-component/spml-connector.git`

1. To setup SPML simulator, refer smpl-connector/simulator/smpl-ws/readme.md in smpl-connector project.

1. SSL support configuration has described in `docs/spml-ssl-support.md`


### Building and Runnning Sigtran Gateway simulator

1. Go to modules/sigtran-gw-sim
2. Execute `./start-sim.sh`

### Building and Running Access-Point simulator
 
1. Start up sbt `./sbt`
2. Execute `project auth-simulator` in sbt console
2. Execute `;clean ;one-jar` in sbt console
3. Go to `modules/auth-simulator/target/scala-2.10/`
4. Execute `java -jar auth-simulator_2.10-1.0.0-SNAPSHOT-one-jar.jar`

### Setting up OpenLDAP Server

Follow the instructions given in *open-ldap-installation.md* to install and configure OpenLDAP Server.

### Setting up SPML keys and simulator
1. To generate keys for SPML connector, refer smpl-connector/simulator/smpl-ws/readme.md in smpl-connector project.
   Note: Key path should be configured in spml.properties file
2. To setup SPML simulator, refer smpl-connector/simulator/smpl-ws/readme.md in smpl-connector project.

### Testing Authentication

Follow the instrutions given in *modules/auth-simulator/readme.md* on how to use Access-Point simulator to test authentication.

