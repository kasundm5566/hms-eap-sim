### Certificate Types

### Certificates and Encodings

an X.509 certificate is a digital document that has been encoded and/or digitally signed according to RFC 5280

the term X.509 certificate usually refers to the IETF’s PKIX Certificate and CRL Profile of the X.509 v3 certificate standard.
commonly referred to as PKIX for `Public Key Infrastructure (X.509).`

### Encodings (also used as extensions)

* .DER 
    - used for binary DER encoded certificates
    - bear the CER or the CRT extension
    - mean: `I have a DER encoded certificate`

* .PEM
    - used for different types of X.509v3 files which contain ASCII (Base64) armored data prefixed with a “—– BEGIN …” line.
    
    
### Common Extensions
* .CRT 
    - CRT extension is used for certificates
    - The certificates may be encoded as binary DER or as ASCII PEM
    - The CER and CRT extensions are nearly synonymous. 
* .CER
    - alternate form of .crt (Microsoft Convention) 
* .KEY  
    - The KEY extension is used both for public and private PKCS#8 keys.
    - The keys may be encoded as binary DER or as ASCII PEM
    
    
    
##Transform

###PEM to DER
openssl x509 -in cert.crt -outform der -out cert.der


###DER to PEM
openssl x509 -in cert.crt -inform der -outform pem -out cert.pem

    
    
### Useful sites, 

* X509 certificate examples for testing and verification    
http://fm4dd.com/openssl/certexamples.htm    


### Encryption Algorithms used in certificates

1. RSA
1. DSA

## Cert Request
  
Use this to request a certificate from trusted authority . We have to provide private key.  

### Format 

.CSR