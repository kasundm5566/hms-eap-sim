#BDB installation guide (4.7.25)
Berkeley DB (BDB) is a software library intended to provide a high-performance
embedded database for key/value data. Berkeley DB is written in C with API bindings
for C++, C#, Java, Perl, PHP, Python, Ruby, Smalltalk, Tcl, and many other programming languages

### Location

download bdb from [4.7.25] (http://www.oracle.com/technetwork/database/database-technologies/berkeleydb/downloads/index-082944.html)


### Version

BDB 4.7.25
Ubuntu 16.04 LTS

### Installation

1. unzip db-4.7.25.tar.gz `tar -xf db-4.7.25.tar.gz`
1. goto build_unix `cd build_unix`
1. run `../dist/configure --prefix=/usr/local/`
1. make `make`
1. install, `sudo make install`

*Note* Documents will be install to /usr/local/docs
