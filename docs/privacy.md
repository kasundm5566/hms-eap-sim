# Pseudo and Privacy 

http://fm4dd.com/openssl/certexamples.htm
https://www.mayrhofer.eu.org/create-x509-certs-in-java
https://support.ssl.com/Knowledgebase/Article/View/19/0/der-vs-crt-vs-cer-vs-pem-certificates-and-how-to-convert-them
 
### EAP Packet Format
 
    0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |     Code      |  Identifier   |            Length             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |    Data ...
       +-+-+-+-+ 
       
       EAP Codes
       ---------
        1. Request
        2. Response
        3. Success
        4. Failure
 
### Full Authentication

    
     Peer                                               Authenticator
           |                               EAP-Request/Identity          |
           |<------------------------------------------------------------|
           |                                                             |
           | EAP-Response/Identity [IMSI, psudo or encripted  ]          |
           |                                                             | 
           |------------------------------------------------------------>|
           |                                                             |
           |           EAP-Request/SIM/Start (AT_VERSION_LIST)           |
           |           RequesType = 18 (SIM), SubType = 10 (start)       |
           |<------------------------------------------------------------|
           |                                                           +-----------------+  
           | EAP-Response/SIM/Start (AT_NONCE_MT, AT_SELECTED_VERSION) | Get GSM verctor | n=2 or 3 for triplet
           |                                                           |                 | [KC, SRES, RAND ]
           |                                                           +-----------------+
           |------------------------------------------------------------>|
           |                                                             |
           |           EAP-Request/SIM/Challenge (AT_RAND, AT_MAC)       |
           |           RequesType = 18 (SIM), SubType = 11 (challenge)   |
           |<------------------------------------------------------------|
       +-------------------------------------+                           |
       | Peer runs GSM algorithms, verifies  |                           |
       | AT_MAC and derives session keys     |                           |
       +-------------------------------------+                           |
           | EAP-Response/SIM/Challenge (AT_MAC)                         |
           |------------------------------------------------------------>|
           |                                                             |
           | EAP-Response/SIM/Client-Error                               |                
           | calculaed AT_MAC not match with server                      |
           |------------------------------------------------------------>|
           |                                             EAP-Success     |
           |<------------------------------------------------------------|
           |                                                             |
    
                Figure 1: EAP-SIM full authentication procedure
                
           AT_VERSION_LIST - Version list that server supported     
           NONCE_MT - random number , chosen by the peer     
           AT_SELECTED_VERSION - version number selected by peer
           AT_MAC - Message authentication code 
           NAI - Network Access Identifier [RFC4282]
                72 oct < lenght < 253 oct
                username@realm
                user@homerealm.example.net
           IMSI - International Mobile Subscriber Identity [15 digits]          
           
           AT_ANY_ID_REQ - General Identity request attribute which server uses

### Username Types in EAP-SIM identities

1. Permanent username
   1123456789098765@myoperator.com
   1123456789098765 is the permanent username
   - can use multiple times
1. Pseudonym username      
   3s7ah6n9q@myoperator.com
   3s7ah6n9q is the pseudonym username 
   - can use multiple times
1. Fast re-authentication username 
   53953754@myoperator.com
   53953754 is the fast re-authentication username
   - one time identifier. which are not reused across multiple EAP exchanges
   
      
### Format of the Permanent Username
1. SIM ==>  "1" |IMSI   | gives the character concatenation
1. 295023820005424 -> 1295023820005424
1. Hex Notation: 31 32 39 35 30 32 33 38 32 30 30 30 35 34 32 34

### Generating Pseudonyms and Fast Re-authentication Identities by the Server
1. produces pseudonym and fast re-auth identity in an implementation dependent manner
1. only EAP server able to map pseudonym and fast re-auth identity with permanent
1. centralized mechanism to map pseudonym, re-auth identity to map with permanent
1. fast re-authentication identity
    - server MAY include realm name to make the fast re-auth identity be forwarded to same eap-server.
    - SHOULD include a random component.
    - SHOULD be fresh and different from the previous ones.
    - random component works as a full authentication context identifier
    - permanent usernames, pseudonym usernames, and fast re-authentication usernames are separate and
         recognizable from each other
    - EAP-SIM and EAP-AKA usernames be distinguishable from each other
    - server should be able to determine that the NAI username is a pseudonym or fast re-auth id
      Ex: start pseudonym with "3" character, 
          start fast-re-auth with "4" character
 1. pseudonym is transmitted as a username without an NAI realm.
 1. fast re-authentication identity is transmitted as a complete NAI.
 1. grammar is same as NAI username grammar
 1. peer MAY fail to save pseudonym identity sent in an EAP-Request/SIM/Challenge. 
    So, server SHOULD maintains most recently used pseudonym in addition to most recently issued. 
    If the authentication exchange is not completed successfully, then
       the server SHOULD NOT overwrite the pseudonym username that was
       issued during the most recent successful authentication exchange.
              
          
### Transmitting Pseudonyms and Fast Re-authentication Identities to the Peer

1. The EAP-Request/SIM/Challenge message MAY include an encrypted
      pseudonym username and/or an encrypted fast re-authentication
      identity in the value field of the AT_ENCR_DATA attribute.
      (Because, Fast re-auth and identity privacy support are optional)      
1. Server may include new fast re-authentication identity in EAP-Request/SIM/Re-authentication
1. peer MAY ignore AT_ENCR_DATA and always use permanent identity.
1. On fast re-authentication, server MAY include new re-auth identity in EAP-Request/SIM/Re-authentication

1. On receipt of the EAP-Request/SIM/Challenge, peer MAY decrypt the encrypted data
   in AT_ENCR_DATA, and if it contains pseudonym, then it will use it
   next full authentication. And If a fast re-authentication identity is included 
   fast re-auth context will update with new re-auth identity.
   
1.  If the authentication exchange does not complete successfully, the peer MUST ignore the received
    pseudonym username and the fast re-authentication identity.   
1. If the peer does not receive a new pseudonym username in the
      EAP-Request/SIM/Challenge message, the peer MAY use an old pseudonym
      username instead of the permanent username on the next full
      authentication.        
      
1. peer MUST NOT re-use fast re-authentication identity even if authentication exchange was not completed 
   [Sampath: This means, eap-server SHOULD send new re-authentication identity with the EAP-Request/SIM/Challenge]
      
      
### Usage of the Pseudonym by the Peer
1. When the optional identity privacy support is used on full
      authentication, the peer MAY use a pseudonym username received as
      part of a previous full authentication sequence as the username
      portion of the NAI.
1. Peer MUST NOT modify the pseudonym username received in AT_NEXT_PSEUDONYM.

1. However, peer MAY need to decorate the username with a string that indicate
   AAA routing information [check this in real environment]
1. Peer concatenates the received pseudonym username with the "@" character 
   and an NAI realm portion.
### Usage of the Fast Re-authentication Identity by the Peer

1. MUST NOT modify the username part of the fast re-authentication identity 
   received in AT_NEXT_REAUTH_ID
1. But it may appended or prepended with another string [[check this in real environment]] 

      
### Communicating the Peer Identity to the Server

1. Two ways, 
    - EAP-Response/Identity
    - EAP-Request/SIM/Start message with AT_ANY_ID_REQ, AT_FULLAUTH_ID_REQ or AT_PERMANENT_ID_REQ
      Peer include identity in AT_IDENTITY of EAP-Response/SIM/Start message
      
1. AT_ANY_ID_REQ - General identity requesting attribute      
1. AT_FULLAUTH_ID_REQ - request permanent and pseudonym      
1. AT_PERMANENT_ID_REQ - request permanent
1. *** Format of AT_IDENTITY is same as EAP-Response/Identity except that identity decoration is not allowed ***     
1. AT_IDENTITY can contain permanent, pseudonym or re-auth identity               


### Relying on EAP-Response/Identity Discouraged

- Not method specific , So do not use
- EAP server MUST use the identity-requesting attributes, above mentioned.
- Then peer will send the identity in AT_IDENTITY
- RECOMMENDED way to start an EAP-SIM exchange is to ignore any received identity strings
- The server SHOULD begin the EAP-SIM exchange by issuing theEAP-Request/SIM/Start


### Choice of Identity for the EAP-Response/Identity

- Peer has maintained fast re-authentication state information
     and wants to use fast re-authentication, then the peer transmits the
     fast re-authentication identity in EAP-Response/Identity
     
- Else, if the peer has a pseudonym username available, then the peer
     transmits the pseudonym identity in EAP-Response/Identity
- In other cases, the peer transmits the permanent identity in
     EAP-Response/Identity          
     
### Server Operation in the Beginning of EAP-SIM Exchange     

- server SHOULD begin the EAP-SIM exchange by issuing the EAP-Request/SIM/Start 

- Three methods to request an identity
    - Sends AT_PERMANENT_ID_REQ for following cases,
        - Server does not support fast re-authentication or identity privacy. 
        - She server recognizes the received identity as a pseudonym identity but the
          server is not able to map the pseudonym to permanent. 
          
    - Send AT_FULLAUTH_ID_REQ for following cases,      
        - Server does not support fast re-authentication and the server supports 
          identity privacy.
        - server recognizes the received identity as a re-authentication identity
          but the server is not able to map the re-authentication identity
          to a permanent identity.
    - Send AT_ANY_ID_REQ for following cases, 
        - server support 3 types of identity processing
        - server does not want to process received identity
        - received identity is wrong
        
       
### Processing of EAP-Request/SIM/Start by the Peer              
[Arosha - use this in the simulator logic]
- EAP-Request/SIM/Start not included an identity request attribute, then 
  response EAP-Response/SIM/Start without AT_IDENTITY
  include AT_SELECTED_VERSION, AT_NONCE_MT  
- If the EAP-Request/SIM/Start includes AT_PERMANENT_ID_REQ, 
    - and peer does not have pseudonym, then reply with permanent identity
    - If the peer has a pseudonym available, peer MAY refuse to send permanent identity
        - respond with EAP-Response/SIM/Start and include the
             permanent identity in AT_IDENTITY 
        - respond with EAP-Response/SIM/  Client-Error packet with the code 
          "unable to process packet".
- If the EAP-Request/SIM/Start includes AT_FULL_AUTH_ID_REQ, 
    - peer has a pseudonym available, peer SHOULD respond with pseudonym identity
    - peer does not have pseudonym available, Peer MUST respond with permanent identity
    - MUST NOT use re-authentication identity
    
- If the EAP-Request/SIM/Start includes AT_ANY_ID_REQ    
    - if peer maintained re-authentication context and want to fast re-auth, 
      then respond with fast re-authentication identity. 
    - else if peer has pseudonym, it responds with pseudonym identity  
    - else respond with permanent identity
- At most 3 EAP/SIM/Start round can be used. So,
  Peer MUST NOT respond to more than 3 EAP-Request/SIM/Start    
  
- The peer MUST verify that the sequence of EAP-Request/SIM/Start packets  
  that the peer receive comply with the sequencing rules defined in this document.
    - AT_ANY_ID_REQ can only use in first EAP-Request/SIM/Start
    - AT_FULLAUTH_ID_REQ MUST NOT be used if previous EAP-Request/SIM/Start included AT_PERMANENT_ID_REQ


### Attacks Against Identity Privacy

- If client don't want to reveal identity for AT_PERMANENT_ID_REQ -> EAP-Response/SIM/Client-Error and error code will be 
  "unable to process packet" and stop exchange.
- "conservative" peer replay EAP-Response/SIM/Client-Error with pseudonym
  because the peer believes that the valid network is able to map the pseudonym identity to 
  the peer's permanent identity.
  (Alternatively, the conservative peer may accept
     AT_PERMANENT_ID_REQ in certain circumstances, for example, if the
     pseudonym was received a long time ago.)
- "liberal" always accept AT_PERMANENT_ID_REQ and reply with permanent identity.
  The benefit of this policy is that it works even if the valid network
     sometimes loses pseudonyms and is not able to map them to the
     permanent identity.
     
     
     
### Processing of AT_IDENTITY by the Server

    
    
    
    
     
      
      
