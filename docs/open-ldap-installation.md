# Installing and Configuring OpenLdap (2.4.44)

### REQUIRED SOFTWARE
BDB 4.7.25 (4.6 - 4.8)
Ubuntu 16.04 LTS

### Install BDB/HDB: (BerkeleyDB)

Refer docs/BDB-installation-guide.md

### Install Open-SSL

Refer docs/Open-SSL.md

### Configuring RadiusProfile LDAP schema

0. Download LDAP from (http://www.openldap.org/software/download/OpenLDAP/openldap-release/) and extract

1. export following environment variables
<pre>
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
export CPPFLAGS=-I/usr/local/include
export LDFLAGS=-L/usr/local/lib
</pre>

2. run configure `./configure`
3. run following command

<pre>
make depend
make
sudo make install
</pre>

slapd start script is available at `/usr/local/libexec/` and installation location is `/usr/local/etc/openldap/`

### Start LDAP server
1. goto slapd location and run `sudo /usr/local/libexec/slapd`

### Stop LDAP server
run following to stop `kill -INT cat /usr/local/var/run/slapd.pid`

### Place radius schema
Copy freeradius.ldif and freeradius.schema files to `/usr/local/etc/openldap/schema`

### Update slapd.conf file

Update the suffix and rootdn settings in `/usr/local/etc/openldap/slapd.conf` file as follows.

    # See slapd.conf(5) for details on configuration options.
    # This file should NOT be world readable.
    include /etc/ldap/schema/cosine.schema
    include /etc/ldap/schema/nis.schema
    include /etc/ldap/schema/inetorgperson.schema
    include /usr/local/etc/openldap/schema/freeradius.schema
    # ...
    #######################################################################
    # BDB database definitions
    #######################################################################
    suffix          "dc=m1net,dc=com"
    rootdn          "cn=m1admin,dc=m1net,dc=com"
    # ... 

### Import Test Data

Test LDAP data is available in `doc/test-data.ldif`. Use following commands to import that.
Import data file using
<pre>
 ldapadd -D "cn=m1admin,dc=m1net,dc=com" -w secret -f /path/to/test-data.ldif
</pre>
