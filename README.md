# Readme

## Introduction
   eap-sim is a middle-ware that enable the wifi facility for m1 customer , singapore.
    This is capable of handle high traffic. 2G, 3G and 3G-GSM sim types are supporting for the
    moment using SIM and AKA (EAP Protocol). eap-sim support re-authentication , which
    help to reduce number of Operator calls (HLR calls). And also it reduce the need of full authentication
    again and again due to disconnect.

## Software

* JDK 1.7.0_80
* Mysql 5.7.17
* Maven 3.5.0 or later
* apache-tomcat-7.0.68
* GCC and MAKE utilities(optional)

## Environment Setup

* Linux kernel with SCTP support. This is not mandatory for DA. If you are willing to
  skip this step, please use module/sigtran-gw-sim. That will give a very simple
  json/http interface which act same as sigtran-gw, `git@gitlablive.hsenidmobile.com:/core-signalling/sigtran-gateway`

## How to build

1. Build following internal projects from mentioned versions with the given command.

tinyradius - 1.0.11 (https://gitlablive.hsenidmobile.com/custom-projects-m1/tinyradius)
radius-connector - 1.0.29 (https://gitlablive.hsenidmobile.com/hsenid-component/radius-connector)
spml-connector - 2.0.0 (https://gitlablive.hsenidmobile.com/hsenid-component/spml-connector)

Command: mvn clean install -DskipTests

2. Run following to build the project

<pre>
    mvn clean install -DskipTests -Dhttps.protocols=TLSv1.2
</pre>

## How to install

### eap-server

1. Go to server build directory `cd modules/eapsim-server/target`
2. copy eapsim-server-bin.tar.gz to a preferred location, `/hms/installs/apps`
3. Go to `/hms/installs/apps` and unzip eapsim-server-bin.tar.gz, `tar -xf eapsim-server-bin.tar.gz`
4. go to conf directory `cd eapsim-server/conf`
5. configure `Ldap Configurations`, `Mysql db connectivity` and `Sigtran gateway Configurations` sections in `eap-sim.conf
6. configure `spml.properties` .
7. configure `wrapper.conf`

<pre>

 wrapper.java.command=/hms/installs/jdk1.7.0_80/bin/java
 wrapper.java.initmemory=256M
 wrapper.java.maxmemory=1024M

</pre>

8. You can start server either foreground or background. Go to `bin` directory and `./eapsim-server console` or
   `./eapsim-server start` respectively.
   
9. Encrypted keys management under privacy protection of identity(IMSI)
9.1 Following link has sample certificate and key-pair files for RSA with 2048 bit.
    http://fm4dd.com/openssl/certexamples.htm
    
9.2 We have to convert key-pair file into PKCS8 format to use in Java as follows.
    Note: key-pair file extension should be PEM
    openssl pkcs8 -topk8 -inform PEM -outform DER -in 2048b-rsa-example-keypair.pem  -nocrypt > pkcs8-2048b-rsa-example-keypair.der
    
9.3 Please refer '<project-root>/docs/how-to-generate-SSL-keys-privacy-cr.txt' for key generations.

### Sigtran Gateway simulator

1. Go to modules/sigtran-gw-sim
2. Execute `./start-sim.sh`
3. Refer 'modules/sigtran-gw-sim/readme.md' for more information

### auth-simulator (Access-Point simulator)

1. goto `modules/auth-simulator/target`
2. start simulator as follows `java -jar auth-simulator-<version>-jar-with-dependencies.jar`
3. run help for more details. Refer command help `docs/functional-test-guide.md`
4. Refer 'modules/auth-simulator/readme.md' for more information

<pre>
Welcome to EAP-SIM Authenticator Simulator.
Press TAB or type "help" then hit ENTER.

Sim> help
</pre>

### LDAP server

Follow `docs/open-ldap-installation.md` to install LDAP and configurations

### SPML Simulator
1. checkout spml-connector, 2.0.0 from  `git@gitlablive.hsenidmobile.com:hsenid-component/spml-connector.git`

1. To setup SPML simulator, refer smpl-connector/simulator/smpl-ws/readme.md in smpl-connector project.

1. SSL support configuration has described in `docs/spml-ssl-support.md`

### CAS 
1. CAS web module for this project can be found in docs/cas/.
2. Make sure to change necessary configurations in 'cas/WEB-INF/classes/cas.properties' according to the environment.

### Admin
1. Build the project at 'http://172.16.0.7/svn/hSenidmobile-adminconsole/branches/admin_M1_SDP/'.

### Run project from InteliJ IDEA (Dev Only)

It is always easy and fast, running an application on IDEA when you are developing.

1. build the project. (When ever you do a change you need to rebuild that module in order to change effect)
1. install bash support plugin on IDEA `BashSupport 1.4.0_build139`
1. configure vm parameter, `jna.library.path` on  IDEA's Debug/Runner configuration of `EapSimSystemStarter.scala`,
   `-Djna.library.path=/path/to/modules/eapsim-server/native_lib`
1. disable IMSI search using spml in `eap-sim.conf` , `eapsim.msisdn.service.spml.enable = false`
1. configure wsdl location, `smpl.ws.wsdl.url=/path/to/modules/eapsim-server/src/main/resources/spml/wsdl/prov-gw-subscriber-1.0.wsdl` in `eap-sim/modules/eapsim-server/src/main/resources/spml.properties`
1. Right click on `EapSimSystemStarter.scala` and `Run`
1. Right click on `SimulatorStarter.scala` and `Run`
1. Right click on `modules/sigtran-gw-sim/start-sim.sh` and click on `Run`





### Mysql
<pre>

sql_mode="STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"


innodb_buffer_pool_size = 8192M
max_allowed_packet = 128M
thread_cache_size=20
sort_buffer_size = 8M
net_buffer_length = 8K
read_buffer_size = 2M
read_rnd_buffer_size = 8M
myisam_sort_buffer_size = 1M
max_connections = 600
query_cache_type = 1
query_cache_size = 64M
query_cache_limit = 4M
long_query_time = 5
max_heap_table_size = 768M
tmp_table_size = 1536M
innodb_file_per_table

innodb_log_file_size = 1024M
innodb_log_buffer_size = 32M
innodb_flush_log_at_trx_commit = 0
innodb_lock_wait_timeout = 400
innodb_flush_method=O_DIRECT
innodb_thread_concurrency=16
innodb_doublewrite=0

bulk_insert_buffer_size=256M

</pre>


### How to do a release

Run `mvn release:prepare -Dmaven.test.skip=true -Darguments="-Dmaven.test.skip=true" -Dresume=false`

### How to select IMSI for each flow

For SIM and AKA flows make sure to use different IMSI numbers as explained below.
    - IMSI is a 15 digit number and username is a 16 digit number.
    - Username's first digit depicts the type of protocol. i.e. AKA - 0, SIM - 1
    - Latter 15 digit depicts the IMSI.
    - Therefore we cannot use same IMSI when testing.
    - For example, Following username values cannot be use at the same time as those IMSI part is unique.
      0123727710000001
      1123727710000001

### Definitions, Acronyms and Abbreviations used
PSK - pre selected key
RADIUS - Remote Authentication Dial-In User Service
IMSI - International Mobile Subscriber Identity
AVP - attribute value pair
SIM - Subscriber Identity Module
AKA - Authentication and Key Agreement
