package hms.eapsim.map.server;

/**
 * {clientId : "test-client-01", "calledPartyPc" : 1120, "imsi": "525036159999983"}
 */
public class SaiRequest {

    private String clientId;

    private String calledPartyPc;

    private String imsi;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCalledPartyPc() {
        return calledPartyPc;
    }

    public void setCalledPartyPc(String calledPartyPc) {
        this.calledPartyPc = calledPartyPc;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SaiRequest{");
        sb.append("clientId='").append(clientId).append('\'');
        sb.append(", calledPartyPc='").append(calledPartyPc).append('\'');
        sb.append(", imsi='").append(imsi).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
