package hms.eapsim.map.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by kasun on 2/13/17.
 */
@SpringBootApplication(scanBasePackages = {"hms.eapsim.map"})
public class SigtranGWSimStarter {

    public static void main(String[] args) {
        SpringApplication.run(SigtranGWSimStarter.class, args);
    }
}