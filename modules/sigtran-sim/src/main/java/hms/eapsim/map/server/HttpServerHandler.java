package hms.eapsim.map.server;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static hms.eapsim.map.server.SAIResponse.QUINTUPLET;
import static hms.eapsim.map.server.SAIResponse.TRIPTLE;
import static hms.eapsim.map.server.SaiHeaderParameter.CORRELATION_ID;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

@ChannelHandler.Sharable
public class HttpServerHandler extends ChannelInboundMessageHandlerAdapter<Object> {

    private ExecutorService executorService;

    private long saiProcessingDealy;

    private HashedWheelTimer timer;

    private Map<String, String> specificSaiResponses;

    private static final Logger logger = Logger.getLogger(HttpServerHandler.class);

    public HttpServerHandler(ExecutorService executorService, long saiProcessingDelay, HashedWheelTimer timer,
                             Map<String, String> specificSaiResponses) {
        this.timer = timer;
        this.executorService = executorService;
        this.saiProcessingDealy = saiProcessingDelay;
        this.specificSaiResponses = specificSaiResponses;
    }

    @Override
    public void messageReceived(final ChannelHandlerContext ctx, final Object msg) throws Exception {

        logger.info("Message Received" + msg);
        final StringBuilder buf = new StringBuilder();

        final HttpRequest request = (HttpRequest) msg;

        if (msg instanceof HttpContent) {
            final HttpContent httpContent = (HttpContent) msg;

            ByteBuf content = httpContent.data();
            if (content.isReadable()) {
                String requestString = content.toString(CharsetUtil.UTF_8);
                logger.info("Received Message [" + requestString + "]");

                if (request.getUri().equals(ServerUris.SAI_URI)) {
                    String correlationId = request.headers().get(CORRELATION_ID);
                    processRequestSAI(buf, requestString);
                    applyProcessingDelayAndSubmit(ctx, buf, request, httpContent, saiProcessingDealy, correlationId);
                } else {
                    logger.error("Invalid request [" + request.getUri() + "]");
                    return;
                }
            }
        }
    }

    private void applyProcessingDelayAndSubmit(final ChannelHandlerContext ctx, final StringBuilder buf, final HttpRequest request,
                                               final HttpContent httpContent, final long delay, final String correlationId) {

        timer.newTimeout(new TimerTask() {
            @Override
            public void run(Timeout timeout) throws Exception {
                executorService.submit(new Runnable() {
                    @Override
                    public void run() {
                        writeResponse(ctx, httpContent, request, buf, delay, correlationId);
                    }
                });
            }
        }, delay, TimeUnit.MILLISECONDS);

    }

    private void processRequestSAI(StringBuilder buf, String requestString) {
        Gson gson = new Gson();
        SaiRequest saiRequest = gson.fromJson(requestString, SaiRequest.class);

        logger.info("SAI request received [" + saiRequest + "]");

        if(specificSaiResponses.containsKey(saiRequest.getImsi())) {
            buf.append(specificSaiResponses.get(saiRequest.getImsi()));
        } else if (saiRequest.getImsi().startsWith("3")) {
            buf.append(TRIPTLE);
        } else {
            buf.append(QUINTUPLET);
        }
    }

    private void writeResponse(ChannelHandlerContext ctx, HttpObject message, HttpRequest request, StringBuilder buf, long delay, String correlationId) {
        boolean keepAlive = isKeepAlive(request);

        // Build the response object.
        FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1, message.getDecoderResult().isSuccess() ? OK : BAD_REQUEST,
                Unpooled.copiedBuffer(buf.toString(), CharsetUtil.UTF_8));

        response.headers().set(CONTENT_TYPE, "application/json; charset=UTF-8");
        response.headers().set(SaiHeaderParameter.CORRELATION_ID, correlationId);

        if (keepAlive) {
            // Add 'Content-Length' header only for a keep-alive connection.
            response.headers().set(CONTENT_LENGTH, response.data().readableBytes());
            // Add keep alive header as per:
            // - http://www.w3.org/Protocols/HTTP/1.1/draft-ietf-http-v11-spec-01.html#Connection
            response.headers().set(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        }

        // Write the response.
//        ctx.nextOutboundMessageBuffer().add(response);
//        ctx.write(response);
//        ctx.fireUserEventTriggered()
        ctx.channel().write(response);//Best way. This is not going through pipeline

        // Close the non-keep-alive connection after the write operation is done.
        if (!keepAlive) {
            ctx.flush().addListener(ChannelFutureListener.CLOSE);
        }
    }

    @Override
    public void endMessageReceived(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(
            ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
