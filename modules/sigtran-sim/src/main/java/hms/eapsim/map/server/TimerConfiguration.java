package hms.eapsim.map.server;

import java.util.concurrent.TimeUnit;

/**
 * Created by sampath on 3/15/17.
 */
public class TimerConfiguration {
    private int tickPerWheel;
    private long tickDuration;
    private TimeUnit timeUnit;

    public TimerConfiguration(int tickPerWheel, long tickDuration, String timeUnit) {
        this.tickPerWheel = tickPerWheel;
        this.tickDuration = tickDuration;
        this.timeUnit = TimeUnit.valueOf(timeUnit);
    }

    public int getTickPerWheel() {
        return tickPerWheel;
    }

    public long getTickDuration() {
        return tickDuration;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }
}
