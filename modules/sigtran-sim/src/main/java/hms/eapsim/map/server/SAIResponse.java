package hms.eapsim.map.server;

/**
 * Created by sampath on 3/2/17.
 */
public class SAIResponse {

    public static final String CORRELATION_ID_KEY = "CORRELATION_ID_KEY";

    public static final String TRIPTLE = "{\"statusCode\":\"1000\",\"statusDescription\":\"Success\",\n" +
            "    \"transactionId\":\"1234567\",\"respondingSystemGt\":\"6594740131\",\n" +
            "    \"gsmTriplets\":[\n" +
            "        {\"kc\":\"1ac2e6202ac2e620\",\"sres\":\"1ac2e620\",\"rand\":\"1ac2e620ccd40bb920f6b3b474306b6b\"},\n" +
            "        {\"kc\":\"2ac2e6202ac2e620\",\"sres\":\"2ac2e620\",\"rand\":\"2ac2e620ccd40bb920f6b3b474306b6b\"},\n" +
            "        {\"kc\":\"3ac2e6202ac2e620\",\"sres\":\"3ac2e620\",\"rand\":\"3ac2e620ccd40bb920f6b3b474306b6b\"}],\n" +
            "    \"quinTuplets\":[]}";
    public static final String QUINTUPLET = "{\"statusCode\":\"1000\",\"statusDescription\":\"Success\",\"transactionId\":\"1234567\", \"respondingSystemGt\":\"6594740131\",\"gsmTriplets\":[],\"quinTuplets\":[{\"ck\":\"8bb41f73b559138cfab2b89bda709ee8\",\"autn\":\"d749908734350000cd298d7429daed26\",\"xres\":\"3fe524d57101d202\",\"ik\":\"bbdd21caaea33e90ccb827399649f189\",\"rand\":\"f12ac2e620ccd40bb920f6b3b474306b\"},{\"ck\":\"8bb41f73b559138cfab2b89bda709ee8\",\"autn\":\"d749908734350000cd298d7429daed26\",\"xres\":\"3fe524d57101d202\",\"ik\":\"bbdd21caaea33e90ccb827399649f189\",\"rand\":\"f12ac2e620ccd40bb920f6b3b474306b\"}]}\n";
}
