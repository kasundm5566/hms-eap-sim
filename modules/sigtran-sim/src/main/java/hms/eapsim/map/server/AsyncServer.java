package hms.eapsim.map.server;

import hms.common.radius.util.DefaultThreadFactory;
import hms.eapsim.util.EapDefaultThreadFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.util.HashedWheelTimer;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncServer {

    private int port;

    private String host = "localhost";

    private int childWorkerCount = 10;

    private int parentWorkerCount = 100;

    private ExecutorService responseHandlerWorker;

    private long saiProcessingDealy = 10;

    private ServerBootstrap serverBootstrap;

    private HashedWheelTimer timer;

    private TimerConfiguration timerConfiguration;

    private static final String FILE_NAME_PREFIX = "sai-resp-";
    private static final String FILE_EXTENSION = ".json";

    private static final Logger logger = Logger.getLogger(AsyncServer.class);

    public AsyncServer(int port, int parentWorkerCount,
                       int childWorkerCount, int serverResponseHandlerCount,
                       long saiProcessingDelay, TimerConfiguration timerConfiguration) {
        this.timerConfiguration = timerConfiguration;
        this.port = port;
        this.parentWorkerCount = parentWorkerCount;
        this.childWorkerCount = childWorkerCount;
        this.responseHandlerWorker = Executors.newFixedThreadPool(serverResponseHandlerCount, new EapDefaultThreadFactory("SG-SERVER-RES-HANDLER"));
        this.saiProcessingDealy = saiProcessingDelay;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void start() throws InterruptedException {
        this.timer = new HashedWheelTimer(timerConfiguration.getTickDuration()
                , timerConfiguration.getTimeUnit(), timerConfiguration.getTickPerWheel());
        timer.start();

        Map<String, String> specificSaiResponses = loadSpecificSaiResponses();

        serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(new NioEventLoopGroup(parentWorkerCount, new DefaultThreadFactory("Server-parent-IO")),
                new NioEventLoopGroup(childWorkerCount, new DefaultThreadFactory("Server-child-IO")));

        serverBootstrap.channel(NioServerSocketChannel.class);
        serverBootstrap.childHandler(new HttpServerChannelInitializer(specificSaiResponses));

        logger.info("Server started");
        Channel ch = serverBootstrap.bind(host, port).sync().channel();

        ch.closeFuture().sync();
    }

    public void stop() {
        timer.stop();
        serverBootstrap.shutdown();
    }

    private Map<String, String> loadSpecificSaiResponses() {
        Map<String, String> specificSaiResponses = new HashMap<>();
        try {
            URI uri = getClass().getResource("/responses/").toURI();
            File dir = new File(uri);
            File[] files = dir.listFiles();
            for (File file : files) {
                String fileName = file.getName();
                if(fileName.startsWith(FILE_NAME_PREFIX)) {
                    String imsi = fileName.replaceAll(FILE_NAME_PREFIX, "").replace(FILE_EXTENSION, "");
                    byte[] bytes = Files.readAllBytes(file.toPath());
                    String content = new String(bytes,"UTF-8");
                    specificSaiResponses.put(imsi, content);
                }
            }
        } catch (Exception e) {
            logger.error("Error occurred while loading responses", e);
        }
        return specificSaiResponses;
    }

    private class HttpServerChannelInitializer extends ChannelInitializer<SocketChannel> {

        private Map<String, String> specificSaiResponses;

        public HttpServerChannelInitializer(Map<String, String> specificSaiResponses) {
            this.specificSaiResponses = specificSaiResponses;
        }

        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            // Create a default pipeline implementation.
            ChannelPipeline p = ch.pipeline();

            // Uncomment the following line if you want HTTPS
            //SSLEngine engine = SecureChatSslContextFactory.getServerContext().createSSLEngine();
            //engine.setUseClientMode(false);
            //p.addLast("ssl", new SslHandler(engine));

            p.addLast("decoder", new HttpRequestDecoder());
            // Uncomment the following line if you don't want to handle HttpChunks.
            p.addLast("aggregator", new HttpObjectAggregator(1048576));
            p.addLast("encoder", new HttpResponseEncoder());
            // Remove the following line if you don't want automatic content compression.
            //p.addLast("deflater", new HttpContentCompressor());
            p.addLast("handler", new HttpServerHandler(responseHandlerWorker, saiProcessingDealy, timer, specificSaiResponses));
        }
    }
}
