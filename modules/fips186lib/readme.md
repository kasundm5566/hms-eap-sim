# C Library for FIPS 186-2 pSuedo Random Number Generation

## Building

Execute following commands
   `make clean`
   `make`
   
After compilation and linking libfips186.so will be created. You have to copy that file to `eapsim-server/native_lib/`. 
