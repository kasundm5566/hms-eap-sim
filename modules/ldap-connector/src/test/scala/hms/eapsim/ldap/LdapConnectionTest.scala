package hms.eapsim.ldap

import org.apache.directory.ldap.client.api.LdapNetworkConnection
import org.apache.directory.api.ldap.model.message.SearchScope
import scala.collection.JavaConversions._

object LdapConnectionTest extends App {
  val connection = new LdapNetworkConnection("localhost", 389);
  val resp = connection.bind("cn=m1admin,dc=m1net,dc=com", "secret");
  println(resp)

  val cursor = connection.search("ou=m1net-consumers,dc=m1net,dc=com", "(uid=6512345678*)", SearchScope.SUBTREE);
  println(cursor.getSearchResultDone())

  println(cursor)
  println(cursor.available())
  while (cursor.next()) {
    val response = cursor.get();
    val ats = response.getAttributes
    ats.foreach(a => {
      println(s"====${a.getId} ${a.get}")

    })
    println(response)


  }

    val done = cursor.getSearchResultDone();
    println(done)
    cursor.close();
}