package hms.eapsim.ldap;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class LDAPConnectorImplTest {

    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Test
    public void testSearch() throws Exception {
        String baseDn = "ou=m1net-consumers,dc=m1net,dc=com";
//        String baseSearchDn = "ou=m1net-consumers,dc=m1net,dc=com";
        String baseSearchDn = "dc=m1net,dc=com";

        LDAPConfiguration conf = new LDAPConfiguration();
        conf.setBindDN(baseDn);
        conf.setBaseSearchDN(baseSearchDn);
        conf.setBindPassword("secret");
        conf.setPort(389);
        conf.setScope(2);
        conf.setMaxActive(5);
        conf.setMaxIdle(5);
        conf.setMaxWait(5);
        conf.setMinIdle(0);
        conf.setHostName("localhost");
        conf.setExhaustedAction(ExhaustedAction.GROW.name());

        LDAPConnectorImpl connector = new LDAPConnectorImpl(conf);
        connector.init();

        List<String> requiredList = new ArrayList<>();
        String radiusclassAttr = "radiusclass";
        requiredList.add(radiusclassAttr);

        String filter = "(uid=*%s)".replaceAll("%s", "6512345679".substring(2));

        long t1 = System.currentTimeMillis();
        Map<String, Object> result = connector.search(baseSearchDn, filter, requiredList);
        long t2 = System.currentTimeMillis();

        assertEquals("wsg_7168", new String((byte[])result.get(radiusclassAttr)));
        System.out.println(result + "in " + (t2 -t1) + " ms");

    }



    @Test
    public void testWithMultiThread() throws CursorException, LdapException, InterruptedException {

        String baseDn = "ou=m1net-consumers,dc=m1net,dc=com";
        String baseSearchDn = "ou=m1net-consumers,dc=m1net,dc=com";

        LDAPConfiguration conf = new LDAPConfiguration();
        conf.setBindDN(baseDn);
        conf.setBaseSearchDN(baseSearchDn);
        conf.setBindPassword("secret");
        conf.setPort(389);
        conf.setScope(2);
        conf.setMaxActive(100);
        conf.setMaxIdle(100);
        conf.setMaxWait(20);
        conf.setMinIdle(0);
        conf.setHostName("localhost");
        conf.setExhaustedAction(ExhaustedAction.GROW.name());

        LDAPConnectorImpl connector = new LDAPConnectorImpl(conf);
        connector.init();

        List<String> requiredList = new ArrayList<>();
        String radiusclassAttr = "radiusclass";
        requiredList.add(radiusclassAttr);

        String filter = "(uid=*%s)".replaceAll("%s", "6512345679".substring(2));

        for (int i = 0; i < 20; i++) {
            executorService.submit(new SearchTask(connector, "dc=m1net,dc=com", filter, requiredList));
            Thread.sleep(100);
        }

    }

    class SearchTask implements Runnable {
        private LDAPConnector connector;
        private String baseSearchDN;
        private String filter;
        private List<String> requiredList;

        public SearchTask(LDAPConnector connector, String baseSearchDN, String filter, List<String> requiredList) {
            this.connector = connector;
            this.baseSearchDN = baseSearchDN;
            this.filter = filter;
            this.requiredList = requiredList;
        }


        @Override
        public void run() {
            try {

                long t1 = System.currentTimeMillis();
                Map<String, Object> search = connector.search(baseSearchDN, filter, requiredList);
                long t2 = System.currentTimeMillis();
                System.out.println(t2 -t1);
            } catch (LdapException e) {
                e.printStackTrace();
            } catch (CursorException e) {
                e.printStackTrace();
            }
        }
    }
}