/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.ldap

import org.apache.directory.api.ldap.model.entry.Attribute
import org.apache.directory.ldap.client.api.LdapConnection
import org.apache.directory.ldap.client.api.LdapNetworkConnection
import org.apache.directory.api.ldap.model.message.SearchScope
import scala.util.control.Exception._
import org.apache.logging.log4j.scala.Logging
import scala.collection.JavaConversions._
import scala.util.Either
import java.util.concurrent.Executors
import scalaz._
import Scalaz._
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import org.apache.directory.ldap.client.api.LdapConnectionConfig
import hms.commons.SnmpLogUtil;
import scala.annotation.tailrec
import scala.util.control.Exception
import scala.Exception

trait LdapConnector {
  def search(baseDn: String, filter: String, requiredAttributes: List[String]): List[Attribute]
}

trait LdapConnectorComponent {

  def ldapConnector: LdapConnector
}

class LdapConnectorImpl(host: String, port: Int, authDn: String, password: String, reconnectDelay: Int = 5000, timeout: Int = 1000, snmpMsgMap: Map[String, String] = Map()) extends LdapConnector with Logging {

  val ConnectionManager: LdapConnectionManager = new LdapConnectionManager(host, port, authDn, password, reconnectDelay, timeout, snmpMsgMap)

  def search(baseDn: String, filter: String, requiredAttributes: List[String]) = {
    logger.debug(s"Doing LDAP search for baseDn[${baseDn}], filter[${filter}], requiredAttributes[${requiredAttributes}]")

    def doSearch(connection: LdapNetworkConnection) = {
      try {
        if (connection.isConnected()) {
          val cursor = connection.search(baseDn, filter, SearchScope.SUBTREE);

          val atrs = if (cursor.next()) {
            val response = cursor.get();
            logger.debug(s"Search results found[${response}]")
            response.getAttributes
              .filter(a => requiredAttributes.contains(a.getId))
          } else List()

          cursor.close();

          atrs.toList
        } else {
          logger.error(s"Not connected to LDAP server [${host}:${port}]")
          try {
            connection.close()
          }
          catch {
            case ex: Exception => {
              //ignore all error
            }
          }
          ConnectionManager.reconnect
          List()
        }
      } catch {
        case ex: Exception => {
          logger.error(s"Error while executing search", ex)
          try {
            connection.close()
          }
          catch {
            case ex: Exception => {
              //ignore all error
            }
          }
          ConnectionManager.reconnect
          List()
        }
      }
    }

    ConnectionManager.LdapConnection.map(doSearch).getOrElse(List())

  }

}

class LdapConnectionManager(host: String, port: Int, authDn: String, password: String, reconnectDelay: Int, timeout: Int, snmpMsgMap: Map[String, String]) extends Logging {
  val ReconnectWait = new Object
  val ConnectionExecutor = Executors.newSingleThreadExecutor

  var LdapConnection: Option[LdapNetworkConnection] = None

  connect

  def reconnect() {
    logger.info(s"Initializing reconnection to LDAP server [${host}:${port}]")
    connect
  }

  private def connect() {
    ConnectionExecutor.submit(new Runnable() {
      def run = {
        newLdapConnection
      }
    })
  }

  private def performDelay {
    ReconnectWait.synchronized {
      logger.debug(s"Reconnecting in [${reconnectDelay}]ms")
      ReconnectWait.wait(reconnectDelay)
    }
  }

  private def createConnection(): Boolean = {
    try {
      if (!LdapConnection.map(conn => conn.isConnected()).getOrElse(false)) {
        logger.info(s"Connecting to LDAP server [${host}:${port}] with AuthDN[${authDn}], with timeout[${timeout}]ms")
        val connection = new LdapNetworkConnection(host, port)
        connection.bind(authDn, password)
        connection.setTimeOut(timeout)
        logger.info("LDAP server connection successfull.")
        putSnmpConnectionUp
        LdapConnection = Some(connection)
      } else {
        logger.info(s"Connection already availalbe for LDAP Server [${host}:${port}]")
      }

      true

    } catch {
      case ex: Exception => {
        logger.error("Error connecting to LDAP server", ex)
        putSnmpConnectionDown
        false
      }
    }
  }

  @tailrec
  private def newLdapConnection(): Unit = {
    val connected = createConnection
    if (!connected) {
      performDelay
      newLdapConnection
    }
  }

  private def putSnmpConnectionUp() {
    snmpMsgMap.get("eapsim.snmp.ldap.connection.up").foreach(v => {
      SnmpLogUtil.clearTrap("eapServerToLDAPConnection", v)
    })
  }

  private def putSnmpConnectionDown() {
    snmpMsgMap.get("eapsim.snmp.ldap.connection.down").foreach(v => {
      SnmpLogUtil.trap("eapServerToLDAPConnection", v)
    })
  }

}

