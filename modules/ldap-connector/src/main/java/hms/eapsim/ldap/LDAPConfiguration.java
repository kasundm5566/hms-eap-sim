package hms.eapsim.ldap;

import org.apache.directory.api.ldap.model.message.SearchScope;

/**
 * configurations required for ldap connector
 */
public class LDAPConfiguration {
    private String hostName;

    private int port;

    private String bindDN;

    private String baseSearchDN;

    private String bindPassword;

    private boolean testOnBorrorw;

    private boolean testOnReturn;

    private boolean testWhileIdle;

    private int maxActive;

    private int maxIdle;

    private int minIdle;

    private int maxWait;

    private long timeoutForConnection = 30000L;

    private SearchScope scope; // 0 - object, 1 - one level, 2 - sub tree

    public String getBaseSearchDN() {
        return baseSearchDN;
    }

    public void setBaseSearchDN(String baseSearchDN) {
        this.baseSearchDN = baseSearchDN;
    }

    public long getTimeoutForConnection() {
        return timeoutForConnection;
    }

    public void setTimeoutForConnection(long timeoutForConnection) {
        this.timeoutForConnection = timeoutForConnection;
    }

    private ExhaustedAction exhaustedAction = ExhaustedAction.POOL_DEFAULT;

    public ExhaustedAction getExhaustedAction() {
        return exhaustedAction;
    }

    public void setExhaustedAction(String exhaustedAction) {
        this.exhaustedAction = ExhaustedAction.valueOf(exhaustedAction);
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getBindDN() {
        return bindDN;
    }

    public void setBindDN(String bindDN) {
        this.bindDN = bindDN;
    }

    public String getBindPassword() {
        return bindPassword;
    }

    public void setBindPassword(String bindPassword) {
        this.bindPassword = bindPassword;
    }

    public boolean isTestOnBorrorw() {
        return testOnBorrorw;
    }

    public void setTestOnBorrorw(boolean testOnBorrorw) {
        this.testOnBorrorw = testOnBorrorw;
    }

    public boolean isTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public boolean isTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public int getMaxActive() {
        return maxActive;
    }

    public void setMaxActive(int maxActive) {
        this.maxActive = maxActive;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(int minIdle) {
        this.minIdle = minIdle;
    }

    public int getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(int maxWait) {
        this.maxWait = maxWait;
    }

    public SearchScope getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = SearchScope.getSearchScope(scope);
    }
}
