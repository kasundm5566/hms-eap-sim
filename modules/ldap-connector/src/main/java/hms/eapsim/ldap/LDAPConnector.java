package hms.eapsim.ldap;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.exception.LdapException;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface LDAPConnector {

    /**
     * Searching list of attributes from ldap
     * Please check examples from
     * http://www.programcreek.com/java-api-examples/index.php?api=org.apache.directory.ldap.client.api.LdapConnectionConfig
     */
    public Map<String, Object> search(String baseDN, String filter, List<String> requiredAttr) throws LdapException, CursorException;
}
