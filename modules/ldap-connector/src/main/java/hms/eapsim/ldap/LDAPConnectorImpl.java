package hms.eapsim.ldap;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.DefaultPoolableLdapConnectionFactory;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Pooler supported ldap connector
 */
public class LDAPConnectorImpl implements LDAPConnector {

    private LdapConnectionPool pool;

    private LDAPConfiguration configuration;

    private static final Logger logger = LoggerFactory.getLogger(LDAPConnectorImpl.class);

    public LDAPConnectorImpl(LDAPConfiguration configuration) {
        this.configuration = configuration;
    }

    public void init() {
        try {
            pool = createConnectionPool(configuration);
        } catch (IOException e) {
            logger.error("Error while creating pool", e);
        }
    }

    public LdapConnectionPool getPool() {
        return pool;
    }

    @Override
    public Map<String, Object> search(String baseDN, String filter, List<String> requiredAttr) throws LdapException, CursorException {
        LdapConnection connection = null;
        EntryCursor cursor = null;

        try {
            connection = pool.getConnection();
            cursor = connection.search(baseDN, filter, configuration.getScope());

            if (cursor.next()) {
                Collection<Attribute> attributes = cursor.get().getAttributes();

                Map<String, Object> result = new HashMap<>();
                for (Attribute attribute : attributes) {
                    if (requiredAttr.contains(attribute.getId())) {
                        result.put(attribute.getId(), attribute.get().getBytes());
                    }
                }
                return result;
            } else {
                return Collections.emptyMap();
            }
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (IOException e) {
                    logger.error("Error while closing cursor", e);
                }
            }
            if (connection != null) {
                pool.releaseConnection(connection);
            }
        }
    }



    private static LdapConnectionPool createConnectionPool(LDAPConfiguration configuration) throws IOException {
        final LdapConnectionConfig config = new LdapConnectionConfig();
        config.setLdapHost(configuration.getHostName());
        config.setLdapPort(configuration.getPort());
        config.setName(configuration.getBindDN());
        config.setCredentials(configuration.getBindPassword());

        final DefaultPoolableLdapConnectionFactory factory = new DefaultPoolableLdapConnectionFactory(config);

        final LdapConnectionPool pool = new LdapConnectionPool(factory);
        pool.setTestOnBorrow(configuration.isTestOnBorrorw());
        pool.setTestOnReturn(configuration.isTestOnReturn());
        pool.setTestWhileIdle(configuration.isTestWhileIdle());

        pool.setMaxActive(configuration.getMaxActive());
        pool.setMaxIdle(configuration.getMaxIdle());
        pool.setMinIdle(configuration.getMinIdle());
        pool.setMaxWait(configuration.getMaxWait());

        setExhaustAction(configuration, pool);
        return pool;
    }

    private static void setExhaustAction(LDAPConfiguration configuration, LdapConnectionPool pool) {
        switch (configuration.getExhaustedAction()) {
            case BLOCK:
                pool.setWhenExhaustedAction(LdapConnectionPool.WHEN_EXHAUSTED_BLOCK);
                break;
            case FAIL:
                pool.setWhenExhaustedAction(LdapConnectionPool.WHEN_EXHAUSTED_FAIL);
                break;
            case GROW:
                pool.setWhenExhaustedAction(LdapConnectionPool.WHEN_EXHAUSTED_GROW);
                break;
            default:
                pool.setWhenExhaustedAction(LdapConnectionPool.DEFAULT_WHEN_EXHAUSTED_ACTION);
        }
    }
}
