package hms.eapsim.ldap;

/**
 * An action to taken when the pool is exhausted
 */
public enum  ExhaustedAction {
    BLOCK,

    FAIL,

    GROW,

    POOL_DEFAULT
}
