/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key.fips186;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 *
 */
/**
 * JNA interface to access fips186_2prf C function
 */
public interface Fips186Lib extends Library {

	Fips186Lib INSTANCE = (Fips186Lib) Native.loadLibrary("libfips186.so", Fips186Lib.class);

	void fips186_2prf(byte[] mk, byte[] finalkey);

}