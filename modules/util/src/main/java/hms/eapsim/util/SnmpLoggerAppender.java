package hms.eapsim.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringWriter;

@Plugin(name="SnmpLoggerAppender", category="Core", elementType="appender", printObject=true)
public class SnmpLoggerAppender extends AbstractAppender{
    private static final Logger logger = LogManager.getLogger(SnmpLoggerAppender.class);
    private String snmpTrapFileName;

    protected SnmpLoggerAppender(String name, String snmpTrapFileName, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
        this.snmpTrapFileName = snmpTrapFileName;
    }

    @PluginFactory
    public static SnmpLoggerAppender createAppender(@PluginAttribute("name") String name,
                                                    @PluginAttribute("snmpTrapFileName") final String snmpTrapFileName,
                                                    @PluginElement("Layout") Layout layout,
                                                    @PluginElement("Filters") Filter filter) {
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new SnmpLoggerAppender(name, snmpTrapFileName, filter, layout);
    }

    @Override
    public void append(LogEvent event) {
        String message = event.getMessage().getFormattedMessage();
        sendTrapMessage(message);
    }

    private void sendTrapMessage(String message) {
        if(logger.isInfoEnabled()) {
            logger.info("Sending SNMP trap >> \n\t - SNMP Trap Sender file = " + snmpTrapFileName + "\n\t - Message = " + message);
        }

        String[] commandArray = new String[]{snmpTrapFileName, message};
        Process process = null;

        try {
            final Process e = Runtime.getRuntime().exec(commandArray, null);
            process = e;
            Thread stdErrReader = new Thread() {
                final char[] msgs = new char[1024];
                int length = -1;

                public void run() {
                    InputStreamReader ins = new InputStreamReader(e.getErrorStream());
                    StringWriter sw = new StringWriter();

                    try {
                        while((length = ins.read(msgs)) > 0) {
                            sw.write(msgs, 0, length);
                        }
                        try {
                            ins.close();
                        } catch (IOException exception) {
                            logger.debug("Error while closing input stream", exception);
                        }
                        if(SnmpLoggerAppender.this.logger.isDebugEnabled()) {
                            logger.debug("STDERR: " + sw.toString());
                        }
                        try {
                            sw.close();
                        } catch (IOException exception) {
                            logger.debug("Error while closing string writer", exception);
                        }
                    } catch (IOException exception) {
                        logger.error("Unable to read stderr:", exception);
                    }
                }
            };
            Thread stdOutReader = new Thread() {
                final char[] msgs = new char[1024];
                int length = -1;

                public void run() {
                    InputStreamReader ins = new InputStreamReader(e.getInputStream());
                    StringWriter sw = new StringWriter();

                    try {
                        while((length = ins.read(msgs)) > 0) {
                            sw.write(msgs, 0, length);
                        }
                        ins.close();
                        if(logger.isDebugEnabled()) {
                            logger.debug("STDOUT: " + sw.toString());
                        }
                        sw.close();
                    } catch (IOException exception) {
                        logger.error("Unable to read stdout:", exception);
                    }
                }
            };
            stdErrReader.start();
            stdOutReader.start();
            e.waitFor();
            stdErrReader.join();
            stdOutReader.join();
            if(logger.isDebugEnabled()) {
                logger.debug("SNMP Trap [" + message + "] was sent successfully >> [exit value=" + e.exitValue() + "]");
            }
        } catch (IOException exception) {
            logger.error("Unable to send the SNMP trap [" + message + "] ... ", exception);
        } catch (InterruptedException exception) {
            logger.error("Sending SNMP trap Interrupted", exception);
        } finally {
            if(process != null) {
                process.destroy();
            }
        }
    }
}
