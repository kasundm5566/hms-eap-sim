package hms.eapsim.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class EapDefaultThreadFactory implements ThreadFactory {
    private String prefix;
    private AtomicInteger count = new AtomicInteger(0);

    public EapDefaultThreadFactory(String prefix) {
        if (prefix == null || prefix.isEmpty()) {
            throw new RuntimeException("Prefix can not be empty");
        }
        this.prefix = prefix;
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, prefix + "-" + count.getAndIncrement());
    }
}
