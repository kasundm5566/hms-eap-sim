# Readme

    There two modes that you can use this simulator in your testing. 
        1. FUNCTIONAL mode
        2. PERFORMANCE mode
        
### Functional mode 
All the requests configured one after the other till relevant file have completed.
1. Set `simulator.running.mode` as 'FUNCTIONAL' in `performance.properties`
1. Start simulator

        http://localhost:8888/start/{FlowType}/1
        Eg:-
            http://localhost:8888/start/3ggsm/1
            http://localhost:8888/start/3g/1
            http://localhost:8888/start/2g/1
1. Stop simulator
        
        http://localhost:8888/stop

## Performance mode 
All the requests configured one after the other till relevant file have completed.
And start again as round robin fashion.

1. Set `simulator.running.mode` as 'PERFORMANCE' in `performance.properties`
2. Goto 'http://localhost:8888/' from any web browser for the help menu.
3. To start, execute 'http://localhost:8888/start/{FlowType}/{TPS} in any web browser or as a curl command.
   Flow Type: all, 2g, 3g, 3ggsm
   TPS: tps you want to test (< 1000)
4. To stop, execute 'http://localhost:8888/stop' in any web browser or as a curl command.
5. To change TPS, execute 'http://localhost:8080/change/{TPS}' in any web browser or as a curl command.
   TPS: tps that you want to change to (< 1000)

## Message sender configurations
1. When it has configured multiple eap servers, it is required to configure the server configurations of message senders accordingly.
   A message sender can be configured as follows.
```xml
    <!-- Create a message sender named messageSender1. -->
    <bean id="messageSender1" class="hms.eapsim.perf.MessageHandler" init-method="init">
        <!-- Server configirations for the message sender is in server1Configuration bean. -->
        <property name="configuration" ref="server1Configuration"/>
    </bean>
```
```xml
    <bean id="messageSenderManager" class="hms.eapsim.perf.FairMessageRouter">
        <constructor-arg>
            <array>
                <ref bean="messageSender1"/>
            </array>
        </constructor-arg>
    </bean>
```
```xml
    <!-- Server configurations of the messageSender1. These configurations are defined in the performance.properties file. -->
    <bean id="server1Configuration" class="hms.eapsim.perf.bean.Configuration">
        <property name="runningMode" value="${simulator.running.mode}"/>
        <property name="sharedSecret" value="${client.secret.shared1}"/>
        <property name="soTimeout" value="${client.so.timeout.in.milli.sec}"/>
        <property name="requestTimeoutInMillSec" value="${request.timeout.in.milli.sec}"/>
        <property name="numberOfRequestRetry" value="${client.retry.count}"/>
        <property name="serverPort"  value="${eap.server.port1}" />
        <property name="hostAddress"  value="${eap.server.host1}" />
        <property name="receiverPoolSize"  value="${receiver.pool.size}" />
        <property name="senderPoolSize"  value="${sender.pool.size}" />
        <property name="flowExecutorPoolSize"  value="${flow.executor.pool.size}" />
        <property name="latencyThresholdInMillSec"  value="${latency.threshold.in.milli.sec}" />
        <property name="timerTickDurationInMilliSec" value="${timer.tick.duration.in.milli.sec}"/>
        <property name="timerTimeUnit" value="${timer.time.unit}"/>
        <property name="tickPerWheel" value="${timer.tick.per.wheel}"/>
        <property name="defaultSleepTimeInMilliSec" value="${timer.default.schedule.delay.in.milli.sec}"/>
        <property name="certificateFilePath" value="${certificate.file.full.path}"/>
        <property name="certificateKeyIdentifier" value="${certificate.key.identifier}"/>
    </bean>
```

    Note: When only one message sender is using, message senders which are not using should be removed from the 'messageSenderManager' bean.