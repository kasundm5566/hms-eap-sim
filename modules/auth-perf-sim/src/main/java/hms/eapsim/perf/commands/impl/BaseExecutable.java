/*
 * (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf.commands.impl;

import hms.common.radius.decoder.eap.EapMessageDecoder;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage;
import hms.common.radius.decoder.eap.elements.EapType;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.AbstractEapSimAka;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes;
import hms.common.radius.util.HexCodec;
import hms.eapsim.perf.MessageSender;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.commands.Executable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.BaseFlow;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.apache.log4j.Logger;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.RadiusPacket;

public abstract class BaseExecutable implements Executable {
    protected BaseFlow parent;
    protected boolean isLastCommand;

    protected MessageSender messageSender;
    protected RequestBuilder requestBuilder;

    private static final Logger logger = Logger.getLogger(BaseExecutable.class);

    public void setParent(BaseFlow parent) {
        this.parent = parent;
    }

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setLastCommand(boolean isLastCommand) {
        this.isLastCommand = isLastCommand;
    }

    public boolean isLastCommand() {
        return isLastCommand;
    }

    public void setRequestBuilder(RequestBuilder requestBuilder) {


        this.requestBuilder = requestBuilder;
    }

    protected String extractReAuthId(EapMessage eapMessage, Context context) {

        String nextReauthId = null;

        if (eapMessage instanceof EapRequestResponseMessage) {
            EapType type = ((EapRequestResponseMessage) eapMessage).getType();

            if (type instanceof AbstractEapSimAka) {
                AbstractEapSimAka simAka = (AbstractEapSimAka) type;
                EapSimAkaAttribute atNcrData = simAka.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA);

                if (atNcrData == null) {
                    return null;
                }
                scala.collection.immutable.List<EapSimAkaAttribute> eapSimAkaAttributeList =
                        requestBuilder.getAttributeBuilder().eapsimAttributeBuilder()
                                .decordEncrData(HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(context.getAkaEncr()), atNcrData);
                for (int i = 0; i < eapSimAkaAttributeList.size(); i++) {
                    EapSimAkaAttribute attribute = eapSimAkaAttributeList.apply(i);

                    if (attribute.getType().getValue() == EapSimAkaAttributeTypes.AT_NEXT_REAUTH_ID.getValue()) {
                        ByteBuf idData = Unpooled.copiedBuffer(attribute.getAttributeData());
                        int length = idData.readShort();
                        byte[] nextId = new byte[length];
                        idData.readBytes(nextId);

                        logger.info("-------------------------------------------------------");
                        nextReauthId = new String(nextId);
                        logger.info("Next reauth id [" + nextReauthId + "]");
                        logger.info("-------------------------------------------------------");
                    }
                }
                return nextReauthId;
            } else {
                throw new RuntimeException("Invalid message type [" + type + "]");
            }
        } else {
            throw new RuntimeException("Invalid eap message");
        }

    }

    protected EapMessage getEapMessage(RadiusPacket response) {
        RadiusAttribute attribute = response.getAttribute(EapMessage.ATTRIBUTE_TYPE);
        return EapMessageDecoder.decode(Unpooled.copiedBuffer(attribute.getAttributeData()));
    }
}
