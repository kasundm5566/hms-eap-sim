package hms.eapsim.perf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Latency {

    private static final Logger latencyLog = LoggerFactory.getLogger("latencyLog");
    private static final String SEPARATOR = "|";

    private String imsi;
    private String requestType;
    private long start;
    private long finish;

    public Latency(String imsi, String requestType, long start) {
        this.imsi = imsi;
        this.requestType = requestType;
        this.start = start;
    }

    public void markStart(long start) {
        this.start = start;
    }

    public void markFinish(long finish, long thresholdInMillis) {
        this.finish = finish;
        printLatency(thresholdInMillis);
    }

    private void printLatency(long thresholdInMillis) {
        long processTimeInMillis = finish - start;
        if (processTimeInMillis > thresholdInMillis) {
            StringBuilder builder = new StringBuilder();
            builder.append(imsi).append(SEPARATOR)
                    .append(requestType).append(SEPARATOR)
                    .append(processTimeInMillis);
            latencyLog.info(builder.toString());
        }
    }

}
