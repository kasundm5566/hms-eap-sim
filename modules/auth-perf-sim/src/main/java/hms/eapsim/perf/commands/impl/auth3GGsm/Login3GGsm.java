package hms.eapsim.perf.commands.impl.auth3GGsm;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public class Login3GGsm extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Login3GGsm.class);

    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        AccessRequest accessRequest = requestBuilder.buildSimIdentityRequest(context.getImsi(),
                context.getConfiguration().getSharedSecret(), context.getRealmPart(), context);
        RadiusPacket response = messageSender.send(accessRequest, context, getName());
        context.setPreviousReceivedPacket(response);

        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGGsmCommand.LOGIN3G_GSM.name();
    }

}
