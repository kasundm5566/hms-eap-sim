package hms.eapsim.perf.commands.impl.reauth3g;

import hms.common.radius.decoder.eap.elements.EapCode;
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes;
import hms.common.radius.util.EapMessageUtil;
import hms.common.radius.util.HexCodec;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import hms.eapsim.perf.util.CommonAttribuiteUtil;
import hms.eapsim.util.EapsimAttributeBuilderComponent;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.apache.log4j.Logger;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.RadiusPacket;
import scala.collection.JavaConversions;
import scala.collection.mutable.Buffer;

import java.util.Arrays;
import java.util.List;

/**
 * Sending EAP-Response/AKA/Re-authentication
 */
public class Reauth3G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Reauth3G.class);

    @Override
    public void execute() throws Exception {

        Context context = parent.getContext(parent.getImsi());

        if (context.getPreviousReceivedPacket() == null) {
            throw new RuntimeException("No previous packet found");
        }
        EapRequestResponseMessage eapMessage = (EapRequestResponseMessage) getEapMessage(context.getPreviousReceivedPacket());
        EapAka aka = (EapAka) eapMessage.getType();

        EapsimAttributeBuilderComponent.EapsimAttributeBuilder attBuilder = requestBuilder.getAttributeBuilder().eapsimAttributeBuilder();


        EapSimAkaAttribute atNcrData = aka.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA);

        byte[] attributeData = atNcrData.getAttributeData();
        ByteBuf encoded = Unpooled.copiedBuffer(attributeData, 2, (attributeData.length - 2));
        byte[] decode = requestBuilder.getAttributeBuilder().keyGenerator()
                .aesWithCbcDecode(HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(context.getAkaEncr()), encoded.array());
        ByteBuf buffer = Unpooled.wrappedBuffer(decode);

        //extracrt AT_COUNTER
        buffer.readShort(); //skip single short for type & length
        short latestCounter = buffer.readShort();

        //Extact AT_NONCE_S  [20 bytes]
        ByteBuf nonceSData = Unpooled.buffer(16);
        buffer.readInt();
        buffer.readBytes(nonceSData);

        //extact AT_NEXT_REAUTH_ID
        buffer.readShort(); //skip single short for type & length
        short len = buffer.readShort();
        byte[] nextId = new byte[len];
        buffer.readBytes(nextId);

        context.setLastAuthId(new String(nextId));
        logger.debug("Re-auth id [" + context.getLastAuthId() + "]");

        //update counter
        context.setCounter(latestCounter);
        EapSimAkaAttribute atCounter = attBuilder.buildAtCounter(latestCounter);

        EapSimAkaAttribute atBlankMac = attBuilder.buildAtMac();
        EapSimAkaAttribute atIv = attBuilder.buildAtIv(HexCodec.hex2Byte(context.getIv()));
        List<EapSimAkaAttribute> dataTobeEncrList = Arrays.asList(atCounter);


        if (context.isCounterTooSmall()) {
            EapSimAkaAttribute atCounterTooSmall = attBuilder.buildAtCounterTooSmall();
            dataTobeEncrList.add(0, atCounterTooSmall);
        }
        Buffer<EapSimAkaAttribute> tmp = JavaConversions.asScalaBuffer(dataTobeEncrList);

        EapSimAkaAttribute atEncrData = attBuilder.buildAtEncrData(tmp.toList(), HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(context.getAkaEncr()));
        EapAka tmpSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, Arrays.asList(atIv, atEncrData, atBlankMac));
        EapRequestResponseMessage tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), tmpSimMsg);
        byte[] encodeEapMsg = tmpEapMsg.encode().array();


        ByteBuf data = Unpooled.copiedBuffer(encodeEapMsg, nonceSData.array());
        byte[] mac = Arrays.copyOf(requestBuilder.getAttributeBuilder().keyGenerator().hmacSha1Encode(HexCodec.hex2Byte(context.getAkaKaut()), data), 16);
        EapSimAkaAttribute atMac = attBuilder.buildAtMac(mac);

        EapAka fnlSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, Arrays.asList(atIv, atEncrData, atMac));

        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), fnlSimMsg);

        RadiusPacket request = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        request.addAttribute(fnlEapMsg);

        RadiusAttribute state = context.getPreviousReceivedPacket().getAttribute("State");
        if (state != null) {
            request.addAttribute(state);
        }
        request.addAttribute(ATTRIBUTE_NAME_USERNAME, context.getImsi() + context.getRealmPart());
        request.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        request.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        request.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(request);
        EapMessageUtil.addMessageAuthenticator(request, context.getConfiguration().getSharedSecret());

        if (logger.isDebugEnabled()) {
            logger.debug("Request : " + request);
        }

        RadiusPacket response = messageSender.send(request, context, getName());
        context.setPreviousReceivedPacket(response);

        if (logger.isDebugEnabled()) {
            logger.debug("Request : " + response);
        }
        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGCommand.REAUTH_3G.name();
    }
}
