/*
 * (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf.flows;

import hms.common.radius.decoder.chain.impl.EapAttributeDecoder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.commands.Executable;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.commands.impl.Sleep;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;
import hms.eapsim.perf.flows.command.TwoGCommand;
import hms.eapsim.perf.util.Statistic;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The base class that should be implemented any {@link hms.eapsim.perf.flows.Flow}
 */
public abstract class BaseFlow implements Flow {
    protected Configuration configuration;

    private String imsi;
    protected List<? extends BaseExecutable> commands;
    protected EapAttributeDecoder attributeDecoder = new EapAttributeDecoder(null);
    protected volatile boolean isThreadBound;
    protected Statistic statistic;
    private Map<String, Context> contextMap = new ConcurrentHashMap<>();

    private static final long DEFAULT_DELAY = 10;


    private static final Logger logger = Logger.getLogger(BaseFlow.class);

    @Override
    public void clear() {
        for (Context context : contextMap.values()) {
            context.clean();
        }
        statistic.clean();
    }

    public Context getContext(String imsi) {
        return contextMap.get(imsi);
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public boolean isThreadBound() {
        return isThreadBound;
    }

    public void assigned() {
        isThreadBound = true;
    }

    public void unassigned() {
        isThreadBound = false;
    }

    public String getImsi() {
        return imsi;
    }
    @Override
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public void setContext(String imsi, Context context) {
        contextMap.put(imsi, context);
    }

    @Override
    public void setCommands(List<? extends BaseExecutable> commands) {
        this.commands = commands;
    }

    public void setAttributeDecoder(EapAttributeDecoder attributeDecoder) {
        this.attributeDecoder = attributeDecoder;
    }

    public List<? extends Executable> getCommands() {
        return commands;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void setStatistic(Statistic statistic) {
        this.statistic = statistic;
    }

    @Override
    public void auth() throws Exception {
        try {
            for (int i = 0; i < commands.size(); i++) {
                Executable executable = commands.get(i);
                long start = System.currentTimeMillis();
                executable.execute();
                long end = System.currentTimeMillis();
                updateStatistics(executable.getName(), start, end);

                if (contextMap.get(imsi).getPreviousReceivedPacket() == null) {
                    //throw new RuntimeException("Previous packet not received");
                    logger.warn("Stopped message sending due to previous packet is empty");
                    break;
                } else {
                    if (!((contextMap.get(imsi).getPreviousReceivedPacket().getPacketType() == RadiusPacket.ACCESS_ACCEPT)
                            || (contextMap.get(imsi).getPreviousReceivedPacket().getPacketType() == RadiusPacket.ACCESS_CHALLENGE))) {
                        //  throw new RuntimeException("Could not continue due to failed response received ["
                        //          + context.getPreviousReceivedPacket() + "]");
                        logger.warn("Stopped message sending due to packet type was not one of [ACCESS_ACCEPT, ACCESS_CHALLENGE]");
                        break;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error while executing 3g flow", e);
        } finally {
            unassigned();
        }
    }

    @Override
    public void schedule(HashedWheelTimer timer, final ExecutorService scheduleWorker, long defaultSleepTimeInMilliSec) throws Exception {
        assigned();
        try {
            long totalDelay = 0;
            for (int i = 0; i < commands.size(); i++) {
                final BaseExecutable executable = commands.get(i);

                if (i == commands.size() - 1) {
                    executable.setLastCommand(true);
                }

                totalDelay += defaultSleepTimeInMilliSec;
                logger.info("Sending =======> [" + i + "][delay: " + totalDelay + "] " + executable);

                timer.newTimeout(new TimerTask() {
                    @Override
                    public void run(Timeout timeout) throws Exception {
                        scheduleWorker.submit(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    logger.info("Executing =======> [" + executable + "]");
                                    executable.execute();

                                } catch (Exception e) {
                                    logger.error("Error while executing command", e);
                                }
                            }
                        });
                    }
                }, totalDelay, configuration.getTimerTimeUnit());
            }
        } catch (Exception e) {
            logger.error("Error while executing flow", e);
        }
    }

    private void updateStatistics(String name, long start, long end) {
        long duration = end - start;
        if(name.equals(TwoGCommand.LOGIN_2G.name()) || name.equals(ThreeGCommand.LOGIN_3G.name()) ||
                name.equals(ThreeGGsmCommand.LOGIN3G_GSM.name())) {
            statistic.setLoginDuration(duration);
        } else if(name.equals(TwoGCommand.SIMSTART.name()) || name.equals(ThreeGCommand.LOGIN_3G_INIT.name()) ||
                name.equals(ThreeGGsmCommand.SIMSTART3G_GSM.name())) {
            statistic.setSimStartDuration(duration);
        } else if(name.equals(TwoGCommand.CHALLENGE2G.name()) || name.equals(ThreeGCommand.CHALLENGE_3G.name()) ||
                name.equals(ThreeGGsmCommand.CHALLENGE3G_GSM.name())) {
            statistic.setChallengeDuration(duration);
        } else if(name.equals(TwoGCommand.REAUTHLOGIN2G.name()) || name.equals(ThreeGCommand.REAUTH_LOGIN_3G.name()) ||
                name.equals(ThreeGGsmCommand.REAUTH_LOGIN_3G_GSM.name())) {
            statistic.setReAuthLoginDuration(duration);
        } else if(name.equals(TwoGCommand.REAUTHSIMSTART.name()) || name.equals(ThreeGCommand.REAUTH_LOGIN_3G_INIT.name()) ||
                name.equals(ThreeGGsmCommand.REAUTHSIMSTART3G_GSM.name())) {
            statistic.setReAuthSimStartDuration(duration);
        } else if(name.equals(TwoGCommand.REAUTH2G.name()) || name.equals(ThreeGCommand.REAUTH_3G.name()) ||
                name.equals(ThreeGGsmCommand.REAUTH3G_GSM.name())) {
            statistic.setReAuthChallengeDuration(duration);
        }
    }
}
