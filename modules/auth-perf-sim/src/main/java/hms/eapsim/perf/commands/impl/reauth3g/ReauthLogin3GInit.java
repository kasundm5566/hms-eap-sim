package hms.eapsim.perf.commands.impl.reauth3g;

import hms.common.radius.decoder.eap.elements.EapCode;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute;
import hms.eapsim.perf.Utility;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import hms.eapsim.perf.util.CommonAttribuiteUtil;
import hms.eapsim.util.EapsimAttributeBuilderComponent;
import org.apache.log4j.Logger;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.RadiusPacket;

import java.util.Arrays;

/**
 * Sending EAP-Response/AKA-Identity including next re-auth id in AT_IDENTITY flag
 */
public class ReauthLogin3GInit extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(ReauthLogin3GInit.class);

    @Override
    public void execute() throws Exception {

        Context context = parent.getContext(parent.getImsi());

        if (Utility.isEmpty(context.getLastAuthId())) {
            throw new RuntimeException("Last re-auth id is empty");
        }
        EapsimAttributeBuilderComponent.EapsimAttributeBuilder eapsimAttributeBuilder =
                requestBuilder.getAttributeBuilder().eapsimAttributeBuilder();

        EapSimAkaAttribute atIdentity = eapsimAttributeBuilder.buildAtIdentity(context.getLastAuthId().getBytes());

        EapAka fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, Arrays.asList(atIdentity));
        EapMessage eapMessage = getEapMessage(context.getPreviousReceivedPacket());
        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), fnlAkaMsg);

        RadiusPacket rp = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(fnlEapMsg);
        rp.addAttribute("User-Name", context.getLastAuthId());
        rp.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        rp.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        rp.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(rp);
        RadiusAttribute state = context.getPreviousReceivedPacket().getAttribute("State");

        if (state != null) {
            rp.addAttribute(state);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Request : " + rp);
        }
        RadiusPacket response = messageSender.send(rp, context, getName());
        context.setPreviousReceivedPacket(response);

        if (logger.isDebugEnabled()) {
            logger.debug("Response : " + response);
        }
        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGCommand.REAUTH_LOGIN_3G_INIT.name();
    }
}
