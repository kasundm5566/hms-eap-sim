package hms.eapsim.perf.commands.impl.sim;

import hms.common.radius.decoder.eap.EapMessageDecoder;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import io.netty.buffer.Unpooled;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public abstract class BaseLoginEncrypt extends BaseExecutable {

    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        EapMessage eapMessage = EapMessageDecoder.decode(
                Unpooled.copiedBuffer(context.getPreviousReceivedPacket().getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()));
        RadiusAttribute state = context.getPreviousReceivedPacket().getAttribute(ATTRIBUTE_NAME_STATE);
        AccessRequest accessRequest =
                requestBuilder.buildEncryptedSimStartRequest(context.getImsi(), eapMessage.getIdentifier(), state,
                        context.getConfiguration().getSharedSecret(), context.getNonceMt(), context.getRealmPart(), context);
        RadiusPacket response = messageSender.send(accessRequest, context, getName());
        context.setPreviousReceivedPacket(response);

        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

}
