package hms.eapsim.perf.commands.impl;

import hms.eapsim.perf.commands.Executable;
import hms.eapsim.perf.domain.Context;

/**
 * Sleeping in between executable commands {@link hms.eapsim.perf.commands.Executable}
 */
public class Sleep extends BaseExecutable {

    private long time;

    public Sleep(long time) {
        this.time = time;
    }

    public static Sleep constructSleepCommand(String sleepCommandName) {

        String sleepTime = sleepCommandName.replace("sleep", "");

        if (sleepTime == null || sleepTime.isEmpty()) {
            throw new RuntimeException("Invalid sleep time input");
        }

        return new Sleep(Long.parseLong(sleepTime));
    }

    @Override
    public void execute() throws Exception {
        Thread.sleep(time);
    }

    public long getTime() {
        return time;
    }

    @Override
    public String getName() {
        return "SLEEP";
    }
}
