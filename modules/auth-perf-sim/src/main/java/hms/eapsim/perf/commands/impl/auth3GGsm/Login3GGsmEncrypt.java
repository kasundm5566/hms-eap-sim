package hms.eapsim.perf.commands.impl.auth3GGsm;

import hms.eapsim.perf.commands.impl.sim.BaseLoginEncrypt;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;

public class Login3GGsmEncrypt extends BaseLoginEncrypt {

    @Override
    public String getName() {
        return ThreeGGsmCommand.SIMSTART3G_GSM_ENCR.name();
    }

}
