package hms.eapsim.perf.commands.impl.reauth2G;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.TwoGCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public class ReAuthLogin2G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(ReAuthLogin2G.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        String lastAuthId = context.getLastAuthId();
        if (lastAuthId != null && !lastAuthId.isEmpty()) {
            AccessRequest accessRequest = requestBuilder.buildReAuthSimIdentityRequest(context.getConfiguration().getSharedSecret(), lastAuthId, context);
            RadiusPacket response = messageSender.send(accessRequest, context, getName());
            context.setPreviousReceivedPacket(response);

            parent.setContext(parent.getImsi(), context);

            if (isLastCommand) {
                parent.unassigned();
            }
        } else {
            logger.error("Re-Authentication id is not available. Looks like you have not authenticated");
            throw new Exception("Re-Authentication id is not available. Looks like you have not authenticated");
        }
    }

    @Override
    public String getName() {
        return TwoGCommand.REAUTHLOGIN2G.name();
    }
}
