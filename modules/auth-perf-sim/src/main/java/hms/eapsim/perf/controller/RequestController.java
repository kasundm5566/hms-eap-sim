/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials
 */

package hms.eapsim.perf.controller;

import hms.eapsim.perf.MessageSendMediator;
import hms.eapsim.perf.Utility;
import hms.eapsim.perf.traffic.TrafficPlan;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

import static hms.eapsim.perf.traffic.TrafficPlan.FlowType.*;
import static hms.eapsim.perf.traffic.TrafficPlan.FlowType.THREE_G;

/**
 * Front interface that accept user's commands
 */

@RestController
public class RequestController {

    @Autowired
    MessageSendMediator messageSendMediator;

    private static final Logger logger = Logger.getLogger(RequestController.class);

    // inject via application.properties
    @Value("${welcome.message:test}")
    private String message = "EAP SIM";

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        model.put("message", "EAP SIMULATOR");
        return getMessage();
    }

    @RequestMapping("/help")
    public String help() {
        return getMessage();
    }

    private String getMessage() {

        StringBuilder msg = new StringBuilder();
        msg.append("=================================================================\n");
        msg.append("            Welcome to EAP-SIM PERFORMANCE SIMULATOR\n");
        msg.append("=================================================================\n");
        msg.append("Current Mode :").append(messageSendMediator.getConfiguration().getRunningMode()).append("\n");
        msg.append("Help :\n");

        msg.append("\t").append("start\n");
        msg.append("\t\t").append("http://localhost:8888/start/{Flow Type}/{TPS}\n");
        msg.append("\t\t\t").append("Flow Type : all, 2g, 3g, 3ggsm\n");
        msg.append("\t\t\t").append("TPS       : tps you want to test (< 1000)\n");
        msg.append("\t").append("Stop Simulator\n");
        msg.append("\t\t").append("http://localhost:8080/stop\n");

        msg.append("\t").append("Change  TPS\n");
        msg.append("\t\t").append("http://localhost:8080/change/{TPS}\n");
        msg.append("\t\t\t").append("TPS       : tps that you want to change to (< 1000)\n");
        msg.append("=================================================================\n");

        return msg.toString();
    }

    /**
     * start sending message for a flow,
     * @param flow all, 2g, 3g, 3ggsm
     * @return
     * @throws IOException
     */
    @RequestMapping("/start/{flow}/{tps}")
    String start(@PathVariable String flow, @PathVariable int tps) throws IOException {

        if (logger.isInfoEnabled()) {
            logger.info("Request received [ flow : " + flow + ", tps : " + tps + "]");
        }
        if (validate(flow, tps)) {
            if (messageSendMediator.isStarted()) {
                logger.info("Message flow is already started");
                return "Message flow already stared";
            }
            messageSendMediator.start(tps, TrafficPlan.FlowType.getFrom(flow));
            return "Message flow started with [ flow : " + flow + ", tps : " + tps + "]";
        }
        return "Validation Failed";
    }

    private boolean validate(String flow, int tps) {
        boolean isValid = true;
        if (Utility.isEmpty(flow)) {
            logger.error("Flow is empty");
            isValid = false;
        } else {
            if (!(flow.toLowerCase().equals(THREE_G.getShortName().toLowerCase())
                    || flow.toLowerCase().equals(THREE_G_GSM.getShortName().toLowerCase())
                    || flow.toLowerCase().equals(TWO_G.getShortName().toLowerCase())
                    || flow.toLowerCase().startsWith(ALL.getShortName().toLowerCase()))) {
                logger.error("Unsupported [ flow : " + flow+ "]");
                isValid = false;
            }

            if (tps <= 0) {
                logger.error("invalid tps [" + tps + "]");
                isValid = false;
            }
        }
        return isValid;
    }

    @RequestMapping("/stop")
    String stop() {

        messageSendMediator.stop();
        return "Message flow stopped";
    }

    /**
     * Change current tps to given value
     * @return
     */
    @RequestMapping("/change/{tps}")
    String change(@PathVariable int tps) {

        messageSendMediator.change(tps);
        return "Changing";
    }

}
