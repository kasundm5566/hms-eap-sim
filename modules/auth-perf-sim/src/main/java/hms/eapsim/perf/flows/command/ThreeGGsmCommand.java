/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials
 */

package hms.eapsim.perf.flows.command;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kasun on 1/19/17.
 */
public enum ThreeGGsmCommand {

    LOGIN3G_GSM("login3g-gsm"),
    ANONYMOUS_3G_GSM("anonymous3g-gsm"),
    SIMSTART3G_GSM("simstart3g-gsm"),
    SIMSTART3G_GSM_ENCR("simstart3g-gsm-encr"),
    CHALLENGE3G_GSM("challenge3g-gsm"),
    REAUTH_LOGIN_3G_GSM("reauthLogin3g-gsm"),
    REAUTHSIMSTART3G_GSM("reauthsimstart3g-gsm"),
    REAUTH3G_GSM("reauth3g-gsm");

    private String commandName;

    ThreeGGsmCommand(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }

    private static final Map<String,ThreeGGsmCommand> ENUM_MAP;

    static {
        Map<String,ThreeGGsmCommand> map = new ConcurrentHashMap<>();
        for (ThreeGGsmCommand instance : ThreeGGsmCommand.values()) {
            map.put(instance.getCommandName(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static ThreeGGsmCommand get (String commandName) {
        return ENUM_MAP.get(commandName);
    }
}
