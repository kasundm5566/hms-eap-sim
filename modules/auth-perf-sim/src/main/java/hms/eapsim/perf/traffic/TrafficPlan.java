package hms.eapsim.perf.traffic;

import hms.eapsim.perf.MessageSender;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.commands.impl.Sleep;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.BaseFlow;
import hms.eapsim.perf.flows.Flow;
import hms.eapsim.perf.flows.command.CommandBuilder;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;
import hms.eapsim.perf.flows.command.TwoGCommand;
import hms.eapsim.perf.flows.impl.AttributeBuilder;
import hms.eapsim.perf.flows.impl.Flow2G;
import hms.eapsim.perf.flows.impl.Flow3G;
import hms.eapsim.perf.flows.impl.Flow3GGsm;
import hms.eapsim.perf.util.Statistic;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.util.*;

import static hms.eapsim.perf.traffic.TrafficPlan.FlowType.*;

/**
 * Hold all the flows related traffic plan.
 * This is not thread safe.
 */
public class TrafficPlan {
    private List<BaseFlow> flowList;

    private List<BaseFlow> flowList2gSIM;
    private List<BaseFlow> flowList3gSIM;
    private List<BaseFlow> flowList3gAKA;

    private Configuration configuration;

    private MessageSender messageSender;

    private RequestBuilder requestBuilder;

    private AttributeBuilder attributeBuilder = new AttributeBuilder();

    private String[] nasIdentifiers = new String[]{"M1NET-WSG12-ROC", "M1NET-WSG12-MOC", "M1NET-WSG11-ROC", "M1NET-WSG11-MOC"};
    private String[] calledStationIds = new String[]{"58:91:cf:0e:07:fe", "5c:f9:dd:40:7a:36", "89:f9:dd:40:7a:61", "ac:f9:dd:40:7a:3a"};

    private static final Logger logger = Logger.getLogger(TrafficPlan.class);

    public enum FlowType {
        ALL("all.txt", "all"),

        TWO_G("2g-sim.txt", "2g"),

        THREE_G("3g-aka.txt", "3g"),

        THREE_G_GSM("3g-sim.txt", "3ggsm");

        private String fileName;
        private String shortName;

        FlowType(String fileName, String shortName) {
            this.fileName = fileName;
            this.shortName = shortName;
        }

        public static FlowType getFrom(String shortName) {
            if (ALL.getShortName().equalsIgnoreCase(shortName)) {
                return ALL;
            } else if (TWO_G.getShortName().equalsIgnoreCase(shortName)) {
                return TWO_G;
            } else if (THREE_G.getShortName().equalsIgnoreCase(shortName)) {
                return THREE_G;
            } else if (THREE_G_GSM.getShortName().equalsIgnoreCase(shortName)) {
                return THREE_G_GSM;
            } else {
                throw new RuntimeException("Invalid short name [" + shortName + "]");
            }
        }

        public String getShortName() {
            return shortName;
        }

        public String getFileName() {
            return fileName;
        }

        public BaseFlow getFlow() {
            if (this == TWO_G) {
                return new Flow2G();
            } else if (this == THREE_G) {
                return new Flow3G();
            } else if (this == THREE_G_GSM) {
                return new Flow3GGsm();
            }
            throw new RuntimeException("Invalid flow type");
        }
    }

    public TrafficPlan() {
        flowList = new ArrayList<>();
    }

    public void add(Flow flow) {
        if (flow instanceof BaseFlow) {
            flowList.add((BaseFlow) flow);
        } else {
            throw new RuntimeException("Invalid flow type");
        }
    }

    public void load() throws IOException {
        flowList2gSIM = loadFlow(TWO_G);
        flowList3gSIM = loadFlow(FlowType.THREE_G_GSM);
        flowList3gAKA = loadFlow(FlowType.THREE_G);

        if (configuration.getRunningMode().equals(Configuration.Mode.PERFORMANCE.name())) {
            Collections.shuffle(flowList2gSIM);
            Collections.shuffle(flowList3gSIM);
            Collections.shuffle(flowList3gAKA);

            Queue<BaseFlow> queue2GSIM = new LinkedList<>(flowList2gSIM);
            Queue<BaseFlow> queue3GSIM = new LinkedList<>(flowList3gSIM);
            Queue<BaseFlow> queue3GAKA = new LinkedList<>(flowList3gAKA);

            double totalCount = flowList2gSIM.size() + flowList3gSIM.size() + flowList3gAKA.size();

            double val1 = flowList2gSIM.size() / totalCount;
            int percentage2gSIM = (int) Math.round(val1 * 100);

            double val2 = flowList3gSIM.size() / totalCount;
            int percentage3gSIM = (int) Math.round(val2 * 100);

            for(int i = 0; i < totalCount; i++) {
                int percentageIndex = i % 100;
                if(percentageIndex < percentage2gSIM) {
                    if (!queue2GSIM.isEmpty()) {
                        flowList.add(queue2GSIM.poll());
                    }
                } else if (percentage2gSIM <= percentageIndex && percentageIndex < (percentage2gSIM + percentage3gSIM)) {
                    if (!queue3GSIM.isEmpty()) {
                        flowList.add(queue3GSIM.poll());
                    }
                } else {
                    if (!queue3GAKA.isEmpty()) {
                        flowList.add(queue3GAKA.poll());
                    }
                }
            }

            if(!queue2GSIM.isEmpty()) {
                logger.error("Some of flows are not included: 2G-SIM [" + queue2GSIM + "]");
            }

            if(!queue3GSIM.isEmpty()) {
                logger.error("Some of flows are not included: 3G-SIM [" + queue3GSIM  + "]");
            }

            if (!queue3GAKA.isEmpty()) {
                logger.error("Some of flows are not included: 3G-AKA [" + queue3GAKA  + "]");
            }
        } else {
            flowList.addAll(flowList2gSIM);
            flowList.addAll(flowList3gSIM);
            flowList.addAll(flowList3gAKA);
        }
    }


    public List<BaseFlow> getFlowList() {
        return flowList;
    }

    private List<BaseFlow> loadFlow(FlowType flowType) throws IOException {
        List<BaseFlow> flowList = new ArrayList<>();
        String fileName = flowType.getFileName();

        InputStream fileStream = getClass().getClassLoader().getResourceAsStream(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));

        String flowEntry = null;
        while ((flowEntry = reader.readLine()) != null) {
            if (flowEntry.isEmpty() || flowEntry.startsWith("#")) {
                continue;
            }
            BaseFlow flow = flowType.getFlow();

            List<String> commands = Arrays.asList(flowEntry.split("\\s*,\\s*"));
            String imsi = commands.get(0).trim();
            flow.setImsi(imsi);
            List<String> executableCommandNames = commands.subList(1, commands.size());
            CommandBuilder commandBuilder = new CommandBuilder();
            List<BaseExecutable> baseCommandList = null;

            if (!commands.isEmpty()) {
                baseCommandList = new ArrayList<>();

                if (flowType == TWO_G) {
                    validateCommandWithFlow(executableCommandNames, TWO_G);
                    convertToExecutableCommands2G(executableCommandNames, baseCommandList, commandBuilder,
                            messageSender, requestBuilder, flow);

                } else if (flowType == THREE_G) {
                    validateCommandWithFlow(executableCommandNames, THREE_G);
                    convertToExecutableCommands3G(executableCommandNames, baseCommandList, commandBuilder,
                            messageSender, requestBuilder, flow);

                } else if (flowType == THREE_G_GSM) {
                    validateCommandWithFlow(executableCommandNames, THREE_G_GSM);
                    convertToExecutableCommands3GGsm(executableCommandNames, baseCommandList, commandBuilder,
                            messageSender, requestBuilder, flow);
                }
            }

            Context context = new Context();
            context.setImsi(imsi);
            int index = Math.abs(imsi.hashCode()) % 4;

            context.setCalledStationId(calledStationIds[index]);
            context.setCallingStationId(calledStationIds[index]);
            context.setNasIndentier(nasIdentifiers[index]);
            context.setConfiguration(configuration);
            context.setRequestBuilder(requestBuilder);
            context.init();

            Statistic statistic = new Statistic();
            flow.setStatistic(statistic);

            flow.setContext(imsi, context);
            flow.setCommands(baseCommandList);
            flow.setConfiguration(configuration);
            requestBuilder.setAttributeBuilder(attributeBuilder);
            flowList.add(flow);
        }
        return flowList;
    }

    private void convertToExecutableCommands3GGsm(List<String> commands,
                                                  List<BaseExecutable> baseCommandList, CommandBuilder commandBuilder,
                                                  MessageSender messageSender, RequestBuilder requestBuilder, BaseFlow flow) {

        for (String commandName : commands) {

            if (!isSleepCommand(commandName.trim())) {
                BaseExecutable baseCommand = commandBuilder.prepare3GGsmFlowCommand(commandName.trim());
                baseCommand.setMessageSender(messageSender);
                baseCommand.setRequestBuilder(requestBuilder);
                baseCommand.setParent(flow);
                baseCommandList.add(baseCommand);
            } else {
                Sleep sleep = Sleep.constructSleepCommand(commandName);
                sleep.setParent(flow);
                baseCommandList.add(sleep);
            }
        }
    }

    private void convertToExecutableCommands3G(List<String> commands,
                                               List<BaseExecutable> baseCommandList, CommandBuilder commandBuilder,
                                               MessageSender messageSender, RequestBuilder requestBuilder, BaseFlow flow) {

        for (String commandName : commands) {

            if (!isSleepCommand(commandName.trim())) {
                BaseExecutable baseCommand = commandBuilder.prepare3GFlowCommand(commandName.trim());
                baseCommand.setMessageSender(messageSender);
                baseCommand.setRequestBuilder(requestBuilder);
                baseCommand.setParent(flow);
                baseCommandList.add(baseCommand);
            } else {
                Sleep sleep = Sleep.constructSleepCommand(commandName);
                sleep.setParent(flow);
                baseCommandList.add(sleep);
            }
        }
    }

    private void convertToExecutableCommands2G(List<String> items, List<BaseExecutable> baseCommandList,
                                               CommandBuilder commandBuilder, MessageSender messageSender,
                                               RequestBuilder requestBuilder, BaseFlow flow) {

        for (String commandName : items) {

            if (!isSleepCommand(commandName.trim())) {
                BaseExecutable responseCommand = commandBuilder.prepare2GFlowCommand(commandName.trim());
                responseCommand.setMessageSender(messageSender);
                responseCommand.setRequestBuilder(requestBuilder);
                responseCommand.setParent(flow);

                baseCommandList.add(responseCommand);

            } else {
                Sleep sleep = Sleep.constructSleepCommand(commandName);
                sleep.setParent(flow);
                baseCommandList.add(sleep);
            }
        }
    }

    private void validateCommandWithFlow(List<String> commands, FlowType flowType) {

        if (flowType == TWO_G) {

            for (String command : commands) {

                if ((TwoGCommand.get(command) == null) && (!isSleepCommand(command))) {
                    throw new RuntimeException("Invalid command found for 2G flow : " + command);
                }
            }
        } else if (flowType == THREE_G) {

            for (String command : commands) {

                if ((ThreeGCommand.get(command) == null) && (!isSleepCommand(command))) {
                    throw new RuntimeException("Invalid command found for 3G flow : " + command);
                }
            }
        } else if (flowType == THREE_G_GSM) {

            for (String command : commands) {

                if ((ThreeGGsmCommand.get(command) == null) && (!isSleepCommand(command))) {
                    throw new RuntimeException("Invalid command found for 3G-GSM flow : " + command);
                }
            }
        }
    }

    private boolean isSleepCommand(String command) {

        return command.contains("sleep");
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setRequestBuilder(RequestBuilder requestBuilder) {
        this.requestBuilder = requestBuilder;
    }
}
