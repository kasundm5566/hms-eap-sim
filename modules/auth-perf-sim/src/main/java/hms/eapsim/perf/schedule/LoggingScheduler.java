/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf.schedule;

import hms.eapsim.perf.util.TPSLog;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by kasun on 1/31/17.
 */
@Component
public class LoggingScheduler {

    @Scheduled(fixedRate = 1000)
    public void loggingTransactionPerSecond() {
        TPSLog.executeLogging();
    }
}
