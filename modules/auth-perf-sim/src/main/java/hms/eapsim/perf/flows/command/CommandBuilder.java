/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials
 */

package hms.eapsim.perf.flows.command;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.commands.impl.auth2G.*;
import hms.eapsim.perf.commands.impl.auth3GGsm.*;
import hms.eapsim.perf.commands.impl.auth3g.*;
import hms.eapsim.perf.commands.impl.reauth2G.ReAuth2G;
import hms.eapsim.perf.commands.impl.reauth2G.ReAuthLogin2G;
import hms.eapsim.perf.commands.impl.reauth2G.ReAuthSimStart;
import hms.eapsim.perf.commands.impl.reauth3GGsm.ReAuth3GGSM;
import hms.eapsim.perf.commands.impl.reauth3GGsm.ReAuthLogin3GGSM;
import hms.eapsim.perf.commands.impl.reauth3GGsm.ReAuthSimStart3GGSM;
import hms.eapsim.perf.commands.impl.reauth3g.Reauth3G;
import hms.eapsim.perf.commands.impl.reauth3g.ReauthLogin3G;
import hms.eapsim.perf.commands.impl.reauth3g.ReauthLogin3GInit;

/**
 * Created by kasun on 1/19/17.
 */
public class CommandBuilder {

    public BaseExecutable prepare2GFlowCommand(String commandName) {

        if (TwoGCommand.get(commandName) == TwoGCommand.LOGIN_2G) {
            return new Login2G();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.ANONYMOUS_2G) {
            return new Anonymous2G();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.SIMSTART) {
            return new SimStart2G();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.SIMSTART_2G_ENCR) {
            return new Login2GEncrypt();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.CHALLENGE2G) {
            return new Challenge2G();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.REAUTHLOGIN2G) {
            return new ReAuthLogin2G();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.REAUTHSIMSTART) {
            return new ReAuthSimStart();
        } else if (TwoGCommand.get(commandName) == TwoGCommand.REAUTH2G) {
            return new ReAuth2G();
        } else {
            return null;
        }
    }

    public BaseExecutable prepare3GFlowCommand(String commandName) {

        if (ThreeGCommand.get(commandName) == ThreeGCommand.LOGIN_3G) {
            return new Login3G();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.ANONYMOUS_3G) {
            return new Anonymous3G();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.INIT_3G_ENCR) {
            return new Login3GEncrypt();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.CHALLENGE_3G) {
            return new Challenge3G();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.LOGIN_3G_INIT) {
            return new Login3GInit();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.REAUTH_3G) {
            return new Reauth3G();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.REAUTH_LOGIN_3G) {
            return new ReauthLogin3G();
        } else if (ThreeGCommand.get(commandName) == ThreeGCommand.REAUTH_LOGIN_3G_INIT) {
            return new ReauthLogin3GInit();
        } else {
            return null;
        }
    }

    public BaseExecutable prepare3GGsmFlowCommand(String commandName) {

        if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.LOGIN3G_GSM) {
            return new Login3GGsm();
        } else if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.ANONYMOUS_3G_GSM) {
            return new Anonymous3GGsm();
        } else if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.SIMSTART3G_GSM) {
            return new SimStart3GGsm();
        } else if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.SIMSTART3G_GSM_ENCR) {
            return new Login3GGsmEncrypt();
        } else if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.CHALLENGE3G_GSM) {
            return new Challenge3GGsm();
        } else if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.REAUTH_LOGIN_3G_GSM) {
            return new ReAuthLogin3GGSM();
        } else if (ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.REAUTHSIMSTART3G_GSM) {
            return new ReAuthSimStart3GGSM();
        } else if(ThreeGGsmCommand.get(commandName) == ThreeGGsmCommand.REAUTH3G_GSM) {
            return new ReAuth3GGSM();
        } else {
            return null;
        }
    }
}
