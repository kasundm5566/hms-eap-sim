/*
 * (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf;


import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.util.Latency;
import hms.eapsim.perf.util.TPSLog;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.util.AsyncRadiusClient;
import org.tinyradius.util.RadiusException;

import java.io.IOException;
import java.net.SocketException;

/**
 * This one support only asynchronous message sending
 */
public class MessageHandler implements MessageSender {

    private static final Logger logger = Logger.getLogger(MessageHandler.class);

    private AsyncRadiusClient client;

    private Configuration configuration;

    public void init() throws SocketException {
        logger.info("Radius clients initializing ... ");

        client = new AsyncRadiusClient(configuration.getHostAddress(),
                configuration.getSharedSecret(),
                configuration.getSenderPoolSize(),
                configuration.getReceiverPoolSize());
        client.setSocketTimeout(configuration.getSoTimeout());
        client.setRequestTimeoutInMilliSeconds(configuration.getRequestTimeoutInMillSec());
        client.setRetryCount(configuration.getNumberOfRequestRetry());
        client.init();

        logger.info("Asynchronous Radius clients initialized");
    }


    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public RadiusPacket send(RadiusPacket accessRequest, Context context, String requestType) throws IOException, RadiusException {
        TPSLog.incrementRequestCount();
        Latency latency = new Latency(context.getImsi(), requestType, System.currentTimeMillis());
        RadiusPacket response = client.communicate(accessRequest, configuration.getServerPort(), context.getImsi());
        latency.markFinish(System.currentTimeMillis(), configuration.getLatencyThresholdInMillSec());
        TPSLog.incrementResponseCount();
        return response;
    }
}
