/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials
 */

package hms.eapsim.perf.flows.command;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kasun on 1/19/17.
 */
public enum ThreeGCommand {

    LOGIN_3G("login3g"),
    ANONYMOUS_3G("anonymous3g"),
    LOGIN_3G_INIT("login3g-init"),
    INIT_3G_ENCR("init3g-encr"),
    CHALLENGE_3G("challenge3g"),
    REAUTH_LOGIN_3G("reauthLogin3g"),
    REAUTH_LOGIN_3G_INIT("reauthLogin3g-init"),
    REAUTH_3G("reauth3g");

    public String commandName;

    ThreeGCommand(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }

    private static final Map<String,ThreeGCommand> ENUM_MAP;

    static {
        Map<String,ThreeGCommand> map = new ConcurrentHashMap<>();
        for (ThreeGCommand instance : ThreeGCommand.values()) {
            map.put(instance.getCommandName(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static ThreeGCommand get (String commandName) {
        return ENUM_MAP.get(commandName);
    }


}
