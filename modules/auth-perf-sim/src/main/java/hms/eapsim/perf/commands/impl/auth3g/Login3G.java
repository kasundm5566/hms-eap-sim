package hms.eapsim.perf.commands.impl.auth3g;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

/**
 * This represents the first eap response from mobile divice for full authentication flow
 * EAP-Response/Identity . Device includes  user's NAI.
 */
public class Login3G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Login3G.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());

        AccessRequest accessRequest = requestBuilder.build3GAccessRequest(context.getImsi(),
                context.getConfiguration().getSharedSecret(), context.getRealmPart(), context);
        RadiusPacket responsePacket = messageSender.send(accessRequest, context, getName());

        logger.info("Response Received [" + responsePacket + "]");
        context.setPreviousReceivedPacket(responsePacket);
        context.setLastAuthId(extractReAuthId(getEapMessage(context.getPreviousReceivedPacket()), context));

        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGCommand.LOGIN_3G.name();
    }
}
