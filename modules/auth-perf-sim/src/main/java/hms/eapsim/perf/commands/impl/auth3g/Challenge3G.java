package hms.eapsim.perf.commands.impl.auth3g;

import hms.common.radius.decoder.eap.elements.EapCode;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute;
import hms.common.radius.util.HexCodec;
import hms.eapsim.key.KeyGeneratorComponent;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import hms.eapsim.perf.util.CommonAttribuiteUtil;
import hms.eapsim.util.EapsimAttributeBuilderComponent;
import io.netty.buffer.ByteBuf;
import org.apache.log4j.Logger;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.RadiusPacket;

import java.util.Arrays;

/**
 * Created by sampath on 1/19/17.
 */
public class Challenge3G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Challenge3G.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        EapMessage eapMessage = context.extractEapFrom();

        EapsimAttributeBuilderComponent.EapsimAttributeBuilder eapsimAttributeBuilder = requestBuilder.getAttributeBuilder().eapsimAttributeBuilder();
        KeyGeneratorComponent.KeyGenerator keyGenerator = requestBuilder.getAttributeBuilder().keyGenerator();

        EapSimAkaAttribute atRes = eapsimAttributeBuilder.buildAtRes(HexCodec.hex2Byte(context.getAkaRes()));
        EapSimAkaAttribute atBlankMac = eapsimAttributeBuilder.buildAtMac();
        EapAka tmpAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, Arrays.asList(atRes, atBlankMac));
        EapRequestResponseMessage tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), tmpAkaMsg);
        ByteBuf encodeEapMsg = tmpEapMsg.encode();

        byte[] mac = Arrays.copyOf(keyGenerator.hmacSha1Encode(HexCodec.hex2Byte(context.getAkaKaut()), encodeEapMsg), 16);

        EapSimAkaAttribute atMac = eapsimAttributeBuilder.buildAtMac(mac);
        EapAka fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, Arrays.asList(atRes, atMac));
        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), fnlAkaMsg);

        RadiusPacket request = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        request.addAttribute(fnlEapMsg);

        RadiusAttribute state = context.getPreviousReceivedPacket().getAttribute("State");
        if (state != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Adding received State Attribute to Response[" + state + "]");
            }
            request.addAttribute(state);
        }
        request.addAttribute(ATTRIBUTE_NAME_USERNAME, context.getImsi() + context.getRealmPart());
        request.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        request.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        request.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(request);
        RadiusPacket response = messageSender.send(request, context, getName());
        logger.info("Response Received [" + response + "]");

        context.setPreviousReceivedPacket(response);
        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }

    }

    @Override
    public String getName() {
        return ThreeGCommand.CHALLENGE_3G.name();
    }
}
