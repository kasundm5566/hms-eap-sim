package hms.eapsim.perf.bean;

import hms.eapsim.perf.MessageSendMediator;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.flows.impl.AttributeBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Provide all the bean required for transport layer
 */
@Configuration
@ImportResource("classpath:spring-context.xml")
public class Transport {

//    @Bean(initMethod = "init")
//    public MessageSender getMessageSender() {
//        MessageHandler messageSender = new MessageHandler();
//        try {
//            messageSender.init();
//        } catch (SocketException e) {
//            throw new RuntimeException("Error while initialising message sender", e);
//        }
//        return messageSender;
//    }
//
//    @Bean
//    public AttributeBuilder getAttributeBuilder() {
//        AttributeBuilder attributeBuilder = new AttributeBuilder();
//        return attributeBuilder;
//    }
//
//    @Bean
//    public RequestBuilder getRequestBuilder() {
//        RequestBuilder requestBuilder = new RequestBuilder();
//        return requestBuilder;
//    }
//
//    @Bean(initMethod = "init")
//    public MessageSendMediator getMessageSendMediator() {
//        MessageSendMediator mediator = new MessageSendMediator();
//        return mediator;
//    }
//
//    @Bean
//    hms.eapsim.perf.bean.Configuration getConfiguration() {
//        hms.eapsim.perf.bean.Configuration configuration = new hms.eapsim.perf.bean.Configuration();
//        return configuration;
//    }
}
