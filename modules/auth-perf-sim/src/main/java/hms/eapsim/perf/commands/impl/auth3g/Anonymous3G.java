package hms.eapsim.perf.commands.impl.auth3g;

import hms.eapsim.perf.commands.impl.BaseAnonymous;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import org.apache.log4j.Logger;

public class Anonymous3G extends BaseAnonymous {

    private static final Logger logger = Logger.getLogger(Anonymous3G.class);

    @Override
    public String getName() {
        return ThreeGCommand.ANONYMOUS_3G.name();
    }

}
