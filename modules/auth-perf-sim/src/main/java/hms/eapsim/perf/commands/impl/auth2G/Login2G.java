package hms.eapsim.perf.commands.impl.auth2G;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.TwoGCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public class Login2G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Login2G.class);

    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());

        AccessRequest accessRequest = requestBuilder.buildSimIdentityRequest(context.getImsi(),
                context.getConfiguration().getSharedSecret(), context.getRealmPart(), context);
        RadiusPacket response = messageSender.send(accessRequest, context, getName());
        context.setPreviousReceivedPacket(response);
        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return TwoGCommand.LOGIN_2G.name();
    }
}
