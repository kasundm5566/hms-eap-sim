package hms.eapsim.perf.domain;

import hms.common.radius.decoder.eap.EapMessageDecoder;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.common.radius.util.HexCodec;
import hms.eapsim.key.KeyGeneratorComponent;
import hms.eapsim.key.Quintuplet;
import hms.eapsim.key.Triplet;
import hms.eapsim.key.VectorConversionUtil;
import hms.eapsim.key.fips186.Fips186Lib;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.Utility;
import hms.eapsim.perf.bean.Configuration;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.packet.RadiusPacket;
import scala.collection.JavaConversions;

import java.util.ArrayList;
import java.util.List;

public class Context {

    private int senderIndex;
    private String imsi;
    private String calledStationId;
    private String callingStationId;
    private String nasIndentier;
    private RadiusPacket previousReceivedPacket;
    private String lastAuthId;
    private int counter;
    private Configuration configuration;
    private RequestBuilder requestBuilder;

    private String akaRes = "3fe524d57101d202";
    private String akaKaut;
    private String akaEncr;
    private String simRes = "1AC2E6202AC2E6203AC2E620";
    private String simKaut;
    private String simEncr;
    private String simRes3G = "4ee4f6d74ee4f6d7";
    private String simKaut3G;
    private String simEncr3G;

    private String iv = "9e18b0c29a652263c06efb54dd00a895";
    private String nonceMt = "749ad02c4a257c20192738be0b40dc86";
    private String realmPart = "@wlan.mnc072.mcc320.3gppnetwork.org";
    private String selectedVersion = "0001";

    private static final String RAND = "f12ac2e620ccd40bb920f6b3b474306b";
    private static final String XRES = "3fe524d57101d202";
    private static final String CK = "8bb41f73b559138cfab2b89bda709ee8";
    private static final String IK = "bbdd21caaea33e90ccb827399649f189";
    private static final String AUTN = "d749908734350000cd298d7429daed26";

    private static final String KC1 = "1ac2e6202ac2e620";
    private static final String KC2 = "2ac2e6202ac2e620";
    private static final String KC3 = "3ac2e6202ac2e620";

    private boolean counterTooSmall;
    private boolean is3GGSMFlow;

    public Context() {
    }

    public void clean() {
        previousReceivedPacket = null;
        lastAuthId = null;
        counter = 0;
        counterTooSmall = false;
        is3GGSMFlow = false;
    }

    public void init() {
        if (Utility.isEmpty(imsi)) {
            throw new RuntimeException("Could not generate master key");
        }
        load2gSimKeys();
        load3gSimKeys();
        load3gAkaKeys();
    }

    private void load2gSimKeys() {
        String identity = HexCodec.byte2Hex((imsi + realmPart).getBytes());
        KeyGeneratorComponent.KeyGenerator keyGenerator = requestBuilder.getAttributeBuilder().keyGenerator();

        List<String> kcList = new ArrayList<>();
        kcList.add(KC1);
        kcList.add(KC2);
        kcList.add(KC3);

        List<String> versionList = new ArrayList<>();
        versionList.add(selectedVersion);
        byte[] masterKey = keyGenerator.generateMasterKeyEapSimForFullAuth(identity, JavaConversions.asScalaBuffer(kcList).toList(),
                nonceMt, JavaConversions.asScalaBuffer(versionList).toList(), selectedVersion);

        Fips186Lib fips186Lib = Fips186Lib.INSTANCE;
        byte[] out = new byte[160];
        fips186Lib.fips186_2prf(masterKey, out);

        ByteBuf randomKey = Unpooled.copiedBuffer(out);

        simEncr = HexCodec.byte2Hex(randomKey.readBytes(16).array());
        simKaut = HexCodec.byte2Hex(randomKey.readBytes(16).array());
    }

    private void load3gSimKeys() {
        String identity = HexCodec.byte2Hex((imsi + realmPart).getBytes());
        KeyGeneratorComponent.KeyGenerator keyGenerator = requestBuilder.getAttributeBuilder().keyGenerator();

        Quintuplet qui = new Quintuplet(RAND, XRES, CK, IK, AUTN);
        Triplet triplet = VectorConversionUtil.umts2Gsm(qui);
        List<String> kcList = new ArrayList<>();
        kcList.add(triplet.kc());
        kcList.add(triplet.kc());

        List<String> versionList = new ArrayList<>();
        versionList.add(selectedVersion);
        byte[] masterKey = keyGenerator.generateMasterKeyEapSimForFullAuth(identity, JavaConversions.asScalaBuffer(kcList).toList(),
                nonceMt, JavaConversions.asScalaBuffer(versionList).toList(), selectedVersion);

        Fips186Lib fips186Lib = Fips186Lib.INSTANCE;
        byte[] out = new byte[160];
        fips186Lib.fips186_2prf(masterKey, out);

        ByteBuf randomKey = Unpooled.copiedBuffer(out);

        simEncr3G = HexCodec.byte2Hex(randomKey.readBytes(16).array());
        simKaut3G = HexCodec.byte2Hex(randomKey.readBytes(16).array());
    }

    private void load3gAkaKeys() {
        String identity = HexCodec.byte2Hex((imsi + realmPart).getBytes());
        KeyGeneratorComponent.KeyGenerator keyGenerator = requestBuilder.getAttributeBuilder().keyGenerator();
        byte[] masterKey = keyGenerator.generateMasterKeyEapAka(identity, IK, CK);

        Fips186Lib fips186Lib = Fips186Lib.INSTANCE;
        byte[] out = new byte[160];
        fips186Lib.fips186_2prf(masterKey, out);

        ByteBuf randomKey = Unpooled.copiedBuffer(out);

        akaEncr = HexCodec.byte2Hex(randomKey.readBytes(16).array());
        akaKaut = HexCodec.byte2Hex(randomKey.readBytes(16).array());
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public RadiusPacket getPreviousReceivedPacket() {
        return previousReceivedPacket;
    }

    public void setPreviousReceivedPacket(RadiusPacket previousReceivedPacket) {
        this.previousReceivedPacket = previousReceivedPacket;
    }

    public String getLastAuthId() {
        return lastAuthId;
    }

    public void setLastAuthId(String lastAuthId) {
        this.lastAuthId = lastAuthId;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public EapMessage extractEapFrom(RadiusPacket response) {
        RadiusAttribute attribute = response.getAttribute(EapMessage.ATTRIBUTE_TYPE);
        return EapMessageDecoder.decode(Unpooled.copiedBuffer(attribute.getAttributeData()));
    }

    public EapMessage extractEapFrom() {
        if (previousReceivedPacket == null) {
            throw new RuntimeException("No previous packet is available to extract eap message");
        }
        RadiusAttribute attribute = previousReceivedPacket.getAttribute(EapMessage.ATTRIBUTE_TYPE);
        return EapMessageDecoder.decode(Unpooled.copiedBuffer(attribute.getAttributeData()));
    }

    public String getAkaRes() {
        return akaRes;
    }

    public String getAkaKaut() {
        return akaKaut;
    }

    public String getAkaEncr() {
        return akaEncr;
    }

    public String getSimRes() {
        return simRes;
    }

    public String getSimKaut() {
        return simKaut;
    }

    public String getSimEncr() {
        return simEncr;
    }

    public String getSimRes3G() {
        return simRes3G;
    }

    public String getSimKaut3G() {
        return simKaut3G;
    }

    public String getSimEncr3G() {
        return simEncr3G;
    }

    public String getIv() {
        return iv;
    }

    public String getNonceMt() {
        return nonceMt;
    }

    public String getRealmPart() {
        return realmPart;
    }

    public void setRequestBuilder(RequestBuilder requestBuilder) {
        this.requestBuilder = requestBuilder;
    }

    public boolean isCounterTooSmall() {
        return counterTooSmall;
    }

    public void setCounterTooSmall(boolean counterTooSmall) {
        this.counterTooSmall = counterTooSmall;
    }

    public boolean isIs3GGSMFlow() {
        return is3GGSMFlow;
    }

    public void setIs3GGSMFlow(boolean is3GGSMFlow) {
        this.is3GGSMFlow = is3GGSMFlow;
    }

    public String getCalledStationId() {
        return calledStationId;
    }

    public void setCalledStationId(String calledStationId) {
        this.calledStationId = calledStationId;
    }

    public String getCallingStationId() {
        return callingStationId;
    }

    public void setCallingStationId(String callingStationId) {
        this.callingStationId = callingStationId;
    }

    public String getNasIndentier() {
        return nasIndentier;
    }

    public void setNasIndentier(String nasIndentier) {
        this.nasIndentier = nasIndentier;
    }
}
