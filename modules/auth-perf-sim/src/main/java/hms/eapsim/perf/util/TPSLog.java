/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by kasun on 1/31/17.
 */
public class TPSLog {

    private static final Logger log = LoggerFactory.getLogger("tpslog");

    private static AtomicInteger requestCount = new AtomicInteger(0);
    private static AtomicInteger responseCount = new AtomicInteger(0);
    private static AtomicInteger pendingQueueSize = new AtomicInteger(0);

    public static void incrementRequestCount() {
        requestCount.incrementAndGet();
    }

    public static void incrementResponseCount() {
        responseCount.incrementAndGet();
    }

    public static void setPendingQueSize(int size) {
        pendingQueueSize.getAndSet(size);
    }

    public static void executeLogging() {
        int tempRequestCount = requestCount.getAndSet(0);
        int tempResponseCount = responseCount.getAndSet(0);
        int tempPendingQueueSize = pendingQueueSize.getAndSet(0);
        log.info("TPS REQ:{}, RES:{}, PQ:{}", tempRequestCount, tempResponseCount, tempPendingQueueSize);
    }
}
