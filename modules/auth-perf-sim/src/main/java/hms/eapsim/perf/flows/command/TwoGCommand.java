/*
 * Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials
 */

package hms.eapsim.perf.flows.command;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kasun on 1/19/17.
 */
public enum TwoGCommand {

    LOGIN_2G("login2g"),
    ANONYMOUS_2G("anonymous2g"),
    SIMSTART("simstart"),
    SIMSTART_2G_ENCR("simstart2g-encr"),
    CHALLENGE2G("challenge2g"),
    REAUTHLOGIN2G("reauthLogin2g"),
    REAUTHSIMSTART("reauthsimstart"),
    REAUTH2G("reauth2g");

    public String commandName;

    TwoGCommand(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }

    private static final Map<String,TwoGCommand> ENUM_MAP;

    static {
        Map<String,TwoGCommand> map = new ConcurrentHashMap<>();
        for (TwoGCommand instance : TwoGCommand.values()) {
            map.put(instance.getCommandName(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static TwoGCommand get (String commandName) {
        return ENUM_MAP.get(commandName);
    }

}
