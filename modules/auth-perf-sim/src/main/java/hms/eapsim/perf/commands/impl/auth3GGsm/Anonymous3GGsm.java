package hms.eapsim.perf.commands.impl.auth3GGsm;

import hms.eapsim.perf.commands.impl.BaseAnonymous;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;
import org.apache.log4j.Logger;

public class Anonymous3GGsm extends BaseAnonymous {

    private static final Logger logger = Logger.getLogger(Anonymous3GGsm.class);

    @Override
    public String getName() {
        return ThreeGGsmCommand.ANONYMOUS_3G_GSM.name();
    }

}
