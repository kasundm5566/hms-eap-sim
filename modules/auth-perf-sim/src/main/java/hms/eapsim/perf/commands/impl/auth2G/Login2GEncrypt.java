package hms.eapsim.perf.commands.impl.auth2G;

import hms.eapsim.perf.commands.impl.sim.BaseLoginEncrypt;
import hms.eapsim.perf.flows.command.TwoGCommand;

public class Login2GEncrypt extends BaseLoginEncrypt {

    @Override
    public String getName() {
        return TwoGCommand.SIMSTART_2G_ENCR.name();
    }
}
