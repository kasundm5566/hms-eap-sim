/*
 * (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf.commands.impl.reauth3GGsm;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;

/**
 * This represents EAP-Response/SIM/start including next re-auth id in AT_IDENTITY of re-authentication
 */
public class ReAuthSimStart3GGSM extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(ReAuthSimStart3GGSM.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        String lastAuthId = context.getLastAuthId();
        if (lastAuthId != null && !lastAuthId.isEmpty()) {
            RadiusPacket request = requestBuilder.buildReAuthSimStartRequest(context.getImsi(),
                    context.getConfiguration().getSharedSecret(), lastAuthId, context.getPreviousReceivedPacket(), context.getRealmPart(), context);
            RadiusPacket response = messageSender.send(request, context, getName());
            context.setPreviousReceivedPacket(response);

            parent.setContext(parent.getImsi(), context);

            if (isLastCommand) {
                parent.unassigned();
            }
        } else {
            logger.error("Re-Authentication id is not available. Looks like you have not authenticated");
            throw new Exception("Re-Authentication id is not available. Looks like you have not authenticated");
        }
    }

    @Override
    public String getName() {
        return ThreeGGsmCommand.REAUTHSIMSTART3G_GSM.name();
    }

}
