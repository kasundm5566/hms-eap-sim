package hms.eapsim.perf.commands.impl.reauth2G;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.TwoGCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;

public class ReAuthSimStart extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(ReAuthSimStart.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        String lastAuthId = context.getLastAuthId();
        if (lastAuthId != null && !lastAuthId.isEmpty()) {
            RadiusPacket request = requestBuilder.buildReAuthSimStartRequest(context.getImsi(),
                    context.getConfiguration().getSharedSecret(), lastAuthId, context.getPreviousReceivedPacket(),
                    context.getRealmPart(), context);
            RadiusPacket response = messageSender.send(request, context, getName());
            context.setPreviousReceivedPacket(response);

            parent.setContext(parent.getImsi(), context);

            if (isLastCommand) {
                parent.unassigned();
            }
        } else {
            logger.error("Re-Authentication id is not available. Looks like you have not authenticated");
            throw new Exception("Re-Authentication id is not available. Looks like you have not authenticated");
        }
    }

    @Override
    public String getName() {
        return TwoGCommand.REAUTHSIMSTART.name();
    }

}
