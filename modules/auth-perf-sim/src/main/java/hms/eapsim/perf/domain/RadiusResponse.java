package hms.eapsim.perf.domain;

import org.tinyradius.packet.RadiusPacket;

public class RadiusResponse {

    private StatusCode statusCode;
    private RadiusPacket response;

    public RadiusResponse(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public RadiusResponse(StatusCode statusCode, RadiusPacket response) {
        this.statusCode = statusCode;
        this.response = response;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public RadiusPacket getResponse() {
        return response;
    }
}
