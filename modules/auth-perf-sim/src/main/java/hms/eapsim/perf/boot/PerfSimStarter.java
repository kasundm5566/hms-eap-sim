package hms.eapsim.perf.boot;

import hms.eapsim.perf.MessageSendMediator;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Performance simulator starter
 */

@SpringBootApplication(scanBasePackages = {"hms.eapsim.perf"})
@EnableScheduling
public class PerfSimStarter extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PerfSimStarter.class);
    }

    private ApplicationContext ctx;

    private static final Logger logger = Logger.getLogger(PerfSimStarter.class);

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx = SpringApplication.run(PerfSimStarter.class,
                args);
        PerfSimStarter mainObj = ctx.getBean(PerfSimStarter.class);
        mainObj.init(ctx);
        mainObj.printBanner();
    }

    private void printBanner() {
        logger.info("");
        logger.info("#####################################");
        logger.info("   Eap-Sim Performance Simulator");
        logger.info("#####################################");
        logger.info("");
        logger.info("");
        logger.info("---------------------------------------");
        logger.info("");
        logger.info("Simulator Mode : " + ctx.getBean(MessageSendMediator.class).getConfiguration().getRunningMode());
        logger.info("");
        logger.info("---------------------------------------");
    }

    public void init(ApplicationContext ctx) throws Exception {
        this.ctx = ctx;
        logger.info("Application context initialized");
    }
}
