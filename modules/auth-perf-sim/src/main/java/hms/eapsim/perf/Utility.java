/*
 * (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 *  These materials are unpublished, proprietary, confidential source code of
 *  hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  of hSenid Software International (Pvt) Limited.
 *
 *  hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  property rights in these materials.
 */

package hms.eapsim.perf;

/**
 * Utility function for performance
 */
public class Utility {

    private Utility() {
        throw new RuntimeException("Constructing not supported");
    }

    public static boolean isNotEmpty(String val) {
        return !isEmpty(val);
    }

    public static boolean isEmpty(String val) {
        return ((val == null) || (val.length() == 0));
    }
}
