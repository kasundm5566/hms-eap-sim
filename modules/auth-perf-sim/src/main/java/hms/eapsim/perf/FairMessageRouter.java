package hms.eapsim.perf;

import hms.eapsim.perf.domain.Context;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.util.RadiusException;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class FairMessageRouter implements MessageSender {

    private MessageSender[] messageSenders;

    private AtomicInteger counter = new AtomicInteger(0);

    public FairMessageRouter(MessageSender[] messageSenders) {
        if (messageSenders == null) {
            throw new RuntimeException("No message senders have assigned");
        }
        this.messageSenders = messageSenders;
    }

    @Override
    public RadiusPacket send(RadiusPacket accessRequest, Context context, String requestType) throws IOException, RadiusException {
        return messageSenders[Math.abs(counter.incrementAndGet()) % messageSenders.length]
                .send(accessRequest, context, requestType);
    }
}
