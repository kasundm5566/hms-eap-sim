package hms.eapsim.perf.commands.impl;

import hms.eapsim.perf.domain.Context;
import org.apache.log4j.Logger;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

public abstract class BaseAnonymous extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(BaseAnonymous.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());

        AccessRequest accessRequest = requestBuilder.buildAnonymousRequest(context.getImsi(),
                context.getConfiguration().getSharedSecret(), context.getRealmPart(), context);
        RadiusPacket responsePacket = messageSender.send(accessRequest, context, getName());

        logger.info("Response Received [" + responsePacket + "]");
        context.setPreviousReceivedPacket(responsePacket);

        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

}
