package hms.eapsim.perf;

import hms.eapsim.perf.domain.Context;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.util.RadiusException;

import java.io.IOException;

/**
 * Sending message to a radius server
 */
public interface MessageSender {

    RadiusPacket send(RadiusPacket accessRequest, Context context, String requestType) throws IOException, RadiusException;
}
