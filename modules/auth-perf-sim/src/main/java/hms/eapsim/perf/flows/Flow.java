package hms.eapsim.perf.flows;

import hms.eapsim.perf.MessageSender;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.commands.Executable;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.util.Statistic;
import io.netty.util.HashedWheelTimer;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Represent a single testable message flow in a traffic plan, {@link hms.eapsim.perf.traffic.TrafficPlan}
 * For a example, we can configure to test full authentication and multiple re-authentication with a configurable
 * delay happen in each steps in the mobile device.
 * It is recommended to not to create this type of object on demand while traffic plan is executing. Create them before
 * start the traffic plan will speedup performance of the simulator.
 */
public interface Flow {

    /**
     * Clear current context, {@link hms.eapsim.perf.domain.Context} of the flow.
     */
    void clear();

    //todo : rename this as execute
    /**
     * Execute message flow
     * @throws Exception
     */
    void auth() throws Exception;

    void schedule(HashedWheelTimer timer, ExecutorService scheduleWorker, long defaultSleepTimeInMilliSec) throws Exception;

    void setCommands(List<? extends BaseExecutable> commands);

    /**
     * Configure configuration
     * @param configuration
     */
    void setConfiguration(Configuration configuration);

    void setContext(String imsi, Context context);

    Context getContext(String imsi);

    void setStatistic(Statistic statistic);
}
