package hms.eapsim.perf;

import hms.common.radius.decoder.eap.EapMessageDecoder;
import hms.common.radius.decoder.eap.elements.EapCode;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage;
import hms.common.radius.decoder.eap.elements.EapType;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka;
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.AbstractEapSimAka;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute;
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes;
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSim;
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSimSubtype;
import hms.common.radius.util.EapMessageUtil;
import hms.common.radius.util.HexCodec;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.impl.AttributeBuilder;
import hms.eapsim.perf.util.CommonAttribuiteUtil;
import hms.eapsim.util.EapsimAttributeBuilderComponent;
import hms.eapsim.util.privacy.PrivacyUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.dictionary.AttributeType;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;
import scala.collection.JavaConversions;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static hms.eapsim.perf.commands.Executable.*;

public class RequestBuilder {

    private static final Logger logger = Logger.getLogger(RequestBuilder.class);

    private static final String ANONYMOUS_USERNAME = "anonymous";

    @Autowired
    private AttributeBuilder attributeBuilder;

    /**
    Code: Access-Request (1)
	attr-val pair
		EAP=>[02000038013031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267]
	             Ascii = [81320727710000010@wlan.mnc072.mcc320.3gppnetwork.org]
			Code: Response (2)
			Id: 0
			Length: 56
			Type: Identity (1)
			Identity: 1320727710000010@wlan.mnc072.mcc320.3gppnetwork.org

			[02|0|56|1|0123727710000010@wlan.mnc072.mcc320.3gppnetwork.org]
     */

    // 2G, 3G & 3G-GSM Auth flow
    public AccessRequest buildAnonymousRequest(String imsi, String secret, String realmPart, Context context) throws IOException {
        String anonymousImsi = imsi.charAt(0) + ANONYMOUS_USERNAME;
        String anonymousIdentity = anonymousImsi + realmPart;
        String codePart = "02";
        String idPart = "00";
        String typePart = "01";
        AccessRequest ar = new AccessRequest();

        AttributeType eapAttrType = ar.getDictionary().getAttributeTypeByName(ATTRIBUTE_NAME_EAP_MESSAGE);
        RadiusAttribute attribute =
                RadiusAttribute.createRadiusAttribute(ar.getDictionary(), eapAttrType.getVendorId(), eapAttrType.getTypeCode());
        byte[] anonymousIdentityBytes = anonymousIdentity.getBytes();
        int length = anonymousIdentityBytes.length + 5;
        byte[] lengthInBytes = new byte[2];
        lengthInBytes[0] = (byte) ((length >> 8) & 0xFF);
        lengthInBytes[1] = (byte) (length & 0xFF);
        String lengthInHex = HexUtils.toHexString(lengthInBytes);
        attribute.setAttributeData(HexCodec.hex2Byte(codePart + idPart + lengthInHex + typePart + HexUtils.toHexString(anonymousIdentityBytes)));
        ar.addAttribute(attribute);

        ar.addAttribute(ATTRIBUTE_NAME_USERNAME, anonymousIdentity);
        ar.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        ar.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        ar.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        EapMessageUtil.addMessageAuthenticator(ar, secret);
        CommonAttribuiteUtil.addCommonAttribute(ar);
        return ar;
    }

    public AccessRequest build3GAccessRequest(String imsi, String secret, String realmPart, Context context) throws IOException {
        AccessRequest ar = new AccessRequest();
        AttributeType eapAttrType = ar.getDictionary().getAttributeTypeByName(ATTRIBUTE_NAME_EAP_MESSAGE);
        RadiusAttribute attribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary(), eapAttrType.getVendorId(), eapAttrType.getTypeCode());
        attribute.setAttributeData(HexCodec.hex2Byte("0200003801" + HexUtils.toHexString(imsi.getBytes()) + "40776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267"));
        ar.addAttribute(attribute);

        ar.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        ar.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        ar.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        ar.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        EapMessageUtil.addMessageAuthenticator(ar, secret);
        CommonAttribuiteUtil.addCommonAttribute(ar);
        return ar;
    }

    public RadiusPacket build3GAccessRequestWithAtIdentity(String imsi, RadiusPacket previousReceivedPacket, EapMessage eapMessage, String SharedSecret, String realmPart, Context context) throws IOException {
        String identity = imsi + realmPart;
        EapSimAkaAttribute atIdentity = attributeBuilder.eapsimAttributeBuilder().buildAtIdentity(identity.getBytes());
        List<EapSimAkaAttribute> akaAttrList = new ArrayList<>();
        akaAttrList.add(atIdentity);
        EapAka fnlSimMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, akaAttrList);

        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), fnlSimMsg);

        RadiusPacket rp  = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(fnlEapMsg);
        rp.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        rp.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        rp.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        rp.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(rp);

        RadiusAttribute state = previousReceivedPacket.getAttribute("State");
        if (state != null) {
            rp.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(rp, SharedSecret);

        return rp;
    }

    public RadiusPacket build3GEncryptedRequestWithAtIdentity(String imsi, RadiusPacket previousReceivedPacket,
                                                              EapMessage eapMessage, String SharedSecret,
                                                              String realmPart, Context context)
            throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException, InvalidKeySpecException {

        String certificateFilePath = context.getConfiguration().getCertificateFilePath();
        String encryptedEncodedImsi = PrivacyUtil.encrypt(certificateFilePath, imsi);

        String modifiedImsi = '\0' + encryptedEncodedImsi;
        String certificateKeyIdentifier = context.getConfiguration().getCertificateKeyIdentifier();
        String keyIdentifier = buildEncryptionKeyIdentifierPart(certificateKeyIdentifier);
        String modifiedIdentity = modifiedImsi + realmPart + keyIdentifier;
        EapSimAkaAttribute atIdentity = attributeBuilder.eapsimAttributeBuilder().buildAtIdentity(modifiedIdentity.getBytes());

        List<EapSimAkaAttribute> akaAttrList = new ArrayList<>();
        akaAttrList.add(atIdentity);
        EapAka fnlSimMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, akaAttrList);

        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessage.getIdentifier(), fnlSimMsg);

        RadiusPacket rp  = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(fnlEapMsg);
        rp.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        rp.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        rp.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        rp.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(rp);

        RadiusAttribute state = previousReceivedPacket.getAttribute("State");
        if (state != null) {
            rp.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(rp, SharedSecret);

        return rp;
    }

    // 2G & 3G-GSM Auth flow
    public AccessRequest buildSimIdentityRequest(String imsi, String sharedSecret, String realmPart, Context context) throws IOException {
        String dataStartPart = "0200003801";
        AccessRequest accessRequest = new AccessRequest();
        AttributeType eapAttrType = accessRequest.getDictionary().getAttributeTypeByName(ATTRIBUTE_NAME_EAP_MESSAGE);
        RadiusAttribute attribute =
                RadiusAttribute.createRadiusAttribute(accessRequest.getDictionary(), eapAttrType.getVendorId(), eapAttrType.getTypeCode());
        attribute.setAttributeData(HexCodec.hex2Byte(dataStartPart + HexUtils.toHexString(imsi.getBytes()) + HexUtils.toHexString(realmPart.getBytes())));

        accessRequest.addAttribute(attribute);
        accessRequest.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        accessRequest.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        accessRequest.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        accessRequest.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(accessRequest);
        EapMessageUtil.addMessageAuthenticator(accessRequest, sharedSecret);
        return accessRequest;
    }

    public AccessRequest buildSimStartRequest(String imsi, int eapMessageIdentifier, RadiusAttribute state, String sharedSecret,
                                              String nonceMt, String realmPart, Context context) throws IOException {
        String dataStartPart = "02";
        String dataEndPart = "0058120a00000e0e0033" + HexUtils.toHexString(imsi.getBytes()) +
                HexUtils.toHexString(realmPart.getBytes()) + "001001000107050000" + nonceMt;

        AccessRequest accessRequest = new AccessRequest();
        AttributeType eapAttrType = accessRequest.getDictionary().getAttributeTypeByName(ATTRIBUTE_NAME_EAP_MESSAGE);
        RadiusAttribute attribute = RadiusAttribute.createRadiusAttribute(accessRequest.getDictionary(), eapAttrType.getVendorId(), eapAttrType.getTypeCode());
        String respPacketId = HexCodec.byte2Hex(new byte[]{(byte) eapMessageIdentifier});
        String eapSimStart = dataStartPart + respPacketId + dataEndPart;
        attribute.setAttributeData(HexCodec.hex2Byte(eapSimStart));
        accessRequest.addAttribute(attribute);
        accessRequest.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        accessRequest.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        accessRequest.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        accessRequest.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(accessRequest);


        if (state != null) {
            logger.info("Adding received state attribute to the request [" + state + "]");
            accessRequest.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(accessRequest, sharedSecret);
        return accessRequest;
    }

    public AccessRequest buildEncryptedSimStartRequest(String imsi, int eapMessageIdentifier, RadiusAttribute state, String sharedSecret,
                                              String nonceMt, String realmPart, Context context)
            throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException, InvalidKeySpecException {

        String certificateFilePath = context.getConfiguration().getCertificateFilePath();
        String encryptedEncodedImsi = PrivacyUtil.encrypt(certificateFilePath, imsi);

        String modifiedImsi = '\0' + encryptedEncodedImsi;
        String certificateKeyIdentifier = context.getConfiguration().getCertificateKeyIdentifier();
        String keyIdentifier = buildEncryptionKeyIdentifierPart(certificateKeyIdentifier);
        String modifiedIdentity = modifiedImsi + realmPart + keyIdentifier;
        EapSimAkaAttribute atIdentity = attributeBuilder.eapsimAttributeBuilder().buildAtIdentity(modifiedIdentity.getBytes());
        EapSimAkaAttribute atSelectedVersion = attributeBuilder.eapsimAttributeBuilder().buildAtSelectedVersion(HexCodec.hex2Byte("0001"));
        EapSimAkaAttribute atNonceServer = attributeBuilder.eapsimAttributeBuilder().buildAtNonceMt(HexCodec.hex2Byte(nonceMt));

        List<EapSimAkaAttribute> attrList = new ArrayList<>();
        attrList.add(atIdentity);
        attrList.add(atSelectedVersion);
        attrList.add(atNonceServer);
        EapSim fnlSimMsg = new EapSim(EapSimSubtype.START, attrList);
        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapMessageIdentifier, fnlSimMsg);

        AccessRequest accessRequest = new AccessRequest();
        accessRequest.addAttribute(fnlEapMsg);

        accessRequest.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        accessRequest.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        accessRequest.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        accessRequest.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(accessRequest);

        if (state != null) {
            accessRequest.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(accessRequest, sharedSecret);
        return accessRequest;
    }

    public RadiusPacket buildSimChallengeRequest(String imsi, Byte requestId, RadiusPacket previousReceivedPacket,
                                                 String sharedSecret, String key, Context context, boolean is3GSimFlow, String realmPart) throws IOException {
        String lastAuthId = extractReAuthId(previousReceivedPacket, key, context);
        context.setLastAuthId(lastAuthId);
        EapRequestResponseMessage fnlEapMsg = extractEapMessage(requestId, is3GSimFlow, context);
        RadiusPacket rp = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(fnlEapMsg);

        rp.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        rp.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        rp.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        rp.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(rp);
        RadiusAttribute state = previousReceivedPacket.getAttribute(ATTRIBUTE_NAME_STATE);
        if (state != null) {
            logger.info("Adding received State Attribute to Response[" + state + "]");
            rp.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(rp, sharedSecret);
        return rp;
    }

    private String extractReAuthId(RadiusPacket previousReceivedPacket, String key, Context context) {
        EapMessage eapMessage =
                EapMessageDecoder.decode(Unpooled.copiedBuffer(previousReceivedPacket.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()));
        if(eapMessage instanceof EapRequestResponseMessage) {
            EapType sim = ((EapRequestResponseMessage) eapMessage).getType();
            if(sim instanceof AbstractEapSimAka) {
                try {
                    EapSimAkaAttribute atNcrData = ((AbstractEapSimAka) sim).getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA);
                    List<EapSimAkaAttribute> attList = JavaConversions.asJavaList(
                            attributeBuilder.eapsimAttributeBuilder().decordEncrData(HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(key), atNcrData));
                    List<EapSimAkaAttribute> filteredIdList = new ArrayList<>();
                    for(EapSimAkaAttribute eapSimAkaAttribute : attList) {
                        if(eapSimAkaAttribute.getType().getValue() == EapSimAkaAttributeTypes.AT_NEXT_REAUTH_ID.getValue()) {
                            filteredIdList.add(eapSimAkaAttribute);
                        }
                    }
                    if(!(filteredIdList.size() > 0)) {
                        throw new IllegalArgumentException("requirement failed: AT_NEXT_REAUTH_ID expected in the encrypted list");
                    }

                    ByteBuf idData = Unpooled.copiedBuffer(filteredIdList.get(0).getAttributeData());
                    short length = idData.readShort();
                    byte[] nextId = new byte[length];
                    idData.readBytes(nextId);
                    String authId = new String(nextId);
                    logger.info("AT_NEXT_REAUTH_ID : [" + authId + "]");
                    return authId;
                }
                catch (Exception e){
                    logger.error("Trying to decoded re-auth id for unsupported message type");
                    return "";
                }
            } else {
                logger.info("No need to extract NEXT-RE-AUTH-ID");
                return "";
            }
        } else {
            logger.error("Invalid message type to find re-auth id");
            return "";
        }
    }

    private EapRequestResponseMessage extractEapMessage(int requestId, boolean is3GSimFlow, Context context) {
        EapSimAkaAttribute atBlankMac = attributeBuilder.eapsimAttributeBuilder().buildAtMac();
        List<EapSimAkaAttribute> blankEapSimAkaAttributeList = new ArrayList<>();
        blankEapSimAkaAttributeList.add(atBlankMac);
        EapSim tmpSimMsg = new EapSim(EapSimSubtype.CHALLENGE, blankEapSimAkaAttributeList);

        EapRequestResponseMessage tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, requestId, tmpSimMsg);
        byte[] encodeEapMsg = tmpEapMsg.encode().array();
        ByteBuf data = Unpooled.copiedBuffer(encodeEapMsg, HexCodec.hex2Byte(is3GSimFlow ? context.getSimRes3G() : context.getSimRes()));
        byte[] mac = Arrays.copyOf(attributeBuilder.keyGenerator().hmacSha1Encode(HexCodec.hex2Byte(is3GSimFlow ? context.getSimKaut3G() : context.getSimKaut()), data), 16);
        logger.info("Generated MAC[" + HexCodec.byte2Hex(mac) + "]");

        EapSimAkaAttribute atMac = attributeBuilder.eapsimAttributeBuilder().buildAtMac(mac);
        List<EapSimAkaAttribute> eapSimAkaAttributeList = new ArrayList<>();
        eapSimAkaAttributeList.add(atMac);
        EapSim fnlSimMsg = new EapSim(EapSimSubtype.CHALLENGE, eapSimAkaAttributeList);

        return EapRequestResponseMessage.build(EapCode.RESPONSE, requestId, fnlSimMsg);
    }

    // 2G & 3G-GSM Re-Auth flow
    public AccessRequest buildReAuthSimIdentityRequest(String sharedSecret, String lastAuthId, Context context) throws IOException {
        AccessRequest accessRequest = new AccessRequest();
        AttributeType eapAttrType = accessRequest.getDictionary().getAttributeTypeByName(ATTRIBUTE_NAME_EAP_MESSAGE);
        RadiusAttribute attribute =
                RadiusAttribute.createRadiusAttribute(accessRequest.getDictionary(), eapAttrType.getVendorId(), eapAttrType.getTypeCode());

        final String reAuthIdAsHex = HexCodec.byte2Hex(lastAuthId.getBytes());
        attribute.setAttributeData(HexCodec.hex2Byte("020000" + Integer.toHexString(((reAuthIdAsHex.length()/2) + 5))+ "01" + reAuthIdAsHex));
        accessRequest.addAttribute(attribute);
        String filteredLastAuthId = lastAuthId.substring(lastAuthId.indexOf("_") + 1);
        accessRequest.addAttribute(ATTRIBUTE_NAME_USERNAME, filteredLastAuthId);
        accessRequest.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        accessRequest.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        accessRequest.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        EapMessageUtil.addMessageAuthenticator(accessRequest, sharedSecret);
        CommonAttribuiteUtil.addCommonAttribute(accessRequest);
        return accessRequest;
    }

    public RadiusPacket buildReAuthSimStartRequest(String imsi, String sharedSecret, String lastAuthId, RadiusPacket previousReceivedPacket, String realmPart, Context context)
            throws IOException {
        EapMessage eapMsg = EapMessageDecoder.decode(
                Unpooled.copiedBuffer(previousReceivedPacket.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()));
        int previousId = eapMsg.getIdentifier();

        EapSimAkaAttribute atIdentity = attributeBuilder.eapsimAttributeBuilder().buildAtIdentity(lastAuthId.getBytes());
        List<EapSimAkaAttribute> identityList = new ArrayList<>();
        identityList.add(atIdentity);
        EapSim simMsg = new EapSim(EapSimSubtype.START, identityList);
        EapRequestResponseMessage createdEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, previousId, simMsg);

        RadiusPacket rp = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
        rp.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        rp.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        rp.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        rp.addAttribute(createdEapMsg);
        CommonAttribuiteUtil.addCommonAttribute(rp);

        RadiusAttribute state = previousReceivedPacket.getAttribute(ATTRIBUTE_NAME_STATE);
        if (state != null) {
            logger.info("Adding received state attribute to the response[" + state + "]");
            rp.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(rp, sharedSecret);
        return rp;
    }

    public RadiusPacket buildReAuthSimRequest(String imsi, Context context, String sharedSecret, RadiusPacket previousReceivedPacket,
                                              boolean isCounterTooSmall, boolean is3GGsmFlow, String realmPart) throws Exception {
        EapMessage eapMsg = EapMessageDecoder.decode(
                Unpooled.copiedBuffer(previousReceivedPacket.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()));
        if(eapMsg instanceof EapRequestResponseMessage) {
            RadiusPacket rp = processReAuthRequest((EapRequestResponseMessage) eapMsg, previousReceivedPacket, isCounterTooSmall, is3GGsmFlow, context, sharedSecret);
            rp.addAttribute(ATTRIBUTE_NAME_USERNAME, imsi + realmPart);
            rp.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
            rp.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
            rp.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
            CommonAttribuiteUtil.addCommonAttribute(rp);
            return rp;
        } else {
            logger.error("EAP message is not in accepted type");
            throw new Exception("EAP message is not in accepted type");
        }
    }

    private RadiusPacket processReAuthRequest(EapRequestResponseMessage eapSim, RadiusPacket previousReceivedPacket,
                                             boolean isCounterTooSmall, boolean is3GGsmFlow, Context context,
                                             String sharedSecret) throws IOException {
        if (eapSim.getType() instanceof EapSim) {
            return buildSimResponsePacketForReAuthRequest(eapSim, previousReceivedPacket, (EapSim)eapSim.getType(),
                    isCounterTooSmall, is3GGsmFlow, context, sharedSecret);
        } else if (eapSim.getType() instanceof EapAka) {
            return buildAkaResponsePacketForReAuthRequest(eapSim, previousReceivedPacket, (EapAka)eapSim.getType(),
                    isCounterTooSmall, context, sharedSecret);
        } else {
            throw new IllegalArgumentException("Un supported message type received");
        }
    }

    private RadiusPacket buildSimResponsePacketForReAuthRequest(EapRequestResponseMessage eapSim, RadiusPacket previousReceivedPacket,
                                                               EapSim sim, boolean isCounterTooSmall, boolean is3GGsmFlow,
                                                               Context context, String sharedSecret) throws IOException {
        EapsimAttributeBuilderComponent.EapsimAttributeBuilder attBuilder = attributeBuilder.eapsimAttributeBuilder();
        EapSimAkaAttribute atNcrData = sim.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA);

        String encrKey = (is3GGsmFlow) ? context.getSimEncr3G() : context.getSimEncr();
        byte[] attributeData = atNcrData.getAttributeData();
        ByteBuf encoded = Unpooled.copiedBuffer(Arrays.copyOfRange(attributeData, 2, attributeData.length));
        byte[] decode = attributeBuilder.keyGenerator().aesWithCbcDecode(HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(encrKey), encoded.array());
        ByteBuf buffer = Unpooled.wrappedBuffer(decode);

        //extracrt AT_COUNTER
        buffer.readShort(); //skip single short for type & length
        short latestCounter = buffer.readShort();

        //Extact AT_NONCE_S  [20 bytes]
        ByteBuf nonceSData = Unpooled.buffer(16);
        buffer.readInt();
        buffer.readBytes(nonceSData);

        //extact AT_NEXT_REAUTH_ID
        buffer.readShort(); //skip single short for type & length
        short len = buffer.readShort();
        byte[] nextId = new byte[len];
        buffer.readBytes(nextId);

        logger.debug("========================");
        logger.debug("id : " + eapSim.getIdentifier());
        logger.debug("counter: " + latestCounter);
        logger.debug("nonceS: " + HexCodec.byte2Hex(nonceSData.array()));
        String lastAuthId = new String(nextId);
        context.setLastAuthId(lastAuthId);
        logger.debug("AT_NEXT_REAUTH_ID : " + lastAuthId);
        logger.debug("========================");

        //update counter
        context.setCounter(latestCounter);
        EapSimAkaAttribute atCounter = attBuilder.buildAtCounter(context.getCounter());

        EapSimAkaAttribute atBlankMac = attBuilder.buildAtMac();
        EapSimAkaAttribute atIv = attBuilder.buildAtIv(HexCodec.hex2Byte(context.getIv()));

        List<EapSimAkaAttribute> dataTobeEncrList = new ArrayList<>();
        if (isCounterTooSmall) {
            EapSimAkaAttribute atCounterTooSmall = attBuilder.buildAtCounterTooSmall();
            dataTobeEncrList.add(atCounterTooSmall);
        }
        dataTobeEncrList.add(atCounter);
        scala.collection.immutable.List<EapSimAkaAttribute> dataTobeEncrListInScala = JavaConversions.asScalaBuffer(dataTobeEncrList).toList();

        EapSimAkaAttribute atEncrData = attBuilder.buildAtEncrData(dataTobeEncrListInScala, HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(encrKey));

        List<EapSimAkaAttribute> tmpSimMsgAttributeList = new ArrayList<>();
        tmpSimMsgAttributeList.add(atIv);
        tmpSimMsgAttributeList.add(atEncrData);
        tmpSimMsgAttributeList.add(atBlankMac);

        EapSim tmpSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, tmpSimMsgAttributeList);
        EapRequestResponseMessage tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier(), tmpSimMsg);
        byte[] encodeEapMsg = tmpEapMsg.encode().array();

        ByteBuf data = Unpooled.copiedBuffer(encodeEapMsg, nonceSData.array());
        byte[] mac = Arrays.copyOf(attributeBuilder.keyGenerator().hmacSha1Encode(HexCodec.hex2Byte(is3GGsmFlow ? context.getSimKaut3G() : context.getSimKaut()), data), 16);
        EapSimAkaAttribute atMac = attributeBuilder.eapsimAttributeBuilder().buildAtMac(mac);

        List<EapSimAkaAttribute> fnlSimMsgAttributeList = new ArrayList<>();
        fnlSimMsgAttributeList.add(atIv);
        fnlSimMsgAttributeList.add(atEncrData);
        fnlSimMsgAttributeList.add(atMac);
        EapSim fnlSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, fnlSimMsgAttributeList);

        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier(), fnlSimMsg);

        RadiusPacket rp = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(fnlEapMsg);

        RadiusAttribute state = previousReceivedPacket.getAttribute(ATTRIBUTE_NAME_STATE);
        if (state != null) {
            logger.debug("Adding received State Attribute to Response[" + state + "]");
            rp.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(rp, sharedSecret);
        return rp;
    }

    private RadiusPacket buildAkaResponsePacketForReAuthRequest(EapRequestResponseMessage eapSim, RadiusPacket previousReceivedPacket,
                                                               EapAka sim, boolean isCounterTooSmall, Context context,
                                                               String sharedSecret) throws IOException {
        logger.info("Extracting EAP-Request/AKA-Reauthentication");
        EapsimAttributeBuilderComponent.EapsimAttributeBuilder attBuilder = attributeBuilder.eapsimAttributeBuilder();
        EapSimAkaAttribute atNcrData = sim.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA);

        byte[] attributeData = atNcrData.getAttributeData();

        ByteBuf encoded = Unpooled.copiedBuffer(Arrays.copyOfRange(attributeData, 2, attributeData.length));
        byte[] decode = attributeBuilder.keyGenerator().aesWithCbcDecode(HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(context.getAkaEncr()), encoded.array());
        ByteBuf buffer = Unpooled.wrappedBuffer(decode);

        //extracrt AT_COUNTER
        buffer.readShort(); //skip single short for type & length
        short latestCounter = buffer.readShort();

        //Extact AT_NONCE_S  [20 bytes]
        ByteBuf nonceSData = Unpooled.buffer(16);
        buffer.readInt();
        buffer.readBytes(nonceSData);

        //extact AT_NEXT_REAUTH_ID
        buffer.readShort(); //skip single short for type & length
        short len = buffer.readShort();
        byte[] nextId = new byte[len];
        buffer.readBytes(nextId);

        logger.debug("========================");
        logger.debug("id : " + eapSim.getIdentifier());
        logger.debug("counter: " + latestCounter);
        logger.debug("nonceS: " + HexCodec.byte2Hex(nonceSData.array()));
        String lastAuthId = new String(nextId);
        context.setLastAuthId(lastAuthId);
        logger.debug("AT_NEXT_REAUTH_ID : " + lastAuthId);
        logger.debug("========================");

        //update counter
        context.setCounter(latestCounter);
        EapSimAkaAttribute atCounter = attBuilder.buildAtCounter(context.getCounter());

        EapSimAkaAttribute atBlankMac = attBuilder.buildAtMac();
        EapSimAkaAttribute atIv = attBuilder.buildAtIv(HexCodec.hex2Byte(context.getIv()));

        List<EapSimAkaAttribute> dataTobeEncrList = new ArrayList<>();
        if (isCounterTooSmall) {
            EapSimAkaAttribute atCounterTooSmall = attBuilder.buildAtCounterTooSmall();
            dataTobeEncrList.add(atCounterTooSmall);
        }
        dataTobeEncrList.add(atCounter);

        scala.collection.immutable.List<EapSimAkaAttribute> dataTobeEncrListInScala = JavaConversions.asScalaBuffer(dataTobeEncrList).toList();

        EapSimAkaAttribute atEncrData = attBuilder.buildAtEncrData(dataTobeEncrListInScala, HexCodec.hex2Byte(context.getIv()), HexCodec.hex2Byte(context.getAkaEncr()));

        List<EapSimAkaAttribute> tmpSimMsgAttributeList = new ArrayList<>();
        tmpSimMsgAttributeList.add(atIv);
        tmpSimMsgAttributeList.add(atEncrData);
        tmpSimMsgAttributeList.add(atBlankMac);
        EapAka tmpSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, tmpSimMsgAttributeList);
        EapRequestResponseMessage tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier(), tmpSimMsg);
        byte[] encodeEapMsg = tmpEapMsg.encode().array();

        ByteBuf data = Unpooled.copiedBuffer(encodeEapMsg, nonceSData.array());
        byte[] mac = Arrays.copyOf(attributeBuilder.keyGenerator().hmacSha1Encode(HexCodec.hex2Byte(context.getAkaKaut()), data), 16);
        EapSimAkaAttribute atMac = attributeBuilder.eapsimAttributeBuilder().buildAtMac(mac);

        List<EapSimAkaAttribute> fnlSimMsgAttributeList = new ArrayList<>();
        fnlSimMsgAttributeList.add(atIv);
        fnlSimMsgAttributeList.add(atEncrData);
        fnlSimMsgAttributeList.add(atMac);
        EapAka fnlSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, fnlSimMsgAttributeList);

        EapRequestResponseMessage fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier(), fnlSimMsg);

        RadiusPacket rp = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
        rp.addAttribute(fnlEapMsg);

        RadiusAttribute state = previousReceivedPacket.getAttribute(ATTRIBUTE_NAME_STATE);
        if (state != null) {
            logger.debug("Adding received State Attribute to Response[" + state + "]");
            rp.addAttribute(state);
        }
        EapMessageUtil.addMessageAuthenticator(rp, sharedSecret);
        return rp;
    }

    private String buildEncryptionKeyIdentifierPart(String certificateKeyIdentifier) {
        if(certificateKeyIdentifier != null && !certificateKeyIdentifier.isEmpty()) {
            return new StringBuilder()
                    .append(",")
                    .append(certificateKeyIdentifier)
                    .append('\0')
                    .toString();
        } else {
            return "";
        }
    }

    public void setAttributeBuilder(AttributeBuilder attributeBuilder) {
        this.attributeBuilder = attributeBuilder;
    }

    public AttributeBuilder getAttributeBuilder() {
        return attributeBuilder;
    }
}
