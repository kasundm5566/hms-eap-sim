package hms.eapsim.perf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Statistic {

    private static final Logger statLog = LoggerFactory.getLogger("statlog");
    private static final String SEPARATOR = "|";

    private double fullAuthAvg;
    private double reAuthAvg;
    private double totalAvg;

    private long loginDuration;
    private long simStartDuration;
    private long challengeDuration;
    private long reAuthLoginDuration;
    private long reAuthSimStartDuration;
    private long reAuthChallengeDuration;

    public void clean() {
        loginDuration = 0;
        simStartDuration = 0;
        challengeDuration = 0;
        reAuthLoginDuration = 0;
        reAuthSimStartDuration = 0;
        reAuthChallengeDuration = 0;
        fullAuthAvg = 0;
        reAuthAvg = 0;
        totalAvg = 0;
    }

    public void printStat() {
        calculateAvgs();
        StringBuilder builder = new StringBuilder();
        builder.append("fullAuthAvg=" + fullAuthAvg).append(SEPARATOR)
                .append("reAuthAvg=" + reAuthAvg).append(SEPARATOR)
                .append("totalAvg=" + totalAvg).append(SEPARATOR)
                .append("FL=" + loginDuration).append(SEPARATOR)
                .append("FS=" + simStartDuration).append(SEPARATOR)
                .append("FC=" + challengeDuration).append(SEPARATOR)
                .append("RL=" + reAuthLoginDuration).append(SEPARATOR)
                .append("RS=" + reAuthSimStartDuration).append(SEPARATOR)
                .append("RC=" + reAuthChallengeDuration);
        statLog.info(builder.toString());
    }

    private void calculateAvgs() {
        fullAuthAvg = (loginDuration + simStartDuration + challengeDuration) / 3;
        reAuthAvg = (reAuthLoginDuration + reAuthSimStartDuration + reAuthChallengeDuration) / 3;
        totalAvg = (fullAuthAvg + reAuthAvg) / 2;
    }

    public long getLoginDuration() {
        return loginDuration;
    }

    public void setLoginDuration(long loginDuration) {
        this.loginDuration = loginDuration;
    }

    public long getSimStartDuration() {
        return simStartDuration;
    }

    public void setSimStartDuration(long simStartDuration) {
        this.simStartDuration = simStartDuration;
    }

    public long getChallengeDuration() {
        return challengeDuration;
    }

    public void setChallengeDuration(long challengeDuration) {
        this.challengeDuration = challengeDuration;
    }

    public long getReAuthLoginDuration() {
        return reAuthLoginDuration;
    }

    public void setReAuthLoginDuration(long reAuthLoginDuration) {
        this.reAuthLoginDuration = reAuthLoginDuration;
    }

    public long getReAuthSimStartDuration() {
        return reAuthSimStartDuration;
    }

    public void setReAuthSimStartDuration(long reAuthSimStartDuration) {
        this.reAuthSimStartDuration = reAuthSimStartDuration;
    }

    public long getReAuthChallengeDuration() {
        return reAuthChallengeDuration;
    }

    public void setReAuthChallengeDuration(long reAuthChallengeDuration) {
        this.reAuthChallengeDuration = reAuthChallengeDuration;
    }

}
