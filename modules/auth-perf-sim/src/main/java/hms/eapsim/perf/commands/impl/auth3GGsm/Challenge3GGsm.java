package hms.eapsim.perf.commands.impl.auth3GGsm;

import hms.common.radius.decoder.eap.EapMessageDecoder;
import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGGsmCommand;
import io.netty.buffer.Unpooled;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;

public class Challenge3GGsm extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Challenge3GGsm.class);

    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());

        EapMessage eapMessage = EapMessageDecoder.decode(
                Unpooled.copiedBuffer(context.getPreviousReceivedPacket().getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()));
        int previousId = eapMessage.getIdentifier();
        RadiusPacket request = requestBuilder.buildSimChallengeRequest(context.getImsi(), (byte) previousId,
                context.getPreviousReceivedPacket(), context.getConfiguration().getSharedSecret(), context.getSimEncr3G(),
                context, true, context.getRealmPart());
        RadiusPacket response = messageSender.send(request, context, getName());
        context.setPreviousReceivedPacket(response);

        parent.setContext(parent.getImsi(), context);


        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGGsmCommand.CHALLENGE3G_GSM.name();
    }
}
