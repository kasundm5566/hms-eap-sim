package hms.eapsim.perf.commands;


public interface Executable {

    String ATTRIBUTE_NAME_EAP_MESSAGE = "EAP-Message";
    String ATTRIBUTE_NAME_USERNAME = "User-Name";
    String ATTRIBUTE_NAME_STATE = "State";
    String ATTRIBUTE_NAS_IDENTIFIER = "NAS-Identifier";
    String ATTRIBUTE_CALLED_STATION_ID = "Called-Station-Id";
    String ATTRIBUTE_CALLING_STATION_ID = "Calling-Station-Id";

    void execute() throws Exception;

    String getName();

}
