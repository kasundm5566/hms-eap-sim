package hms.eapsim.perf.commands.impl.auth3g;

import hms.common.radius.decoder.eap.elements.EapMessage;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;

/**
 * This represents the second eap response from mobile device for full authentication flow, EAP-Response/AKA-Challenge
 */
public class Login3GInit extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(Login3GInit.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        EapMessage eapMessage = context.extractEapFrom();

        RadiusPacket challengeRequest = requestBuilder.build3GAccessRequestWithAtIdentity(context.getImsi(), context.getPreviousReceivedPacket(),
                eapMessage, context.getConfiguration().getSharedSecret(), context.getRealmPart(), context);

        RadiusPacket responsePacket = messageSender.send(challengeRequest, context, getName());
        logger.info("Response Received [" + responsePacket + "]");

        context.setPreviousReceivedPacket(responsePacket);
        context.setLastAuthId(extractReAuthId(getEapMessage(context.getPreviousReceivedPacket()), context));

        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGCommand.LOGIN_3G_INIT.name();
    }
}
