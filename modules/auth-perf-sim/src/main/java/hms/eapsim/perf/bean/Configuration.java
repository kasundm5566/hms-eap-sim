package hms.eapsim.perf.bean;

import java.util.concurrent.TimeUnit;

/**
 * Represent system configuration
 */

//@org.springframework.context.annotation.Configuration
//@PropertySource("classpath:performance.properties")
public class Configuration {

//    @Value("${simulator.running.mode}")
    private String runningMode;

//    @Value("${client.secret.shared}")
    private String sharedSecret;

//    @Value("${client.so.timeout.in.milli.sec}")
    private int soTimeout;

//    @Value("${request.timeout.in.milli.sec}")
    private int requestTimeoutInMillSec;

//    @Value("${client.retry.count}")
    private int numberOfRequestRetry;

//    @Value("${eap.server.port}")
    private int serverPort;

//    @Value("${eap.server.host}")
    private String hostAddress;

//    @Value("${receiver.pool.size}")
    private int receiverPoolSize = 10;

//    @Value("${sender.pool.size}")
    private int senderPoolSize = 10;

//    @Value("${flow.executor.pool.size}")
    private int flowExecutorPoolSize = 10;

//    @Value("${latency.threshold.in.milli.sec}")
    private int latencyThresholdInMillSec = 5000;

    private int timerTickDurationInMilliSec = 2;

    private TimeUnit timerTimeUnit;

    private int tickPerWheel = 1024;

    private long defaultSleepTimeInMilliSec = 20;

    private String certificateFilePath;

    private String certificateKeyIdentifier;

    public long getDefaultSleepTimeInMilliSec() {
        return defaultSleepTimeInMilliSec;
    }

    public void setDefaultSleepTimeInMilliSec(long defaultSleepTimeInMilliSec) {
        this.defaultSleepTimeInMilliSec = defaultSleepTimeInMilliSec;
    }

    public int getTickPerWheel() {
        return tickPerWheel;
    }

    public void setTickPerWheel(int tickPerWheel) {
        this.tickPerWheel = tickPerWheel;
    }

    public int getTimerTickDurationInMilliSec() {
        return timerTickDurationInMilliSec;
    }

    public void setTimerTickDurationInMilliSec(int timerTickDurationInMilliSec) {
        this.timerTickDurationInMilliSec = timerTickDurationInMilliSec;
    }

    public String getCertificateFilePath() {
        return certificateFilePath;
    }

    public void setCertificateFilePath(String certificateFilePath) {
        this.certificateFilePath = certificateFilePath;
    }

    public String getCertificateKeyIdentifier() {
        return certificateKeyIdentifier;
    }

    public void setCertificateKeyIdentifier(String certificateKeyIdentifier) {
        this.certificateKeyIdentifier = certificateKeyIdentifier;
    }

    public TimeUnit getTimerTimeUnit() {
        return timerTimeUnit;
    }

    public void setTimerTimeUnit(String timerTimeUnit) {
        this.timerTimeUnit = TimeUnit.valueOf(timerTimeUnit);
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public int getReceiverPoolSize() {
        return receiverPoolSize;
    }

    public int getSenderPoolSize() {
        return senderPoolSize;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public int getRequestTimeoutInMillSec() {
        return requestTimeoutInMillSec;
    }

    public int getNumberOfRequestRetry() {
        return numberOfRequestRetry;
    }

    public String getRunningMode() {
        return runningMode;
    }

    public int getFlowExecutorPoolSize() {
        return flowExecutorPoolSize;
    }

    public int getLatencyThresholdInMillSec() {
        return latencyThresholdInMillSec;
    }

    public enum Mode {
        FUNCTIONAL,

        PERFORMANCE,

        SCHEDULED_PERFORMANCE,
    }

    public void setRunningMode(String runningMode) {
        this.runningMode = runningMode;
    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    public void setSoTimeout(int soTimeout) {
        this.soTimeout = soTimeout;
    }

    public void setRequestTimeoutInMillSec(int requestTimeoutInMillSec) {
        this.requestTimeoutInMillSec = requestTimeoutInMillSec;
    }

    public void setNumberOfRequestRetry(int numberOfRequestRetry) {
        this.numberOfRequestRetry = numberOfRequestRetry;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public void setReceiverPoolSize(int receiverPoolSize) {
        this.receiverPoolSize = receiverPoolSize;
    }

    public void setSenderPoolSize(int senderPoolSize) {
        this.senderPoolSize = senderPoolSize;
    }

    public void setFlowExecutorPoolSize(int flowExecutorPoolSize) {
        this.flowExecutorPoolSize = flowExecutorPoolSize;
    }

    public void setLatencyThresholdInMillSec(int latencyThresholdInMillSec) {
        this.latencyThresholdInMillSec = latencyThresholdInMillSec;
    }
}


