package hms.eapsim.perf.commands.impl.auth2G;

import hms.eapsim.perf.commands.impl.BaseAnonymous;
import hms.eapsim.perf.flows.command.TwoGCommand;
import org.apache.log4j.Logger;

public class Anonymous2G extends BaseAnonymous {

    private static final Logger logger = Logger.getLogger(Anonymous2G.class);

    @Override
    public String getName() {
        return TwoGCommand.ANONYMOUS_2G.name();
    }

}
