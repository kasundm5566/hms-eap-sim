package hms.eapsim.perf.flows.impl;

import hms.eapsim.key.*;
import hms.eapsim.util.EapsimAttributeBuilderComponent;
import org.apache.log4j.Logger;

public class AttributeBuilder implements EapsimAttributeBuilderComponent, KeyGeneratorComponent, KeyManagerComponent, Fips186RandomGeneratorComponent {

    private static final Logger logger = Logger.getLogger(AttributeBuilder.class);

    private EapsimAttributeBuilder eapsimAttributeBuilder;
    private KeyGenerator keyGenerator;
    private KeyManager keyManager;
    private Fips186PRNGenerator fips186PRNGenerator;

    public AttributeBuilder() {
        this.eapsimAttributeBuilder = new EapsimAttributeBuilder();
        this.keyGenerator = new KeyGenerator();
        this.keyManager = new DummyKeyManager();
        this.fips186PRNGenerator = new DummyFips186PRNGenerator();
    }

    @Override
    public EapsimAttributeBuilder eapsimAttributeBuilder() {
        return eapsimAttributeBuilder;
    }

    @Override
    public KeyGenerator keyGenerator() {
        return keyGenerator;
    }

    @Override
    public KeyManager keyManager() {
        return keyManager;
    }

    @Override
    public Fips186PRNGenerator fips186Generator() {
        return fips186PRNGenerator;
    }

}
