package hms.eapsim.perf.commands.impl.reauth2G;

import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.TwoGCommand;
import org.apache.log4j.Logger;
import org.tinyradius.packet.RadiusPacket;

public class ReAuth2G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(ReAuth2G.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        RadiusPacket request = requestBuilder.buildReAuthSimRequest(context.getImsi(), context, context.getConfiguration().getSharedSecret(),
                context.getPreviousReceivedPacket(), false, false, context.getRealmPart());
        RadiusPacket response = messageSender.send(request, context, getName());
        context.setPreviousReceivedPacket(response);

        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return TwoGCommand.REAUTH2G.name();
    }

}
