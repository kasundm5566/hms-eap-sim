package hms.eapsim.perf.commands.impl.reauth3g;

import hms.common.radius.util.EapMessageUtil;
import hms.common.radius.util.HexCodec;
import hms.eapsim.perf.commands.impl.BaseExecutable;
import hms.eapsim.perf.domain.Context;
import hms.eapsim.perf.flows.command.ThreeGCommand;
import hms.eapsim.perf.util.CommonAttribuiteUtil;
import org.apache.log4j.Logger;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.dictionary.AttributeType;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;

import static hms.eapsim.perf.Utility.isEmpty;

/**
 * Sending EAP-Response/AKA/Identity
 */
public class ReauthLogin3G extends BaseExecutable {

    private static final Logger logger = Logger.getLogger(ReauthLogin3G.class);

    @Override
    public void execute() throws Exception {
        Context context = parent.getContext(parent.getImsi());
        if (isEmpty(context.getLastAuthId())) {
            throw new RuntimeException("Last re-auth is not available");
        }
        AccessRequest ar = new AccessRequest();
        AttributeType eapAttrType = ar.getDictionary().getAttributeTypeByName("EAP-Message");
        RadiusAttribute attribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary(), eapAttrType.getVendorId(), eapAttrType.getTypeCode());
        if (logger.isDebugEnabled()) {
            logger.debug("Last re-auth [" + context.getLastAuthId() + "]");
        }
        final String reAuthIdAsHex = HexCodec.byte2Hex(context.getLastAuthId().getBytes());
        attribute.setAttributeData(HexCodec.hex2Byte("020000" + Integer.toHexString((reAuthIdAsHex.length()/2) + 5) + "01" + reAuthIdAsHex));

        ar.addAttribute(attribute);
        ar.addAttribute("User-Name", context.getImsi() + context.getRealmPart());
        ar.addAttribute(ATTRIBUTE_CALLED_STATION_ID, context.getCalledStationId());
        ar.addAttribute(ATTRIBUTE_CALLING_STATION_ID, context.getCallingStationId());
        ar.addAttribute(ATTRIBUTE_NAS_IDENTIFIER, context.getNasIndentier());
        CommonAttribuiteUtil.addCommonAttribute(ar);
        EapMessageUtil.addMessageAuthenticator(ar, context.getConfiguration().getSharedSecret());

        if (logger.isDebugEnabled()) {
            logger.debug("Request : " + ar);
        }

        RadiusPacket response = messageSender.send(ar, context, getName());
        context.setPreviousReceivedPacket(response);

        if (logger.isDebugEnabled()) {
            logger.debug("Response : " + response);
        }
        parent.setContext(parent.getImsi(), context);

        if (isLastCommand) {
            parent.unassigned();
        }
    }

    @Override
    public String getName() {
        return ThreeGCommand.REAUTH_LOGIN_3G.name();
    }
}
