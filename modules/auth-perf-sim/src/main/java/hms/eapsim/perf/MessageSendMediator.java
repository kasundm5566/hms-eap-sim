package hms.eapsim.perf;

import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.flows.BaseFlow;
import hms.eapsim.perf.flows.impl.Flow2G;
import hms.eapsim.perf.flows.impl.Flow3G;
import hms.eapsim.perf.flows.impl.Flow3GGsm;
import hms.eapsim.perf.traffic.TrafficPlan;
import hms.eapsim.perf.util.TPSLog;
import io.netty.util.HashedWheelTimer;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * Message sending mediator
 */

public class MessageSendMediator {

    private RequestBuilder requestBuilder;

    private Configuration configuration;

    private MessageSender messageSender;

    private TrafficPlan trafficPlan;

    private Thread testPlan;

    private boolean started;

    private ExecutorService executorService;

    private LinkedBlockingQueue<Runnable> workQueue;

    private boolean isShuffleAllowed;

    private HashedWheelTimer timer;

    private static final Logger logger = Logger.getLogger(MessageSendMediator.class);

    public synchronized void start(final Integer tps, final TrafficPlan.FlowType flow) throws IOException {
        if (trafficPlan != null) {
            testPlan = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (configuration.getRunningMode().equalsIgnoreCase(Configuration.Mode.PERFORMANCE.name())) {
                        doPerformanceTest(tps, flow);
                    } else if (configuration.getRunningMode().equalsIgnoreCase(Configuration.Mode.FUNCTIONAL.name())) {
                        doFunctionTest(flow);
                    } else if (configuration.getRunningMode().equalsIgnoreCase(Configuration.Mode.SCHEDULED_PERFORMANCE.name())) {
                        doSchedule(tps, flow);
                    } else {
                        throw new RuntimeException("Invalid test mode [" + configuration.getRunningMode() + "]");
                    }
                    logger.debug("Test plan stopped !!!");
                }
            }, "TestPlan");
            testPlan.setDaemon(true);
            testPlan.start();

            started = true;
        }
    }

    private void doFunctionTest(TrafficPlan.FlowType flow) {
        List<BaseFlow> flowList = getFlow(flow);

        for (BaseFlow current : flowList) {
            try {
                current.auth();
            } catch (Exception e) {
                logger.error("Error while executing", e);
            }
        }
    }

    private List<BaseFlow> getFlow(TrafficPlan.FlowType flow) {
        List<BaseFlow> flowList = trafficPlan.getFlowList();

        if (flow == TrafficPlan.FlowType.ALL) {
            if (isShuffleAllowed) {
                Collections.shuffle(flowList);
            }
            return flowList;
        } else {
            List<BaseFlow> specificFlowList = new ArrayList<>();

            for (BaseFlow current : flowList) {
                if (flow == TrafficPlan.FlowType.THREE_G_GSM && current instanceof Flow3GGsm) {
                    specificFlowList.add(current);
                } else if (flow == TrafficPlan.FlowType.THREE_G && current instanceof Flow3G) {
                    specificFlowList.add(current);
                } else if (flow == TrafficPlan.FlowType.TWO_G && current instanceof Flow2G) {
                    specificFlowList.add(current);
                }
            }
            return specificFlowList;
        }
    }


    private void doSchedule(Integer tps, TrafficPlan.FlowType flow) {
        while (started) {
            List<BaseFlow> flowList = getFlow(flow);

            if (flowList == null || flowList.isEmpty()) {
                break;
            }

            boolean isAllBounded = true;

            for (int i = 0; i < flowList.size(); i++) {
                BaseFlow currentFlow = flowList.get(i);

                if (!currentFlow.isThreadBound()) {
                    isAllBounded = false;
                    try {
                        currentFlow.schedule(timer, executorService, configuration.getDefaultSleepTimeInMilliSec());
                    } catch (Exception e) {
                        logger.error("Error while scheduling flow [" + currentFlow.getImsi() + "]", e);
                    }
                }
                tpsSleep(tps);
            }

            if (isAllBounded) {
                try {
                    logger.warn("No spare imsi left for send message. increase number of imsi");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    logger.info("Error while sleeping", e);
                }
            }
        }
    }

    private void tpsSleep(Integer tps) {
        try {
            Thread.sleep(1000/tps);
        } catch (InterruptedException e) {
            logger.error("Error while schedule sleeping", e);
        }
    }


    private void doPerformanceTest(int tps, TrafficPlan.FlowType flow) {
        while (started) {
            try {
                List<BaseFlow> flowList = getFlow(flow);
                boolean vacantFlowFound = false;
                if (flowList != null) {
                    for (int i = 0; i < flowList.size(); i++) {
                        final BaseFlow current = flowList.get(i);

                        if (!current.isThreadBound()) {
                            vacantFlowFound = true;
                            current.assigned();
                            executorService.submit(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        NDC.push(current.getImsi());
                                        current.auth();
                                    } catch (Exception e) {
                                        logger.error("Error while executing", e);
                                    } finally {
                                        current.getStatistic().printStat();
                                        current.clear();
                                        NDC.pop();
                                    }
                                }
                            });
                        }
                        Thread.sleep(1000/tps);
                        if ((i == flowList.size() - 1) && !vacantFlowFound) {
                            Thread.sleep(50);
                        }
                        TPSLog.setPendingQueSize(workQueue.size());
                    }
                } else {
                    logger.error("No traffic plan loaded");
                    break;
                }
            } catch (Exception e) {
                logger.error("Error occurred in Test Plan ", e);
            }
        }
    }

    public void init() throws IOException {
//        initMessageSender();
        initTrafficPlan();

        workQueue = new LinkedBlockingQueue<>();
        executorService = new  ThreadPoolExecutor(configuration.getFlowExecutorPoolSize(),
                configuration.getFlowExecutorPoolSize(),
                0L, TimeUnit.MILLISECONDS,
                workQueue);

        initTimer();
    }

    private void initTimer() {
        if (configuration.getRunningMode().equals(Configuration.Mode.SCHEDULED_PERFORMANCE.name())) {

            timer = new HashedWheelTimer(configuration.getTimerTickDurationInMilliSec(),
                    configuration.getTimerTimeUnit(),
                    configuration.getTickPerWheel());
        }
    }

    private void initTrafficPlan() throws IOException {
        trafficPlan = new TrafficPlan();
        trafficPlan.setConfiguration(configuration);
        trafficPlan.setMessageSender(messageSender);
        trafficPlan.setRequestBuilder(requestBuilder);
        trafficPlan.load();
    }

    public synchronized Boolean stop() {
        if (started) {
            this.started = false;
            logger.info("Test stopped successfully");
            return Boolean.TRUE;
        }
        logger.info("Test already have stopped");
        return Boolean.FALSE;
    }

    public synchronized Boolean change(Integer tps) {

        logger.info("MessageSendMediator changed successfully");
        return Boolean.TRUE;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public void setRequestBuilder(RequestBuilder requestBuilder) {
        this.requestBuilder = requestBuilder;
    }

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }


    public boolean isStarted() {
        return started;
    }

    public void setShuffleAllowed(boolean isSuffleAllowed) {
        this.isShuffleAllowed = isSuffleAllowed;
    }
}
