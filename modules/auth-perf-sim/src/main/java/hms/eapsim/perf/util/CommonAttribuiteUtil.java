package hms.eapsim.perf.util;

import hms.common.radius.util.HexCodec;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.attribute.VendorSpecificAttribute;
import org.tinyradius.packet.RadiusPacket;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by sampath on 7/12/17.
 */
public class CommonAttribuiteUtil {

    private static List<RadiusAttribute> attributeList = new ArrayList();
    private static final Lock lock = new ReentrantLock();

    static {
        lock.tryLock();
        try {
            ByteBuf byteBuf = Unpooled.buffer(4).writeInt(10);

            attributeList.add(new RadiusAttribute(44, "5963D10B-2CDB1000".getBytes()));
            attributeList.add(new RadiusAttribute(4, HexCodec.hex2Byte("c0a83516")));
            attributeList.add(new RadiusAttribute(5, byteBuf.array()));
            attributeList.add(new RadiusAttribute(128, HexCodec.hex2Byte("3130534717234d52547c45573136204f757472616d205061726b7c4f50412d42322d45574c2d415033")));
            attributeList.add(new RadiusAttribute(6, byteBuf.array()));
            attributeList.add(new RadiusAttribute(89, "11".getBytes()));
            attributeList.add(new RadiusAttribute(61, byteBuf.array()));
            attributeList.add(new RadiusAttribute(77, "CONNECT 802.11a/n".getBytes()));
            VendorSpecificAttribute v1 = new VendorSpecificAttribute(25053);
            VendorSpecificAttribute v2 = new VendorSpecificAttribute(25053);
            VendorSpecificAttribute v3 = new VendorSpecificAttribute(25053);
            RadiusAttribute attribute1 = new RadiusAttribute(3, HexCodec.hex2Byte("576972656c65737340534778"));
            RadiusAttribute attribute2 = new RadiusAttribute(3, HexCodec.hex2Byte("576972656c65737340534778"));
            RadiusAttribute attribute3 = new RadiusAttribute(3, HexCodec.hex2Byte("576972656c65737340534778"));
            attribute1.setVendorId(25053);
            attribute2.setVendorId(25053);
            attribute3.setVendorId(25053);
            v1.addSubAttribute(attribute1);
            v2.addSubAttribute(attribute2);
            v3.addSubAttribute(attribute3);

            attributeList.add(v1);
            attributeList.add(v2);
            attributeList.add(v3);
        } finally {
            lock.unlock();
        }
    }

    public static final void addCommonAttribute(RadiusPacket packet) {
        for (RadiusAttribute radiusAttribute : attributeList) {
            packet.addAttribute(radiusAttribute);
        }
    }
}
