package hms.eapsim.perf.commands.impl.auth3GGsm;

import hms.eapsim.perf.MessageSender;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.bean.Transport;
import hms.eapsim.perf.domain.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tinyradius.packet.RadiusPacket;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {Transport.class})
public class Login3GGsmTest {

    @Autowired
    private RequestBuilder requestBuilder;
    @Autowired
    private MessageSender messageSender;
    @Autowired
    private Configuration configuration;

    private Context context;
    private Login3GGsm login3GGsm;

    @Before
    public void setUp() throws Exception {
        context = new Context();
        context.setImsi("1123727710000010");
        context.setConfiguration(configuration);
        login3GGsm = createLogin3GGsm();
    }

    @Test
    public void testExecute() throws Exception {
        login3GGsm.execute();
        assertEquals(RadiusPacket.ACCESS_CHALLENGE, context.getPreviousReceivedPacket().getPacketType());
    }

    private Login3GGsm createLogin3GGsm() {
        Login3GGsm login3GGsm = new Login3GGsm();
        login3GGsm.setRequestBuilder(requestBuilder);
        login3GGsm.setMessageSender(messageSender);
        return login3GGsm;
    }

}