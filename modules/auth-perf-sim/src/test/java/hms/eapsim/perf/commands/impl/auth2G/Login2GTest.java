package hms.eapsim.perf.commands.impl.auth2G;

import hms.eapsim.perf.MessageSender;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.bean.Transport;
import hms.eapsim.perf.domain.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tinyradius.packet.RadiusPacket;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-context.xml")
public class Login2GTest {

    @Autowired
    private RequestBuilder requestBuilder;
    @Autowired
    private MessageSender messageSender;
    @Autowired
    private Configuration configuration;

    private Context context;
    private Login2G login2G;

    @Before
    public void setUp() throws Exception {
        context = new Context();
        context.setImsi("1320727710000010");
        context.setConfiguration(configuration);
        login2G = createLogin2G();
    }

    @Test
    public void testExecute() throws Exception {
        login2G.execute();
        assertEquals(RadiusPacket.ACCESS_CHALLENGE, context.getPreviousReceivedPacket().getPacketType());
    }

    private Login2G createLogin2G() {
        Login2G login2G = new Login2G();
        login2G.setRequestBuilder(requestBuilder);
        login2G.setMessageSender(messageSender);
        return login2G;
    }
}