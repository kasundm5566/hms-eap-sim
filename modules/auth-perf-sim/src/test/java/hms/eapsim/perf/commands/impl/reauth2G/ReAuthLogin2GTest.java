package hms.eapsim.perf.commands.impl.reauth2G;

import hms.eapsim.perf.MessageSender;
import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.bean.Transport;
import hms.eapsim.perf.commands.impl.auth2G.Challenge2G;
import hms.eapsim.perf.commands.impl.auth2G.Login2G;
import hms.eapsim.perf.commands.impl.auth2G.SimStart2G;
import hms.eapsim.perf.domain.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tinyradius.packet.RadiusPacket;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {Transport.class})
public class ReAuthLogin2GTest {

    @Autowired
    private RequestBuilder requestBuilder;
    @Autowired
    private MessageSender messageSender;
    @Autowired
    private Configuration configuration;

    private Context context;
    private Login2G login2G;
    private SimStart2G simStart2G;
    private Challenge2G challenge2G;
    private ReAuthLogin2G reAuthLogin2G;

    @Before
    public void setUp() throws Exception {
        context = new Context();
        context.setImsi("1320727710000010");
        context.setConfiguration(configuration);
        login2G = createLogin2G();
        simStart2G = createSimStart2G();
        challenge2G = createChallenge2G();
        reAuthLogin2G = createReAuthLogin2G();
    }

    @Test
    public void execute() throws Exception {
        login2G.execute();
        assertEquals(RadiusPacket.ACCESS_CHALLENGE, context.getPreviousReceivedPacket().getPacketType());

        simStart2G.execute();
        assertEquals(RadiusPacket.ACCESS_CHALLENGE, context.getPreviousReceivedPacket().getPacketType());

        challenge2G.execute();
        assertEquals(RadiusPacket.ACCESS_ACCEPT, context.getPreviousReceivedPacket().getPacketType());

        reAuthLogin2G.execute();
        assertEquals(RadiusPacket.ACCESS_CHALLENGE, context.getPreviousReceivedPacket().getPacketType());
    }

    private Login2G createLogin2G() {
        Login2G login2G = new Login2G();
        login2G.setRequestBuilder(requestBuilder);
        login2G.setMessageSender(messageSender);
        return login2G;
    }

    private SimStart2G createSimStart2G() {
        SimStart2G simStart2G = new SimStart2G();
        simStart2G.setRequestBuilder(requestBuilder);
        simStart2G.setMessageSender(messageSender);
        return simStart2G;
    }

    private Challenge2G createChallenge2G() {
        challenge2G = new Challenge2G();
        challenge2G.setRequestBuilder(requestBuilder);
        challenge2G.setMessageSender(messageSender);
        return challenge2G;
    }

    private ReAuthLogin2G createReAuthLogin2G() {
        ReAuthLogin2G reAuthLogin2G = new ReAuthLogin2G();
        reAuthLogin2G.setRequestBuilder(requestBuilder);
        reAuthLogin2G.setMessageSender(messageSender);
        return reAuthLogin2G;
    }
}