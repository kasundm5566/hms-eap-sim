package hms.eapsim.perf;

import org.apache.tomcat.util.buf.HexUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class RequestBuilderTest {

    @Test
    public void testIdentity0123727710000010() {
        //3031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267
        //0123727710000010@wlan.mnc072.mcc320.3gppnetwork.org
        assertEquals("3031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267", HexUtils.toHexString("0123727710000010@wlan.mnc072.mcc320.3gppnetwork.org".getBytes()));
    }

}