package hms.eapsim.perf.traffic;

import hms.eapsim.perf.RequestBuilder;
import hms.eapsim.perf.bean.Configuration;
import hms.eapsim.perf.commands.Executable;
import hms.eapsim.perf.flows.BaseFlow;
import hms.eapsim.perf.flows.impl.Flow2G;
import hms.eapsim.perf.flows.impl.Flow3G;
import hms.eapsim.perf.flows.impl.Flow3GGsm;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class TrafficPlanTest {

//    @Test
//    public void loadTest() throws IOException {
//
//        TrafficPlan trafficPlan = new TrafficPlan();
//        RequestBuilder requestBuilder = new RequestBuilder();
//        trafficPlan.setRequestBuilder(requestBuilder);
//        trafficPlan.load();
//
//        List<BaseFlow> flow2GS = trafficPlan.getFlow2GList();
//        assertEquals(2, flow2GS.size());
//
//        BaseFlow flow2GOne = flow2GS.get(0);
//        List<? extends Executable> flow2GOneCommandsOne = flow2GOne.getCommands();
//        assertEquals(5, flow2GOneCommandsOne.size());
//
//        BaseFlow flow2GTwo = flow2GS.get(1);
//        List<Executable> flow2GOneCommandsTwo = (List<Executable>) flow2GTwo.getCommands();
//        assertEquals(9, flow2GOneCommandsTwo.size());
//
//        List<BaseFlow> flow3GS = trafficPlan.getFlow3GList();
//        assertEquals(2, flow3GS.size());
//
//        BaseFlow flow3GOne = flow3GS.get(0);
//        List<Executable> flow3GOneCommands = (List<Executable>) flow3GOne.getCommands();
//        assertEquals(5, flow3GOneCommands.size());
//
//        BaseFlow flow3GTwo = flow3GS.get(1);
//        List<Executable> flow3GTwoCommands = (List<Executable>) flow3GTwo.getCommands();
//        assertEquals(9, flow2GOneCommandsTwo.size());
//
//        List<BaseFlow> flow3GGsms = trafficPlan.getFlow3GGsmList();
//        assertEquals(2, flow3GGsms.size());
//
//        BaseFlow flow3GGsmOne = flow3GGsms.get(0);
//        List<Executable> flow3GGsmOneCommands = (List<Executable>) flow3GGsmOne.getCommands();
//        assertEquals(5, flow3GOneCommands.size());
//
//        BaseFlow flow3GGsmTwo = flow3GGsms.get(1);
//        List<Executable> flow3GGsmTwoCommands = (List<Executable>) flow3GGsmTwo.getCommands();
//        assertEquals(9, flow2GOneCommandsTwo.size());
//
//
//    }
//
//    @Test
//    public void testAdd() {
//        TrafficPlan plan = new TrafficPlan();
//        assertEquals(0, plan.getFlow2GList().size());
//        plan.add(new Flow2G());
//        plan.add(new Flow2G());
//        assertEquals(2, plan.getFlow2GList().size());
//
//
//        assertEquals(0, plan.getFlow3GList().size());
//        plan.add(new Flow3G());
//        plan.add(new Flow3G());
//        assertEquals(2, plan.getFlow3GList().size());
//
//        assertEquals(0, plan.getFlow3GGsmList().size());
//        plan.add(new Flow3GGsm());
//        plan.add(new Flow3GGsm());
//        assertEquals(2, plan.getFlow3GGsmList().size());
//    }
//
//    @Test(expected = RuntimeException.class)
//    public void addInvalidType() {
//        TrafficPlan plan = new TrafficPlan();
//
//        plan.add(new Flow2G() {
//            @Override
//            public void clear() {
//
//            }
//
//            @Override
//            public void auth() {
//                //do nothing
//            }
//
//            public void setCommands(Executable executable) {
//                //do nothing
//            }
//
//            @Override
//            public void setConfiguration(Configuration configuration) {
//                //do nothing
//            }
//        });
//    }
//
//    @Test
//    public void testGetNext2G() throws Exception {
//        TrafficPlan plan = new TrafficPlan();
//        Flow2G flow0 = new Flow2G();
//        Flow2G flow1 = new Flow2G();
//        Flow2G flow2 = new Flow2G();
//
//        plan.add(flow0);
//        plan.add(flow2);
//        plan.add(flow1);
//
//        assertEquals(3, plan.getFlow2GList().size());
//
//        assertEquals(flow0, plan.getNext2G());
//        assertEquals(flow2, plan.getNext2G());
//        assertEquals(flow1, plan.getNext2G());
//        assertEquals(flow0, plan.getNext2G());
//    }
//
//    @Test
//    public void testGetNext3G() throws Exception {
//        TrafficPlan plan = new TrafficPlan();
//        Flow3G flow0 = new Flow3G();
//        Flow3G flow1 = new Flow3G();
//        Flow3G flow2 = new Flow3G();
//
//        plan.add(flow0);
//        plan.add(flow2);
//        plan.add(flow1);
//
//        assertEquals(3, plan.getFlow3GList().size());
//
//        assertEquals(flow0, plan.getNext3G());
//        assertEquals(flow2, plan.getNext3G());
//        assertEquals(flow1, plan.getNext3G());
//        assertEquals(flow0, plan.getNext3G());
//
//    }
//
//    @Test
//    public void testGetNext3GGsm() throws Exception {
//        TrafficPlan plan = new TrafficPlan();
//
//        Flow3GGsm flow0 = new Flow3GGsm();
//        Flow3GGsm flow1 = new Flow3GGsm();
//        Flow3GGsm flow2 = new Flow3GGsm();
//
//        plan.add(flow0);
//        plan.add(flow2);
//        plan.add(flow1);
//
//        assertEquals(3, plan.getFlow3GGsmList().size());
//
//        assertEquals(flow0, plan.getNext3GGsm());
//        assertEquals(flow2, plan.getNext3GGsm());
//        assertEquals(flow1, plan.getNext3GGsm());
//        assertEquals(flow0, plan.getNext3GGsm());
//
//    }
}