<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <jsp:include page="../library.jsp"/>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../resources/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.autocomplete.js"></script>
</head>
<div id="wrap">
    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="errorContent">
        <div id="heading_top">
            <div class="heading_name">Error</div>
        </div>
        <div id="heading_middle">
            <div class="center">
                <h1><fmt:message key="http.error.500.heading"/> </h1>
                <h3><fmt:message key="http.error.500.messsage"/></h3>
            </div>
        </div>
        <div id="heading_bottom">
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>