<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title></title>
</head>
<body>
	<div class="urbangreymenu">

			<h3 class="headerbar">
				<a href=""><fmt:message key="content.left.eapsim.reports.overall" /></a>
			</h3>
			<ul class="submenu">

				<li><a href="report"><fmt:message
							key="content.left.eapsim.reports.events.comparison" /></a></li>
				<li><a href="summary-report"><fmt:message
						key="content.left.summary.report"/></a></li>
				<li><a href="location-summary-report"><fmt:message
						key="content.left.location.based.summary.report"/></a></li>


			</ul>

	</div>
</body>
</html>