<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Canceled</title>
    <link href="../css/custom.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<div id="wrap">

    <div id="header_right">
        </a><a href="/j_spring_security_logout">Log Out</a>
    </div>

    <div id="top_banner" onclick="window.location ='<fmt:message key="url.eapsim.reports"/>'"></div>

    <div id="content">
        <div>
            <div id="heading_top" style="background-size: 100%; width: 900px;">
            </div>
            <div id="heading_middle" style="padding-left: 20px; width:878px;">
                <h1>Task canceled</h1>
                Click <a href="/spring/report">here</a> to continue

            </div>

            <div id="heading_bottom" style="background-size: 100%; width: 900px;"></div>
        </div>
     <div id="footer">
        <div class="footer_heading" align="center" style="float:none;margin-left: 190px;">
            Copyright &copy; 2012-2017 <a href="http://www.m1.com.sg/M1/site/M1Corp/" target="_blank"
                                          style="color:#fff">M1</a>,
            <a href="http://www.hsenidmobile.com/" target="_blank" style="color:#fff">hSenid Mobile Solutions</a>. All
            rights reserved.
        </div>
    </div>
    </div>
</div>
</body>

</html>
