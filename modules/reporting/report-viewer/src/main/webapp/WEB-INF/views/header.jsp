<%@ page import="org.springframework.security.core.userdetails.UserDetails" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript">
        function logout(){
            var logout= confirm("Do you really want to logout??");
            if (logout == true){
                window.location = "../j_spring_security_logout";
            }
        }
    </script>
</head>
<body>
    <div class="header-navigation">
        <span style="font-weight: bold"><fmt:message key="reporting.header.message"/></span>
        <div id="header_right" ><fmt:message key="header.right.welcome"/> <%=((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()%>
            <a class="top-navigation" href="/cdrquery"><fmt:message key="header.cdrquery.module"/></a>
            <a class="top-navigation" href="/admin"><fmt:message key="header.admin.module"/></a>
            <a class="top-navigation" href="/m1-sdp" ><fmt:message key="header.reporting.module"/></a>
            <a class="top-navigation" href="/dashboard" ><fmt:message key="header.dashboard"/></a>
            <a class="top-navigation" href="/admin/change_password.html"><fmt:message key="change_password"/></a>
            <a class="top-navigation" href="#" onclick="logout()"><fmt:message key="logout"/></a>
        </div>
    </div>
    <div id="top_banner" onclick="window.location ='<fmt:message key="url.eapsim.reports"/>'"></div>
</body>
</html>