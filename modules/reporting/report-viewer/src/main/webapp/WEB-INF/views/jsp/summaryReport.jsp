<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.report.template"/></title>
    <jsp:include page="../library.jsp"/>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../resources/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.autocomplete.js"></script>
    <script  language="javascript" type="text/javascript">
        window.onload = setRulesForDateDefault;
//        window.onload = typeMSISDNRuleAlertNeutralize;
        window.onload = typeIMSIRuleAlertNeutralize;
   </script>
    <script type="text/javascript">
        var err1 = "<fmt:message key="content.main.content.alert.invalid.date.future.date"/>";
        var err2 = "<fmt:message key="content.main.content.alert.invalid.date.duration"/>";
        var noOfMonths = "<fmt:message key="content.main.content.no.of.months"/>";
        var err3 = "<fmt:message key="content.main.content.alert.date.range"/>";
        var dateRange = "<fmt:message key="content.main.content.date.range"/>";
        var err4 = "<fmt:message key="content.main.content.alert.invalid.date.range"/>";
        var err5 = "<fmt:message key="content.main.content.alert.invalid.idType.msisdn"/>";
        var err6 = "<fmt:message key="content.main.content.alert.invalid.idType.imsi"/>";
        var err7 = "<fmt:message key="content.main.content.alert.empty.msisdn"/>";
        var err8 = "<fmt:message key="content.main.content.alert.empty.imsi"/>";
    </script>
</head>
<body>


<div id="wrap">  <%--div warp starts--%>

    <div>
        <jsp:include page="../header.jsp"/>
    </div>


    <div id="content"> <%--div content starts--%>
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">  <%--div main_content starts--%>
            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.heading.campaignReport"/></div>
            </div>
        <div id="heading_middle">  <%--div heading_middle starts--%>
                <form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="__stage" >
                    <input type='hidden' name='__report' value="report/transaction_report.rptdesign">

                    <table border="0" align="center" id="tableEvents">

                        <div class="clearfix">
                            <div class="table-cell">
                                <div id="startDateLable" width="150" ><fmt:message key="content.main.content.start.date" /><a class="star"> *</a></div>
                            </div>
                            <div class="table-cell-2">
                                <input type="text" class="inputDate" name="dateFrom" id="datepicker1" readonly="readonly"
                                       onchange="setRulesForDate(document.getElementById('datepicker1').value, document.getElementById('datepicker2').value,'select date')" >
                            </div>

                            <div class="table-cell">
                                <div id="endDateLable" width="150"><fmt:message key="content.main.content.end.date"/><a class="star"> *</a></div>
                            </div>
                            <div class="table-cell-2">
                                <input type="text" class="inputDate" name="dateTo" id="datepicker2" readonly="readonly"
                                       onchange="setRulesForDate(document.getElementById('datepicker1').value, document.getElementById('datepicker2').value,'select date')" >
                            </div>
                            <div class="alert table-cell-3">
                                <div  id="dateAlert2"><fmt:message key="content.main.content.alert.invalid.date.duration"/></div>
                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="table-cell" >
                                <div id="searchBy" width="150"><fmt:message key="content.main.content.search.by"/><a class="star"> *</a></div>
                            </div>
                            <div class="table-cell-2">
                                <select id="searchByList" class="searchByList" name="type" onchange="selectedItem(this);">
                                    <option value="IMSI" ><fmt:message key="content.main.content.search.by.option.IMSI"/></option>
                                    <option value="MSISDN" ><fmt:message key="content.main.content.search.by.option.MSISDN"/></option>
                                </select>
                            </div>

                            <div class="table-cell">
                                <div id="typeIdLabel" width="150"></div>
                            </div>
                            <div class="table-cell-2">
                                <input type="text" class="inputTypeId" name="id" id="typeId" onchange="setValidRulesForIdType(document.getElementById('typeId').value,'enter valid MSISDN !')">
                            </div>
                            <div class="alert table-cell-3">
                                <div  id="typeIdMSISDNAlert"><fmt:message key="content.main.content.alert.invalid.idType.msisdn"/></div>
                                <div  id="typeIdIMSIAlert"><fmt:message key="content.main.content.alert.invalid.idType.imsi"/></div>
                            </div>
                        </div>

                        <div class="clearfix" >

                            <div class="table-cell-btn ">
                                <div id="blankLabel" width="150">&nbsp;</div>
                            </div>
                            <div class="table-cell-btn-2 ">
                                <div id="formButton">
                                    <input id="resetBtn" type="image" src="../images/buttonReset.gif" width="63" height="25"  border="0" alt="reset" onclick="return formRest(this);"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input id="submitBtn" type="image" src="../images/button.gif" width="63" height="25"
                                           border="0" alt="submit" onclick="return formBasedSubmit(this, err1, err2, noOfMonths, err3, dateRange, err4, err5, err6, err7, err8);"/>
                                </div>
                            </div>
                        </div>
                    </table>
                 </form>
                            </div>
            <div id="heading_bottom">
            </div>
        </div>  <%--div heading_middle ends--%>
        </div> <%--div main_content ends--%>
        <div id="footer">
             <jsp:include page="../footer.jsp"/>
        </div>
    </div> <%--div content ends--%>
</div> <%--div warp ends--%>
</body>
</html>
