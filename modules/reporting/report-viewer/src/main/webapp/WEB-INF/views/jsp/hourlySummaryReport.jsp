<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <title><fmt:message key="title.hourly.summary.report"/></title>
    <jsp:include page="../library.jsp"/>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../resources/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.autocomplete.js"></script>
    <script type="text/javascript">

        var err1 = "<fmt:message key="content.main.content.alert.enter.day"/>";
        var err2 = "<fmt:message key="content.main.content.alert.invalid.hour.select"/>";
        var noOfMonths = "<fmt:message key="content.main.content.no.of.months"/>";
        var err3 = "<fmt:message key="content.main.content.alert.invalid.future.date"/>";
        var err4 = "<fmt:message key="content.main.content.alert.date.range.hourly.report"/>";
    </script>
</head>
<div id="wrap">
    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">

            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.heading.hourly.summary"/></div>
            </div>
            <div id="heading_middle">
                <form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onSubmit="return hourlySummary(this,err1,err2,noOfMonths,err3,err4);">
                    <div class="clearfix">
                    <table border="0" align="center" id="tableEvents">

                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/hourly_summary_report.rptdesign"></td>
                            <td><input type='hidden' name='RP_date' id="RP_date"
                                       value=""></td>
                            <td><input type='hidden' name='RP_from_hour' id="RP_from_hour" value="">
                            </td>
                            <td><input type='hidden' name='RP_to_hour' id="RP_to_hour" value="">
                            </td>

                        </tr>


                        <tr>
                            <td class="table-cell-3"><fmt:message key="content.main.content.date"/><a class="star"> *</a>
                            </td>

                            <td class="table-cell-2"><input type="text" name="date" id="datepicker3" class="inputDate">
                            </td>
                            <td id="dateAlert2" class="alert">
                                <fmt:message key="content.main.content.alert.invalid.date"/>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td id="hourAlert2" class="alert">
                                <fmt:message key="content.main.content.alert.select.hour"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td class="table-cell-3"><fmt:message key="content.main.content.from.hour"/><a class="star">
                                *</a>
                            </td>
                            <td class="table-cell-2"><label>
                                <select name="fromHours" id="fromHours"
                                        data-placeholder='<fmt:message key="content.main.content.from.hour"/>'
                                        class="searchByList"
                                        onchange="hourAlertNeutralize()">
                                    <c:forEach items="${hours}" var="hour">
                                        <option value="<c:out value="${hour}"/>">
                                                ${hour}
                                        </option>
                                    </c:forEach>
                                </select>
                            </label></td>
                            <td class="table-cell-3"><fmt:message key="content.main.content.to.hour"/><a class="star">
                                *</a>
                            </td>
                            <td class="table-cell-2"><label>
                                <select name="toHours" id="toHours"
                                        data-placeholder='<fmt:message key="content.main.content.to.hour"/>'
                                        class="searchByList"
                                        onchange="hourAlertNeutralize()">
                                    <c:forEach items="${hours}" var="hour">
                                        <option value="<c:out value="${hour}"/>">
                                                ${hour}
                                        </option>
                                    </c:forEach>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                        </tr>
                    </table>
                    </div>
                </form>
            </div>
            <div id="heading_bottom">
            </div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>