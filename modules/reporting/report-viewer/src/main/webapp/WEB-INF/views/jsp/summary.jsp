<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <title><fmt:message key="title.summary.report"/></title>
    <jsp:include page="../library.jsp"/>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../resources/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.autocomplete.js"></script>
</head>
<div id="wrap">
    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">

            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.heading.summary.report"/></div>
            </div>
            <div id="heading_middle">
                <form:form commandName="form" id="form" name="form" method="GET" action="summary-report-action">
                    <div class="clearfix">
                        <table border="0" align="center" id="tableEvents">
                            <tr >
                                <td class="table-cell">
                                    <label for="fromDate"><fmt:message
                                            key="content.main.content.from.date"/><a class="star">*</a> </label>
                                </td>
                                <td class="table-cell-4">
                                    <form:errors path="fromDate" cssClass="error_message_above_field" element="div"/>
                                    <form:input type="text" class="inputDate" path="fromDate" id="datepicker3"/>
                                </td>
                                <td id="dateAlert2" class="alert">
                                    <fmt:message key="content.main.content.alert.invalid.date"/>
                                </td>
                            </tr>

                            <tr>
                                <td class="table-cell">
                                    <label for="toDate">
                                        <fmt:message key="content.main.content.to.date"/><a class="star"> *</a>
                                    </label>
                                </td>

                                <td class="table-cell-4">
                                    <form:errors path="toDate" cssClass="error_message_above_field" element="div"/>
                                    <form:input type="text" class="inputDate" path="toDate" id="datepicker4"></form:input>
                                </td>
                            </tr>

                            <tr>
                                <td class="table-cell"><fmt:message key="content.main.report.type"/>
                                </td>

                                <td class="table-cell-4">
                                    <form:select path="reportType">
                                        <form:option value="hourly"><fmt:message key="content.main.report.type.hourly"/></form:option>
                                        <form:option value="daily"><fmt:message key="content.main.report.type.daily"/></form:option>
                                    </form:select>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td></td>
                                <td><input type="image" src="../images/button.gif" width="63" height="25"
                                           border="0" alt="submit"/></td>
                            </tr>
                        </table>
                    </div>
                </form:form>
            </div>
            <div id="heading_bottom">
            </div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>