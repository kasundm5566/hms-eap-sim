<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <title><fmt:message key="title.location.based.summary.report"/></title>
    <jsp:include page="../library.jsp"/>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../resources/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../resources/js/jquery.autocomplete.js"></script>
</head>
<div id="wrap">
    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">

            <div id="heading_top">
                <div class="heading_name"><fmt:message key="${pageData.getHeading()}"/></div>
            </div>
            <div id="heading_middle">
                <div class="heading_details">
                    ${pageData.getFromDate()} to  ${pageData.getToDate()}
                </div>

                <table class="report_table">
                        <tr>
                            <td class="report_th">Date</td>
                            <td class="report_th">File Path</td>
                        </tr>
                        <c:forEach items="${pageData.getDownloadLinkTableList()}" var="item">
                            <tr class="report_tr">
                                <td class="report_td">${item.getDate()}</td>
                                <td class="report_td">
                                    <c:if test="${item.isReportAvailable() == true}">
                                        <a class="report_link_valid" href="${item.getDownloadLink()}">Download Link</a>
                                    </c:if>
                                    <c:if test="${item.isReportAvailable() == false}">
                                        <div class="report_link_invaild">Report not available</div>
                                    </c:if>
                                <td>
                            </tr>
                        </c:forEach>
                        <c:if test="${pageData.getPaginationNumbers() == null || pageData.getPaginationNumbers().isEmpty()}">
                            <tr>
                                <td colspan="2">
                                    <div class="center report_link_invaild">
                                        <fmt:message key="content.main.content.empty.list.message"/>
                                    </div>
                                </td>
                            </tr>
                         </c:if>
                </table>
                <div class="center">
                    <div class="pagination">
                        <c:if test="${pageData.getPaginationNumbers() != null && !pageData.getPaginationNumbers().isEmpty()}">
                            <a style="margin-right: 20px;" href="${pageData.getReportUrl()}?page=1&fromDate=${pageData.getFromDate()}&toDate=${pageData.getToDate()}&reportType=${pageData.getReportType()}">&laquo;</a>
                            <c:forEach items="${pageData.getPaginationNumbers()}" var="page">
                                <a <c:if test="${page eq pageData.getPage()}">class="active"</c:if> href="${pageData.getReportUrl()}?page=${page}&fromDate=${pageData.getFromDate()}&toDate=${pageData.getToDate()}&reportType=${pageData.getReportType()}">${page}</a>
                            </c:forEach>
                            <a style="margin-left: 20px;" href="${pageData.getReportUrl()}?page=${pageData.getPaginationNumbers().get(pageData.getPaginationNumbers().size()-1)}&fromDate=${pageData.getFromDate()}&toDate=${pageData.getToDate()}&reportType=${pageData.getReportType()}">&raquo;</a>
                        </c:if>
                    </div>
                </div>

            </div>
            <div id="heading_bottom">
            </div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>