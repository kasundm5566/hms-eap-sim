$(function() {
    var stDay=new Date();
    stDay.setMonth( stDay.getMonth( ));
    $("#datepicker1").datepicker({showOtherMonths: true, selectOtherMonths: true});
    $("#datepicker1").datepicker("option", "dateFormat", "yy-mm-dd");
});


$(function() {
    $("#datepicker2").datepicker({showOtherMonths: true, selectOtherMonths: true});
    $("#datepicker2").datepicker("option", "dateFormat", "yy-mm-dd");
});

function setRulesForDate(startDate, endDate, defaultCode) {

    document.getElementById("dateAlert").style.visibility = "hidden";
    document.getElementById("dateAlert2").style.visibility = "hidden";

    //alert(startDate +">>>"+ endDate);

    if (startDate == "" || endDate == "" ) {
        $('#campaignRuleList').html("<option value=\"\">" + defaultCode + "</option>");
        return;
    }

}

function setValidRulesForIdType(idType, defaultCode){

    document.getElementById("typeIdMSISDNAlert").style.visibility = "hidden";
    document.getElementById("typeIdIMSIAlert").style.visibility = "hidden";
}


function setRulesForDateDefault(){
    var toDay=new Date();
    var stDay=new Date();
    stDay.setMonth( stDay.getMonth( ));
    var d1 = $.datepicker.formatDate('dd/mm/yy',toDay);
    var d2 = $.datepicker.formatDate('dd/mm/yy',stDay);

    $('#datepicker1').val(d2);
    $('#datepicker2').val(d1);
    document.getElementById("typeIdLabel").style.visibility = "hidden";
    $('#typeId').val('');
    document.getElementById("typeId").style.visibility = "hidden";
}

function selectedItem(el) {
    document.getElementById("typeId").value = "";
    window["type"+el.options[el.selectedIndex].value+"RuleAlertNeutralize"]();
}

function typeIMSIRuleAlertNeutralize(){

    document.getElementById("typeIdLabel").style.visibility = "visible";
    $("#tempary").remove();
    $("#typeIdLabel").append("<div id='tempary'>IMSI<a class='star'> *</a></div>");
    document.getElementById("typeId").style.visibility = "visible";
    document.getElementById("typeIdMSISDNAlert").style.visibility = "hidden"
    document.getElementById("typeIdIMSIAlert").style.visibility = "hidden";

}

function typeMSISDNRuleAlertNeutralize(){

    document.getElementById("typeIdLabel").style.visibility = "visible";
    $("#tempary").remove();
    $("#typeIdLabel").append("<div id='tempary'>MSISDN<a class='star'> *</a></div>");
    document.getElementById("typeId").style.visibility = "visible";
    document.getElementById("typeIdMSISDNAlert").style.visibility = "hidden"
    document.getElementById("typeIdIMSIAlert").style.visibility = "hidden";

}

function typeRuleAlertNeutralize(){

    document.getElementById("typeIdLabel").style.visibility = "hidden";
    $('#typeId').val('');
    document.getElementById("typeId").style.visibility = "hidden";
    document.getElementById("typeIdMSISDNAlert").style.visibility = "hidden";
    document.getElementById("typeIdIMSIAlert").style.visibility = "hidden";
}

function formRest() {
        window.location.reload();
        return false;
}

function formBasedSubmit(current_form, err1, err2, noOfMonths, err3, dateRange, err4, err5, err6, err7, err8){


    var datepicker1 =  $("#datepicker1").datepicker("getDate");
    var sDat = new Date(datepicker1);
    var datepicker2 =  $("#datepicker2").datepicker("getDate");
    var eDat = new Date(datepicker2);
    var today = new Date();
    var pastDate = new Date();
    pastDate.setMonth(pastDate.getMonth()-noOfMonths)
    var monthMin = eDat.getMonth() - noOfMonths;

    if (eDat < sDat) {
        document.getElementById("dateAlert2").innerHTML = err2;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    }else if(eDat >= today) {
        document.getElementById("dateAlert2").innerHTML = err1;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    }else if(sDat < pastDate){
        document.getElementById("dateAlert2").innerHTML = err3;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    } else if((eDat-sDat)>dateRange){
        document.getElementById("dateAlert2").innerHTML = err4;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    }
    else
    {
        document.getElementById("dateAlert2").style.visibility = "hidden";
    }

    var inputVal = $('#typeId').val();
    var ccMSISDNReg = /^65[0-9]{8}$/;
    var ccIMSIReg = /^[0-9]{15}$/;

    if($("#searchByList").val()=='MSISDN')
    {
        if(inputVal==''){
            document.getElementById("typeIdMSISDNAlert").innerHTML = err7;
            document.getElementById("typeIdIMSIAlert").style.visibility = "hidden";
            document.getElementById("typeIdMSISDNAlert").style.visibility = "visible";
            return false;
        }
        else if(!ccMSISDNReg.test(inputVal)) {
            document.getElementById("typeIdMSISDNAlert").innerHTML = err5;
            document.getElementById("typeIdIMSIAlert").style.visibility = "hidden";
            document.getElementById("typeIdMSISDNAlert").style.visibility = "visible";
            return false;
        }
    }

    else if($("#searchByList").val()=='IMSI')
    {
        if(inputVal==''){
            document.getElementById("typeIdIMSIAlert").innerHTML = err8;
            document.getElementById("typeIdMSISDNAlert").style.visibility = "hidden"
            document.getElementById("typeIdIMSIAlert").style.visibility = "visible";
            return false;
        }
        else if(!ccIMSIReg.test(inputVal)) {
            document.getElementById("typeIdIMSIAlert").innerHTML = err6;
            document.getElementById("typeIdMSISDNAlert").style.visibility = "hidden"
            document.getElementById("typeIdIMSIAlert").style.visibility = "visible";
            return false;
        }
    }
    return true;

}


function hourlySummary(current_form, err1,err2,noOfMonths,err3,err4) {
    var date = current_form["date"].value;
    var fromHour = current_form["fromHours"].value;
    var toHour = current_form["toHours"].value;
    var datepicker1 =  $("#datepicker3").datepicker("getDate");
    var sDat = new Date(datepicker1);
    var pastDate = new Date();
    var today = new Date();
    pastDate.setMonth(pastDate.getMonth()-noOfMonths)
    if(date ==  "") {
        document.getElementById("dateAlert2").innerHTML = err1;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    } else if (sDat < pastDate) {
        document.getElementById("dateAlert2").innerHTML = err4;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    } else if(sDat >= today) {
        document.getElementById("dateAlert2").innerHTML = err3;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    } else {
        document.getElementById("dateAlert2").style.visibility = "hidden";

        if(fromHour > toHour) {
            document.getElementById("hourAlert2").innerHTML = err2;
            document.getElementById("hourAlert2").style.visibility = "visible";
            return false;
        }
        
        document.getElementById("hourAlert2").style.visibility = "hidden";
        document.getElementById('RP_date').value = date;
        document.getElementById('RP_from_hour').value = fromHour;
        document.getElementById('RP_to_hour').value = toHour;
        return true;
    }
}

function datePickAlertNeutralize() {
    document.getElementById("dateAlert").style.visibility = "hidden";
}

function hourAlertNeutralize() {
    document.getElementById("hourAlert2").style.visibility = "hidden";
}

function dailySummary(current_form,err1,err2,noOfMonths,err3,dateRange,err4) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;


    var datepicker1 =  $("#datepicker3").datepicker("getDate");
    var sDat = new Date(datepicker1);
    var datepicker2 =  $("#datepicker4").datepicker("getDate");
    var eDat = new Date(datepicker2);
    var today = new Date();
    var pastDate = new Date();
    pastDate.setMonth(pastDate.getMonth()-noOfMonths)
    var monthMin = eDat.getMonth() - noOfMonths;

    if (eDat < sDat) {
        document.getElementById("dateAlert2").innerHTML = err2;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    }else if(eDat >= today) {
        document.getElementById("dateAlert2").innerHTML = err1;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    }else if(sDat < pastDate){
        document.getElementById("dateAlert2").innerHTML = err3;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    } else if((eDat-sDat)>dateRange){
        document.getElementById("dateAlert2").innerHTML = err4;
        document.getElementById("dateAlert2").style.visibility = "visible";
        return false;
    }
    else
    {
        document.getElementById("dateAlert2").style.visibility = "hidden";
        document.getElementById('RP_from_date').value = fromDateString;
        document.getElementById('RP_to_date').value = toDateString;
        return true;
    }

    // if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
    //     var fromDateArray = fromDateString.split("-");
    //     var toDateArray = toDateString.split("-");
    //     var fromDate = new Date();
    //     var toDate = new Date();
    //     fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
    //     toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
    //     if (toDate < fromDate) {
    //         showDateError(err4);
    //         return false;
    //     }
    //     else if (toDate - fromDate > 2592000000) {
    //         showDateError(err5);
    //         return false;
    //     }
    //     document.getElementById('RP_sso_from_date').value = fromDateString;
    //     document.getElementById('RP_sso_to_date').value = toDateString;
    //
    //     return true;
    // }

}

$(function (){
    var initVal = document.getElementById('datepicker3').getAttribute("value");
    $("#datepicker3").datepicker({showOtherMonths: true, selectOtherMonths: true});
    $("#datepicker3").datepicker("option", "dateFormat", "yy-mm-dd");
    $("#datepicker3").datepicker('setDate', initVal);
});

$(function () {
    var initVal = document.getElementById('datepicker4').getAttribute("value");
    $("#datepicker4").datepicker({showOtherMonths: true, selectOtherMonths: true});
    $("#datepicker4").datepicker("option", "dateFormat", "yy-mm-dd");
    $("#datepicker4").datepicker('setDate', initVal);
});