/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.controller;

import hms.eapsim.report.domain.ReportListPageData;
import hms.eapsim.report.domain.SummaryForm;
import hms.eapsim.report.service.ReportListPageDataGenerator;
import hms.eapsim.report.service.ReportType;
import hms.eapsim.report.validator.SummaryFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Controller
public class SummaryController {

    private static final String SUMMARY_REPORT_VIEW = "/jsp/summary";
    private static final String DOWNLOAD_REPORT_VIEW = "/jsp/downloadLinkList";
    private static final String SUMMARY_REPORT_TITLE_PORPERTY = "title.summary.report";

    @Autowired
    SummaryFormValidator summaryValidator;
    @Autowired
    ReportListPageDataGenerator reportListPageDataGenerator;

    @RequestMapping(value = "/summary-report", method = RequestMethod.GET)
    public String summaryReport(Map<String, Object> map) {
        map.put("form", new SummaryForm());
        return SUMMARY_REPORT_VIEW;
    }

    @RequestMapping(value = "/summary-report-action", method = RequestMethod.GET)
    public ModelAndView summaryReportAction(@ModelAttribute("form") SummaryForm form, BindingResult result,
                                            HttpServletRequest request) {

        summaryValidator.validate(form, result);
        if (result.hasErrors()) {
            return new ModelAndView(SUMMARY_REPORT_VIEW, "form" , form);
        } else {
            ModelAndView view = new ModelAndView(DOWNLOAD_REPORT_VIEW);
            Date fromDate = summaryValidator.getValidatedDate(form.getFromDate());
            Date toDate = summaryValidator.getValidatedDate(form.getToDate());
            ReportType reportType;
            ReportListPageData pageData;

            if (form.getReportType().equals("hourly")) {
                reportType = ReportType.SUMMARY_HOURLY;
            } else {
                reportType = ReportType.SUMMARY_DAILY;
            }

            pageData = reportListPageDataGenerator.generateData(
                    form,request,fromDate,toDate,reportType,
                    "summary-report-action",
                    SUMMARY_REPORT_TITLE_PORPERTY
            );
            view.addObject("pageData", pageData);
            return view;
        }

    }
}
