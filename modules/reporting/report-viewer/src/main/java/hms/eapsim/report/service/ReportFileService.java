/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.service;

import hms.eapsim.report.controller.DownloadController;
import hms.eapsim.report.domain.ReportTableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ReportFileService {

    private static final Logger logger = LoggerFactory.getLogger(ReportFileService.class);
    private static final String TABLE_DATE_PATTERN = "yyyy-MM-dd";
    private static final String DOWNLOAD_LINK_FORMAT = "download/%s/%s";

    private String reportDirectory;
    private String directoryDateFormat;
    private String hourlySummaryReportFileName;
    private String dailySummaryReportFileName;
    private String locationBasedHourlySummaryReportFileName;
    private String locationBasedDailySummaryReportFileName;
    private String fileExtension;
    private String directoryPrefix;
    private boolean showUnavailableReports;

    public FileWriteStatus writeResponseData(HttpServletResponse response , String date , String file) {
        FileWriteStatus returnVal;

        try (FileInputStream downloadFileStream = getFileData(date, file)){
            if (downloadFileStream == null) {
                logger.info("File not found - {}" , date + "_" +file );
                return FileWriteStatus.RESOURCE_NOT_FOUND;
            }
            try ( OutputStream responseStream = new BufferedOutputStream(response.getOutputStream());
                  BufferedInputStream inputStream = new BufferedInputStream(downloadFileStream);
                  BufferedOutputStream outputStream = new BufferedOutputStream(responseStream)) {

                int numberOfBytes;
                byte[] buffer = new byte[1024 * 8];
                while ((numberOfBytes = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, numberOfBytes);
                }
                outputStream.flush();
                returnVal = FileWriteStatus.SUCCESS;
            }
        } catch (IOException ex){
            logger.error("Error occured during file reading - {}", ex.getMessage());
            returnVal = FileWriteStatus.INTERNAL_SERVER_ERROR;
        }
        return returnVal;
    }

    private FileInputStream getFileData(String date, String type) throws IOException {
        ReportType reportType = ReportType.getFromShortCode(type);
        String filePath;
        Date reportDate;
        SimpleDateFormat formatter = new SimpleDateFormat(TABLE_DATE_PATTERN);

        if (reportType == ReportType.NONE) {
            return null;
        }
        try {
            reportDate =  formatter.parse(date);
        } catch (ParseException ex) {
            return null;
        }
        filePath = getFullFilePath(reportDate,getReportFileName(reportType));
        if (!(new File(filePath.toString()).exists())){
            return null;
        }
        return new FileInputStream(filePath);
    }

    public List<ReportTableItem> getReportList(Date startDate , Date endDate , ReportType reportType) {
        List<ReportTableItem> reportList = new ArrayList<>();
        Date tempDate = new Date(startDate.getTime());
        SimpleDateFormat formatter = new SimpleDateFormat(TABLE_DATE_PATTERN);
        String reportFileName = getReportFileName(reportType);
        Calendar calendar = Calendar.getInstance();

        if (startDate.after(endDate)) {
            return reportList;
        }
        calendar.setTime(tempDate);

        while (!tempDate.after(endDate)) {
            ReportTableItem item = new ReportTableItem();
            String dateString = formatter.format(tempDate);
            String filePath = getFullFilePath(tempDate,reportFileName);

            if (new File(filePath.toString()).exists()) {
                item.setReportAvailable(true);
                item.setDate(dateString);
                item.setDownloadLink(String.format(DOWNLOAD_LINK_FORMAT,dateString,reportType.getShortCode()));
                reportList.add(item);
            } else {
                if (showUnavailableReports) {
                    item.setDate(dateString);
                    item.setReportAvailable(false);
                    reportList.add(item);
                }
            }
            calendar.add(Calendar.DATE, 1);
            tempDate = calendar.getTime();
        }
        return reportList;
    }

    private String getFullFilePath(Date date, String reportFileName) {
        StringBuilder filePath = new StringBuilder();

        filePath.append(getDirectoryPath(date))
                .append(reportFileName);
        return filePath.toString();
    }

    private String getDirectoryPath(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(directoryDateFormat);

        StringBuilder dirPath = new StringBuilder()
                .append(reportDirectory)
                .append(directoryPrefix)
                .append(formatter.format(date))
                .append(File.separator);
        return dirPath.toString();
    }

    private String getReportFileName(ReportType reportType) {
        StringBuilder fileName = new StringBuilder();

        switch (reportType){
            case LOCATION_BASED_DAILY:
                fileName.append(locationBasedDailySummaryReportFileName);
                break;
            case LOCATION_BASED_HOURLY:
                fileName.append(locationBasedHourlySummaryReportFileName);
                break;
            case SUMMARY_DAILY:
                fileName.append(dailySummaryReportFileName);
                break;
            case SUMMARY_HOURLY:
                fileName.append(hourlySummaryReportFileName);
                break;
        }
        fileName.append(fileExtension);
        return fileName.toString();
    }

    public String getDirectoryPrefix() {
        return directoryPrefix;
    }

    public void setDirectoryPrefix(String directoryPrefix) {
        this.directoryPrefix = directoryPrefix;
    }

    public String getReportDirectory() {
        return reportDirectory;
    }

    public void setReportDirectory(String reportDirectory) {
        if(!reportDirectory.endsWith(File.separator)){
            this.reportDirectory = reportDirectory + File.separator;
        }else {
            this.reportDirectory = reportDirectory;
        }
    }

    public String getDirectoryDateFormat() {
        return directoryDateFormat;
    }

    public void setDirectoryDateFormat(String directoryDateFormat) {
        this.directoryDateFormat = directoryDateFormat;
    }

    public String getHourlySummaryReportFileName() {
        return hourlySummaryReportFileName;
    }

    public void setHourlySummaryReportFileName(String hourlySummaryReportFileName) {
        this.hourlySummaryReportFileName = hourlySummaryReportFileName;
    }

    public String getDailySummaryReportFileName() {
        return dailySummaryReportFileName;
    }

    public void setDailySummaryReportFileName(String dailySummaryReportFileName) {
        this.dailySummaryReportFileName = dailySummaryReportFileName;
    }

    public String getLocationBasedHourlySummaryReportFileName() {
        return locationBasedHourlySummaryReportFileName;
    }

    public void setLocationBasedHourlySummaryReportFileName(String locationBasedHourlySummaryReportFileName) {
        this.locationBasedHourlySummaryReportFileName = locationBasedHourlySummaryReportFileName;
    }

    public String getLocationBasedDailySummaryReportFileName() {
        return locationBasedDailySummaryReportFileName;
    }

    public void setLocationBasedDailySummaryReportFileName(String locationBasedDailySummaryReportFileName) {
        this.locationBasedDailySummaryReportFileName = locationBasedDailySummaryReportFileName;
    }

    public boolean isShowUnavailableReports() {
        return showUnavailableReports;
    }

    public void setShowUnavailableReports(boolean showUnavailableReports) {
        this.showUnavailableReports = showUnavailableReports;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }
}
