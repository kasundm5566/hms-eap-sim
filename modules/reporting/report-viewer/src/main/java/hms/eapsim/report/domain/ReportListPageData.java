/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.domain;

import java.util.List;

public class ReportListPageData {

    private String heading;
    private String fromDate;
    private String toDate;
    private List<ReportTableItem> downloadLinkTableList;
    private String reportUrl;
    private String reportType;
    private  List<Integer> paginationNumbers;
    private int page;

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Integer> getPaginationNumbers() {
        return paginationNumbers;
    }

    public void setPaginationNumbers(List<Integer> paginationNumbers) {
        this.paginationNumbers = paginationNumbers;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<ReportTableItem> getDownloadLinkTableList() {
        return downloadLinkTableList;
    }

    public void setDownloadLinkTableList(List<ReportTableItem> downloadLinkTableList) {
        this.downloadLinkTableList = downloadLinkTableList;
    }
}
