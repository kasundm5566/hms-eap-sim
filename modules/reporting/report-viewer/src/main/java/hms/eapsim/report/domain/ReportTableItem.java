/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.domain;

public class ReportTableItem {

    private String date;
    private boolean isReportAvailable;
    private String downloadLink;

    public boolean isReportAvailable() {
        return isReportAvailable;
    }

    public void setReportAvailable(boolean reportAvailable) {
        isReportAvailable = reportAvailable;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportTableItem{");
        sb.append("date='").append(date).append('\'');
        sb.append(", isReportAvailable=").append(isReportAvailable);
        sb.append(", downloadLink='").append(downloadLink).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
