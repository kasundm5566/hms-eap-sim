package hms.eapsim.report.listener;

import hms.commons.SnmpLogUtil;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationStatusEventListener implements ServletContextListener {
    private static String startTrap;
    private static String stopTrap;

    public void setStartTrap(String startTrap) {
        this.startTrap = startTrap;
    }

    public void setStopTrap(String stopTrap) {
        this.stopTrap = stopTrap;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SnmpLogUtil.log(startTrap != null ? startTrap : "2051.390.1.2|1|reporting-module started");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        SnmpLogUtil.log(stopTrap != null ? stopTrap : "2051.390.1.1|1|reporting-module is down");
    }

}
