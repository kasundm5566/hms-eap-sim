/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.validator;

import hms.eapsim.report.domain.SummaryForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SummaryFormValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(SummaryFormValidator.class);
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String START_DATE_FORMAT_ERROR_PROPERTY = "alert.start.invalid.date.format";
    private static final String END_DATE_FORMAT_ERROR_PROPERTY = "alert.end.invalid.date.format";
    private static final String END_DATE_INVALID_PROPERTY = "alert.end.invalid.date";
    private static final String START_DATE_INVALID_PROPERTY = "alert.start.invalid.date";
    private static final String START_DATE_LARGE_THAN_END_DATE_PROPERTY = "alert.start.and.end.date.invalid.error";
    private static final String START_DATE_IS_FUTURE_DATE_PROPERTY = "alert.start.invalid.date.after";
    private static final String END_DATE_IS_FUTURE_DATE_PROPERTY = "alert.end.invalid.date.after";

    private int maxDurationInMonths;

    @Override
    public boolean supports(Class<?> aClass) {
        return SummaryForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        SummaryForm form = (SummaryForm) object;
        String startDate = form.getFromDate();
        String endDate = form.getToDate();
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        Date start;
        Date end;
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, -1 * maxDurationInMonths);
        Date cornerDate = calendar.getTime();

        try {
            start = formatter.parse(startDate);
        } catch (ParseException ex) {
            errors.rejectValue("fromDate", START_DATE_FORMAT_ERROR_PROPERTY);
            return;
        }
        try {
            end = formatter.parse(endDate);
        } catch (ParseException ex) {
            errors.rejectValue("toDate", END_DATE_FORMAT_ERROR_PROPERTY);
            return;
        }

        validateStartDate(start, cornerDate, errors);
        validateEndDate(start, end, cornerDate, errors);
        validateReportType(form.getReportType(), errors);

    }

    private void validateStartDate(Date start, Date cornerDate, Errors errors) {
        if (start.before(cornerDate)) {
            errors.rejectValue("fromDate", START_DATE_INVALID_PROPERTY, new String[]{String.valueOf(maxDurationInMonths)}, null);
        }
        if (start.after(new Date())) {
            errors.rejectValue("fromDate", START_DATE_IS_FUTURE_DATE_PROPERTY, new String[]{String.valueOf(maxDurationInMonths)}, null);
        }
    }

    private void validateReportType(String reportType , Errors errors) {
        if (isEmpty(reportType)) {
            errors.rejectValue("reportType", "");
        } else {
            if (!(reportType.equals("hourly") || reportType.equals("daily"))) {
                errors.rejectValue("reportType", "");
            }
        }
    }

    private void validateEndDate(Date start, Date end, Date cornerDate, Errors errors) {
        if (end.before(cornerDate)) {
            errors.rejectValue("toDate", END_DATE_INVALID_PROPERTY);
        }
        if (end.after(new Date())) {
            errors.rejectValue("toDate", END_DATE_IS_FUTURE_DATE_PROPERTY);
        }
        if (end.before(start)) {
            errors.rejectValue("toDate", START_DATE_LARGE_THAN_END_DATE_PROPERTY);
        }
    }

    public Date getValidatedDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        try {
            return formatter.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public boolean isEmpty(String value) {
        return !(value != null && !value.equals(""));
    }

    public int getMaxDurationInMonths() {
        return maxDurationInMonths;
    }

    public void setMaxDurationInMonths(int maxDurationInMonths) {
        this.maxDurationInMonths = maxDurationInMonths;
    }

}