package hms.eapsim.report.controller;

import org.apache.cxf.common.i18n.Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.SimpleFormatter;

@Controller
@RequestMapping("/report")
@SessionAttributes("indexForm")
public class ReportPageController {

    private static final Logger logger = LoggerFactory.getLogger(ReportPageController.class);

    private static final String view = "/jsp/summaryReport";

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView initWelcome(Model model, HttpServletRequest request) throws Exception {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        logger.info("User [{}] has requested Report index page", userName);
        request.getSession().setAttribute("userName", userName);

        return new ModelAndView(view, "model", model);
    }

}
