/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.controller;

import hms.eapsim.report.service.FileWriteStatus;
import hms.eapsim.report.service.ReportFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
public class DownloadController {

    private static final Logger logger = LoggerFactory.getLogger(DownloadController.class);
    private static final String DOWNLOAD_FILE_NAME_EXTENSION = ".csv";
    private static final String CSV_CONTENT_TYPE = "application/csv";
    private static final String EAPSIM_REPORTING_USER_ROLE = "ROLE_EAPSIM_REPORTING";

    @Autowired
    ReportFileService reportFileService;

    @RequestMapping(value = "/download/{date}/{file}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable("date") String date, @PathVariable("file") String file,
                             HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String filename;
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();

        if (!request.isUserInRole(EAPSIM_REPORTING_USER_ROLE)) {
            response.sendRedirect(request.getContextPath() + "/spring/403");
            logger.info("Use {} access denied", userName);
            return;
        }

        filename = date +"_" + file + DOWNLOAD_FILE_NAME_EXTENSION;
        response.setContentType(CSV_CONTENT_TYPE);
        response.setHeader( "Content-Disposition", "attachment;filename=" + filename );
        FileWriteStatus result = reportFileService.writeResponseData(response,date,file);

        if (result == FileWriteStatus.RESOURCE_NOT_FOUND) {
            response.sendRedirect(request.getContextPath() + "/spring/404");
            return;
        } else if (result == FileWriteStatus.INTERNAL_SERVER_ERROR) {
            response.sendRedirect(request.getContextPath() + "/spring/500");
            return;
        } else if (result == FileWriteStatus.SUCCESS){
             logger.info("User [{}] downloaded report {}", new Object[]{userName,filename});
        }
    }
}