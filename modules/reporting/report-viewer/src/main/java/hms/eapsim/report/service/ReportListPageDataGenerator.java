/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.service;

import hms.eapsim.report.domain.ReportListPageData;
import hms.eapsim.report.domain.ReportTableItem;
import hms.eapsim.report.domain.SummaryForm;
import hms.eapsim.report.util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ReportListPageDataGenerator {

    @Autowired
    ReportFileService reportFileService;
    @Autowired
    Pagination pagination;

    public ReportListPageData generateData(SummaryForm form,  HttpServletRequest request,
                                           Date fromDate , Date toDate , ReportType reportType ,
                                           String reportUrl , String reportTitle) {
        Map<String, String[]> parameters = request.getParameterMap();
        List<ReportTableItem> tableData;
        ReportListPageData pageData;

        int page = 1;
        if (parameters.containsKey("page")) {
            try {
                page = (int) Integer.parseInt(request.getParameter("page"));
                if (page < 1) {
                    page = 1;
                }
            } catch (Exception ex) {
            }
        }

        tableData = reportFileService.getReportList(fromDate, toDate, reportType);

        pageData = new ReportListPageData();
        pageData.setDownloadLinkTableList(pagination.refineList(tableData, page));
        pageData.setPaginationNumbers(pagination.getPaginationNumbers(tableData.size(), page));
        pageData.setReportUrl(reportUrl);
        pageData.setFromDate(form.getFromDate());
        pageData.setToDate(form.getToDate());
        pageData.setHeading(reportTitle);
        pageData.setReportType(form.getReportType());
        pageData.setPage(page);

        return  pageData;
    }

}
