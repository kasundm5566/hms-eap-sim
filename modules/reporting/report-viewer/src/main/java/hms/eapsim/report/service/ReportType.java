/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.service;

public enum ReportType {

    LOCATION_BASED_HOURLY("location_hourly"),
    LOCATION_BASED_DAILY("location_daily"),
    SUMMARY_HOURLY("hourly"),
    SUMMARY_DAILY("daily"),
    NONE("none");

    private final String shortCode;

    ReportType(String shortCode){
        this.shortCode = shortCode;
    }

    public String getShortCode(){
        return shortCode;
    }

    public static ReportType getFromShortCode(String shortCode) {
        ReportType reportType = NONE;

        for (ReportType s : ReportType.values()) {
            if (s.getShortCode().equals(shortCode)) {
                reportType =  s;
            }
        }
        return  reportType;
    }
}
