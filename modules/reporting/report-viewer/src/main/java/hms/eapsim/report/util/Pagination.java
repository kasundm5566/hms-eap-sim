/*
 * (C) Copyright 2010-2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.eapsim.report.util;

import hms.eapsim.report.domain.ReportTableItem;

import java.util.ArrayList;
import java.util.List;

public class Pagination {

    private int itemsPerPage;

    public List<ReportTableItem> refineList(List<ReportTableItem> tableList ,int page) {
        int itemsToRemove = itemsPerPage * (page - 1);

        if (itemsToRemove >= tableList.size()) {
            return new ArrayList<ReportTableItem>();
        } else {
            if ((itemsToRemove + itemsPerPage) > tableList.size() ) {
                 return tableList.subList(itemsToRemove , tableList.size());
            } else {
                return tableList.subList(itemsToRemove , itemsToRemove + itemsPerPage);
            }
        }
    }

    public List<Integer> getPaginationNumbers(int count , int page) {
        int maxPaginationNumbers = 7;
        List<Integer> paginationNumbers = new ArrayList<>();
        int numberOfPages = (count / itemsPerPage) + 1;

        if ((count % itemsPerPage)== 0 ) {
            numberOfPages--;
        }
        if (numberOfPages > maxPaginationNumbers) {
            if(page < 4){
                for (int i = 0; i < maxPaginationNumbers; i++) {
                    paginationNumbers.add(i + 1);
                }
            }
            else if (numberOfPages - page < 4) {
                for (int i = numberOfPages - maxPaginationNumbers; i < numberOfPages; i++) {
                    paginationNumbers.add(i + 1);
                }
            }
            else {
                for (int i = page - 3; i < page + 4; i++) {
                        paginationNumbers.add(i);
                }
            }
        } else {
            for (int i = 0; i < numberOfPages; i++) {
                paginationNumbers.add(i + 1);
            }
        }
        return paginationNumbers;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
}