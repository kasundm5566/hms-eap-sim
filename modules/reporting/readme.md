#   Reporting module for Eap-Sim/Aka Authentication Server

1. Reporting Build System (BIRT version - 4_3_1).

1.1
    1) Birt Runtime Viewer setup
        * Copy the birt-runtime-4_3_1.zip (Available at eap-sim/docs/birt-run-time/birt-runtime-4_3_1.zip)
        * Extract the birt-runtime-4_3_1.zip file.
        * Set the "<BIRT_VIEWER_HOME>" path in the pom.xml.

    2) Log files setup for project
        * Set the "<EAPSIM_LOG_ROOT>" path in the pom.xml .

    3) Setup the base_url,cas database url in 'reporting/viewer/src/main/resources/report-viewer.properties'.
    eg:
        cas.base.url=http://dev.hsenidmobile.com:8080
        cas.db.url=jdbc:mysql://127.0.0.1:3306/common_admin?autoReconnect=true

    4) Setup the cas database's db_name, db_username, db_password in pom.xml
    eg:
             db_name :         <CAS_DB_SCHEMA>common_admin</CAS_DB_SCHEMA>
             db_username :     <CAS_DB_USER>root</CAS_DB_USER>
             db_password :     <CAS_DB_PASSWORD></CAS_DB_PASSWORD>

    5) Setup the baseURL in 'reporting/viewer/src/main/webapp/WEB_INF/report-viewer.properties'
    eg:
        base_url=http://dev.hsenidmobile.com:8080

    6) Add `ROLE_EAPSIM_REPORTING` role in existing cas databases.
       Run the "report_role.sql" script file in "db-setup/report_role.sql"
    e.g:
        source report_role.sql

    7) update reporting/report-viewer/src/main/webapp/META-INF/context.xml with your db settings

    8) Setup log level for Report Engine in 'reporting/viewer/src/main/webapp/WEB_INF/web.xml'
    log levels in descending order are: [ OFF (highest value), SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST, ALL (lowest value) ]
    e.g:      OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST and ALL
           ...............
                     <!-- Report engine log level -->
                     <context-param>
                        <param-name>BIRT_VIEWER_LOG_LEVEL</param-name>
                       <param-value>FINE</param-value>
                     </context-param>
           ...............

1.2 Configure and Deploy application on Tomcat

    1) Run `mvn clean install` from ${app.home}/report-viewer
    2) Deploy the ${app.home}/report-viewer/target/radius-reporting.war to $CATALINA_HOME/webapps/
    3) Start the tomcat server
    4) Go to web browser and type "http://dev.hsenidmobile.com:8080/radius-reporting" to view the application

Assumptions:
i. Mysql database should be available with initial data.
ii.CAS module should be deployed.
