# ################### Configurations for trans logs ####################

# ========= general transaction manager properties ==================

# ----- transaction manager's log ----------
log_path = '/home/lmpeiris/Desktop/implementation/radius/db_writer/logs/transaction-manager.log'
log_roll_over_time='midnight'

# ------ mysql connection ---------------
mysql_host = '127.0.0.1'
mysql_port = 3306
mysql_user = 'ajuba'
mysql_password = 'ajuba'
mysql_database = 'eapsim'

# ----- snmp ----------------------------
snmp_file_location = '/hms/logs/ajuba/test.sh'
# snmp traps
snmp_sql_down='2021.420.1.2|1|Mysql connection is down'
snmp_internal_error='2021.420.2.1|1|Internal error occurred in'

# =========== transaction data inserting =======================
# path to directory that contain trans logs
trans_log_directory = '/home/lmpeiris/Desktop/implementation/radius/db_writer/source'
# path to directory that will hold completed trans logs
completed_trans_log_directory = '/home/lmpeiris/Desktop/implementation/radius/db_writer/completed'
transaction_file_name_pattern = 'trans.log.*'
file_line_field_terminator = '|'
transaction_column_list='response_time,ip,mac_address,nas_id,server_id,correlation_id,protocol,flow_type,request_type,response_type,imsi,msisdn,status,description,created'
# ============ transaction data summarization ===================

# amount of historic records to consider
hourly_summary_history_in_hours = 980
daily_summary_history_in_days = 50

# group by columns used for summarisation, except the time field
summary_group_by = "flow_type,protocol,nas_id"

# columns which values are inserted in to in summary table
summary_insert_fields = "date_time,nas_id,protocol,flow_type,sim_count,aka_count,fulauth_count,reauth_count,pseudo_full_auth_count,encrypted_full_auth_count,success_count,failed_count,total_count"

# columns mapping from trans log table to hourly summary table. Note: 'created' column not mentioned at the front
summary_transaction_column_mapping = "nas_id,protocol,flow_type,sum(protocol = 'SIM') as sim_count, sum(protocol = 'AKA') as aka_count, sum(flow_type = 'FULL_AUTHENTICATION') as fulauth_count, sum(flow_type = 'RE_AUTHENTICATION') as reauth_count, sum(flow_type = 'PSEUDO_FULL_AUTHENTICATION') as pseudo_full_auth_count, sum(flow_type = 'ENCRYPTED_FULL_AUTHENTICATION') as encrypted_full_auth_count, sum(status = 'S1000') as success_count, sum(status != 'S1000') AS failed_count, count(*) as total_count"

# summary table to upward summary table (ex: hourly to daily) column mapping, without time field
summary_to_summary_column_mapping = "nas_id,protocol,flow_type,sum(sim_count), sum(aka_count), sum(fulauth_count), sum(reauth_count), sum(pseudo_full_auth_count), sum(encrypted_full_auth_count), sum(success_count), sum(failed_count), sum(total_count)"


