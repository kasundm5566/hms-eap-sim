__author__ = 'malshan'

import logging
import logging.handlers
import sys
import os
import time
from datetime import timedelta
import datetime
import subprocess

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.insert(0, "../../conf")
sys.path.insert(0, "../sql")
import config
from sql_connection import SqlConnection


class DailyDataLoader:
    sql_connection_success = False
    logger = logging.getLogger('DailyDataLoader')

    def __init__(self):
        self.logger.setLevel(logging.DEBUG)
        handler = logging.handlers.TimedRotatingFileHandler(config.log_path,
                                                            when=config.log_roll_over_time,
                                                            interval=1,
                                                            backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        try:
            self.sql_connection = SqlConnection()
            self.sql_connection_success = True
        except Exception as e:
            self.logger.error("error occurred while opening sql connection [%s]", e)
            subprocess.call(["/bin/bash", config.snmp_file_location] + [config.snmp_sql_down])
            raise

    def load_data(self):
        self.logger.info("inserting data to daily summary")

        i = config.daily_summary_history_in_days
        init_time = datetime.datetime.now()
        while i > 0:
            start_time = (init_time + timedelta(days=-i)).strftime("%Y-%m-%d")
            end_time = (init_time + timedelta(days=(-i + 1))).strftime("%Y-%m-%d")
            if not self.sql_connection.is_executed(start_time, "DAILY"):
                self.logger.info("Daily summary with date time [%s] not executed executing", start_time)
                self.sql_connection.insert_to_summary_table_status(start_time, "DAILY", "EXECUTED")
                self.sql_connection.insert_data_to_daily_summary(start_time, end_time)
            else:
                self.logger.info("Daily summary with date time [%s] already executed", start_time)
            i = i - 1


    def commit(self):
        self.sql_connection.db.commit()
        self.sql_connection.close()


all_success = False
daily_start_time = time.time()
dailyDataLoader = DailyDataLoader()
if dailyDataLoader.sql_connection_success:
    try:
        dailyDataLoader.load_data()
        all_success = True
    except Exception as e:
        dailyDataLoader.logger.error("Error occurred while creating daily summary [%s]", e)
        all_success = False
    finally:
        if all_success:
            dailyDataLoader.commit()
            dailyDataLoader.logger.info("Daily summary creation completed")
        else:
            snmp_internal_error = config.snmp_internal_error + ' daily-summary-script'
            subprocess.call(["/bin/bash", config.snmp_file_location] + [snmp_internal_error])
    daily_end_time = time.time()
    DailyDataLoader.logger.info("Execution time in seconds : %f" % (daily_end_time - daily_start_time))