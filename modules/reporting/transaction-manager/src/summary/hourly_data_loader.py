__author__ = 'malshan'
import logging
import logging.handlers
import os
import sys
import time
from datetime import timedelta
import datetime
import subprocess

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.insert(0, "../../conf")
sys.path.insert(0, "../sql")
import config
from sql_connection import SqlConnection


class HourlyDataLoader:
    sql_connection_success = False
    logger = logging.getLogger('HourlyDataLoader')


    def __init__(self):

        self.logger.setLevel(logging.DEBUG)
        handler = logging.handlers.TimedRotatingFileHandler(config.log_path,
                                                            when=config.log_roll_over_time,
                                                            interval=1,
                                                            backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        try:
            self.sql_connection = SqlConnection()
            self.sql_connection_success = True
        except Exception as e:
            self.logger.error("error occurred while opening sql connection [%s]", e)
            subprocess.call(["/bin/bash", config.snmp_file_location] + [config.snmp_sql_down])
            raise


    def load_data(self):
        self.logger.info("inserting data to hourly summary")
        i = config.hourly_summary_history_in_hours
        init_time = datetime.datetime.now()
        while i > 0:
            start_time = (init_time + timedelta(hours=-i)).strftime("%Y-%m-%d %H")
            end_time = (init_time + timedelta(hours=(-i + 1))).strftime("%Y-%m-%d %H")
            if not self.sql_connection.is_executed(start_time, "HOURLY"):
                self.logger.info("Hourly summary with date time [%s] not executed executing", start_time)
                self.sql_connection.insert_data_to_hourly_summary(start_time, end_time)
                self.sql_connection.insert_to_summary_table_status(start_time, "HOURLY", "EXECUTED")
            else:
                self.logger.info("Hourly summary with date time [%s] already executed", start_time)
            i = i - 1


    def commit(self):
        self.sql_connection.db.commit()
        self.sql_connection.close()


all_success = False
hourly_start_time = time.time()
hourlyDataLoader = HourlyDataLoader()
if hourlyDataLoader.sql_connection_success:
    try:
        hourlyDataLoader.load_data()
        all_success = True
    except Exception as e:
        hourlyDataLoader.logger.error("Error occurred while creating hourly summary [%s]", e)
        all_success = False
    finally:
        if all_success:
            hourlyDataLoader.commit()
            hourlyDataLoader.logger.info("Creating hourly summary success")
        else:
            snmp_internal_error = config.snmp_internal_error + ' hourly-summary-script'
            subprocess.call(["/bin/bash", config.snmp_file_location] + [snmp_internal_error])
    hourly_end_time = time.time()
    HourlyDataLoader.logger.info("Execution time in seconds : %f" % (hourly_end_time - hourly_start_time))