#! /bin/bash

PYTHON_BIN="/home/ajuba/installs/python3/bin/python3.5"
PROC_NAME=python3.5

if ps aux | grep -v grep| grep '"${PROC_NAME}"  $1'
then
  echo "Currently Python Script is running. Skipping this time"
else
  echo "No python script is running. Executing now"
  $PYTHON_BIN $1
fi

