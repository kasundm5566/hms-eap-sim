__author__ = 'malshan'

import pymysql
import logging
import logging.handlers
import sys

sys.path.insert(0, "../../conf")
import config


class SqlConnection:
    logger = logging.getLogger('SqlConnection')

    def __init__(self):
        self.logger.setLevel(logging.DEBUG)
        handler = logging.handlers.TimedRotatingFileHandler(config.log_path,
                                                            when=config.log_roll_over_time,
                                                            interval=1,
                                                            backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.db = pymysql.connect(db=config.mysql_database, user=config.mysql_user, passwd=config.mysql_password,
                                  host=config.mysql_host, port=config.mysql_port, local_infile=1)
        self.cursor = self.db.cursor()

    def insert_data_from_file(self, file_path):
        sql_script = "LOAD DATA LOCAL INFILE %(file_path)s INTO TABLE trans_log FIELDS " \
                     "TERMINATED BY %(field_terminator)s (" + config.transaction_column_list + ")"

        param = {
            'file_path': file_path,
            'field_terminator': config.file_line_field_terminator
        }
        try:
            self.cursor.execute(sql_script, param)
            # self.db.commit()
        except Exception as e:
            self.logger.error('error occurred while inserting data from file [%s]', e)
            raise

    def is_transaction_empty(self):
        is_transaction_empty_sql = "SELECT count(*) from trans_log"
        try:
            self.cursor.execute(is_transaction_empty_sql)
            result = self.cursor.fetchone()
            return result[0] == 0
        except Exception as e:
            self.logger.error('error occurred while checking transaction table data existence [%s]', e)
            return True

    def is_executed(self, date_time, summary_type):
        date_time = str(date_time)
        param = {'summary_type': summary_type
                 }
        is_executed_sql = "SELECT count(*) from summary_table_status " \
                          "where  summary_type = %(summary_type)s and date_time like '%%" + date_time + "%%'"
        try:
            self.cursor.execute(is_executed_sql, param)
            result = self.cursor.fetchone()
            return result[0] > 0
        except Exception as e:
            self.logger.error('error occurred while checking summary_table data existence [%s]', e)
            return True

    def insert_data_to_hourly_summary(self, start_date, end_date):
        insert_sql = "INSERT INTO hourly_summary (" + config.summary_insert_fields + ") SELECT DATE_FORMAT(created,'%Y-%m-%d %H')," + \
                     config.summary_transaction_column_mapping + " FROM trans_log where  created >= '" +\
                     start_date + "' and created <'" + end_date + "' GROUP BY DATE_FORMAT" \
                                                                  "(created,'%Y-%m-%d %H')," + config.summary_group_by
        try:
            self.cursor.execute(insert_sql)
        except Exception as e:
            self.logger.error('error occurred while inserting data to hourly summary [%s]', e)
            raise

    def insert_data_to_daily_summary(self, start_date, end_date):

        insert_sql = "INSERT INTO daily_summary (" + config.summary_insert_fields + \
                     ") SELECT DATE_FORMAT(date_time, '%Y-%m-%d')," + config.summary_to_summary_column_mapping +\
                     "FROM hourly_summary where  date_time >= '" + start_date + "' and date_time <'" + end_date \
                     + "' GROUP BY DATE_FORMAT(date_time, '%Y-%m-%d')," + config.summary_group_by
        try:
            self.cursor.execute(insert_sql)
            self.db.commit()
        except Exception as e:
            self.logger.error('error occurred while inserting data to daily summary [%s]', e)
            raise

    def insert_to_summary_table_status(self, date_time, summary_type, status):
        param = {'date_time': date_time,
                 'summary_type': summary_type,
                 'status': status}

        insert_sql = "INSERT INTO summary_table_status (date_time,summary_type,status) " \
                     "VALUES (%(date_time)s,%(summary_type)s,%(status)s)"
        try:
            self.cursor.execute(insert_sql, param)
        except Exception as e:
            self.logger.error('error occurred while inserting data to service code parameters [%s]', e)
            raise


    def close(self):
        self.db.close()
