__author__ = 'malshan'
import os
import logging
import logging.handlers
import sys
import fnmatch
import re
import time
import shutil
from datetime import datetime
import subprocess

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.insert(0, "../../conf")
sys.path.insert(0, "../sql")
from sql_connection import SqlConnection
import config


class TransactionDataLoader:
    sql_connection_success = False
    logger = logging.getLogger('TransactionDataLoader')

    def __init__(self):
        self.logger.setLevel(logging.DEBUG)
        handler = logging.handlers.TimedRotatingFileHandler(config.log_path,
                                                            when=config.log_roll_over_time,
                                                            interval=1,
                                                            backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        try:
            self.sqlConnection = SqlConnection()
            self.sql_connection_success = True
        except Exception as e:
            self.logger.error("error occurred while opening sql connection [%s]", e)
            subprocess.call(["/bin/bash", config.snmp_file_location] + [config.snmp_sql_down])

    def load_data_from_file(self, file_name):
        try:
            file_path = directory_path + '/' + file_name
            self.logger.info('inserting data in file [%s] to trans_log table', file_name)
            self.sqlConnection.insert_data_from_file(file_path)
        except Exception as e:
            self.logger.error('Error occurred while loading trans log files from the directory [%s]', e)
            raise

    def is_correct_file_name(self, file_name):
        regex = fnmatch.translate(config.transaction_file_name_pattern)
        compiled_regx = re.compile(regex)
        return compiled_regx.match(file_name)

    def move_finished_files(self, file_name):
        try:
            destination = config.completed_trans_log_directory
            if self.is_correct_file_name(file_name):
                if os.path.isdir(destination):
                    dt = datetime.now()
                    new_file_name = file_name
                    os.rename(config.trans_log_directory + '/' + file_name,
                              config.trans_log_directory + '/' + new_file_name)
                    shutil.move(config.trans_log_directory + '/' + new_file_name, destination)
                    self.logger.info("moved file [%s] to [%s]", file_name, destination)
                else:
                    self.logger.warning('destination [%s] does not exist', destination)

            else:
                self.logger.warning('file name [%s] does not match with defined file name pattern', file_name)

        except Exception as e:
            self.logger.error("error occurred while moving completed trans log files [%s]", e)
            raise


    def commit(self):
        self.sqlConnection.db.commit()


start_time = time.time()
trans = TransactionDataLoader()
all_success = False
if trans.sql_connection_success:
    directory_path = config.trans_log_directory
    for file_name in os.listdir(directory_path):
        if trans.is_correct_file_name(file_name):
            try:
                trans.load_data_from_file(file_name)
                trans.move_finished_files(file_name)
                all_success = True
            except Exception as e:
                trans.logger.error("Error occurred while loading trans log data of file [%s] [%s]", file_name, e)
                all_success = False
            finally:
                if all_success:
                    trans.commit()
                else:
                    snmp_internal_error = config.snmp_internal_error + ' transaction dataloader script'
                    subprocess.call(["/bin/bash", config.snmp_file_location] + [snmp_internal_error])
        else:
            trans.logger.warning('file name [%s] does not match with configured file name pattern', file_name)
    trans.sqlConnection.close()
    end_time = time.time()
    print ("Execution time in seconds : %f" % (end_time - start_time))

