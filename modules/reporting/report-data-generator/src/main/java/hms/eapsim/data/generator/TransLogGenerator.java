package hms.eapsim.data.generator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TransLogGenerator {

    private Date startDate;
    private Date endDate;

    private long corrilationId;
    private int numberOfDays = 1;
    private int numberOfSubscribers = 5000;

    private int numberOfSimSuccessRTO = 1;
    private int numberOfSimFailuresRTO = 1;
    private int numberOfAkaSuccssRTO = 1;
    private int numberOfAkaFailureRTO = 1;

    private Random imsiGenerator = new Random();
    private Random msisdnGenerator = new Random();
    private Random failureCodeGenerator = new Random();

    private final ExecutorService worker = Executors.newFixedThreadPool(8);
    private final ConnectionManager connectionManager = new ConnectionManager();

    private String[] failureCode = new String[]{"E1001", "E2002", "E3000", "E3002", "E1000", "E2000", "E2001"};


    public void init() {
        connectionManager.init();
        corrilationId = System.currentTimeMillis();
        final Calendar today = Calendar.getInstance();
        this.endDate = today.getTime();
        today.add(Calendar.DAY_OF_MONTH, -numberOfDays);
        this.startDate = today.getTime();
    }

    public static void main(String[] args) throws SQLException {
        final TransLogGenerator transLogGenerator = new TransLogGenerator();
        transLogGenerator.init();
        transLogGenerator.generateData();
        System.out.println("Done");
    }

    private TransInfo buildTransLog(Date date, String corrilationId, String imsi, String description,
                                    String msisdn, String protocol, String requestType, String responseType, String errorCode) {
        TransInfo identity = new TransInfo();
        identity.setCorrilationId(corrilationId);
        identity.setDesc(description);
        identity.setImsi(imsi);
        identity.setMsisdn(msisdn);
        identity.setProtocol(protocol);
        identity.setRequetType(requestType);
        identity.setResponseType(responseType);
        identity.setStatus(errorCode);
        identity.setTime(date);
        return identity;
    }

    private List<TransInfo> getAkaSuccess(Date date) {
        String corrilationId = String.valueOf(nextCorrId());
        String imsi = getImsi();
        String msisdn = getMsisdn();

        List<TransInfo> logList = new ArrayList<>();
        logList.add(buildTransLog(date, corrilationId, imsi, "Identity Success", null, "AKA", "Identity", "CHALLENGE", "S1000"));
        logList.add(buildTransLog(new Date(date.getTime() + 200), corrilationId, imsi, "Successfully Authenticated", msisdn, "AKA", "CHALLENGE", "ACCESS_ACCEPT", "S1000"));
        return logList;
    }

    private List<TransInfo> getAkaFailure(Date date) {
        String corrilationId = String.valueOf(nextCorrId());
        String imsi = getImsi();

        List<TransInfo> errorList = new ArrayList<>();
        errorList.add(buildTransLog(date, corrilationId, imsi, "Time out", null, "AKA", "Identity", "FAILURE", nextFailureCode()));

        return errorList;
    }

    private List<TransInfo> getSimSuccess(Date date) {
        String corrilationId = String.valueOf(nextCorrId());
        String imsi = getImsi();
        String msisdn = getMsisdn();

        List<TransInfo> logList = new ArrayList<>();
        logList.add(buildTransLog(date, corrilationId, imsi, "Identity Success", null, "SIM", "Identity", "CHALLENGE", "S1000"));
        logList.add(buildTransLog(new Date(date.getTime() + 100), corrilationId, imsi, "Challenge generated", null, "SIM", "START", "CHALLENGE", "S1000"));
        logList.add(buildTransLog(new Date(date.getTime() + 200), corrilationId, imsi, "Successfully Authenticated", msisdn, "SIM", "CHALLENGE", "ACCESS_ACCEPT", "S1000"));

        return logList;
    }

    private List<TransInfo> getSimFailure(Date date) {
        String corrilationId = String.valueOf(nextCorrId());
        String imsi = getImsi();
        List<TransInfo> errorLog = new ArrayList<>();
        errorLog.add(buildTransLog(date, corrilationId, imsi, "System error", null, "SIM", "START", "CHALLENGE", nextFailureCode()));

        return errorLog;
    }

    private Long nextCorrId() {
        return corrilationId++;
    }

    private String nextFailureCode() {
        return failureCode[Math.abs(failureCodeGenerator.nextInt(7))];
    }

    private String getImsi() {
        int pad = String.valueOf(numberOfSubscribers).length() + 1;
        return "1234567890" + String.format("%0" + pad + "d", Math.abs(imsiGenerator.nextInt(numberOfSubscribers)));
    }

    private String getMsisdn() {
        int pad = String.valueOf(numberOfSubscribers).length() + 1;
        return "65123" + String.format("%0" + pad + "d", Math.abs(msisdnGenerator.nextInt(numberOfSubscribers)));
    }

    public void generateData() throws SQLException {
        for (int i = 0; i < numberOfDays; i++) {
            for (long st = startDate.getTime(); st < endDate.getTime(); st += 1000) {
                final Date date = new Date(st);

                for (int p = 0; p < numberOfSimSuccessRTO; p++) {
                    for (TransInfo transInfo : getSimSuccess(date)) {
                        insertRecord(transInfo);
                    }
                }
                for (int p = 0; p < numberOfSimFailuresRTO; p++) {
                    for (TransInfo transInfo : getSimFailure(date)) {
                        insertRecord(transInfo);
                    }
                }
                for (int p = 0; p < numberOfAkaSuccssRTO; p++) {
                    for (TransInfo transInfo : getAkaSuccess(date)) {
                        insertRecord(transInfo);
                    }
                }
                for (int p = 0; p < numberOfAkaFailureRTO; p++) {
                    for (TransInfo transInfo : getAkaFailure(date)) {
                        doInsert(transInfo);
                    }
                }
            }
        }
    }

    private void insertRecord(final TransInfo log) {
        worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    doInsert(log);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

//        worker.shutdown();
    }

    private void doInsert(TransInfo log) throws SQLException {
        Connection connection = null;
        try {
            connection = connectionManager.checkout();

            final PreparedStatement stmt = connection.prepareStatement("insert into trans_log (request_type, imsi, status, description, msisdn, created, protocol, response_type, correlation_id, flow_type, server_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setString(1, log.getRequetType());
            stmt.setString(2, log.getImsi());
            stmt.setString(3, log.getStatus());
            stmt.setString(4, log.getDesc());
            stmt.setString(5, log.getMsisdn());
            stmt.setTimestamp(6, new Timestamp(log.getTime().getTime()));
            stmt.setString(7, log.getProtocol());
            stmt.setString(8, log.getResponseType());
            stmt.setString(9, log.getCorrilationId());
            stmt.setString(10, log.getFlowType());
            stmt.setString(11, log.getServerId());
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
}
