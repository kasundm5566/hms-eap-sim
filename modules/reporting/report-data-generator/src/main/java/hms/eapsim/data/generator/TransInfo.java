package hms.eapsim.data.generator;

import java.util.Date;

public class TransInfo {
    private String clientId = "client1";
    private String macAddress;
    private String ip;
    private String serverId = "eap-sim1";
    private String requetType;
    private String imsi;
    private String status;
    private String desc;
    private String msisdn;
    private String protocol;
    private String responseType;
    private String corrilationId;
    private String flowType = FlowType.FULL_AUTHENTICATION.name();
    private Date time;

    enum FlowType {
        FULL_AUTHENTICATION,
        RE_AUTHENTICATION
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getRequetType() {
        return requetType;
    }


    public String getFlowType() {
        return flowType;
    }

    public void setFlowType(String flowType) {
        this.flowType = flowType;
    }

    public void setRequetType(String requetType) {
        this.requetType = requetType;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getCorrilationId() {
        return corrilationId;
    }

    public void setCorrilationId(String corrilationId) {
        this.corrilationId = corrilationId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
