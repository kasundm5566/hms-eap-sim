package hms.eapsim.data.generator;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionManager {

    private ComboPooledDataSource dataSource;
    private Lock lock = new ReentrantLock();

    public void close() {
        dataSource.close();
    }

    public void init() {
        this.dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass("com.mysql.jdbc.Driver"); //loads the jdbc driver
            dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/eapsim");
            dataSource.setUser("user");
            dataSource.setPassword("password");
            dataSource.setMinPoolSize(5);
            dataSource.setAcquireIncrement(5);
            dataSource.setMaxPoolSize(20);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

    public Connection checkout() throws SQLException {
        return dataSource.getConnection();
    }
}
