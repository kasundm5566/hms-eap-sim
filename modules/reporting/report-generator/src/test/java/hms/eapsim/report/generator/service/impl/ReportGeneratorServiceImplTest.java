package hms.eapsim.report.generator.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:spring-context.xml"})
public class ReportGeneratorServiceImplTest {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final String LOCATION = "/hms/eap-sim/report";

    @Autowired
    private ReportGeneratorServiceImpl reportGeneratorService;

    @Test
    public void testGenerateHourlyReport() throws Exception {
        reportGeneratorService.generateHourlyTrxSummaryReport(LOCATION, DATE_FORMAT.parse("2017-06-10 20:00:00"));
    }

    @Test
    public void testGenerateDailyReport() throws Exception {
        reportGeneratorService.generateDailyTrxSummaryReport(LOCATION, DATE_FORMAT.parse("2017-06-10 00:00:00"));
    }

    @Test
    public void testGenerateHourlyLocationBasedReport() throws Exception {
        reportGeneratorService.generateHourlyLocationBasedTrxSummaryReport(LOCATION, DATE_FORMAT.parse("2017-06-10 20:00:00"));
    }

    @Test
    public void testGenerateDailyLocationBasedReport() throws Exception {
        reportGeneratorService.generateDailyLocationBasedTrxSummaryReport(LOCATION, DATE_FORMAT.parse("2017-06-10 00:00:00"));
    }
}
