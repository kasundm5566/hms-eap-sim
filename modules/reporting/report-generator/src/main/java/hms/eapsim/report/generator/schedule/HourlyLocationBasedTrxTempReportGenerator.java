package hms.eapsim.report.generator.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Generate hourly location based transaction reports
 */
@Component
public class HourlyLocationBasedTrxTempReportGenerator extends BaseReportGenerator {

    private static final Logger logger = LoggerFactory.getLogger(HourlyLocationBasedTrxTempReportGenerator.class);

    @PostConstruct
    @Override
    public void init() throws Exception {
        //do nothing
    }

    @Scheduled(cron = "${hourly.location.based.temp.report.generator.cron.expression}")
    public void run() {
        super.doGenerate(new Date());
    }

    @Override
    protected void generateReport(Date dateToBeGenerated) {
        logger.info("Start Hourly location base Transaction Report Generator {}", reportGeneratorService);
        try {
            SimpleDateFormat format = new SimpleDateFormat(configuration.getDirCreateFileFormat());
            File dir = new File(configuration.getReportBaseDirectory(), format.format(dateToBeGenerated));

            reportGeneratorService.generateHourlyLocationBasedTrxSummaryReport(dir.getAbsolutePath(), dateToBeGenerated);
        } catch (Exception e) {
            printErrorMessage(e);
        }
    }


}
