package hms.eapsim.report.generator.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static hms.eapsim.report.generator.ReportGenerateState.FAILED;
import static hms.eapsim.report.generator.ReportGenerateState.SUCCESS;
import static hms.eapsim.report.generator.ReportName.DAILY_LOCATION_BASED_TRX_REPORT;
import static hms.eapsim.report.generator.StatusLog.log;

/**
 * Generate daily location based transaction reports
 */
@Component
public class DailyLocationBasedTrxReportGenerator extends BaseReportGenerator {

    private static final Logger logger = LoggerFactory.getLogger(DailyLocationBasedTrxReportGenerator.class);

    @Scheduled(cron = "${daily.location.based.report.generator.cron.expression}")
    public void run() {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, -1);
        super.doGenerate(today.getTime());
    }

    @Override
    protected void generateReport(Date dateToBeGenerated) {
        logger.info("Start Daily location base Transaction Report Generator {}", reportGeneratorService);
        Date startDate = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat(configuration.getDirCreateFileFormat());
            File dir = new File(configuration.getReportBaseDirectory(), format.format(dateToBeGenerated));

            reportGeneratorService.generateDailyLocationBasedTrxSummaryReport(dir.getAbsolutePath(), dateToBeGenerated);
            Date endDate = new Date();
            log(dateToBeGenerated, DAILY_LOCATION_BASED_TRX_REPORT, startDate, endDate, SUCCESS, SUCCESS.name());
        } catch (Exception e) {
            printErrorMessage(e);
            log(dateToBeGenerated, DAILY_LOCATION_BASED_TRX_REPORT, startDate, new Date(), FAILED, e.getMessage());
        }
    }


}
