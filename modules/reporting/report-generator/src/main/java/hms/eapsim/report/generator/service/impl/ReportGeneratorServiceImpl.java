package hms.eapsim.report.generator.service.impl;

import au.com.bytecode.opencsv.CSVWriter;
import hms.eapsim.report.generator.NoRecordsFoundException;
import hms.eapsim.report.generator.ReportGeneratorExeption;
import hms.eapsim.report.generator.SysConfiguration;
import hms.eapsim.report.generator.dao.impl.DailyLocationBasedTrxSummaryDao;
import hms.eapsim.report.generator.dao.impl.DailyTrxSummaryDao;
import hms.eapsim.report.generator.dao.impl.HourlyLocationBasedTrxSummaryDao;
import hms.eapsim.report.generator.dao.impl.HourlyTrxSummaryDao;
import hms.eapsim.report.generator.domain.DailyLocationBasedTrxSummary;
import hms.eapsim.report.generator.domain.DailyTrxSummary;
import hms.eapsim.report.generator.domain.HourlyLocationBasedTrxSummary;
import hms.eapsim.report.generator.domain.HourlyTrxSummary;
import hms.eapsim.report.generator.service.ReportGeneratorService;
import hms.eapsim.report.generator.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReportGeneratorServiceImpl implements ReportGeneratorService {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String TIME_FORMAT = "HH";

    private static final Logger logger = LoggerFactory.getLogger(ReportGeneratorServiceImpl.class);

    @Autowired
    private SysConfiguration configuration;

    @Autowired
    private HourlyTrxSummaryDao hourlyTrxSummaryDao;

    @Autowired
    private DailyTrxSummaryDao dailyTrxSummaryDao;

    @Autowired
    private HourlyLocationBasedTrxSummaryDao hourlyLocationBasedTrxSummaryDao;

    @Autowired
    private DailyLocationBasedTrxSummaryDao dailyLocationBasedTrxSummaryDao;

    @Override
    public void generateHourlyTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption {
        Date formattedDate = formatDateParameter(date);
        logger.info("Querying hourly transaction summaries for the date [{}]", formattedDate);
        List<HourlyTrxSummary> hourlyTrxSummaries =
                hourlyTrxSummaryDao.find(configuration.getHourlyTrxSummarySql(), new Object[]{formattedDate});

        if (hourlyTrxSummaries.isEmpty()) {
            throw new NoRecordsFoundException("Hourly trx summaries are empty");
        }

        List<String[]> lines = new ArrayList<>();
        String columnNamesStr = configuration.getHourlyTrxSummaryReportColumnNames();
        addHeaderIfRequired(lines, columnNamesStr);

        for (HourlyTrxSummary hourlyTrxSummary : hourlyTrxSummaries) {
            String[] arr = generateHourlyTrxSummaryRecord(hourlyTrxSummary);
            lines.add(arr);
        }
        logger.info("Writing hourly transaction CSV report for the date [{}]", formattedDate);
        String fullPath = createFullPath(location, configuration.getHourlyTrxSummaryReportFileName());
        writeCsvReport(lines, fullPath);
    }

    @Override
    public void generateDailyTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption {
        Date formattedDate = formatDateParameter(date);
        logger.info("Querying daily transaction summaries for the date [{}]", formattedDate);
        List<DailyTrxSummary> dailyTrxSummaries =
                dailyTrxSummaryDao.find(configuration.getDailyTrxSummarySql(), new Object[]{formattedDate});

        if (dailyTrxSummaries.isEmpty()) {
            throw new NoRecordsFoundException("Daily trx summaries are empty");
        }

        List<String[]> lines = new ArrayList<>();
        String columnNamesStr = configuration.getDailyTrxSummaryReportColumnNames();
        addHeaderIfRequired(lines, columnNamesStr);

        for (DailyTrxSummary dailyTrxSummary : dailyTrxSummaries) {
            String[] arr = generateDailyTrxSummaryRecord(dailyTrxSummary);
            lines.add(arr);
        }

        logger.info("Writing daily transaction CSV report for the date [{}]", formattedDate);
        String fullPath = createFullPath(location, configuration.getDailyTrxSummaryReportFileName());
        writeCsvReport(lines, fullPath);
    }

    @Override
    public void generateHourlyLocationBasedTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption {
        Date formattedDate = formatDateParameter(date);
        logger.info("Querying hourly location based transaction summaries for the date [{}]", formattedDate);
        List<HourlyLocationBasedTrxSummary> hourlyLocationBasedTrxSummaries =
                hourlyLocationBasedTrxSummaryDao.find(configuration.getHourlyLocationBasedTrxSummarySql(), new Object[]{formattedDate});

        if (hourlyLocationBasedTrxSummaries.isEmpty()) {
            throw new NoRecordsFoundException("Hourly location based trx summaries are empty");
        }

        List<String[]> lines = new ArrayList<>();
        String columnNamesStr = configuration.getHourlyLocationBasedTrxSummaryReportColumnNames();
        addHeaderIfRequired(lines, columnNamesStr);

        for (HourlyLocationBasedTrxSummary hourlyLocationBasedTrxSummary : hourlyLocationBasedTrxSummaries) {
            String[] arr = generateHourlyLocationBasedTrxSummaryRecord(hourlyLocationBasedTrxSummary);
            lines.add(arr);
        }

        logger.info("Writing hourly location based transaction CSV report for the date [{}]", formattedDate);
        String fullPath = createFullPath(location, configuration.getHourlyLocationBasedTrxSummaryReportFileName());
        writeCsvReport(lines, fullPath);
    }

    @Override
    public void generateDailyLocationBasedTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption {
        Date formattedDate = formatDateParameter(date);
        logger.info("Querying daily location based transaction summaries for the date [{}]", formattedDate);
        List<DailyLocationBasedTrxSummary> dailyLocationBasedTrxSummaries =
                dailyLocationBasedTrxSummaryDao.find(configuration.getDailyLocationBasedTrxSummarySql(), new Object[]{formattedDate});

        if (dailyLocationBasedTrxSummaries.isEmpty()) {
            throw new NoRecordsFoundException("Daily location based trx summaries are empty");
        }

        List<String[]> lines = new ArrayList<>();
        String columnNamesStr = configuration.getDailyLocationBasedTrxSummaryReportColumnNames();
        addHeaderIfRequired(lines, columnNamesStr);

        for (DailyLocationBasedTrxSummary dailyLocationBasedTrxSummary : dailyLocationBasedTrxSummaries) {
            String[] arr = generateDailyLocationBasedTrxSummaryRecord(dailyLocationBasedTrxSummary);
            lines.add(arr);
        }

        logger.info("Writing daily location based transaction CSV report for the date [{}]", formattedDate);
        String fullPath = createFullPath(location, configuration.getDailyLocationBasedTrxSummaryReportFileName());
        writeCsvReport(lines, fullPath);
    }

    private Date formatDateParameter(Date date) throws ReportGeneratorExeption {
        Date formattedDate = date;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            formattedDate = dateFormat.parse(dateFormat.format(date));
        } catch (ParseException e) {
            logger.error("Error occurred while formatting the date [{}]", date);
            throw new ReportGeneratorExeption("Error occurred while formatting the date", e);
        }
        return formattedDate;
    }

    private void addHeaderIfRequired(List<String[]> lines, String columnNamesStr) {
        if (configuration.isHeaderRequired()) {
            String[] columnNames = columnNamesStr.split(configuration.getColumnNamesSeparator());
            lines.add(columnNames);
        }
    }

    private String createFullPath(String location, String fileName) {
        return location + "/" + fileName + configuration.getReportFileExtension();
    }

    private void writeCsvReport(List<String[]> lines, String fullPath) throws ReportGeneratorExeption {
        try (CSVWriter writer = new CSVWriter(new FileWriter(fullPath), configuration.getSeparator(), configuration.getQuoteChar())) {
            writer.writeAll(lines);
        } catch (IOException e) {
            logger.error("Error occurred while writing to the file [{}]", fullPath);
            throw new ReportGeneratorExeption("Error occurred while writing to the file", e);
        }
    }

    private String[] generateHourlyTrxSummaryRecord(HourlyTrxSummary hourlyTrxSummary) {
        String[] arr = new String[11];
        SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
        Integer hour = Integer.valueOf(timeFormat.format(hourlyTrxSummary.getDate()));
        arr[0] = Util.formatHourField(hour, configuration.getReportTimeFormat(), configuration.getReportHourFormat());
        arr[1] = String.valueOf(hourlyTrxSummary.getSimCount());
        arr[2] = String.valueOf(hourlyTrxSummary.getAkaCount());
        arr[3] = String.valueOf(hourlyTrxSummary.getFullAuthCount());
        arr[4] = String.valueOf(hourlyTrxSummary.getReAuthCount());
        arr[5] = String.valueOf(hourlyTrxSummary.getPseudoFullAuthCount());
        arr[6] = String.valueOf(hourlyTrxSummary.getEncryptedFullAuthCount());
        arr[7] = String.valueOf(hourlyTrxSummary.getSuccessCount());
        arr[8] = String.valueOf(hourlyTrxSummary.getErrorCount());
        arr[9] = String.valueOf(hourlyTrxSummary.getTotalTransactions());
        arr[10] = Util.round(hourlyTrxSummary.getSuccessRate(), 2, configuration.getReportPercentageFormat());
        return arr;
    }

    private String[] generateDailyTrxSummaryRecord(DailyTrxSummary dailyTrxSummary) {
        String[] arr = new String[7];
        SimpleDateFormat dateFormat = new SimpleDateFormat(configuration.getReportDateFormat());
        arr[0] = dateFormat.format(dailyTrxSummary.getDate());
        arr[1] = dailyTrxSummary.getProtocol();
        arr[2] = dailyTrxSummary.getAuthenticationType();
        arr[3] = String.valueOf(dailyTrxSummary.getSuccessCount());
        arr[4] = String.valueOf(dailyTrxSummary.getErrorCount());
        arr[5] = String.valueOf(dailyTrxSummary.getTotalTransactions());
        arr[6] = Util.round(dailyTrxSummary.getSuccessRate(), 2, configuration.getReportPercentageFormat());
        return arr;
    }

    private String[] generateHourlyLocationBasedTrxSummaryRecord(HourlyLocationBasedTrxSummary hourlyLocationBasedTrxSummary) {
        String[] arr = new String[12];
        SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
        Integer hour = Integer.valueOf(timeFormat.format(hourlyLocationBasedTrxSummary.getDate()));
        arr[0] = Util.formatHourField(hour, configuration.getReportTimeFormat(), configuration.getReportHourFormat());
        arr[1] = hourlyLocationBasedTrxSummary.getNasId();
        arr[2] = String.valueOf(hourlyLocationBasedTrxSummary.getSimCount());
        arr[3] = String.valueOf(hourlyLocationBasedTrxSummary.getAkaCount());
        arr[4] = String.valueOf(hourlyLocationBasedTrxSummary.getFullAuthCount());
        arr[5] = String.valueOf(hourlyLocationBasedTrxSummary.getReAuthCount());
        arr[6] = String.valueOf(hourlyLocationBasedTrxSummary.getPseudoFullAuthCount());
        arr[7] = String.valueOf(hourlyLocationBasedTrxSummary.getEncryptedFullAuthCount());
        arr[8] = String.valueOf(hourlyLocationBasedTrxSummary.getSuccessCount());
        arr[9] = String.valueOf(hourlyLocationBasedTrxSummary.getErrorCount());
        arr[10] = String.valueOf(hourlyLocationBasedTrxSummary.getTotalTransactions());
        arr[11] = Util.round(hourlyLocationBasedTrxSummary.getSuccessRate(), 2, configuration.getReportPercentageFormat());
        return arr;
    }

    private String[] generateDailyLocationBasedTrxSummaryRecord(DailyLocationBasedTrxSummary dailyLocationBasedTrxSummary) {
        String[] arr = new String[12];
        SimpleDateFormat dateFormat = new SimpleDateFormat(configuration.getReportDateFormat());
        arr[0] = dateFormat.format(dailyLocationBasedTrxSummary.getDate());
        arr[1] = dailyLocationBasedTrxSummary.getNasId();
        arr[2] = String.valueOf(dailyLocationBasedTrxSummary.getSimCount());
        arr[3] = String.valueOf(dailyLocationBasedTrxSummary.getAkaCount());
        arr[4] = String.valueOf(dailyLocationBasedTrxSummary.getFullAuthCount());
        arr[5] = String.valueOf(dailyLocationBasedTrxSummary.getReAuthCount());
        arr[6] = String.valueOf(dailyLocationBasedTrxSummary.getPseudoFullAuthCount());
        arr[7] = String.valueOf(dailyLocationBasedTrxSummary.getEncryptedFullAuthCount());
        arr[8] = String.valueOf(dailyLocationBasedTrxSummary.getSuccessCount());
        arr[9] = String.valueOf(dailyLocationBasedTrxSummary.getErrorCount());
        arr[10] = String.valueOf(dailyLocationBasedTrxSummary.getTotalTransactions());
        arr[11] = Util.round(dailyLocationBasedTrxSummary.getSuccessRate(), 2, configuration.getReportPercentageFormat());

        return arr;
    }

}