package hms.eapsim.report.generator;

/**
 * Report generation status
 */
public enum ReportGenerateState {
    SUCCESS,

    FAILED,
}
