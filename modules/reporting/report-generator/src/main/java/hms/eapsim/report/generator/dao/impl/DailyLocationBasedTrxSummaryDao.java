package hms.eapsim.report.generator.dao.impl;

import hms.eapsim.report.generator.dao.TrxSummaryDao;
import hms.eapsim.report.generator.domain.DailyLocationBasedTrxSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class DailyLocationBasedTrxSummaryDao extends JdbcDaoSupport implements TrxSummaryDao<DailyLocationBasedTrxSummary> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<DailyLocationBasedTrxSummary> find(String query, Object[] params) {
        List<DailyLocationBasedTrxSummary> dailyLocationBasedTrxSummaries = getJdbcTemplate().query(query, params, new RowMapper<DailyLocationBasedTrxSummary>() {
            @Override
            public DailyLocationBasedTrxSummary mapRow(ResultSet rs, int i) throws SQLException {
                DailyLocationBasedTrxSummary dailyLocationBasedTrxSummary = new DailyLocationBasedTrxSummary();
                dailyLocationBasedTrxSummary.setDate(rs.getTimestamp(1));
                dailyLocationBasedTrxSummary.setNasId(rs.getString(2));
                dailyLocationBasedTrxSummary.setSimCount(rs.getLong(3));
                dailyLocationBasedTrxSummary.setAkaCount(rs.getLong(4));
                dailyLocationBasedTrxSummary.setFullAuthCount(rs.getLong(5));
                dailyLocationBasedTrxSummary.setReAuthCount(rs.getLong(6));
                dailyLocationBasedTrxSummary.setPseudoFullAuthCount(rs.getLong(7));
                dailyLocationBasedTrxSummary.setEncryptedFullAuthCount(rs.getLong(8));
                dailyLocationBasedTrxSummary.setSuccessCount(rs.getLong(9));
                dailyLocationBasedTrxSummary.setErrorCount(rs.getLong(10));
                dailyLocationBasedTrxSummary.setTotalTransactions(rs.getLong(11));
                dailyLocationBasedTrxSummary.setSuccessRate(rs.getFloat(12));

                return dailyLocationBasedTrxSummary;
            }
        });
        return dailyLocationBasedTrxSummaries;
    }
}
