package hms.eapsim.report.generator.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;

public class Util {

    public static String round(float percentage, int decimalPlacesCount, String percentageFormatStr) {
        BigDecimal bd = new BigDecimal(percentage);
        bd = bd.setScale(decimalPlacesCount, BigDecimal.ROUND_HALF_UP);
        DecimalFormat df = new DecimalFormat(percentageFormatStr);
        return df.format(bd);
    }

    public static String formatHourField(int hour, String hourFormat, String hourFormatStr) {
        int nextHour = hour + 1;
        String fromHour = String.format(hourFormatStr, hour);
        String toHour = String.format(hourFormatStr, nextHour);
        return MessageFormat.format(hourFormat, new Object[]{fromHour, toHour});
    }

}
