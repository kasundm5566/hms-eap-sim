package hms.eapsim.report.generator.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static hms.eapsim.report.generator.ReportGenerateState.FAILED;
import static hms.eapsim.report.generator.ReportGenerateState.SUCCESS;
import static hms.eapsim.report.generator.ReportName.HOURLY_LOCATION_BASED_TRX_REPORT;
import static hms.eapsim.report.generator.StatusLog.log;

/**
 * Generate hourly location based transaction reports
 */
@Component
public class HourlyLocationBasedTrxCompleteReportGenerator extends BaseReportGenerator {

    private static final Logger logger = LoggerFactory.getLogger(HourlyLocationBasedTrxCompleteReportGenerator.class);

    @Scheduled(cron = "${hourly.location.based.complete.report.generator.cron.expression}")
    public void run() {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, -1);
        super.doGenerate(today.getTime());
    }

    @Override
    protected void generateReport(Date dateToBeGenerated) {
        logger.info("Start Hourly location base Transaction Report Generator {}", reportGeneratorService);
        Date startDate = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat(configuration.getDirCreateFileFormat());
            File dir = new File(configuration.getReportBaseDirectory(), format.format(dateToBeGenerated));

            reportGeneratorService.generateHourlyLocationBasedTrxSummaryReport(dir.getAbsolutePath(), dateToBeGenerated);
            log(dateToBeGenerated, HOURLY_LOCATION_BASED_TRX_REPORT, startDate, new Date(), SUCCESS, SUCCESS.name());
        } catch (Exception e) {
            log(dateToBeGenerated, HOURLY_LOCATION_BASED_TRX_REPORT, startDate, new Date(), FAILED, e.getMessage());
            printErrorMessage(e);
        }
    }

}
