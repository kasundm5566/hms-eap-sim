package hms.eapsim.report.generator.dao.impl;

import hms.eapsim.report.generator.dao.TrxSummaryDao;
import hms.eapsim.report.generator.domain.HourlyTrxSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class HourlyTrxSummaryDao extends JdbcDaoSupport implements TrxSummaryDao<HourlyTrxSummary> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<HourlyTrxSummary> find(String query, Object[] params) {
        List<HourlyTrxSummary> hourlyTrxSummaries = getJdbcTemplate().query(query, params, new RowMapper<HourlyTrxSummary>() {
            @Override
            public HourlyTrxSummary mapRow(ResultSet rs, int i) throws SQLException {
                HourlyTrxSummary hourlyTrxSummary = new HourlyTrxSummary();
                hourlyTrxSummary.setDate(rs.getTimestamp(1));
                hourlyTrxSummary.setSimCount(rs.getLong(2));
                hourlyTrxSummary.setAkaCount(rs.getLong(3));
                hourlyTrxSummary.setFullAuthCount(rs.getLong(4));
                hourlyTrxSummary.setReAuthCount(rs.getLong(5));
                hourlyTrxSummary.setPseudoFullAuthCount(rs.getLong(6));
                hourlyTrxSummary.setEncryptedFullAuthCount(rs.getLong(7));
                hourlyTrxSummary.setSuccessCount(rs.getLong(8));
                hourlyTrxSummary.setErrorCount(rs.getLong(9));
                hourlyTrxSummary.setTotalTransactions(rs.getLong(10));
                hourlyTrxSummary.setSuccessRate(rs.getFloat(11));
                return hourlyTrxSummary;
            }
        });
        return hourlyTrxSummaries;
    }
}
