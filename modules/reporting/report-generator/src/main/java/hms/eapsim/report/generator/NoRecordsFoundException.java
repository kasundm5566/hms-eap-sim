package hms.eapsim.report.generator;

/**
 * No summary data found
 */
public class NoRecordsFoundException extends ReportGeneratorExeption {
    public NoRecordsFoundException() {
    }

    public NoRecordsFoundException(String message) {
        super(message);
    }

    public NoRecordsFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoRecordsFoundException(Throwable cause) {
        super(cause);
    }

    public NoRecordsFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
