package hms.eapsim.report.generator.schedule;

import hms.eapsim.report.generator.NoRecordsFoundException;
import hms.eapsim.report.generator.SysConfiguration;
import hms.eapsim.report.generator.service.ReportGeneratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Base report generator for all the report
 */
public abstract class BaseReportGenerator {

    private static final Logger logger = LoggerFactory.getLogger(BaseReportGenerator.class);
    @Autowired
    protected ReportGeneratorService reportGeneratorService;
    @Autowired
    protected SysConfiguration configuration;
    @Autowired
    protected DirectoryCreator directoryCreator;
    protected boolean isRecoveryRunning;

    @PostConstruct
    public void init() throws Exception {
        if (configuration.isRecoveryEnabled()) {
            isRecoveryRunning = true;
            recovery();
            isRecoveryRunning = false;
        }
    }

    private void recovery() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat(configuration.getDirCreateFileFormat());
        String recoveryStartDates = configuration.getRecoveryStartDate();

        List<String> recoveryDateList = Arrays.asList(recoveryStartDates.split(",", 0));

        for (String recoveryDate : recoveryDateList) {
            Date dateToBeGenerated = format.parse(recoveryDate);
            directoryCreator.createDirectory(dateToBeGenerated);
            generateReport(dateToBeGenerated);
        }
    }

    protected abstract void generateReport(Date dateToBeGenerated);

    public void doGenerate(Date date) {
        if (isRecoveryRunning) {
            logger.info("Recovery is running. Skip Schedule");
            return;
        }
        generateReport(date);
    }

    protected void printErrorMessage(Exception e) {
        if (e instanceof NoRecordsFoundException) {
            logger.warn(e.getLocalizedMessage());
        } else {
            logger.error("Error while generating report", e);
        }
    }
}
