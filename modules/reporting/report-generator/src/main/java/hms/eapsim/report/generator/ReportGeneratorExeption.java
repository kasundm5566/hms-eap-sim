package hms.eapsim.report.generator;

/**
 * Report generation related error scenarios
 */
public class ReportGeneratorExeption extends Exception {
    public ReportGeneratorExeption() {
    }

    public ReportGeneratorExeption(String message) {
        super(message);
    }

    public ReportGeneratorExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public ReportGeneratorExeption(Throwable cause) {
        super(cause);
    }

    public ReportGeneratorExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
