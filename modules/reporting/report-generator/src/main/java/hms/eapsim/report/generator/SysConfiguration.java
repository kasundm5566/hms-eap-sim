package hms.eapsim.report.generator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:report-generator.properties")
@ImportResource("classpath:spring-context.xml")
public class SysConfiguration {

    @Value("${enable.report.recovery}")
    private boolean isRecoveryEnabled;

    @Value("${reports.recovery.dates}")
    private String recoveryStartDate;

    @Value("${report.generate.directory}")
    private String reportBaseDirectory;

    @Value("${dir.create.file.format.prefix}")
    private String dirCreateFileFormatPrefix;


    @Value("${dir.create.file.suffix.date.format}")
    private String dirCreateFileFormat;


    @Value("${csv.report.separator}")
    private char separator;

    @Value("${csv.report.quote.char}")
    private char quoteChar;

    @Value("${csv.report.date.format}")
    private String reportDateFormat;

    @Value("${csv.report.time.format}")
    private String reportTimeFormat;

    @Value("${csv.report.hour.format}")
    private String reportHourFormat;

    @Value("${csv.report.percentage.format}")
    private String reportPercentageFormat;


    @Value("${is.header.required}")
    private boolean isHeaderRequired;

    @Value("${report.column.names.separator}")
    private String columnNamesSeparator;

    @Value("${hourly.trx.summary.report.column.names}")
    private String hourlyTrxSummaryReportColumnNames;

    @Value("${daily.trx.summary.report.column.names}")
    private String dailyTrxSummaryReportColumnNames;

    @Value("${hourly.location.based.trx.summary.report.column.names}")
    private String hourlyLocationBasedTrxSummaryReportColumnNames;

    @Value("${daily.location.based.trx.summary.report.column.names}")
    private String dailyLocationBasedTrxSummaryReportColumnNames;

    @Value("${report.file.extension}")
    private String reportFileExtension;

    @Value("${hourly.trx.summary.report.file.name}")
    private String hourlyTrxSummaryReportFileName;

    @Value("${daily.trx.summary.report.file.name}")
    private String dailyTrxSummaryReportFileName;

    @Value("${hourly.location.based.trx.summary.report.file.name}")
    private String hourlyLocationBasedTrxSummaryReportFileName;

    @Value("${daily.location.based.trx.summary.report.file.name}")
    private String dailyLocationBasedTrxSummaryReportFileName;

    @Value("${hourly.trx.summary.sql}")
    private String hourlyTrxSummarySql;

    @Value("${daily.trx.summary.sql}")
    private String dailyTrxSummarySql;

    @Value("${hourly.location.based.trx.summary.sql}")
    private String hourlyLocationBasedTrxSummarySql;

    @Value("${daily.location.based.trx.summary.sql}")
    private String dailyLocationBasedTrxSummarySql;


    public String getReportBaseDirectory() {
        return reportBaseDirectory;
    }

    public String getDirCreateFileFormatPrefix() {
        return dirCreateFileFormatPrefix;
    }

    public String getDirCreateFileFormat() {
        return dirCreateFileFormat;
    }

    public char getSeparator() {
        return separator;
    }

    public char getQuoteChar() {
        return quoteChar;
    }

    public String getReportDateFormat() {
        return reportDateFormat;
    }

    public String getReportTimeFormat() {
        return reportTimeFormat;
    }

    public String getReportHourFormat() {
        return reportHourFormat;
    }

    public String getReportPercentageFormat() {
        return reportPercentageFormat;
    }

    public boolean isHeaderRequired() {
        return isHeaderRequired;
    }

    public String getColumnNamesSeparator() {
        return columnNamesSeparator;
    }

    public String getHourlyTrxSummaryReportColumnNames() {
        return hourlyTrxSummaryReportColumnNames;
    }

    public String getDailyTrxSummaryReportColumnNames() {
        return dailyTrxSummaryReportColumnNames;
    }

    public String getHourlyLocationBasedTrxSummaryReportColumnNames() {
        return hourlyLocationBasedTrxSummaryReportColumnNames;
    }

    public String getDailyLocationBasedTrxSummaryReportColumnNames() {
        return dailyLocationBasedTrxSummaryReportColumnNames;
    }

    public String getReportFileExtension() {
        return reportFileExtension;
    }

    public String getHourlyTrxSummaryReportFileName() {
        return hourlyTrxSummaryReportFileName;
    }

    public String getDailyTrxSummaryReportFileName() {
        return dailyTrxSummaryReportFileName;
    }

    public String getHourlyLocationBasedTrxSummaryReportFileName() {
        return hourlyLocationBasedTrxSummaryReportFileName;
    }

    public String getDailyLocationBasedTrxSummaryReportFileName() {
        return dailyLocationBasedTrxSummaryReportFileName;
    }

    public String getHourlyTrxSummarySql() {
        return hourlyTrxSummarySql;
    }

    public String getDailyTrxSummarySql() {
        return dailyTrxSummarySql;
    }

    public String getHourlyLocationBasedTrxSummarySql() {
        return hourlyLocationBasedTrxSummarySql;
    }

    public String getDailyLocationBasedTrxSummarySql() {
        return dailyLocationBasedTrxSummarySql;
    }

    public boolean isRecoveryEnabled() {
        return isRecoveryEnabled;
    }

    public String getRecoveryStartDate() {
        return recoveryStartDate;
    }
}
