package hms.eapsim.report.generator.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"hms.eapsim.report.generator"})
@EnableScheduling
public class ReportGeneratorStarter {


    public static void main(String[] args) {
        SpringApplication.run(ReportGeneratorStarter.class, args);
    }
}
