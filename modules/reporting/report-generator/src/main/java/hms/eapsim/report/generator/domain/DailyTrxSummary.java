package hms.eapsim.report.generator.domain;

import java.util.Date;

public class DailyTrxSummary {

    private Date date;
    private String protocol;
    private String authenticationType;
    private long successCount;
    private long errorCount;
    private long totalTransactions;
    private float successRate;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }

    public long getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(long totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public float getSuccessRate() {
        return successRate;
    }

    public void setSuccessRate(float successRate) {
        this.successRate = successRate;
    }

    @Override
    public String toString() {
        return "DailyTrxSummary{" +
                "date='" + date + '\'' +
                ", protocol='" + protocol + '\'' +
                ", authenticationType=" + authenticationType +
                ", successCount=" + successCount +
                ", errorCount=" + errorCount +
                ", totalTransactions=" + totalTransactions +
                ", successRate=" + successRate +
                '}';
    }
}
