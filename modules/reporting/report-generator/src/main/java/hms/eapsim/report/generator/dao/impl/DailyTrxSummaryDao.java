package hms.eapsim.report.generator.dao.impl;

import hms.eapsim.report.generator.dao.TrxSummaryDao;
import hms.eapsim.report.generator.domain.DailyTrxSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class DailyTrxSummaryDao extends JdbcDaoSupport implements TrxSummaryDao<DailyTrxSummary> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<DailyTrxSummary> find(String query, Object[] params) {
        List<DailyTrxSummary> dailyTrxSummaries = getJdbcTemplate().query(query, params, new RowMapper<DailyTrxSummary>() {
            @Override
            public DailyTrxSummary mapRow(ResultSet rs, int i) throws SQLException {
                DailyTrxSummary dailyTrxSummary = new DailyTrxSummary();
                dailyTrxSummary.setDate(rs.getTimestamp(1));
                dailyTrxSummary.setProtocol(rs.getString(2));
                dailyTrxSummary.setAuthenticationType(rs.getString(3));
                dailyTrxSummary.setSuccessCount(rs.getLong(4));
                dailyTrxSummary.setErrorCount(rs.getLong(5));
                dailyTrxSummary.setTotalTransactions(rs.getLong(6));
                dailyTrxSummary.setSuccessRate(rs.getFloat(7));
                return dailyTrxSummary;
            }
        });
        return dailyTrxSummaries;
    }
}
