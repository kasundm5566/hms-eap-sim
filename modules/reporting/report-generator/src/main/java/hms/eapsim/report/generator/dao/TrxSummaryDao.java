package hms.eapsim.report.generator.dao;

import java.util.List;

public interface TrxSummaryDao<T> {

    List<T> find(String query, Object[] params);

}
