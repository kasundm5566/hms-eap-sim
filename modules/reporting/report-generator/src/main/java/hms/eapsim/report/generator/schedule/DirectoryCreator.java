package hms.eapsim.report.generator.schedule;

import hms.eapsim.report.generator.ReportGeneratorExeption;
import hms.eapsim.report.generator.SysConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This schedule responsible for creating directory structure so that report generators can
 * generate reports on those locations
 */

@Component
public class DirectoryCreator {

    private static final Logger logger = LoggerFactory.getLogger(DirectoryCreator.class);
    @Autowired
    private SysConfiguration configuration;

    @PostConstruct
    public void init() throws ReportGeneratorExeption {
        logger.info("Directory creation is initializing");

        validate();

        createDirectoryForYesterday();
        logger.info("Directory creation is initialized");
    }

    private void validate() throws ReportGeneratorExeption {
        File parent = new File(configuration.getReportBaseDirectory());

        if (parent.exists()) {
            if (!parent.isDirectory()) {
                throw new ReportGeneratorExeption("Parent directory is not a directory [" + parent.getAbsolutePath() + "]");
            } else if (!parent.canWrite()) {
                throw new ReportGeneratorExeption("Parent directory is not writable [" + parent.getAbsolutePath() + "]");
            }
        } else {
            throw new ReportGeneratorExeption("Parent dir [" + parent.getAbsolutePath() + "] not exist");
        }
    }

    @Scheduled(cron = "${dir.create.cron.expression}")
    public void createDirectory() {
        try {
            createDirectoryForToday();
        } catch (Exception e) {
            logger.error("Error while creating directory");
        }
    }

    private void createDirectoryForYesterday() throws ReportGeneratorExeption {
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, -1);

        createDirectory(today.getTime());
    }

    void createDirectory(Date today) throws ReportGeneratorExeption {
        String dirName = createDirectoryName(configuration.getDirCreateFileFormatPrefix() == null ? ""
                : configuration.getDirCreateFileFormatPrefix(), today);

        if (dirName.isEmpty()) {
            throw new ReportGeneratorExeption("Can not create directory for empty name");
        }
        File dir = new File(configuration.getReportBaseDirectory(), dirName);

        if (dir.exists()) {
            logger.debug("Directory [{}] is already exist. No need to create", dir.getAbsolutePath());
        } else {
            logger.debug("Creating directory [{}]", dir.getAbsolutePath());
            try {
                boolean status = dir.mkdirs();
                logger.info("Directory created [ dir: {}] [ status: {}]", dir.getAbsolutePath(),
                        status ? "CREATED" : "NOT_CREATED");
            } catch (SecurityException e) {
                throw new ReportGeneratorExeption("No permission to create directory [" + dir.getAbsolutePath() + "]");
            }
        }
    }

    private String createDirectoryName(String prefix, Date date) {
        SimpleDateFormat format = new SimpleDateFormat(configuration.getDirCreateFileFormat());

        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder.append(prefix).append(format.format(date));
        return nameBuilder.toString();
    }

    private void createDirectoryForToday() throws ReportGeneratorExeption {
        createDirectory(new Date());
    }
}
