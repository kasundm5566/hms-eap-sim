package hms.eapsim.report.generator.domain;

import java.util.Date;

public class HourlyTrxSummary {

    private Date date;
    private long simCount;
    private long akaCount;
    private long fullAuthCount;
    private long reAuthCount;
    private long pseudoFullAuthCount;
    private long encryptedFullAuthCount;
    private long successCount;
    private long errorCount;
    private long totalTransactions;
    private float successRate;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getSimCount() {
        return simCount;
    }

    public void setSimCount(long simCount) {
        this.simCount = simCount;
    }

    public long getAkaCount() {
        return akaCount;
    }

    public void setAkaCount(long akaCount) {
        this.akaCount = akaCount;
    }

    public long getFullAuthCount() {
        return fullAuthCount;
    }

    public void setFullAuthCount(long fullAuthCount) {
        this.fullAuthCount = fullAuthCount;
    }

    public long getReAuthCount() {
        return reAuthCount;
    }

    public void setReAuthCount(long reAuthCount) {
        this.reAuthCount = reAuthCount;
    }

    public long getPseudoFullAuthCount() {
        return pseudoFullAuthCount;
    }

    public void setPseudoFullAuthCount(long pseudoFullAuthCount) {
        this.pseudoFullAuthCount = pseudoFullAuthCount;
    }

    public long getEncryptedFullAuthCount() {
        return encryptedFullAuthCount;
    }

    public void setEncryptedFullAuthCount(long encryptedFullAuthCount) {
        this.encryptedFullAuthCount = encryptedFullAuthCount;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }

    public long getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(long totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public float getSuccessRate() {
        return successRate;
    }

    public void setSuccessRate(float successRate) {
        this.successRate = successRate;
    }

    @Override
    public String toString() {
        return "HourlyTrxSummary{" +
                "date=" + date +
                ", simCount=" + simCount +
                ", akaCount=" + akaCount +
                ", fullAuthCount=" + fullAuthCount +
                ", reAuthCount=" + reAuthCount +
                ", pseudoFullAuthCount=" + pseudoFullAuthCount +
                ", encryptedFullAuthCount=" + encryptedFullAuthCount +
                ", successCount=" + successCount +
                ", errorCount=" + errorCount +
                ", totalTransactions=" + totalTransactions +
                ", successRate=" + successRate +
                '}';
    }
}
