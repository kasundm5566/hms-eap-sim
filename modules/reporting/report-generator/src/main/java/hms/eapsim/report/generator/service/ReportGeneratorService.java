package hms.eapsim.report.generator.service;

import hms.eapsim.report.generator.ReportGeneratorExeption;

import java.util.Date;

public interface ReportGeneratorService {

    void generateHourlyTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption;

    void generateDailyTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption;

    void generateHourlyLocationBasedTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption;

    void generateDailyLocationBasedTrxSummaryReport(String location, Date date) throws ReportGeneratorExeption;

}
