package hms.eapsim.report.generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Prints the status of report generation
 */
public class StatusLog {
    private static final String SEPARATOR = "|";
    private static final Logger logger = LoggerFactory.getLogger("statusLog");

    public static final void log(Date reportDate, ReportName reportName, Date start, Date end, ReportGenerateState state, String description) {
        SimpleDateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
        StringBuilder record = new StringBuilder();
        record.append(reportName).append(SEPARATOR).append(formater.format(reportDate))
                .append(SEPARATOR).append((end.getTime() - start.getTime())).append(" ms");
        record.append(SEPARATOR).append(state.name()).append(SEPARATOR).append(description);
        logger.info(record.toString());
    }
}
