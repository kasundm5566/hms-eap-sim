package hms.eapsim.report.generator;

/**
 * Names of csv reports
 */
public enum ReportName {
    DAILY_LOCATION_BASED_TRX_REPORT,

    DAILY_TRX_REPORT,

    HOURLY_LOCATION_BASED_TRX_REPORT,

    HOURLY_TRX_REPORT,

}
