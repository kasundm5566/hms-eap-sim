package hms.eapsim.report.generator.dao.impl;

import hms.eapsim.report.generator.dao.TrxSummaryDao;
import hms.eapsim.report.generator.domain.HourlyLocationBasedTrxSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class HourlyLocationBasedTrxSummaryDao extends JdbcDaoSupport implements TrxSummaryDao<HourlyLocationBasedTrxSummary> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<HourlyLocationBasedTrxSummary> find(String query, Object[] params) {
        List<HourlyLocationBasedTrxSummary> hourlyLocationBasedTrxSummaries = getJdbcTemplate().query(query, params, new RowMapper<HourlyLocationBasedTrxSummary>() {
            @Override
            public HourlyLocationBasedTrxSummary mapRow(ResultSet rs, int i) throws SQLException {
                HourlyLocationBasedTrxSummary hourlyLocationBasedTrxSummary = new HourlyLocationBasedTrxSummary();
                hourlyLocationBasedTrxSummary.setDate(rs.getTimestamp(1));
                hourlyLocationBasedTrxSummary.setNasId(rs.getString(2));
                hourlyLocationBasedTrxSummary.setSimCount(rs.getLong(3));
                hourlyLocationBasedTrxSummary.setAkaCount(rs.getLong(4));
                hourlyLocationBasedTrxSummary.setFullAuthCount(rs.getLong(5));
                hourlyLocationBasedTrxSummary.setReAuthCount(rs.getLong(6));
                hourlyLocationBasedTrxSummary.setPseudoFullAuthCount(rs.getLong(7));
                hourlyLocationBasedTrxSummary.setEncryptedFullAuthCount(rs.getLong(8));
                hourlyLocationBasedTrxSummary.setSuccessCount(rs.getLong(9));
                hourlyLocationBasedTrxSummary.setErrorCount(rs.getLong(10));
                hourlyLocationBasedTrxSummary.setTotalTransactions(rs.getLong(11));
                hourlyLocationBasedTrxSummary.setSuccessRate(rs.getFloat(12));

                return hourlyLocationBasedTrxSummary;
            }
        });
        return hourlyLocationBasedTrxSummaries;
    }
}
