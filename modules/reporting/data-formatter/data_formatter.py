#!/usr/bin/python3

# (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
# All Rights Reserved.
#
# These materials are unpublished, proprietary, confidential source code of
# hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
# of hSenid Software International (Pvt) Limited.
#
# hSenid Software International (Pvt) Limited retains all title to and intellectual
# property rights in these materials.

import configparser
import os
import re
import shutil
import sys
import csv
import timeit
import abstract_formatter
from itertools import (takewhile,repeat)
import logging
from logging.handlers import TimedRotatingFileHandler

def rawincount(filename):
    f = open(filename, 'rb')
    bufgen = takewhile(lambda x: x, (f.raw.read(1024*1024) for _ in repeat(None)))
    return sum( buf.count(b'\n') for buf in bufgen )

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

Config = configparser.ConfigParser()
Config.read('data_formatter.conf')
## config loggers
log_enable=0
log_enable_str=Config['script-configurations']['log.enable']
if(log_enable_str.lower() == 'true'):
    log_enable=1
else:
    log_enable=0
formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
log_path=Config['script-configurations']['log.directory']
handler = TimedRotatingFileHandler(log_path+'formatter.log',when='midnight',backupCount=10)
handler.setFormatter(formatter)
logger = logging.getLogger('formatter')
logger.setLevel(logging.INFO)
if (log_enable):
    logger.addHandler(handler)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)

logger.info("=========== Starting ===========")
logger.info("=== Initializing configurations")
script_start_time = timeit.default_timer()
total_args = len(sys.argv)
cmd_args = sys.argv
show_progress_str=Config['script-configurations']['show.progress']
if(show_progress_str.lower() == 'true'):
    show_progress=1
else:
    show_progress=0

file_names=[]
log_scan_path = Config['file-configurations']['file.scan.directory']
output_path = Config['file-configurations']['file.out.directory']
log_file_match_regex = Config['file-configurations']['file.pattern']
file_delimiter=Config['file-configurations']['file.csv.delimiter']
operation_after_format=Config['file-configurations']['operation.after.format']
accepted_operations=['archive','rename','delete']
archive_directory=Config['file-configurations']['archive.directory']
rename_prefix=Config['file-configurations']['rename.prefix']
if (operation_after_format not in accepted_operations):
    logger.error("[ERROR] unkown operation mode for operation.after.format. Please use 'archive','rename' or 'delete' ")
    sys.exit()
if(total_args > 1):
    logger.info("=== Reading given file list")
    file_names = [fn for fn in cmd_args]
    del file_names[0]
else:
    logger.info("=== Reading default file list")
    file_names = [fn  for fn in os.listdir(log_scan_path) if re.match(log_file_match_regex, fn)]

logger.info("=== Loaded log file list")
num_columns = 3
file_name_row=""
count=0
for item in file_names:
    file_name_row=file_name_row + "\t" + log_scan_path + item
    count+=1
    if count % num_columns == 0:
        print(file_name_row)
        file_name_row=""
logger.info(file_name_row)
logger.info("")

logger.info("=== Loading analyzing scripts")
target_cloumns_temp = Config['formatting-directory-mapping']
target_columns = {}
target_column_dir=[]
formatting_modules={}
total_script_count=0
for x in target_cloumns_temp:
    target_columns.update({int(x): Config['formatting-directory-mapping'][x]})
    module_dir_name=Config['formatting-directory-mapping'][x]
    script_file_names = [fn for fn in os.listdir('./'+module_dir_name) if re.match('format_[a-zA-Z0-9_-]*.py$', fn)]
    if os.path.basename(__file__) in script_file_names: script_file_names.remove(os.path.basename(__file__))
    script_module_names = []
    for fn in script_file_names:
        script_module_names.append(os.path.splitext(os.path.basename(fn))[0])
    logger.info('=== Located formatting scripts for column : ' + x)
    logger.info(script_file_names)
    formatting_modules_temp = {}
    unIdentified = 0
    count = 0
    for script_file in script_module_names:
        formatting_module = __import__(module_dir_name + '.' + script_file, fromlist=[module_dir_name])
        if(issubclass(formatting_module.Formatter, abstract_formatter.AbstractFormatter)):
            formatting_modules_temp.update({formatting_module: 0})
            total_script_count+=1
            logger.info('[LOADED] ' + module_dir_name +'.' + script_file + ' { ' + formatting_module.Formatter.getInfoString() + ' }')
        else:
            logger.warning('[WARN] Formatting script '+ module_dir_name + '.' + script_file +' not loaded')
    formatting_modules.update({int(x):formatting_modules_temp})

logger.info('=== '+str(total_script_count) + " analyzing scripts loaded")
logger.info("")
logger.info("Log file output directory : " + output_path)
logger.info("=========== Processing selected log files ===========")

log_file_time={}
for log_file in file_names:
    start_time = timeit.default_timer()
    processed_log_file_path=output_path + log_file
    full_log_file_path = log_scan_path + log_file
    logger.info("")
    logger.info("> Processing log file : " + full_log_file_path + " -> "  + processed_log_file_path)
    total_row_count=0
    try:
        total_row_count = rawincount(full_log_file_path)
        with open(full_log_file_path, 'r') as f:
            with open(processed_log_file_path, 'w') as outfile:
                writer = csv.writer(outfile, delimiter=file_delimiter, quoting=csv.QUOTE_NONE)
                reader = csv.reader(f, delimiter=file_delimiter, quoting=csv.QUOTE_NONE)
                row_count=0
                for row in reader:
                    original_row = list(row)
                    column_offset = 0

                    for target_column in  formatting_modules.keys():
                        formatting_modules_temp = formatting_modules[target_column]
                        isIdetified = 0
                        data=original_row[target_column]
                        for module in formatting_modules_temp.keys():
                            formatted_data = module.Formatter.validateFormat(data,original_row,target_column)
                            if (formatted_data!=0):
                                isIdetified = 1
                                formatting_modules_temp[module] += 1
                                break

                        if(isIdetified==1):
                            new_target_column = target_column+column_offset
                            row.pop(new_target_column)
                            row[new_target_column:new_target_column]=formatted_data
                            if(formatted_data.__len__() > 1):
                                column_offset+= (formatted_data.__len__()-1)
                    row_count+=1
                    writer.writerow(row)
                    if(show_progress):
                        if(((row_count/total_row_count)*100)%5 <= 0.0001):
                            printProgressBar(row_count, total_row_count, prefix='Progress:', suffix='Complete', length=50)
        if(operation_after_format == 'archive'):
            shutil.move(full_log_file_path,archive_directory+log_file)
            logger.info('> File archived : ' + full_log_file_path + ' > ' + archive_directory+log_file)
        elif (operation_after_format == 'rename'):
            os.rename(full_log_file_path,log_scan_path + rename_prefix + log_file)
            logger.info('> File renamed : ' + full_log_file_path + ' > ' + log_scan_path + rename_prefix + log_file)
        elif (operation_after_format == 'delete'):
            os.remove(full_log_file_path)
            logger.info('> File deleted : ' + full_log_file_path)
        elapsed = timeit.default_timer() - start_time
        log_file_time.update({log_file:{'time':elapsed,'row_count':total_row_count}})
    except Exception as ex:
        logger.error("> [ERROR] File " + full_log_file_path + " not processed :: " + str(ex))
logger.info("")
logger.info("=========== Statistics ===========")
logger.info("")
for x in formatting_modules:
    logger.info('> Column ' + str(x) + ' formatting stats')
    for y in formatting_modules[x]:
        logger.info('  '+y.Formatter.getInfoString()+ ' matches : '+ str(formatting_modules[x][y]))

logger.info('')
logger.info('> Log time stats')
total_time=0
file_count=0
for x in log_file_time:
    total_time+=log_file_time[x]['time']
    file_count+=1
    if(log_file_time[x]['time'] == 0):
        logger.info('  ' + str(x) + ' -> ' + '{0:.6f}'.format(log_file_time[x]['time']) + ' s for\t' + str(
            log_file_time[x]['row_count']) + ' log entries')
    else:
        logger.info('  ' + str(x) + ' -> ' + '{0:.6f}'.format(log_file_time[x]['time']) + ' s for\t' + str(
            log_file_time[x]['row_count']) + ' log entries at (' + '{0:.3f}'.format(
            log_file_time[x]['row_count'] / log_file_time[x]['time']) + ' rows/sec)')
logger.info('')
if(file_count==0):
    logger.info('> ' + 'Total time ' + '{0:.6f}'.format(total_time) + ' s for ' + str(
        file_count) + ' files')
else:
    logger.info('> ' + 'Total time ' + '{0:.6f}'.format(total_time) + ' s for ' + str(file_count) + ' files at ' +  '{0:.3f}'.format(total_time/file_count) +' sec/file' )
logger.info('')
elapsed = timeit.default_timer() - script_start_time
logger.info('> Total script time : ' + '{0:.6f}'.format(elapsed) + ' s')
logger.info('=========== Formatting done ===========')
