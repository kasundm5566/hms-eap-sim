# DOCUMENTATION
## Data Formatter 

---
### Table of Contents
- Introduction
- Flow
- Using The Script
- Configuration
- Writing Custom Formatters
- Sample Formatters 

### Introduction
This python script will format the csv file by using custom formatting scritps (*see Writing custom formatters section for more information*). Script iterate through all rows by appling formatting to each column an it will generate new csv formatted file. Script will not modify any content of the original file. However user can configure what to do with the processed file. (*see Configuration section for more information*)
### Flow
Script will first either scan the directory and load the files that macth the filename with regex pattern defined in the configuration file or load the files given in the parameter list ( *see Using the script section for more information*)
Then it will start to process each loaded files using custom formatting scripts

After that it will write a new processed file in directory defined in the in the configuration file and script can either rename , delete or move original file. This operation can be configured in the configuration file
Finally it will display the statistics
### Using the script
Script can be run in linux shell in two modes.

**Scan all files mode**
Just run the script without any parameters ans it will process all the files defined in the *file.scan.directory*
```sh
$ ./data_formatter.py
```

**Custom files mode**
Give file names as individual parameters so that script will do processing on those specified files only. Note that this will skip the *file.pattern* match. So given names not need to match file name to the regex *file.pattern*. (*see Configuration section for more information about file.pattern*)
```sh
$ ./data_formatter.py trans.log error.log
```
### Configuration
All configurations are defined in the *data_formatter.conf* file. It need to be in the same directory that *data_formatter.py* script is running.

#### [file-configurations] section

**file.scan.directory** : Script will scan files in this directory  ( Note :  when defining directory please make sure that directory will end with / *eg: ./input_logs/*  )</br>
**file.out.directory** : Script will write processed files in to this directory with tha same name of processed file</br>
**file.pattern** : Script will process files if file name match for this regex in Scan all file mode. If script is running in Custom files mode it will bypass the file name match</br>
**file.csv.delimiter** : This character will use as the csv delimiter character</br>
**operation.after.format** : </br>
Set the operation to perform on original file after the formatting complete
Three options
   1. **archive** : in this mode script will move the processed file to the directory defined in the archive.directory
   2. **delete** : in this this mode script will delete the processed file from the file.scan.directory
   3. **rename** : in this mode script will rename the original file by adding prefix defined in the rename.prefix
  
Note : make sure that file name after adding the prefix will not match the file.pattern regex otherwise script will again format the same file in next time also 

**archive.directory** : Processed file archiving directory</br>
**rename.prefix** : Processed file renaming prefix *( See operation.after.format section for more information)*</br>

#### [formatting-directory-mapping] section
In this section define the custom column formatting script mapping. Note that column number is a zero based index
If you want to format a column,
   1. Add new directory containing specific column formatting scripts ( please use only lowercase alphebetic charactes and _ )
   2. Set a column number,directory mapping in formatting-directory-mapping section of this configuration file. Ex: If you want to format column number 3 and you placed all column 3 formatting scripts in directory column_three_formattng then you should add a new configuraion entry in formatting-directory-mapping ***3=column_three_formattng*** . Idea is just to map column number to the directory that contain its formatters.
   3. Name formatting script file in ***format_[a-zA-Z0-9_-]*.py** regex format
   4. This Script should extend from abstact class AbstractFormatter in abstract_formatter.py *(See Writing Custom Formatters section for more information)*

#### [script-configurations] section

**show.progress** : Two options, *true* will show progress bar in console during the processing and *false (or other)* will not show the progressbar. *Deafult is false*</br>
**log.directory** : Script log directory</br>
**log.enable** : Enabling or Disabling of script logging can be defined here. Two options  *true* will enable script logging and *false (or other)* disable it. *Deafult is false*</br>

###  Writing Custom Formatters
Custom formatter is a python class extending from abstract class AbstractFormatter defined in abstract_formatter.py module. To write a newcustom formatter create a new module ad described in the ***[formatting-directory-mapping] section*** and create a new Formatter class extending from AbstractFormatter. Following code template can be used to create a new custom formatter.

```py
import abstract_formatter

class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
        formatted_list = [target_col_val]
        # Add your column formatting logic here
        return formatted_list

    def getInfoString():
        return 'Add your formatter type info string here'
```

Any Formatter class need to implement two functions validateFormat and getInfoString

***validateFormat***
This function will recieve theree parameters.target_col_val,row,target_col_index
target_col_val is the value of the clumn that need to format in addition to that all row vector and the tarhet clolumn index is also passed to the Formatter class by formtting script. Do any formatting to the target_col_val.
There are four type of return options
1. 0 : return zero if this formatter is not interested in formatting the given value. And if zero is returned formatter script will look for another formatters for the same column
2. [] : return empty list if you need to delete the column
3. ['Value'] : return one value list if you want to replace the given column value
4. ['Value1', 'Value2', ... ,'ValueN'] : return list with more than one element if you want to replace given column with several new columns
( Note : Except for returning 0 main formatter script will stop calling other formatters for same column for this row.)

### Sample Formatters 
***Delete Column***
```py
import abstract_formatter

class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
        return []

    def getInfoString():
        return 'Column N Delete'
```

***Convert to upper case***
```py
import abstract_formatter

class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
        return [target_col_val.upper()]

    def getInfoString():
        return 'Column N Upper'
```

***Split IP and Port***
```py
import re
import abstract_formatter

class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
         __format = "^((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])):?([0-9]{1,5})?$"
        matchObj = re.match(__format, target_col_val)
        if matchObj:
            return [matchObj.group(1), matchObj.group(5)]
        else:
            return 0

    def getInfoString():
        return 'IP : PORT Split'
```
