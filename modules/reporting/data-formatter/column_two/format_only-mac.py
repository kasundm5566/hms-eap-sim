# (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
# All Rights Reserved.
#
# These materials are unpublished, proprietary, confidential source code of
# hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
# of hSenid Software International (Pvt) Limited.
#
# hSenid Software International (Pvt) Limited retains all title to and intellectual
# property rights in these materials.

import re
import abstract_formatter

# This will split mac and nas id into two columns , MAC Will be in lowercase
# |00:0a:FF:cc:cc:ff| -> |00:0a:ff:cc:cc:ff|
# if next column is empty or it is not a Nas id it will we filled with default value

class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
        __format = "^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$"
        __default_nas = 'DEFAULT'
        matchObj = re.match(__format, target_col_val)
        if matchObj:
            __format_nas="^[A-Z0-9-]*$"
            matchNas = re.match(__format_nas, row[target_col_index+1])
            if(matchNas):
                return [target_col_val.lower()]
            else:
                return [target_col_val.lower(), __default_nas]
        else:
            return 0

    def getInfoString():
        return "Mac Only"
