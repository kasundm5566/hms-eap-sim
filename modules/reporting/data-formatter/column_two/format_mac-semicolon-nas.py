# (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
# All Rights Reserved.
#
# These materials are unpublished, proprietary, confidential source code of
# hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
# of hSenid Software International (Pvt) Limited.
#
# hSenid Software International (Pvt) Limited retains all title to and intellectual
# property rights in these materials.

import re
import abstract_formatter

# This will split mac and nas id into two columns , MAC Will be in lowercase
# |00:0a:95:9d:cc:16:M1NET-WSG12-ROC| -> |00:0a:95:9d:cc:16|M1NET-WSG12-ROC|
class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
        __format = '^(([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])):([A-Z0-9-]*)$'
        matchObj = re.match(__format, target_col_val)
        if matchObj:
            return [matchObj.group(1).lower(), matchObj.group(4)]
        else:
            return 0

    def getInfoString():
        return "Mac : Nas"
