# (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
# All Rights Reserved.
#
# These materials are unpublished, proprietary, confidential source code of
# hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
# of hSenid Software International (Pvt) Limited.
#
# hSenid Software International (Pvt) Limited retains all title to and intellectual
# property rights in these materials.

import re
import abstract_formatter

# This will create new nad id if nas id not available in next column and fill it with the default value
# |192.168.1.1|| -> |192.168.1.1|DEFAULT_NAS|
# |192.168.1.1|M1NET-WSG12-ROC| -> |192.168.1.1|M1NET-WSG12-ROC|

class Formatter(abstract_formatter.AbstractFormatter):
    def validateFormat(target_col_val,row,target_col_index):
        __format = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
        __default_nas='DEFAULT'
        matchObj = re.match(__format, target_col_val)
        if matchObj:
            __format_nas = "^[A-Z0-9-]*$"
            matchNas = re.match(__format_nas, row[target_col_index + 1])
            if (matchNas):
                return [target_col_val]
            else:
                return [target_col_val, __default_nas]
        else:
            return 0

    def getInfoString():
        return "No-Mac No-Nas Only IP"
