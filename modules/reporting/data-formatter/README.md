# README
## Data Formatter
***This python script will format the csv file by using custom formatting scritps***

*This README only describe how to setup the data_formatter.py , For more information please refer the documentation at doc/DOC.md*

---
### Set basic configurations
1. Set the file scaning directory that contains the csv files to process
directory canbe relative or absolut. Make sure that directory name ends with /
`file.scan.directory=./input_logs/`
`file.scan.directory=/hms/log/unformatted/`
2. Set the file output directory. To this directory new formatted files will be written
`file.out.directory=./out/`
`file.out.directory=/hms/log/formatted/`
3. Set file name reg ex pattern to scan and filter the files in file.scan.directory 
`file.pattern=trans.log.[0-9-]*$`
4. Set post processing method.
`operation.after.format=[rename|delete|archive]` 
Set the operation to perform on original file after the formatting complete
Three options
   1. **archive** : in this mode script will move the processed file to the directory defined in the archive.directory
   2. **delete** : in this this mode script will delete the processed file from the file.scan.directory
   3. **rename** : in this mode script will rename the original file by adding prefix defined in the rename.prefix
  
Note : make sure that file name after adding the prefix will not match the file.pattern regex otherwise script will again format the same file in next time also

### Run the script
Script can be run in linux shell in two modes.

**Scan all files mode**
Just run the script without any parameters ans it will process all the files defined in the *file.scan.directory*
```sh
$ ./data_formatter.py
```
**Custom files mode**
Give file names as individual parameters so that script will do processing on those specified files only. (*see doc/DOC.md for more information*)
```sh
$ ./data_formatter.py trans.log error.log
```
