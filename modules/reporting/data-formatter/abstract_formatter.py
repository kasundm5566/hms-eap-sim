# (C) Copyright 2008-2017 hSenid Software International (Pvt) Limited.
# All Rights Reserved.
#
# These materials are unpublished, proprietary, confidential source code of
# hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
# of hSenid Software International (Pvt) Limited.
#
# hSenid Software International (Pvt) Limited retains all title to and intellectual
# property rights in these materials.

import abc

class AbstractFormatter(abc.ABC):

    @staticmethod
    @abc.abstractmethod
    def validateFormat(target_col_val,row,target_col_index):
        raise NotImplementedError

    @staticmethod
    @abc.abstractmethod
    def getInfoString():
        raise NotImplementedError
