/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.simulator

import org.tinyradius.util.RadiusClient
import org.tinyradius.packet.AccessRequest
import org.tinyradius.dictionary.AttributeType
import org.tinyradius.attribute.RadiusAttribute
import hms.common.radius.util._
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.decoder.eap.EapMessageDecoder
import hms.common.radius.decoder.eap.elements._
import io.netty.buffer.Unpooled
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype
import hms.eapsim.util.{EapsimAttributeBuilderComponent}
import hms.eapsim.repo.SimpleRequestRepo
import hms.eapsim.key.DummyKeyManager
import hms.eapsim.key.KeyManagerComponent
import hms.eapsim.key.DummyFips186PRNGenerator
import hms.eapsim.key.KeyGeneratorComponent
import hms.eapsim.key.Fips186RandomGeneratorComponent
import hms.eapsim.key.KeyManager
import scala.collection.JavaConversions._
import hms.common.radius.decoder.chain.impl.EapAttributeDecoder
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSim
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSimSubtype
import java.util.Arrays
import hms.eapsim.repo.TranslogRepositoryComponent
import hms.eapsim.repo.TranslogRepository
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.{AbstractEapSimAka, EapSimAkaAttributeTypes}
import scala.Some
import hms.eapsim.repo.TransLog
import hms.eapsim.EapSimResult

/**
 *
 */

trait AttributeBuilder extends EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with Fips186RandomGeneratorComponent {
  val eapsimAttributeBuilder = new EapsimAttributeBuilder
  val requestRepo = new SimpleRequestRepo(10000) with TranslogRepositoryComponent {
    val translogRepository = new TranslogRepository {
      def log(entry: TransLog)  = {
        println("TL===>" + entry)
      }
    }
  }
  val keyGenerator: KeyGenerator = new KeyGenerator
  val keyManager: KeyManager = new DummyKeyManager
  val fips186Generator = new DummyFips186PRNGenerator
}

object RadiusRequests {
  var counter : Int = 0
  var lastAuthId: Option[String] = Some("")

  val SharedSecret = "test123"
  val radiusClient: RadiusClient = new RadiusClient("127.0.0.1", SharedSecret)
  var previousReceivedPacket: Option[RadiusPacket] = None
  val attributeBuilder = new AttributeBuilder {}
  val eapAttributeDecoder = new EapAttributeDecoder(null)

  val iv = "9e18b0c29a652263c06efb54dd00a895"

  val SimSres = "1AC2E6202AC2E6203AC2E620"
  val SimKaut = "660af7fc12a275baeae6e061906604f1"
  val SimEncr = "5c90af0da35c5046446bd235165805b2"

  val AkaRes = "3fe524d57101d202"
  val AkaKaut = "d8cb2d4f44e8b633ebab64dffeb77b5f"
  val AkaEncr = "c456037004ccf170916205184c604499"

  var Aka3GSimEncr = "6e3d00630cd8b5bee03f7073b7cc0510"
  var Aka3GSimKaut = "2e0d0ea0125a29a49e90c71afae9f033"
  var Aka3GSimSres = "4ee4f6d74ee4f6d7"

  def sendIdentityUnknown = {
    val request = identityReqWithImsi("525000000000000")
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendIdentity2G = {
    val request = accessReqWithIdentity("333230")
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendClientError(isAka: Boolean = true) = {
    val request = sendClientErrorResponse(isAka)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendIdentityReAuth2G = {
    val request = accessReAuthReqWithIdentity
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendIdentityReAuth3G = {
    val request = accessReAuthReqWithIdentity
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendIdentityReAuth3GWithAtIdentity = {
    val request = accessReAuthReqWithAtIdentity
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }


  def sendReAuthenticateResponse2G = {
    val COUNTER_TOO_SMALL_ENABLE = false
    val request = accessReAuthReqWithChallenge(COUNTER_TOO_SMALL_ENABLE)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendReAuthenticateResponse3GForGSM = {
    val COUNTER_TOO_SMALL_ENABLE = false
    val IS_3G_GSM_FLOW = true
    val request = accessReAuthReqWithChallenge(COUNTER_TOO_SMALL_ENABLE, IS_3G_GSM_FLOW)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendReAuthenticateResponseWithCounterTooSmallForGSM = {
    val COUNTER_TOO_SMALL_ENABLE = true
    val IS_3G_GSM_FLOW = true
    val request = accessReAuthReqWithChallenge(COUNTER_TOO_SMALL_ENABLE, IS_3G_GSM_FLOW)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendReAuthenticateResponse3G = {
    val COUNTER_TOO_SMALL_ENABLE = false
    val request = accessReAuthReqWithChallenge(COUNTER_TOO_SMALL_ENABLE)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendReAuthenticateResponseWithCounterTooSmallFlag2G = {
    val COUNTER_TOO_SMALL_ENABLE = true
    val request = accessReAuthReqWithChallenge(COUNTER_TOO_SMALL_ENABLE)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendReAuthenticateResponseWithCounterTooSmallFlag3G = {
    val COUNTER_TOO_SMALL_ENABLE = true
    val request = accessReAuthReqWithChallenge(COUNTER_TOO_SMALL_ENABLE)
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendIdentity3G = {
    val request = accessReqWithIdentity("313233", "30")
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    extractNextReAuthIdFromChallenge(AkaEncr)
    printResponse(resp)
  }

  def sendIdentity3GWithAtIdentity = {
    val request = accessReqWithAtIdentity
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    extractNextReAuthIdFromChallenge(AkaEncr)
    printResponse(resp)
  }


  def sendIdentityWithoutUserId = {
    val request = accessRequestWithoutIdentity
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendIdentity3GWithGsmAuth = {
    val request = accessReqWithIdentity("313233", "31")
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def sendEapSimStartResp  {
    sendRespPacket(accessReqWithEapSimStartResp)
    extractNextReAuthIdFromChallenge(SimEncr)
  }

  def sendEapSimStartRespFor3GGSM  {
    sendRespPacket(accessReqWith3GGSMStartResp)
    extractNextReAuthIdFromChallenge(Aka3GSimEncr)
  }

  def extractNextReAuthIdFromChallenge(key: String) {
    val eapMsg  =
      previousReceivedPacket.map(pkt => EapMessageDecoder.decode(Unpooled.copiedBuffer(pkt.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData())))
    eapMsg.map(m => extractReAuthId(m, key))
  }

  def sendEapSimStartRespForReAuth  {
    val request = createSimStartRespForReAuth
    printRequest(request)
    val resp = radiusClient.communicate(request, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }


  def extractReAuthId(eapMsg: EapMessage, key: String) {

    eapMsg match {
      case eapRRM: EapRequestResponseMessage => doExtractReAuthId(eapRRM, key)
      case _ => println("Invalid message type for find re-auth id")
    }
  }

  def use2VectorFor3GSimFlow() {
    Aka3GSimEncr = "6e3d00630cd8b5bee03f7073b7cc0510"
    Aka3GSimKaut = "2e0d0ea0125a29a49e90c71afae9f033"
    Aka3GSimSres = "4ee4f6d74ee4f6d7"
  }

  def use3VectorFor3GSimFlow() {
      Aka3GSimEncr = "b1b342caf02648cf0b17cd183d7544d9"
      Aka3GSimKaut = "ffb4e771466f68d4a548310eff98801e"
      Aka3GSimSres = "4ee4f6d74ee4f6d74ee4f6d7"
  }

  def doExtractReAuthId(eapSim: EapRequestResponseMessage, key: String) {
    eapSim.getType match {
      case sim: AbstractEapSimAka =>
        try {
          val atNcrData = sim.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA)
          val attList = attributeBuilder.eapsimAttributeBuilder.decordEncrData(HexCodec.hex2Byte(iv), HexCodec.hex2Byte(key), atNcrData)

          val filteredIdList = attList.filter(x => x.getType.getValue == EapSimAkaAttributeTypes.AT_NEXT_REAUTH_ID.getValue)
          require(filteredIdList.size > 0, "AT_NEXT_REAUTH_ID expected in the encrypted list")
          val idData = Unpooled.copiedBuffer(filteredIdList.get(0).getAttributeData)
          val length = idData.readShort()
          val nextId = new Array[Byte](length)
          idData.readBytes(nextId)

          println("========================")
          lastAuthId = Some(new String(nextId))
          println(s"AT_NEXT_REAUTH_ID : $lastAuthId}")
          println("========================")
        }
        catch {
          case e: Exception => print("Trying to decoded re-auth id for unsupported message type")
        }

      case _ => println("No need to extract NEXT-RE-AUTH-ID")
    }
  }

  def sendEapSimChallengeResp = {
    sendRespPacket(accessChallengeRespSim)
    val result: Option[Boolean] = previousReceivedPacket.map(r => {
      val decode = EapMessageDecoder.decode(Unpooled.copiedBuffer(r.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()))
      println(decode.getClass)
      decode match {
        case eapSim: EapSuccessFailureMessage => true
        case _ => false
      }
    })
  }

  def sendEap3GSimChallengeResp = {
    sendRespPacket(accessChallengeResp3GSim)
    val result: Option[Boolean] = previousReceivedPacket.map(r => {
      val decode = EapMessageDecoder.decode(Unpooled.copiedBuffer(r.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()))
      println(decode.getClass)
      decode match {
        case eapSim: EapSuccessFailureMessage => true
        case _ => false
      }
    })
  }

  def sendEapAkaChallengeResp = sendRespPacket(accessChallengeRespAka)

  def sendNak = sendRespPacket(nak)

  def close = radiusClient.close

  private def sendRespPacket(respPacketGenerator: Byte => RadiusPacket) = {
    val previousId = previousReceivedPacket.map(r => {
      val eapMsg = EapMessageDecoder.decode(Unpooled.copiedBuffer(r.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()))
      println(s"Using Id of last packet${eapMsg.getIdentifier()}")
      eapMsg.getIdentifier()
    }).getOrElse(0)
    val packet2Send = respPacketGenerator(previousId.asInstanceOf[Byte])
    printRequest(packet2Send)
    val resp = radiusClient.communicate(packet2Send, 1812);
    previousReceivedPacket = Some(resp)
    printResponse(resp)
  }

  def accessReqWithIdentity(imsiStartPart: String, startDigit: String = "31"): AccessRequest = {
    val ar = new AccessRequest
    val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
    val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)
    attribute.setAttributeData(HexCodec.hex2Byte(s"0200003801${startDigit}${imsiStartPart}37323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267"))
    ar.addAttribute(attribute);

//    ar.addAttribute("NAS-Identifier", "this.is.my.nas-identifier.de");
//    ar.addAttribute("NAS-IP-Address", "192.168.0.100");
//    ar.addAttribute("Service-Type", "Login-User");
//    ar.addAttribute("WISPr-Redirection-URL", "http://www.sourceforge.net/");
//    ar.addAttribute("WISPr-Location-ID", "net.sourceforge.ap1");
    ar.addAttribute("User-Name", new String(HexCodec.hex2Byte(startDigit + imsiStartPart)) + "727710000010@wlan.mnc072.mcc320.3gppnetwork.org")

    EapMessageUtil.addMessageAuthenticator(ar, SharedSecret)

    ar
  }


  def accessRequestWithoutIdentity: AccessRequest = {
    val ar = new AccessRequest
    val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
    val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)
    attribute.setAttributeData(HexCodec.hex2Byte("020100280140776c616e2e6d6e633030332e6d63633532352e336770706e6574776f726b2e6f7267"))
    ar.addAttribute(attribute)

    ar.addAttribute("User-Name", "@wlan.mnc003.mcc525.3gppnetwork.org")

    EapMessageUtil.addMessageAuthenticator(ar, SharedSecret)

    ar
  }


  def getPreviousId(): Int = {
    val previousId = previousReceivedPacket.map(r => {
      val eapMsg = EapMessageDecoder.decode(Unpooled.copiedBuffer(r.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()))
      println(s"Using Id of last packet${eapMsg.getIdentifier()}")
      eapMsg.getIdentifier()
    }).getOrElse(0)
    previousId
  }

  def accessReqWithAtIdentity: RadiusPacket = {
    val identity = "0123727710000010@wlan.mnc072.mcc320.3gppnetwork.org"
    val atIdentity = attributeBuilder.eapsimAttributeBuilder.buildAtIdentity(identity.getBytes)
    val fnlSimMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, List(atIdentity))

    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, getPreviousId(), fnlSimMsg)

    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)
    rp.addAttribute(fnlEapMsg)
    rp.addAttribute("User-Name", new String(identity))
    val state = previousReceivedPacket.get.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[$state]")
      rp.addAttribute(state)
    }
    EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)
    rp
  }

  def sendClientErrorResponse(isAka: Boolean = true): RadiusPacket = {
    val atClientErrorCode = attributeBuilder.eapsimAttributeBuilder.buildClientError(2)
    val fnlSimMsg = if (isAka) new EapAka(EapAkaSubtype.AKA_CLIENT_ERROR, List(atClientErrorCode))
      else new EapSim(EapSimSubtype.CLIENT_ERROR, List(atClientErrorCode))

    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, getPreviousId(), fnlSimMsg)

    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)
    rp.addAttribute(fnlEapMsg)
    val state = previousReceivedPacket.get.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[$state]")
      rp.addAttribute(state)
    }
    EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)
    rp
  }


  def accessReAuthReqWithIdentity(): AccessRequest = {
    require(!lastAuthId.isEmpty, "Re-Authentication id not available. Look like you have not authenticated !..")

    val ar = new AccessRequest
    val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
    val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)
    attribute.setAttributeData(HexCodec.hex2Byte(s"0200005d01${HexCodec.byte2Hex(lastAuthId.get.getBytes)}"))
//    println("========================")
//    println(s"0200006701${HexCodec.byte2Hex(lastReAuthId.get.getBytes)}")
//    println(s"0200006701${HexCodec.byte2Hex(lastReAuthId.get.getBytes)}".length)
//    println("========================")
    ar.addAttribute(attribute);
    ar.addAttribute("User-Name", lastAuthId.map(id => id.substring(id.indexOf("_") + 1)).get)
    EapMessageUtil.addMessageAuthenticator(ar, SharedSecret)

    ar
  }

  def accessReAuthReqWithAtIdentity(): RadiusPacket = {
    require(!lastAuthId.isEmpty, "Re-Authentication id not available. Look like you have not authenticated !..")

    val atIdentity = attributeBuilder.eapsimAttributeBuilder.buildAtIdentity(lastAuthId.get.getBytes)

    val fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, List(atIdentity))
    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, getPreviousId(), fnlAkaMsg)

    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)
    rp.addAttribute(fnlEapMsg)
    rp.addAttribute("User-Name", lastAuthId.get)
    val state = previousReceivedPacket.get.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[$state]")
      rp.addAttribute(state)
    }
    rp
  }

  def buildSimResponsePacketForReAuthRequest(eapSim: EapRequestResponseMessage, response: Option[RadiusPacket], sim: EapSim, isCounterTooSmall: Boolean, is3GGsmFlow: Boolean = false): RadiusPacket = {
    val attBuilder = attributeBuilder.eapsimAttributeBuilder
    val atNcrData = sim.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA)

    val encrKey = if (is3GGsmFlow) Aka3GSimEncr else SimEncr
    val encoded = Unpooled.copiedBuffer(atNcrData.getAttributeData.array.drop(2))
    val decode = attributeBuilder.keyGenerator.aesWithCbcDecode(HexCodec.hex2Byte(iv), HexCodec.hex2Byte(encrKey), encoded.array())
    val buffer = Unpooled.wrappedBuffer(decode)

    //extracrt AT_COUNTER
    buffer.readShort() //skip single short for type & length
    val latestCounter = buffer.readShort()

    //Extact AT_NONCE_S  [20 bytes]
    val nonceSData = Unpooled.buffer(16)
    buffer.readInt()
    buffer.readBytes(nonceSData)                                                       //        println(mac)

    //extact AT_NEXT_REAUTH_ID
    buffer.readShort() //skip single short for type & length
    val len = buffer.readShort()
    val nextId = new Array[Byte](len)
    buffer.readBytes(nextId)

    println("========================")
    println("id : " + eapSim.getIdentifier)
    println("counter: " + latestCounter)
    println("nonceS: " + HexCodec.byte2Hex(nonceSData.array()))
    lastAuthId = Some(new String(nextId))
    println(s"AT_NEXT_REAUTH_ID : $lastAuthId")
    println("========================")

    //update counter
    counter = latestCounter
    val atCounter = attBuilder.buildAtCounter(counter)

    val atBlankMac = attBuilder.buildAtMac
    val atIv = attBuilder.buildAtIv(HexCodec.hex2Byte(iv))
    var dataTobeEncrList = List(atCounter)

    if (isCounterTooSmall) {
      val atCounterTooSmall = attBuilder.buildAtCounterTooSmall()
      dataTobeEncrList = atCounterTooSmall :: dataTobeEncrList
    }
    val atEncrData = attBuilder.buildAtEncrData(dataTobeEncrList, HexCodec.hex2Byte(iv), HexCodec.hex2Byte(encrKey))
    val tmpSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEncrData, atBlankMac))
    val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier, tmpSimMsg)
    val encodeEapMsg = tmpEapMsg.encode.array

    val data = Unpooled.copiedBuffer(encodeEapMsg, nonceSData.array())
    val mac = Arrays.copyOf(attributeBuilder.keyGenerator.hmacSha1Encode(HexCodec.hex2Byte(if (is3GGsmFlow) Aka3GSimKaut else SimKaut), data), 16)
    val atMac = attributeBuilder.eapsimAttributeBuilder.buildAtMac(mac)

    val fnlSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEncrData, atMac))

    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier, fnlSimMsg)

    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)
    rp.addAttribute(fnlEapMsg)

    val state = response.get.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[$state]")
      rp.addAttribute(state)
    }
    EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)
    rp  
  }

  def buildAkaResponsePacketForReAuthRequest(eapSim: EapRequestResponseMessage, response: Option[RadiusPacket], sim: EapAka, isCounterTooSmall: Boolean): RadiusPacket = {
    println("Extracting EAP-Request/AKA-Reauthentication")
    val attBuilder = attributeBuilder.eapsimAttributeBuilder
    val atNcrData = sim.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA)

    val encoded = Unpooled.copiedBuffer(atNcrData.getAttributeData.array.drop(2))
    val decode = attributeBuilder.keyGenerator.aesWithCbcDecode(HexCodec.hex2Byte(iv), HexCodec.hex2Byte(AkaEncr), encoded.array())
    val buffer = Unpooled.wrappedBuffer(decode)

    //extracrt AT_COUNTER
    buffer.readShort() //skip single short for type & length
    val latestCounter = buffer.readShort()

    //Extact AT_NONCE_S  [20 bytes]
    val nonceSData = Unpooled.buffer(16)
    buffer.readInt()
    buffer.readBytes(nonceSData)

    //extact AT_NEXT_REAUTH_ID
    buffer.readShort() //skip single short for type & length
    val len = buffer.readShort()
    val nextId = new Array[Byte](len)
    buffer.readBytes(nextId)

    println("========================")
    println("id : " + eapSim.getIdentifier)
    println("counter: " + latestCounter)
    println("nonceS: " + HexCodec.byte2Hex(nonceSData.array()))
    lastAuthId = Some(new String(nextId))
    println(s"AT_NEXT_REAUTH_ID : $lastAuthId")
    println("========================")

    //update counter
    counter = latestCounter
    val atCounter = attBuilder.buildAtCounter(counter)

    val atBlankMac = attBuilder.buildAtMac
    val atIv = attBuilder.buildAtIv(HexCodec.hex2Byte(iv))
    var dataTobeEncrList = List(atCounter)

    if (isCounterTooSmall) {
      val atCounterTooSmall = attBuilder.buildAtCounterTooSmall()
      dataTobeEncrList = atCounterTooSmall :: dataTobeEncrList
    }

    val atEncrData = attBuilder.buildAtEncrData(dataTobeEncrList, HexCodec.hex2Byte(iv), HexCodec.hex2Byte(AkaEncr))
    val tmpSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, List(atIv, atEncrData, atBlankMac))
    val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier, tmpSimMsg)
    val encodeEapMsg = tmpEapMsg.encode.array

    val data = Unpooled.copiedBuffer(encodeEapMsg, nonceSData.array())
    val mac = Arrays.copyOf(attributeBuilder.keyGenerator.hmacSha1Encode(HexCodec.hex2Byte(AkaKaut), data), 16)
    val atMac = attributeBuilder.eapsimAttributeBuilder.buildAtMac(mac)

    val fnlSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, List(atIv, atEncrData, atMac))

    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, eapSim.getIdentifier, fnlSimMsg)

    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)
    rp.addAttribute(fnlEapMsg)

    val state = response.get.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[$state]")
      rp.addAttribute(state)
    }
    EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)
    rp
  }
  
  def processReAuthRequest(eapSim: EapRequestResponseMessage, response: Option[RadiusPacket], isCounterTooSmall: Boolean, is3GGsmFlow: Boolean = false): RadiusPacket = {
    eapSim.getType match {
      case sim: EapSim => buildSimResponsePacketForReAuthRequest(eapSim, response, sim, isCounterTooSmall, is3GGsmFlow)
      case aka: EapAka => buildAkaResponsePacketForReAuthRequest(eapSim, response, aka, isCounterTooSmall)
      case _ => throw new IllegalArgumentException("Un supported message type received")
    }
  }

  def accessReAuthReqWithChallenge(isCounterTooSmall: Boolean, is3GGSMFlow: Boolean = false): RadiusPacket = {
    val result = previousReceivedPacket.map(r => {
      val eapMsg = EapMessageDecoder.decode(Unpooled.copiedBuffer(r.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData()))

      eapMsg match {
        case eapSim: EapRequestResponseMessage => {
          processReAuthRequest(eapSim, previousReceivedPacket, isCounterTooSmall, is3GGSMFlow)
        }
      }
    })
    result.get
  }

  def identityReqWithImsi(imsi: String): RadiusPacket = {
    val identity = s"1${imsi}@wlan.mnc072.mcc320.3gppnetwork.org"

    val rp = new AccessRequest
    val eapAttrType: AttributeType = rp.getDictionary.getAttributeTypeByName("EAP-Message")
    val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(rp.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)
    attribute.setAttributeData(HexCodec.hex2Byte(s"0200003801${HexCodec.byte2Hex(identity.getBytes)}"))
    rp.addAttribute(attribute);
    rp.addAttribute("User-Name", identity)

    EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)

    rp
  }

  private def accessReqWithEapSimStartResp(simStartReqId: Byte): AccessRequest = {
    previousReceivedPacket.map(pr => {
      createSimStart(simStartReqId, pr)
    }).get
  }

  private def accessReqWith3GGSMStartResp(simStartReqId: Byte): AccessRequest = {
    previousReceivedPacket.map(pr => {
      createSimStartFor3GGSM(simStartReqId, pr)
    }).get
  }

  def createSimStartRespForReAuth(): RadiusPacket = {
    val eapMsg = previousReceivedPacket.map(r => EapMessageDecoder.decode(Unpooled.copiedBuffer(r.getAttribute(EapMessage.ATTRIBUTE_TYPE).getAttributeData())))
    val previousId = eapMsg.map(msg => msg.getIdentifier).getOrElse(0)

    lastAuthId.map(x => require(!x.isEmpty))
    lastAuthId.map(lastReAuthId => {
      val atIdentity = attributeBuilder.eapsimAttributeBuilder.buildAtIdentity(lastReAuthId.getBytes)
      val simMsg = new EapSim(EapSimSubtype.START, List(atIdentity))
      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)

      val eapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, previousId, simMsg)
      rp.addAttribute(eapMsg)

      val state = previousReceivedPacket.get.getAttribute("State")
      if (state != null) {
        println(s"Adding received State Attribute to Response[${state}]")
        rp.addAttribute(state)
      }
      EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)
      rp
    }).get
  }

 def createSimStart(requestId: Byte, receivedResp: RadiusPacket) : AccessRequest = {
   val ar = new AccessRequest
   val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
   val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)

   val respPacketId = HexCodec.byte2Hex(Array(requestId))
   val eapSimStart = s"02${respPacketId}0058120a00000e0e00333133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267001001000107050000749ad02c4a257c20192738be0b40dc86";

   attribute.setAttributeData(HexCodec.hex2Byte(eapSimStart))
   ar.addAttribute(attribute);
   ar.addAttribute("User-Name", "1320727710000010@wlan.mnc072.mcc320.3gppnetwork.org")
   val state = receivedResp.getAttribute("State")
   if (state != null) {
     println(s"Adding received State Attribute to Response[${state}]")
     ar.addAttribute(state)
   }
   EapMessageUtil.addMessageAuthenticator(ar, SharedSecret)

   ar
 }

  def createSimStartFor3GGSM(requestId: Byte, receivedResp: RadiusPacket) : AccessRequest = {
    val ar = new AccessRequest
    val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
    val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)

    val respPacketId = HexCodec.byte2Hex(Array(requestId))
    val eapSimStart = s"02${respPacketId}0058120a00000e0e00333131323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267001001000107050000749ad02c4a257c20192738be0b40dc86";

    attribute.setAttributeData(HexCodec.hex2Byte(eapSimStart))
    ar.addAttribute(attribute);
    ar.addAttribute("User-Name", "1123727710000010@wlan.mnc072.mcc320.3gppnetwork.org")
    val state = receivedResp.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[${state}]")
      ar.addAttribute(state)
    }
    EapMessageUtil.addMessageAuthenticator(ar, SharedSecret)

    ar
  }

  private def accessChallengeRespSim(challengeReqId: Byte) = {

    previousReceivedPacket.map(pr => {
      createSimChallengeResp(challengeReqId, pr)
    }).get

  }

  private def accessChallengeResp3GSim(challengeReqId: Byte) = {
    previousReceivedPacket.map(pr => {
      createSimChallengeResp(challengeReqId, pr, true)
    }).get

  }

  def createSimChallengeResp(requestId: Byte, receivedResp: RadiusPacket, is3GSimFlow: Boolean = false) = {
    val atBlankMac = attributeBuilder.eapsimAttributeBuilder.buildAtMac
    val tmpSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List(atBlankMac))
    val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, requestId, tmpSimMsg)
    val encodeEapMsg = tmpEapMsg.encode.array

    val data = Unpooled.copiedBuffer(encodeEapMsg, HexCodec.hex2Byte(if (is3GSimFlow) Aka3GSimSres else SimSres))
    val mac = Arrays.copyOf(attributeBuilder.keyGenerator.hmacSha1Encode(HexCodec.hex2Byte(if (is3GSimFlow) Aka3GSimKaut else SimKaut), data), 16)
    println(s"Generated MAC[${HexCodec.byte2Hex(mac)}]")

    val atMac = attributeBuilder.eapsimAttributeBuilder.buildAtMac(mac)
    val fnlSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List(atMac))
    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, requestId, fnlSimMsg)

    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
    rp.addAttribute(fnlEapMsg);
    val state = receivedResp.getAttribute("State")
    if (state != null) {
      println(s"Adding received State Attribute to Response[${state}]")
      rp.addAttribute(state)
    }
    EapMessageUtil.addMessageAuthenticator(rp, SharedSecret)
    rp
  }

  private def accessChallengeRespAka(challengeReqId: Byte) = {
    previousReceivedPacket.map(pr => {
      val drp = eapAttributeDecoder.decode(pr)
      val atRes = attributeBuilder.eapsimAttributeBuilder.buildAtRes(HexCodec.hex2Byte(AkaRes))
      val atBlankMac = attributeBuilder.eapsimAttributeBuilder.buildAtMac
      val tmpAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atRes, atBlankMac))
      val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, challengeReqId, tmpAkaMsg)
      val encodeEapMsg = tmpEapMsg.encode

      val mac = Arrays.copyOf(attributeBuilder.keyGenerator.hmacSha1Encode(HexCodec.hex2Byte(AkaKaut), encodeEapMsg), 16)

      val atMac = attributeBuilder.eapsimAttributeBuilder.buildAtMac(mac)
      val fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atRes, atMac))
      val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, challengeReqId, fnlAkaMsg)

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
      val state = pr.getAttribute("State")
      if (state != null) {
        println(s"Adding received State Attribute to Response[${state}]")
        rp.addAttribute(state)
      }
      rp.addAttribute(fnlEapMsg);
      rp

    }).get
  }

  private def nak(challengeReqId: Byte) = {
    previousReceivedPacket.map(pr => {

      val nak = new EapNak(18.asInstanceOf[Byte])
      val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, challengeReqId, nak)
      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
      rp.addAttribute(fnlEapMsg);
      rp

    }).get
  }

  private def printRequest(req: RadiusPacket) {
    println(">>> Sending Request")
    println(s">>> [${req}]")
  }

  private def printResponse(resp: RadiusPacket) {
    println("<<< Response Received")
    println(s"<<< [${resp}]")
  }
}