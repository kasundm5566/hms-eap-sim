/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.simulator

import java.io.PrintWriter
import jline.console.ConsoleReader
import jline.console.completer.StringsCompleter

/**
 *
 */
class SimulatorConsole {

  val commandList = Array("start", "help", "exit", "login2g", "login3g", "login3g-gsm", "loginUnknownSubs", "simstart", "simstart3g-gsm", "challenge2g", "challenge3g", "challenge3g-gsm", "nak", "reauthLogin2g", "reauth2g", "reauth3g-gsm", "reauthsimstart", "reauthsimstart3g-gsm", "countertoosmall2g", "countertoosmall3g", "reauthLogin3g", "reauthLogin3g-gsm", "reauth3g", "use2vector", "use3vector", "login3g-init", "reauthLogin3g-init", "countertoosmall3g-gsm", "clienterror2g", "clienterror3g", "clienterror3g-gsm", "no-user-id")

  def run(): Unit = {
    printWelcome
    val reader: ConsoleReader = new ConsoleReader
    reader.addCompleter(new StringsCompleter(commandList: _*))

    val out: PrintWriter = new PrintWriter(System.out);

    while (true) {
      readLine(reader, "") match {
        case "help" => printHelp
        case "exit" => return
        case "login2g" => identity2G
        case "login3g" => identity3G
        case "login3g-init" => identity3GInitialize
        case "login3g-gsm" => identity3GWithGsm
        case "loginUnknownSubs" => identity2G
        case "simstart" => simStart
        case "simstart3g-gsm" => simStart3gGsm
        case "challenge2g" => challenge2G
        case "challenge3g-gsm" => challenge3GSim
        case "challenge3g" => challenge3G
        case "reauthLogin2g" => identityReAuth2G
        case "reauthLogin3g" => identityReAuth3G
        case "reauthLogin3g-init" => identityReAuth3GInit
        case "reauthLogin3g-gsm" => identityReAuth3GForGSM
        case "reauth2g" => reAuthenticate2G
        case "reauth3g-gsm" => reAuthenticate3GForGSM
        case "reauth3g" => reAuthenticate3G
        case "countertoosmall2g" => reAuthenticateWithCounterTooSmall2G
        case "countertoosmall3g" => reAuthenticateWithCounterTooSmall3G
        case "countertoosmall3g-gsm" => reAuthenticateWithCounterTooSmall3GGsm
        case "reauthsimstart" => simStartForReAuth
        case "reauthsimstart3g-gsm" => simStartForReAuth
        case "use2vector" => useTwoVectorFor3GGSMFlow
        case "use3vector" => useThreeVectorFor3GGSMFlow
        case "clienterror2g" => clientError2G
        case "clienterror3g" => clientError3G
        case "clienterror3g-gsm" => clientError2G
        case "no-user-id" => identityWithoutUserId
        case "nak" => nak
        case _ => printBlank
      }
      out.flush
    }

  }

  def identityUnknowSubs = {
    println("Starting authentication: Send Identity with IMSI of Unknow subscriber")
    try {
      RadiusRequests.sendIdentityUnknown
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identityReAuth2G = {
    println("Starting sending reauthLogin2g: Send Identity response with 2G IMSI and next re-auth-id")
    try {
      RadiusRequests.sendIdentityReAuth2G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identityReAuth3G = {
    println("Starting sending reauthLogin3g: Send Identity response with 3G IMSI and next re-auth-id")
    try {
      RadiusRequests.sendIdentityReAuth3G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identityReAuth3GInit = {
    println("Starting sending reauthLogin3g-init: Send Identity response with 3G IMSI and next re-auth-id")
    try {
      RadiusRequests.sendIdentityReAuth3GWithAtIdentity
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identityReAuth3GForGSM = {
    println("Starting sending reauthLogin3g: Send Identity response with 3G IMSI and next re-auth-id")
    try {
      RadiusRequests.sendIdentityReAuth3G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def reAuthenticate2G = {
    println("Starting sending reauth2g: Send response for re-auth request for server with 2G IMSI")
    try {
      RadiusRequests.sendReAuthenticateResponse2G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def reAuthenticate3GForGSM = {
    println("Starting sending reauth3g-gsm: Send response for re-auth request for server with 2G IMSI")
    try {
      RadiusRequests.sendReAuthenticateResponse3GForGSM
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def reAuthenticate3G = {
    println("Starting sending reauth3g: Send response for re-auth request for server with 3G IMSI")
    try {
      RadiusRequests.sendReAuthenticateResponse3G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def reAuthenticateWithCounterTooSmall2G = {
    println("Starting sending countertoosmall2g: Send response for re-auth with counter too small flag in 2G IMSI")
    try {
      RadiusRequests.sendReAuthenticateResponseWithCounterTooSmallFlag2G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }
  def reAuthenticateWithCounterTooSmall3G = {
    println("Starting sending countertoosmall3g: Send response for re-auth with counter too small flag in 3G IMSI")
    try {
      RadiusRequests.sendReAuthenticateResponseWithCounterTooSmallFlag3G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def reAuthenticateWithCounterTooSmall3GGsm = {
    println("Starting sending countertoosmall3g-gsm: Send response for re-auth with counter too small flag in 3G-gsm IMSI")
    try {
      RadiusRequests.sendReAuthenticateResponseWithCounterTooSmallForGSM
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identity2G = {
    println("Starting authentication: Send Identity with 2G IMSI")
    try {
      RadiusRequests.sendIdentity2G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def clientError2G = {
    println("Starting clientError2G: Send client error code for 2G")
    try {
      RadiusRequests.sendClientError(false)
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def clientError3G = {
    println("Starting clientError3G: Send client error code for 3G")
    try {
      RadiusRequests.sendClientError(true)
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identity3G = {
    println("Starting authentication: Send Identity with 3G IMSI")
    try {
      RadiusRequests.sendIdentity3G
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identity3GInitialize = {
    println("Starting authentication: Send Identity with 3G IMSI for initialise")
    try {
      RadiusRequests.sendIdentity3GWithAtIdentity
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identityWithoutUserId = {
    println("Starting authentication: Send Identity without IMSI/ReAuthentication-id")
    try {
      RadiusRequests.sendIdentityWithoutUserId
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def identity3GWithGsm = {
    println("Starting authentication: Send Identity with 3G IMSI with GSM Authentication")
    try {
      RadiusRequests.sendIdentity3GWithGsmAuth
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def simStart = {
    println("Sending Eap-Sim/Start/Response")
    try {
      RadiusRequests.sendEapSimStartResp
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def simStart3gGsm = {
    println("Sending Eap-Sim/Start/Response for 3G - gsm")
    try {
      RadiusRequests.sendEapSimStartRespFor3GGSM
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def simStartForReAuth = {
    println("Sending Eap-Sim/Start/Response for Re-Authenticaion")
    try {
      RadiusRequests.sendEapSimStartRespForReAuth
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def challenge2G = {
    println("Sending EAP-Response/SIM/Challenge")
    try {
      RadiusRequests.sendEapSimChallengeResp
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def challenge3GSim = {
    println("Sending EAP-Response/SIM/Challenge for 3G Sim")
    try {
      RadiusRequests.sendEap3GSimChallengeResp
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def useTwoVectorFor3GGSMFlow = {
    println("Authentication continues using 2 vectors")
    RadiusRequests.use2VectorFor3GSimFlow
  }

  def useThreeVectorFor3GGSMFlow = {
    println("Authentication continues using 3 vectors")
    RadiusRequests.use3VectorFor3GSimFlow
  }

  def challenge3G = {
    println("Sending EAP-Response/AKA-Challenge")
    try {
      RadiusRequests.sendEapAkaChallengeResp
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def nak = {
    println("Sending EAP-Response/NAK")
    try {
      RadiusRequests.sendNak
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }

  def reAuth = {
    println("Sending EAP-Response/Identity")
    try {
      RadiusRequests.sendEapAkaChallengeResp
    } catch {
      case e:Throwable => e.printStackTrace
    }
  }


  def printHelp = {
    println("== EAP-SIM/EAP-AKA Authenticator Simulator ==")
    println("-----------------------------------------------------------")
    println("     ** help - Display Help")
    println("     ** exit - Exit")
    println("-----------------------------------------------------------")

    println("")
    println("Full Authentication")
    println("")
    println("2G Flow")
    println("     1. login2g - EAP-Response/Identity - 2G")
    println("     2. simstart - EAP-Response/SIM/Start with AT_IDENTITY flag")
    println("     3. challenge2g - EAP-Response/SIM/Challenge")
    println("")
    println("3G Flow")
    println("     1. login3g - EAP-Response/Identity - 3G")
    println("     2. login3g-init - EAP-Response/AKA-Identity with AT_IDENTITY flag- 3G")
    println("     3. challenge3g - EAP-Response/AKA-Challenge")
    println("")
    println("3G-GSM Flow")
    println("     1. login3g-gsm - EAP-Response/Identity - 3G - GSM")
    println("     2. simstart3g-gsm - Eap-Sim/Start/Response with AT_IDENTITY flag - 3G - GSM")
    println("     3. challenge3g-gsm - EAP-Response/SIM/Challenge - 3G - GSM")
    println("-----------------------------------------------------------")
    println("")
    println("Re-Authentication")
    println("Note : Full authentication followed by Re-authentication")
    println("")
    println("2G Flow")
    println("     1. reauthLogin2g - EAP-Response/Identity with - 2G")
    println("     2. reauthsimstart - EAP-Response/SIM/Start including re auth id in AT_IDENTITY flag  - 2G")
    println("     3. reauth2g - EAP-Response/SIM/Re-authentication - 2G")
    println("")
    println("3G Flow")
    println("     1. reauthLogin3g - EAP-Response/AKA/Identity with next re-auth id - 3G")
    println("     2. reauthLogin3g-init - EAP-Response/AKA-Identity including next re-auth id in AT_IDENTITY flag - 3G")
    println("     3. reauth3g - EAP-Response/AKA/Re-authentication - 3G")
    println("")
    println("3G-GSM Flow")
    println("     1. reauthLogin3g-gsm - EAP-Response/SIM/Identity - 3G - GSM")
    println("     2. reauthsimstart3g-gsm - EAP-Response/SIM/start including next re-auth id in AT_IDENTITY flag- 3G-GSM")
    println("     3. reauth3g-gsm - EAP-Response/SIM/Re-authentication - 3G")
    println("-----------------------------------------------------------")
    println("Miscellaneous")
    println("     1. loginUnknownSubs - EAP-Response/Identity for Unknown subscriber")
    println("     2. nak - EAP-Response/NAK")
    println("     3. countertoosmall2g - EAP-Response/SIM/Re-authentication with counter too small fag - 2G")
    println("     4. countertoosmall3g - EAP-Response/SIM/Re-authentication with counter too small fag - 3G")
    println("     5. countertoosmall3g-gsm - EAP-Response/SIM/Re-authentication with counter too small fag - 3G-gsm")
    println("     6. clienterror2g - EAP-Response/SIM/Client-Error for 2G")
    println("     7. clienterror3g - EAP-Response/Client-Error for 3G")
    println("     8. clienterror3g-gsm - EAP-Response/SIM/Client-Error for 3G-gsm")
    println("     9. no-user-id - EAP-Response/Identity without user id")
  }

  def printBlank = println("")

  def printWelcome = {
    println("Welcome to EAP-SIM Authenticator Simulator. \nPress TAB or type \"help\" then hit ENTER.")
  }

  def readLine(reader: ConsoleReader, promt: String) = reader.readLine(promt + "\nSim> ").trim

}