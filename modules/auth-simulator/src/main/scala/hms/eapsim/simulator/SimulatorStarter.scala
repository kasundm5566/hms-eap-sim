/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.simulator


/**
 *
 */
object SimulatorStarter {

  def main(args: Array[String]): Unit = {
    val console = new SimulatorConsole
    console.run
  }

  def imsiBuilder(count: Int): List[String] ={
//    val start = "5250"
//    val range = for(i <- 1 to count) yield f"${i}%011d"
//    range.map(i => s"5250${i}").toList

    val a = for(i <-1 to count ) yield "320727710000010"
    a.toList
  }

}