# Access Point Simulator User Guide

This simulator act as the authenticator or the access-point of the WiFi EAP-SIM authentication.

## Building
Execute following on sbt console
    `project auth-simulator`
    `;clean ;compile ;one-jar`

## Running
Executable jar can be found inside *target/scala-2.1** directory. Execute that.
    `java -jar auth-simulator_<version>-one-jar.jar`

If simulator started properly you'll get the following prompt. 

<pre>
Welcome to EAP-SIM Authenticator Simulator. 
Press TAB or type "help" then hit ENTER.

Sim>
</pre>

Type `help` on the prompt and it will show all possible commands.

<pre>
Sim> help
== EAP-SIM/EAP-AKA Authenticator Simulator ==
-----------------------------------------------------------
     ** help - Display Help
     ** exit - Exit
-----------------------------------------------------------

Full Authentication

2G Flow
     1. login2g - EAP-Response/Identity - 2G
     2. simstart - EAP-Response/SIM/Start with AT_IDENTITY flag
     3. challenge2g - EAP-Response/SIM/Challenge

3G Flow
     1. login3g - EAP-Response/Identity - 3G
     2. login3g-init - EAP-Response/AKA-Identity with AT_IDENTITY flag- 3G
     3. challenge3g - EAP-Response/AKA-Challenge

3G-GSM Flow
     1. login3g-gsm - EAP-Response/Identity - 3G - GSM
     2. simstart3g-gsm - Eap-Sim/Start/Response with AT_IDENTITY flag - 3G - GSM
     3. challenge3g-gsm - EAP-Response/SIM/Challenge - 3G - GSM
-----------------------------------------------------------

Re-Authentication
Note : Full authentication followed by Re-authentication

2G Flow
     1. reauthLogin2g - EAP-Response/Identity with - 2G
     2. reauthsimstart - EAP-Response/SIM/Start including re auth id in AT_IDENTITY flag  - 2G
     3. reauth2g - EAP-Response/SIM/Re-authentication - 2G

3G Flow
     1. reauthLogin3g - EAP-Response/AKA/Identity with next re-auth id - 3G
     2. reauthLogin3g-init - EAP-Response/AKA-Identity including next re-auth id in AT_IDENTITY flag - 3G
     3. reauth3g - EAP-Response/AKA/Re-authentication - 3G

3G-GSM Flow
     1. reauthLogin3g-gsm - EAP-Response/SIM/Identity - 3G - GSM
     2. reauthsimstart3g-gsm - EAP-Response/SIM/start including next re-auth id in AT_IDENTITY flag- 3G-GSM
     3. reauth3g-gsm - EAP-Response/SIM/Re-authentication - 3G
-----------------------------------------------------------
Miscellaneous
     1. loginUnknownSubs - EAP-Response/Identity for Unknown subscriber
     2. nak - EAP-Response/NAK
     3. countertoosmall2g - EAP-Response/SIM/Re-authentication with counter too small fag - 2G
     4. countertoosmall3g - EAP-Response/SIM/Re-authentication with counter too small fag - 3G
     5. countertoosmall3g-gsm - EAP-Response/SIM/Re-authentication with counter too small fag - 3G-gsm
     6. clienterror2g - EAP-Response/SIM/Client-Error for 2G
     7. clienterror3g - EAP-Response/Client-Error for 3G
     8. clienterror3g-gsm - EAP-Response/SIM/Client-Error for 3G-gsm
     9. no-user-id - EAP-Response/Identity without user id
</pre>

## Tesing Eap-SIM Authentication

Eap-SIM protocol is used to authenticate 2G sims. For more information on Eap-SIM protocol refer RFC4186 in docs/RFC. Following command sequence should be used when sending sample SIM authentication request.

<pre>
Sim> login2g
[Server Response Message]

Sim> simstart
[Server Response Message]

Sim> challenge2g
[Server Response Message]

</pre>

Below is a sample of SIM authentication session.

<pre>
Sim> login2g
Starting authentication: Send Identity with 2G IMSI
Generating new request-authenticator
2014-06-15 00:32:59,801 DEBUG RadiusPacket - [main] [] Creating request authenticator
2014-06-15 00:32:59,807 DEBUG EapMessageUtil - [main] [] Request authenticator created[fcd6b4974b399642119ff6f7c18a38b8]
2014-06-15 00:32:59,814 DEBUG EapMessageUtil - [main] [] Recived to create hmacMD5 [01010095fcd6b4974b399642119ff6f7c18a38b84f3a02000038013133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f726701353133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267501200000000000000000000000000000000]
2014-06-15 00:32:59,991 DEBUG EapMessageUtil - [main] [] Generated Message-Authenticator value[419af3229e292bbf5407b546c2d2d9d1]
>>> Sending Request
>>> [Access-Request, ID[1], Attributes[[EAP-Message: 0x02000038013133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267][User-Name: 1320727710000010@wlan.mnc072.mcc320.3gppnetwork.org][Message-Authenticator: 0x419af3229e292bbf5407b546c2d2d9d1]]]
Request-authenticator is provided with the request.
2014-06-15 00:33:00,117 INFO  RadiusPacket - [main] [] Creating response authenticator
<<< Response Received
<<< [Access-Challenge, ID[1], Attributes[[EAP-Message: 0x01070014120a00000f0200020001000011010100][State: 0x31313430363135303033333030303033][Message-Authenticator: 0xe95336af90588edc6505779a3bffff85]]]

Sim> simstart
Sending Eap-Sim/Start/Response
2014-06-15 00:33:02,550 DEBUG InternalLoggerFactory - [main] [] Using SLF4J as the default logging framework
Using Id of last packet7
Adding received State Attribute to Response[State: 0x31313430363135303033333030303033]
Generating new request-authenticator
2014-06-15 00:33:02,578 DEBUG RadiusPacket - [main] [] Creating request authenticator
2014-06-15 00:33:02,579 DEBUG EapMessageUtil - [main] [] Request authenticator created[f674375ab5d9fa28374731c0285b9484]
2014-06-15 00:33:02,588 DEBUG EapMessageUtil - [main] [] Recived to create hmacMD5 [010200c7f674375ab5d9fa28374731c0285b94844f5a02070058120a00000e0e00333133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267001001000107050000749ad02c4a257c20192738be0b40dc8601353133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267181231313430363135303033333030303033501200000000000000000000000000000000]
2014-06-15 00:33:02,589 DEBUG EapMessageUtil - [main] [] Generated Message-Authenticator value[8be3508409191a006d7cd9d3721733f9]
>>> Sending Request
>>> [Access-Request, ID[2], Attributes[[EAP-Message: 0x02070058120a00000e0e00333133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267001001000107050000749ad02c4a257c20192738be0b40dc86][User-Name: 1320727710000010@wlan.mnc072.mcc320.3gppnetwork.org][State: 0x31313430363135303033333030303033][Message-Authenticator: 0x8be3508409191a006d7cd9d3721733f9]]]
Request-authenticator is provided with the request.
2014-06-15 00:33:02,679 INFO  RadiusPacket - [main] [] Creating response authenticator
<<< Response Received
<<< [Access-Challenge, ID[2], Attributes[[EAP-Message: 0x010800c8120b0000010d00001ac2e620ccd40bb920f6b3b474306b6b2ac2e620ccd40bb920f6b3b474306b6b3ac2e620ccd40bb920f6b3b474306b6b810500009e18b0c29a652263c06efb54dd00a89582190000befb682d13e88bc05b6ff2e66535975f677040165e4b5fc2882f5fa6f4d18d59d752fad191cb95d6f09f8c4bdee80d7db7a7fb1121eba8161816877ba0f9d41ab3c549aa5acff24babd91756202e27d1eed02aa6eec892a150762bfb10b11c300b0500009c9f199a0a78b58d6367baf3cb9ab37e][State: 0x31313430363135303033333030303033][Message-Authenticator: 0x52dfc2ade85072e89e6b3c1733bceb4c]]]
========================
AT_NEXT_REAUTH_ID : Some(217d5253-53fc-4396-b457-f930162c84a0_1320727710000010@wlan.mnc072.mcc320.3gppnetwork.org)}
========================

Sim> challenge2g
Sending EAP-Response/SIM/Challenge
Using Id of last packet8
Generated MAC[0f5c7c49e2c54fd11fb319e65a28eb62]
Adding received State Attribute to Response[State: 0x31313430363135303033333030303033]
Generating new request-authenticator
2014-06-15 00:33:06,344 DEBUG RadiusPacket - [main] [] Creating request authenticator
0208001c120b00000b0500000f5c7c49e2c54fd11fb319e65a28eb62
2014-06-15 00:33:06,346 DEBUG EapMessageUtil - [main] [] Request authenticator created[4414d632fac203a2d6edfe3989a0dc21]
2014-06-15 00:33:06,349 DEBUG EapMessageUtil - [main] [] Recived to create hmacMD5 [010300564414d632fac203a2d6edfe3989a0dc214f1e0208001c120b00000b0500000f5c7c49e2c54fd11fb319e65a28eb62181231313430363135303033333030303033501200000000000000000000000000000000]
2014-06-15 00:33:06,350 DEBUG EapMessageUtil - [main] [] Generated Message-Authenticator value[119f08f8016be34ce0408ae4ad4d9ec8]
>>> Sending Request
>>> [Access-Request, ID[3], Attributes[[EapResponseMessage [EapMessage [code=RESPONSE, identifier=8, length=24], type=[EapSim [subType=CHALLENGE, attributes=[EapSimAttribute [type=AT_MAC, length=18, attributeData=00000f5c7c49e2c54fd11fb319e65a28eb62]]]]][State: 0x31313430363135303033333030303033][Message-Authenticator: 0x119f08f8016be34ce0408ae4ad4d9ec8]]]
Request-authenticator is provided with the request.
0208001c120b00000b0500000f5c7c49e2c54fd11fb319e65a28eb62
2014-06-15 00:33:06,431 INFO  RadiusPacket - [main] [] Creating response authenticator
<<< Response Received
<<< [Access-Accept, ID[3], Attributes[[Class: 0x7773675f37313638][EAP-Message: 0x03090004][Service-Type: Framed-User][Session-Timeout: 10800][Idle-Timeout: 1800][Acct-Interim-Interval: 1800][Vendor-Specific: MS (311), SubAttributes[[  MS-MPPE-Recv-Key: 0x84f0a29232d0fd36ed49bcf125b85996d3b6507088f141b2aaa27545c180ad0fe2535fe0861ea778dc5834e0187c81656311]]][Vendor-Specific: MS (311), SubAttributes[[  MS-MPPE-Send-Key: 0x832666fe6e5d5290e330634bf97c7584652c095fa9a998c78ae4640d70396c0f3e8bdda90927794ab41133d4d606a4c81912]]][Message-Authenticator: 0x630b6c16342bae5650dc30147dccaf3f]]]
class hms.common.radius.decoder.eap.elements.EapSuccessFailureMessage

</pre>


## Testing Eap-AKA Authentication

Eap-AKA protocol is used to authenticate 3G sims. For more information on Eap-AKA protocol refer RFC4187 in docs/RFC. Following command sequence should be used when sending sample AKA authentication request.

<pre>
Sim> login3g
[Server Response Message]

Sim> challenge3g
[Server Response Message]

</pre>

Below is a sample of AKA authentication session.


<pre>
Sim> login3g
Starting authentication: Send Identity with 3G IMSI
Generating new request-authenticator
2014-06-15 00:40:33,953 DEBUG RadiusPacket - [main] [] Creating request authenticator
2014-06-15 00:40:33,954 DEBUG EapMessageUtil - [main] [] Request authenticator created[43bcfcea7421325e3c0914493c81d474]
2014-06-15 00:40:33,961 DEBUG EapMessageUtil - [main] [] Recived to create hmacMD5 [0108009543bcfcea7421325e3c0914493c81d4744f3a02000038013031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f726701353031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267501200000000000000000000000000000000]
2014-06-15 00:40:33,964 DEBUG EapMessageUtil - [main] [] Generated Message-Authenticator value[7d1c434176ed7593da40c68d4f0e2537]
>>> Sending Request
>>> [Access-Request, ID[8], Attributes[[EAP-Message: 0x02000038013031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267][User-Name: 0123727710000010@wlan.mnc072.mcc320.3gppnetwork.org][Message-Authenticator: 0x7d1c434176ed7593da40c68d4f0e2537]]]
Request-authenticator is provided with the request.
2014-06-15 00:40:34,157 INFO  RadiusPacket - [main] [] Creating response authenticator
========================
AT_NEXT_REAUTH_ID : Some(c01dd757-1115-4463-839a-6f24cea678ff_0123727710000010@wlan.mnc072.mcc320.3gppnetwork.org)}
========================
<<< Response Received
<<< [Access-Challenge, ID[8], Attributes[[EAP-Message: 0x010e00bc1701000001050000f12ac2e620ccd40bb920f6b3b474306b02050000d749908734350000cd298d7429daed260b0500003fb833c78ccb73a6ab9e60f99a75550182190000f8385647f0ba9473c71c7733e4f76480179e5e7894dc94caae5a10f91954b99dc09581a8745b31c852c056425e00083326063d4f2e022af07daba3159b98102eec73dad3ccbccadd967a290ab5c94177a4e3f282989f36f7f57ee36b27d7bea9810500009e18b0c29a652263c06efb54dd00a895][State: 0x31313430363135303034303333303038][Message-Authenticator: 0xee405b8dfc21522979d2e830c8e8f63d]]]

Sim> challenge3g
Sending EAP-Response/AKA-Challenge
Using Id of last packet14
2014-06-15 00:40:39,777 INFO  EapAttributeDecoder - [main] [] Decoded EAP-Message[EapResponseMessage [EapMessage [code=REQUEST, identifier=14, length=188], type=[EapAka [subType=AKA_CHALLENGE, attributes=[EapSimAttribute [type=AT_RAND, length=18, attributeData=0000f12ac2e620ccd40bb920f6b3b474306b], EapSimAttribute [type=AT_AUTN, length=18, attributeData=0000d749908734350000cd298d7429daed26], EapSimAttribute [type=AT_MAC, length=18, attributeData=00003fb833c78ccb73a6ab9e60f99a755501], EapSimAttribute [type=AT_ENCR_DATA, length=98, attributeData=0000f8385647f0ba9473c71c7733e4f76480179e5e7894dc94caae5a10f91954b99dc09581a8745b31c852c056425e00083326063d4f2e022af07daba3159b98102eec73dad3ccbccadd967a290ab5c94177a4e3f282989f36f7f57ee36b27d7bea9], EapSimAttribute [type=AT_IV, length=18, attributeData=00009e18b0c29a652263c06efb54dd00a895]]]]]
Adding received State Attribute to Response[State: 0x31313430363135303034303333303038]
>>> Sending Request
>>> [Access-Request, ID[9], Attributes[[State: 0x31313430363135303034303333303038][EapResponseMessage [EapMessage [code=RESPONSE, identifier=14, length=36], type=[EapAka [subType=AKA_CHALLENGE, attributes=[EapSimAttribute [type=AT_RES, length=10, attributeData=00083fe524d57101d202], EapSimAttribute [type=AT_MAC, length=18, attributeData=0000a9d0397ea53d26123cf4b7ad7504a981]]]]]]]
Generating new request-authenticator
2014-06-15 00:40:39,782 DEBUG RadiusPacket - [main] [] Creating request authenticator
020e002817010000030300083fe524d57101d2020b050000a9d0397ea53d26123cf4b7ad7504a981
2014-06-15 00:40:39,922 INFO  RadiusPacket - [main] [] Creating response authenticator
<<< Response Received
<<< [Access-Accept, ID[9], Attributes[[Class: 0x7773675f37313638][EAP-Message: 0x030f0004][Service-Type: Framed-User][Session-Timeout: 10800][Idle-Timeout: 1800][Acct-Interim-Interval: 1800][Vendor-Specific: MS (311), SubAttributes[[  MS-MPPE-Recv-Key: 0x8421287a917c7b8842a07f0b56cfcf2b9a398d27ceaa3e207ada3df2e5592886e9e6642bf0eec03cffac594f81860275725e]]][Vendor-Specific: MS (311), SubAttributes[[  MS-MPPE-Send-Key: 0x81ef6a934b46cdf7b37b1265286cde6d97094903c6e18f62438de193754f1d7898a14e135fe1bd5ca48fe5a89b833fc34175]]][Message-Authenticator: 0xe08fc627cc813543a2ac595d34274e0d]]]

Sim> 

</pre>