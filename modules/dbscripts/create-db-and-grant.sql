-- ------------------------------------------------------------------------

-- create DB and grant
-- ------------------------------------------------------------------------

create database eapsim;

grant all privileges on eapsim.* to 'user'@'localhost' identified by 'password';

create database common_admin;

grant all privileges on common_admin.* to 'user'@'localhost' identified by 'password';