ALTER TABLE `re_auth_ctx` DROP COLUMN `id`;
ALTER TABLE `re_auth_ctx` DROP INDEX `imsi`;
ALTER TABLE `re_auth_ctx` DROP INDEX `imsi_index`;
ALTER TABLE `re_auth_ctx` ADD PRIMARY KEY (`imsi`);