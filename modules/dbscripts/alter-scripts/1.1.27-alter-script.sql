CREATE TABLE `trans_log` (
  `client_id` varchar(20) NOT NULL,
  `response_time` long,
  `ip` varchar(16) NOT NULL,
  `mac_address` varchar(17) NOT NULL,
  `server_id` varchar(15) NOT NULL,
  `correlation_id` bigint(20) NOT NULL,
  `protocol` varchar(20) DEFAULT NULL,
  `flow_type` enum('RE_AUTHENTICATION', 'FULL_AUTHENTICATION') NOT NULL,
  `request_type` varchar(100) DEFAULT NULL,
  `response_type` varchar(30) DEFAULT NULL,
  `imsi` varchar(50) DEFAULT NULL,
  `msisdn` varchar(12) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`server_id`,`correlation_id`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;