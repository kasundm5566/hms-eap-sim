alter table trans_log drop column client_id;

DROP TABLE IF EXISTS `hourly_summary`;

CREATE TABLE `hourly_summary` (
  `average_resp_time` int NOT NULL DEFAULT 0,
  `sum_resp_times` bigint NOT NULL DEFAULT 0,
  `success_count` int NOT NULL DEFAULT 0,
  `failed_count` int NOT NULL DEFAULT 0,
  `mac_address` varchar(17) NOT NULL,
  `flow_type` enum('RE_AUTHENTICATION','FULL_AUTHENTICATION') NOT NULL,
  `request_type` varchar(100) NOT NULL DEFAULT '',
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`mac_address`,`flow_type`,`request_type`,`date_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `daily_summary`;

CREATE TABLE `daily_summary` (
  `average_resp_time` int NOT NULL DEFAULT 0,
  `sum_resp_times` bigint NOT NULL DEFAULT 0,
  `success_count` int NOT NULL DEFAULT 0,
  `failed_count` int NOT NULL DEFAULT 0,
  `mac_address` varchar(17) NOT NULL,
  `flow_type` enum('RE_AUTHENTICATION','FULL_AUTHENTICATION') NOT NULL,
  `request_type` varchar(100) NOT NULL DEFAULT '',
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`mac_address`,`flow_type`,`request_type`,`date_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `summary_table_status`;

CREATE TABLE `summary_table_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` varchar(25) DEFAULT '0000-00-00 00',
  `summary_type` enum('HOURLY','DAILY','WEEKLY','MONTHLY','YEARLY') DEFAULT NULL,
  `status` enum('EXECUTED','EXECUTING') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


