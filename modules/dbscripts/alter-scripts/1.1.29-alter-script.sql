use eapsim;

CREATE TABLE `mac_address_map` (
  `mac_address` varchar(20) DEFAULT NULL,
  `location` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;