use eapsim;

ALTER TABLE daily_summary ADD nas_id VARCHAR(30);
ALTER TABLE hourly_summary ADD nas_id VARCHAR(30);
ALTER TABLE trans_log ADD nas_id varchar(30) after mac_address;

