DROP TABLE IF EXISTS `daily_summary`;
CREATE TABLE `daily_summary` (
  `nas_id` varchar(30) NOT NULL DEFAULT 'unknown',
  `protocol` enum('SIM','AKA') NOT NULL,
  `flow_type` enum('RE_AUTHENTICATION','FULL_AUTHENTICATION') NOT NULL,
  `sim_count` int(11) NOT NULL DEFAULT '0',
  `aka_count` int(11) NOT NULL DEFAULT '0',
  `fulauth_count` int(11) NOT NULL DEFAULT '0',
  `reauth_count` int(11) NOT NULL DEFAULT '0',
  `success_count` int(11) NOT NULL DEFAULT '0',
  `failed_count` int(11) NOT NULL DEFAULT '0',
  `total_count` int(11) NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`date_time`,`nas_id`,`protocol`,`flow_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `hourly_summary`;
CREATE TABLE `hourly_summary` (
  `nas_id` varchar(30) NOT NULL DEFAULT 'unknown',
  `protocol` enum('SIM','AKA') NOT NULL,
  `flow_type` enum('RE_AUTHENTICATION','FULL_AUTHENTICATION') NOT NULL,
  `sim_count` int(11) NOT NULL DEFAULT '0',
  `aka_count` int(11) NOT NULL DEFAULT '0',
  `fulauth_count` int(11) NOT NULL DEFAULT '0',
  `reauth_count` int(11) NOT NULL DEFAULT '0',
  `success_count` int(11) NOT NULL DEFAULT '0',
  `failed_count` int(11) NOT NULL DEFAULT '0',
  `total_count` int(11) NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`date_time`,`nas_id`,`protocol`,`flow_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `mac_address_map`;
CREATE TABLE `mac_address_map` (
  `mac_address` varchar(20) DEFAULT NULL,
  `location` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `msisdn_cache`;
CREATE TABLE `msisdn_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `imsi` varchar(50) NOT NULL,
  `msisdn` varchar(12) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `imsi` (`imsi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `re_auth_ctx`;
CREATE TABLE `re_auth_ctx` (
  `correlation_id` varchar(50) NOT NULL,
  `protocol` enum('SIM','AKA') NOT NULL,
  `imsi` bigint(20) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `identity` varchar(200) DEFAULT NULL,
  `next_re_auth_id` varchar(200) DEFAULT NULL,
  `counter` int(11) NOT NULL DEFAULT '0',
  `master_key` varchar(100) DEFAULT NULL,
  `k_aut` varchar(100) DEFAULT NULL,
  `k_encr` varchar(100) DEFAULT NULL,
  `nonce_s` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`imsi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
/*!50100 PARTITION BY HASH (imsi)
PARTITIONS 100 */;

DROP TABLE IF EXISTS `summary_table_status`;
CREATE TABLE `summary_table_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` varchar(25) DEFAULT '0000-00-00 00',
  `summary_type` enum('HOURLY','DAILY','WEEKLY','MONTHLY','YEARLY') DEFAULT NULL,
  `status` enum('EXECUTED','EXECUTING') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `trans_log`;
CREATE TABLE `trans_log` (
  `response_time` mediumtext,
  `ip` varchar(16) NOT NULL,
  `mac_address` varchar(17) NOT NULL,
  `nas_id` varchar(30) DEFAULT NULL,
  `server_id` varchar(15) NOT NULL,
  `correlation_id` bigint(20) NOT NULL,
  `protocol` enum('SIM','AKA') NOT NULL,
  `flow_type` enum('RE_AUTHENTICATION','FULL_AUTHENTICATION') NOT NULL,
  `request_type` enum('START','CHALLENGE','AKA_IDENTITY','AKA_CHALLENGE','RE_AUTHENTICATE','Identity') NOT NULL,
  `response_type` varchar(30) DEFAULT NULL,
  `imsi` bigint(15) DEFAULT '0',
  `msisdn` bigint(12) DEFAULT '0',
  `status` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`server_id`,`correlation_id`,`created`,`request_type`),
  KEY `imsi_idx` (`imsi`),
  KEY `msisdn_idx` (`msisdn`),
  KEY `correlation_id_idx` (`correlation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE (TO_DAYS(`created`))
(PARTITION `2017-05-19` VALUES LESS THAN (736833) ENGINE = InnoDB,
 PARTITION `2017-05-20` VALUES LESS THAN (736834) ENGINE = InnoDB,
 PARTITION `2017-05-21` VALUES LESS THAN (736835) ENGINE = InnoDB,
 PARTITION `2017-05-22` VALUES LESS THAN (736836) ENGINE = InnoDB,
 PARTITION `2017-05-23` VALUES LESS THAN (736837) ENGINE = InnoDB,
 PARTITION `2017-05-24` VALUES LESS THAN (736838) ENGINE = InnoDB,
 PARTITION `2017-05-25` VALUES LESS THAN (736839) ENGINE = InnoDB,
 PARTITION `2017-05-26` VALUES LESS THAN (736840) ENGINE = InnoDB,
 PARTITION `2017-05-27` VALUES LESS THAN (736841) ENGINE = InnoDB,
 PARTITION `2017-05-28` VALUES LESS THAN (736842) ENGINE = InnoDB,
 PARTITION `2017-05-29` VALUES LESS THAN (736843) ENGINE = InnoDB,
 PARTITION `2017-05-30` VALUES LESS THAN (736844) ENGINE = InnoDB,
 PARTITION `2017-05-31` VALUES LESS THAN (736845) ENGINE = InnoDB,
 PARTITION `2017-06-01` VALUES LESS THAN (736846) ENGINE = InnoDB,
 PARTITION `2017-06-02` VALUES LESS THAN (736847) ENGINE = InnoDB,
 PARTITION `2017-06-03` VALUES LESS THAN (736848) ENGINE = InnoDB,
 PARTITION `2017-06-04` VALUES LESS THAN (736849) ENGINE = InnoDB,
 PARTITION `2017-06-05` VALUES LESS THAN (736850) ENGINE = InnoDB,
 PARTITION `2017-06-06` VALUES LESS THAN (736851) ENGINE = InnoDB,
 PARTITION `2017-06-07` VALUES LESS THAN (736852) ENGINE = InnoDB,
 PARTITION `2017-06-08` VALUES LESS THAN (736853) ENGINE = InnoDB,
 PARTITION `2017-06-09` VALUES LESS THAN (736854) ENGINE = InnoDB,
 PARTITION `2017-06-10` VALUES LESS THAN (736855) ENGINE = InnoDB,
 PARTITION `2017-06-11` VALUES LESS THAN (736856) ENGINE = InnoDB,
 PARTITION `2017-06-12` VALUES LESS THAN (736857) ENGINE = InnoDB,
 PARTITION `2017-06-13` VALUES LESS THAN (736858) ENGINE = InnoDB,
 PARTITION `2017-06-14` VALUES LESS THAN (736859) ENGINE = InnoDB,
 PARTITION `2017-06-15` VALUES LESS THAN (736860) ENGINE = InnoDB,
 PARTITION `2017-06-16` VALUES LESS THAN (736861) ENGINE = InnoDB,
 PARTITION `2017-06-17` VALUES LESS THAN (736862) ENGINE = InnoDB,
 PARTITION `2017-06-18` VALUES LESS THAN (736863) ENGINE = InnoDB,
 PARTITION `2017-06-19` VALUES LESS THAN (736864) ENGINE = InnoDB,
 PARTITION daily_rest VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */

DROP TABLE IF EXISTS `keystore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keystore` (
  `key_identifier` varchar(150) NOT NULL,
  `certificate` blob NOT NULL,
  `private_key` blob NOT NULL,
  `status` enum('ENABLED','DISABLED') NOT NULL DEFAULT 'ENABLED',
  `algorithm` varchar(20) NOT NULL,
  `key_size` int(10) NOT NULL,
  `format` varchar(5) NOT NULL,
  `user` varchar(45) NOT NULL,
  PRIMARY KEY (`key_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;