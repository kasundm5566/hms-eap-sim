/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.server

import hms.common.radius.connector.{MessageType, ServerEndPoint}
import hms.common.radius.connector.serialise.ContextDumpUtility.serialize
import hms.eapsim.util.EapMsgUtil
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.{RadiusInboundHandler, RequestContext}
import org.apache.logging.log4j.scala.Logging
import hms.eapsim._
import org.slf4j.LoggerFactory

/**
 *
 */
class SimpleRadiusRequestListener extends RadiusInboundHandler {


  this: RadiusRequestReceiverComponent with RadiusResponseSenderComponent with RadiusRequestDelegateComponent =>
  val log = LoggerFactory.getLogger("RRL")


  override def handleRequest(requestContext: RequestContext): Unit = {
    log.debug(s"Request processing - started [ isRemote:${requestContext.isRemote}, request: ${requestContext.getReceived} ]")

    val response: Either[EapSimError, RadiusPacket] = radiusRequestReceiver.accessRequestReceived(requestContext);

    log.debug(s"Request processing completed [ isRemote:${requestContext.isRemote}, response: ${response}]")

    response match {
      case Right(rp: RadiusPacket) =>  {
        messageResponder.handleResponse(rp, requestContext)
      }
      case Left(e : EapSimError) => {
        if (e.statusCode == AsynchronousResponse) {
          log.debug("Response will be received asynchronously")
        } else {
          log.error(s"Error occurred while processing request [${e}}]")
          val errorResponse: Either[EapSimError, RadiusPacket] = EapMsgUtil.createDefaultEapError(e, requestContext.getReceived, errorCodeMapper)
          val rp: RadiusPacket = errorResponse.fold(
            e => EapMsgUtil.createDefaultRadiusError(requestContext.getReceived, Some(e.description)),
            rp => rp
          )
          messageResponder.handleResponse(rp, requestContext)
        }
      }
    }
  }

  override def delegateRequest(context: RequestContext, endPoint: ServerEndPoint): Unit = {
    log.debug(s"Request context before serializing in SimpleRadiusRequestListener [${context}]")
    endPoint.getMessageSender.send(MessageType.RADIUS_REQ, context.getCorrelationId.toLong, serialize(context))
  }
}