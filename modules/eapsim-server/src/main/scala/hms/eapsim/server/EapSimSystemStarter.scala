/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.server

import java.util.concurrent.{Executors, ScheduledExecutorService, TimeUnit}

import hms.common.radius.ClientProxy
import hms.common.radius.connector.ServerEndPoint
import hms.common.radius.connector.router.RequestRouter
import hms.common.radius.connector.router.impl.HashRouter
import org.apache.logging.log4j.scala.Logging
import hms.common.radius.decoder.chain.impl.EapAttributeDecoder
import hms.common.radius.server.RadiusServer
import hms.commons.SnmpLogUtil
import hms.eapsim.repo.{DataLoaderDaoImpl, TpsLog}
import org.tanukisoftware.wrapper.{WrapperListener, WrapperManager}

import scala.collection.JavaConversions._

/**
 *
 */
object EapSimSystemStarter extends Logging with WrapperListener {

  private lazy val RadiusServerThread = new Thread(new Runnable() {

    def run() {
      try {
        logger.info("Initializing Radius")
        val server: RadiusServer = new RadiusServer
        server.setPort(Settings.getInt("eapsim.radius.server.port"))
        server.setSharedSecretMap(getSecretKeyMap)
        server.setServerId(Settings.getInt("eapsim.server.node.id"))
        server.setRequestInboundHandler(ServiceRegistry.RadiusRequestListener)
        server.setCustomAttributeDecoder(new EapAttributeDecoder)
        server.setHost(Settings.getString("eapsim.radius.server.host"))
        server.setWorkerPoolSize(Settings.getInt("eapsim.radius.server.request.handler.worker.pool.size"))
        server.setIoThreadPoolSize(Settings.getInt("eapsim.radius.server.io.pool.size"))
        server.setRequiredAttIdList(Settings.getString("eapsim.radius.server.required.attribute.type.id.list"))
        server.setRequestRouter(getRouter)
        server.setDataLoaderDao(new DataLoaderDaoImpl(ServiceRegistry.RepositoryRegistry.reAuthContextRepository))
        server.start
      } catch {
        case e: Exception => logger.error("Could not initialized Radius server", e)
      }
    }
  })

  def getRouter: RequestRouter = {
    val routingEndPoints: String = Settings.getString("tcp.client.connecting.end.point")

    val router = if (routingEndPoints != null && !routingEndPoints.isEmpty) {
      val failoverRouter = new HashRouter
      failoverRouter.setConnectingHost(routingEndPoints)
      failoverRouter.setManager(ServiceRegistry.clientManager)
      failoverRouter.init()
      failoverRouter
    } else {
      new RequestRouter() {
        override def route(s: String): ServerEndPoint = new ServerEndPoint(0, "127.0.0.1", 0)

        override def getSelfIndexId: Int = 0
      }
    }
    router
  }

  def getSecretKeyMap : Map[String, ClientProxy] = {
    val proxyEntries = Settings.getString("client.secrets.key.map")

    proxyEntries.split(",").map(_.split(":")).map(arr => arr(0) -> new ClientProxy(arr(2), arr(1), arr(0))).toMap
  }

  def main(args: Array[String]) {
    WrapperManager.start(this, args)
  }

  private val scheduler: ScheduledExecutorService = Executors.newScheduledThreadPool(1)

  def startTPSLogScheduler() {

    logger.info("TPS logging scheduler is Starting")

      val logRunnableThread: Runnable = new Runnable {
        override def run() = TpsLog.executeTPSLogging()
      }
      val tpsLoggingScheduler = scheduler.scheduleAtFixedRate(logRunnableThread, 10, 1, TimeUnit.SECONDS)
  }

  override def controlEvent(i: Int): Unit = {

  }

  override def stop(i: Int): Int = {
    try {
      ServiceRegistry.mysqlConnector.destroy()
    }
    catch {
      case e: Exception => logger.error("Error occurred while closing pool")
    }
    logger.info("Eap Authentication Server has Stopped....")
    SnmpLogUtil.log(Settings.getString("eapsim.snmp.server.stopped"))
    0
  }

  override def start(strings: Array[String]): Integer = {
    startTPSLogScheduler()
    logger.info("Eap Authentication Server is Starting....")
    RadiusServerThread.start
    SnmpLogUtil.log(Settings.getString("eapsim.snmp.server.started"))
    null
  }
}