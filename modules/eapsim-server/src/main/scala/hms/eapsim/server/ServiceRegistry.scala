/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.server

import hms.common.radius.RadiusOutboundHandlerImpl
import hms.common.radius.connector.tcp.client.ClientManagerImpl
import hms.common.radius.connector.tcp.server.Server
import hms.common.radius.util.DefaultThreadFactory
import hms.eapsim.async.http.client.client.impl.{AsyncHttpClient, AsyncHttpClientManager}
import hms.eapsim.async.http.client.client.{Client, ClientManager}
import hms.eapsim.async.http.client.connector.impl.SigtranConnectorImpl
import hms.eapsim.cache.{AuthenticationVectorCacheImpl, CacheComponent, CacheManagerImpl, ReAuthContextCleanerManagerImpl}
import hms.eapsim.consumer.{CacheConfig, ConsumerDataServiceComponent, MsisdnDispatcherService, MsisdnServiceComponent}
import hms.eapsim.gw.integration.SigtranResponseListener
import hms.eapsim.key._
import hms.eapsim.key.fips186.Fips186Lib
import hms.eapsim.keystore.{IdentityDecoderComponent, IdentityDecoderService, IdentityDecoderServiceImpl}
import hms.eapsim.ldap._
import hms.eapsim.mapgw.{RoutingTable, URIKey}
import hms.eapsim.protocol.{AccessAcceptParameter, EapAkaProtocolHandlerComponent, EapSimProtocolHandlerComponent, ProtocolHandlerComponent}
import hms.eapsim.repo.{MysqlConnectorParams, _}
import hms.eapsim.spml.SpmlGatewayConnectorComponent
import hms.eapsim.util._
import hms.eapsim.{RadiusRequestDelegateComponent, RadiusRequestReceiverComponent, RadiusResponseSenderComponent}
import io.netty.buffer.{ByteBuf, Unpooled}
import org.apache.logging.log4j.scala.Logging

import scala.collection.immutable.Iterable
import scala.collection.{JavaConversions, mutable}

/**
 *
 */
object ServiceRegistry {

  object JnaFips186PRNGenerator extends Fips186PRNGenerator {
    val FipsLib = Fips186Lib.INSTANCE

    def generateRandom(masterKey: Array[Byte]): ByteBuf = {
      val out: Array[Byte] = new Array(160)

      FipsLib.fips186_2prf(masterKey, out)

      Unpooled.copiedBuffer(out)
    }

  }

  val mysqlConnector = new MysqlConnectorImpl(MysqlConnectorParams(
    Settings.getString("eapsim.mysql.user.name"),
    Settings.getString("eapsim.mysql.user.password"),
    Settings.getString("eapsim.mysql.url"),
    Settings.getString("eapsim.mysql.driver.class"),
    Settings.getInt("eapsim.mysql.min.pool.size"),
    Settings.getInt("eapsim.mysql.initial.pool.size"),
    Settings.getInt("eapsim.mysql.max.pool.size"),
    Settings.getInt("eapsim.mysql.acquire.increment"),
    Settings.getInt("eapsim.mysql.max.idle.time.in.seconds"),
    Settings.getInt("eapsim.mysql.checkout.timeout.in.milli.seconds"),
    Settings.getInt("eapsim.mysql.unReturnedConnectionTimeout.seconds"),
    Settings.getBoolean("eapsim.mysql.debugUnReturnedConnectionStackTraces")

  ))

  object RepositoryRegistry extends RepositoryComponentRegistry

  trait RepositoryComponentRegistry extends RepositoryComponent {

    val dbDownSnmpMessage = Settings.getString("eapsim.snmp.db.connection.down")
    val dbUpSnmpMessage = Settings.getString("eapsim.snmp.db.connection.up")
    val translogSeparator = Settings.getString("eapsim.translog.separator")
    val serverId = Settings.getString("eapsim.server.id")
    val serverNodeId = Settings.getInt("eapsim.server.node.id")
    val isInMemoryCacheEnabled = Settings.getString("eapsim.re.auth.context.cache.in.memory.enable")
    val reAuthCacheMaxSize = Settings.getString("eapsim.re.auth.context.cache.max.size")
    val reAuthCacheEntryLifeTime = Settings.getString("eapsim.re.auth.context.cache.entry.life.time.in.seconds")
    val mysqlSOTimeout = Settings.getInt("eapsim.mysql.connection.socket.so.timeout.in.milli.seconds")
    val soMsisdnCachePoolSize = Settings.getInt("eapsim.msisdn.cache.so.pool.size")
    val soReAuthCtxPoolSize = Settings.getInt("eapsim.reauth.ctx.so.pool.size")

    val translogRepository: TranslogRepository = new MysqlTransLogRepository(mysqlConnector.pool, dbDownSnmpMessage,
      dbUpSnmpMessage, translogSeparator, serverId)
    val reAuthContextRepository : ReAuthContextRepository =
      if(isInMemoryCacheEnabled.toBoolean)
        new InMemoryReAuthContextRepositoryImpl(reAuthCacheMaxSize.toLong, reAuthCacheEntryLifeTime.toLong)
      else
        new ReAuthContextRepositoryImpl(mysqlConnector.pool, dbDownSnmpMessage, dbUpSnmpMessage, mysqlSOTimeout, soReAuthCtxPoolSize)
    val msisdnStoreRepository: MsisdnStoreRepository = new MsisdnStoreRepositoryImpl(mysqlConnector.pool, dbDownSnmpMessage, dbUpSnmpMessage, soMsisdnCachePoolSize)

    val keystoreRepository: KeystoreRepository = new KeystoreRepositoryImpl(mysqlConnector.pool, dbDownSnmpMessage, dbUpSnmpMessage, soMsisdnCachePoolSize)
  }

  private val connectingEndPoints: String = Settings.getString("tcp.client.connecting.end.point")
  private val tcpClientIOPoolSize: Int = Settings.getInt("tcp.client.io.thread.pool.size")
  private val tcpClientReconnectDelayInMillis: Int = Settings.getInt("tcp.client.reconnect.delay.in.millis")
  private val tcpClientNoOfConnections: Int = Settings.getInt("tcp.client.no.of.connections")
  private val tcpClientConnectionTimeoutInMillis: Int = Settings.getInt("tcp.client.connection.timeout.in.millis")
  private val tcpClientChannelReaderIdleTimeInSeconds: Int = Settings.getInt("tcp.client.channel.reader.idle.time.in.seconds")
  private val tcpClientChannelWriterIdleTimeInSeconds: Int = Settings.getInt("tcp.client.channel.writer.idle.time.in.seconds")
  private val tcpClientHeartbeatEvictionPeriodInSeconds = Settings.getLong("tcp.client.heartbeat.eviction.period.in.milli.seconds")
  private val tcpClientHeartbeatResponseTimeoutInMilliSeconds = Settings.getLong("tcp.client.heartbeat.response.timeout.in.milli.seconds")
  private val tcpClientEapUpSnmpMessage: String = Settings.getString("eapsim.snmp.tcp.client.eap.up.snmp.message")
  private val tcpClientEapDownSnmpMessage: String = Settings.getString("eapsim.snmp.tcp.client.eap.down.snmp.message")

  val clientManager: ClientManagerImpl = new ClientManagerImpl
  clientManager.setServerEndPoints(connectingEndPoints)
  clientManager.setClientIOPoolSize(tcpClientIOPoolSize)
  clientManager.setReconnectDelayInMillis(tcpClientReconnectDelayInMillis)
  clientManager.setNoOfConnections(tcpClientNoOfConnections)
  clientManager.setConnectionTimeoutInMillis(tcpClientConnectionTimeoutInMillis)
  clientManager.setReaderIdleTimeSeconds(tcpClientChannelReaderIdleTimeInSeconds)
  clientManager.setWriterIdleTimeSeconds(tcpClientChannelWriterIdleTimeInSeconds)
  clientManager.setEvictionPeriodInMilliSec(tcpClientHeartbeatEvictionPeriodInSeconds)
  clientManager.setResponseTimeoutInMilliSec(tcpClientHeartbeatResponseTimeoutInMilliSeconds)
  clientManager.setEapUpSnmpMessage(tcpClientEapUpSnmpMessage)
  clientManager.setEapDownSnmpMessage(tcpClientEapDownSnmpMessage)
  clientManager.init()

  val messageAuthenticator = Settings.getBoolean("eapsim.radius.server.append.message.authenticator")
  val saiResPoolSize =  Settings.getInt("eapsim.radius.server.sigtran.sai.res.pool")

  val messageOutboundHandler = new RadiusOutboundHandlerImpl(messageAuthenticator)
  val baseErrorCodeMapper = new ConfigBasedErrorCodeMapper(Settings.getSubConfig("eapsim.error.map"))

  val enableAuthVectorCache = Settings.getBoolean("eapsim.auth.vector.cache.enable")
  val authVectorCacheClanupInterval = Settings.getLong("eapsim.auth.vector.cache.clean.delay.in.millisecond")
  val authVectorCacheMaxEntryCount =  Settings.getLong("eapsim.auth.vector.cache.max.entry.count")
  val authVectorCacheEntryLifeTime =  Settings.getLong("eapsim.auth.vector.cache.entry.lifetime.in.millisecond")

  val tempAuthVectorCache = new AuthenticationVectorCacheImpl(Some(CacheConfig(authVectorCacheClanupInterval, authVectorCacheEntryLifeTime,
    authVectorCacheMaxEntryCount, enableAuthVectorCache)))

  val sigtranResponseListener = new SigtranResponseListener(saiResPoolSize, messageOutboundHandler, baseErrorCodeMapper,
  RepositoryRegistry.translogRepository, RadiusRequestListener.requestRepo, tempAuthVectorCache, clientManager)


  trait ReceivedRequestRepositoryRegistry extends ReceivedRequestRepositoryComponent {
    val sessionTimeout = Settings.getLong("eapsim.login.session.timeout.millis")
    val timeoutCheckInterval = Settings.getLong("eapsim.login.session.timeout.check.interval.in.milli.sec")

    val requestRepo = new SimpleRequestRepo(sessionTimeout, timeoutCheckInterval) with TranslogRepositoryComponent with RepositoryComponentRegistry {
    }
  }

  object SigtranGwConnectorRegistry extends Logging {
    private val clientManager: AsyncHttpClientManager = new AsyncHttpClientManager
    private val connectorList = loadConnectorList(clientManager)

    val hlrRtConfig = Settings.getString("eapsim.sigtran.imsi.hlr.point.code.gt.mapping")
    val sigtranGwClientId = Settings.getString("sigtran.gateway.clientId")
    val connectionValidityCheckPeriodInMillSec = Settings.getLong("sigtran.gateway.connection.validity.check.period.in.milli.second")
    val faultyConnectionIdlePeriodInMillSec = Settings.getLong("sigtran.gateway.faulty.connection.idle.period.in.mill.second")
    val successErrorCode = Settings.getString("sigtran.gateway.success.error.code")
    val requestTimeoutInMillis = Settings.getInt("sigtran.gateway.timeout.mill.sec")
    val timeoutSchedulerPeriodInMillis = Settings.getInt("sigtran.gateway.timeout.scheduler.period.mill.sec")
    val timeoutWorkerPoolSize = Settings.getInt("sigtran.gateway.timeout.worker.pool.size")
    val retriableErorrCode = loadRetriableErrorCode()
    val uriMap = scala.collection.mutable.Map(URIKey.SaiCallUrl -> Settings.getString("sigtran.uri.map.sai.call"))
    val sigtranDownSnmpMessage = Settings.getString("eapsim.snmp.sigtran.connection.down")
    val sigtranUpSnmpMessage = Settings.getString("eapsim.snmp.sigtran.connection.up")

    val sigtranGWConnector: SigtranConnectorImpl = new SigtranConnectorImpl(clientManager, sigtranResponseListener,
      successErrorCode, JavaConversions.asJavaList(retriableErorrCode), connectorList.size, requestTimeoutInMillis,
      timeoutSchedulerPeriodInMillis, timeoutWorkerPoolSize, sigtranUpSnmpMessage, sigtranDownSnmpMessage)
    sigtranGWConnector.init()

    clientManager.setAllClients(JavaConversions.asJavaMap(connectorList))
    clientManager.setSigtranConnector(sigtranGWConnector)
    clientManager.setFaultyConnectionValidityCheckPeriodInMillis(connectionValidityCheckPeriodInMillSec)
    clientManager.setFaultyConnectionIdlePeriodInMillis(faultyConnectionIdlePeriodInMillSec)
    clientManager.init()

    val routingTable: RoutingTable = new RoutingTable(hlrRtConfig)
    logger.info(s"SIGTRAN gateway connectors initiated with ${connectorList.size} connectors ===> [ $connectorList]")
  }

  object SpmlGatewayConnectorRegistry extends SpmlGatewayConnectorComponent with Logging {
    val templateRelativePath = Settings.getString("eapsim.spml.gw.template.relative.path")
    val searchTemplateName = Settings.getString("eapsim.spml.gw.template.search.req")
    val imsiKey = Settings.getString("eapsim.spml.gw.key.imsi")
    val msisdnKey = Settings.getString("eapsim.spml.gw.key.msisdn")
    val msisdnXpath = Settings.getString("eapsim.spml.gw.xpath.msisdn")
    val msisdnResponseId = Settings.getInt("eapsim.spml.gw.msisdn.response.id")
    val spmlDownMessage = Settings.getString("eapsim.snmp.spml.connection.down")
    val spmlUpMessage = Settings.getString("eapsim.snmp.spml.connection.up")

    val spmlGWConnector: SpmlGWConnector = new SpmlGWConnector(templateRelativePath, searchTemplateName, imsiKey,
      msisdnKey, msisdnXpath, msisdnResponseId, spmlDownMessage, spmlUpMessage)
    spmlGWConnector.init
  }

  trait KeyGeneratorComponentRegistry extends KeyGeneratorComponent with Fips186RandomGeneratorComponent {
    val keyGenerator: KeyGenerator = new KeyGenerator
    val fips186Generator: Fips186PRNGenerator = JnaFips186PRNGenerator
  }

  trait KeyManagerComponentRegistry extends KeyManagerComponent with Logging {
    val keyManager: KeyManager = new SigtranGwAuthKeyManager(SigtranGwConnectorRegistry.sigtranGWConnector,
      SigtranGwConnectorRegistry.sigtranGwClientId, SigtranGwConnectorRegistry.routingTable, SigtranGwConnectorRegistry.uriMap)
  }

  trait CoreRadiusRequestReceiverRegistry extends RadiusRequestReceiverComponent with RadiusRequestDelegateComponent with ReceivedRequestRepositoryRegistry with KeyGeneratorComponentRegistry with KeyManagerComponentRegistry with EapsimAttributeBuilderComponent with ProtocolHandlerComponent with EapSimProtocolHandlerComponent with EapAkaProtocolHandlerComponent with ConsumerDataServiceComponent with MsisdnServiceComponent with LdapConnectorComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with RepositoryComponentRegistry with CacheComponent with AuthenticationHelperUtilityComponent with RadiusResponseSenderComponent with IdentityDecoderComponent {
    val ignoreSimClientMacValidation = Settings.getBoolean("eapsim.disable.client.mac.validation.for.sim")
    val ignoreAkaClientMacValidation = Settings.getBoolean("eapsim.disable.client.mac.validation.for.aka")

    val includeMsMppeKeys = Settings.getBoolean("eapsim.include.ms.mppe.keys")
    val msisdnCacheClanupInterval = Settings.getLong("eapsim.msisdn.cache.clean.delay.in.second")
    val msisdnCacheEntryLifeTime = Settings.getLong("eapsim.msisdn.cache.entry.lifetime.in.second")
    val maxReAuthCounterForSim = Settings.getInt("eapsim.max.re.auth.counter.for.sim")
    val maxReAuthCounterForAka = Settings.getInt("eapsim.max.re.auth.counter.for.aka")
    val enableQueryLDAP = Settings.getBoolean("eapsim.enable.query.ldap.for.radius.class")

    val errorCodeMapper: ErrorCodeMapper = baseErrorCodeMapper

    val radiusRequestReceiver = new RadiusRequestReceiver(serverNodeId)
    val eapsimAttributeBuilder = new EapsimAttributeBuilder
    val defaultAccessAcceptParams = createAccessAcceptDefaultParamList
    val eapsimProtocolHandler = new EapSimProtocolHandler(defaultAccessAcceptParams, ignoreSimClientMacValidation, includeMsMppeKeys, maxReAuthCounterForSim)
    val eapakaProtocolHandler = new EapAkaProtocolHandler(defaultAccessAcceptParams, ignoreAkaClientMacValidation, includeMsMppeKeys, maxReAuthCounterForAka)
    val msisdnService = new MsisdnDispatcherService(SpmlGatewayConnectorRegistry, SpmlGatewayConnectorRegistry.imsiKey,
      SpmlGatewayConnectorRegistry.msisdnKey, SpmlGatewayConnectorRegistry.msisdnResponseId,
      SigtranGwConnectorRegistry.sigtranGwClientId, SigtranGwConnectorRegistry.routingTable, msisdnCacheClanupInterval,
      msisdnCacheEntryLifeTime, msisdnStoreRepository, enableQueryLDAP)
    val identityDecoder: IdentityDecoderService = new IdentityDecoderServiceImpl(keystoreRepository)

    val saiDataCallUri = "sigtran.uri.map.sai.call"

    val ldapHost: String = Settings.getString("eapsim.ldap.host")
    val ldapPort: Int = Settings.getInt("eapsim.ldap.port")
    val ldapAuthDn: String = Settings.getString("eapsim.ldap.auth.dn")
    val ldapPwd: String = Settings.getString("eapsim.ldap.auth.pwd")
    val reconnectionDelay = Settings.getInt("eapsim.ldap.reconnection.delay")
    val responseTimeout = Settings.getInt("eapsim.ldap.response.timeout")
    val ldapConnector: LdapConnector = new LdapConnectorImpl(ldapHost, ldapPort, ldapAuthDn, ldapPwd, reconnectionDelay, responseTimeout, ldapSnmpMsgMap)
    val ldapConnectorV2: LDAPConnectorImpl = new LDAPConnectorImpl(getLdapconfiguration())
    ldapConnectorV2.init()

    val ldapSearchDn = Settings.getString("eapsim.ldap.search.base.dn")
    val ldapFilterTxt = Settings.getString("eapsim.ldap.search.filter")
    val ldapDefaultRadiusClass = Settings.getString("eapsim.ldap.default.radius.class")


    val reAuthCtxCleanupInterval =  Settings.getLong("eapsim.re.auth.context.cleanup.interval.in.millisecond")
    val reAuthCtxLifeTime =  Settings.getLong("eapsim.re.auth.context.life.time.in.millisecond")
    val maxExpiredCtxToBeLoad = Settings.getInt("eapsim.re.auth.context.max.expired.tobe.load")
    val authVectorCount =  Settings.getInt("eapsim.requested.number.of.auth.vector.count")

    val messageResponder = messageOutboundHandler


    private val listeningHost = Settings.getString("tcp.server.listening.host")
    private val listeningPort = Settings.getInt("tcp.server.listening.port")
    private val tcpServerBossPoolSize: Int = Settings.getInt("tcp.server.boss.pool.size")
    private val tcpServerWorkerPoolSize: Int = Settings.getInt("tcp.server.worker.pool.size")
    private val tcpServerRequestResponseWorkerPoolSize: Int = Settings.getInt("tcp.server.request.response.pool.size")

    val getServerMessageListener = new ServerMessageHandler(clientManager)

    private val tcpServer: Server = new Server
    tcpServer.setHost(listeningHost)
    tcpServer.setPort(listeningPort)
    tcpServer.setBossPoolSize(tcpServerBossPoolSize)
    tcpServer.setWorkerPoolSize(tcpServerWorkerPoolSize )
    tcpServer.setContextMessageListener(getServerMessageListener)
    tcpServer.setServerRequestResponseHandlerPoolSize(tcpServerRequestResponseWorkerPoolSize)
    tcpServer.init()

    val consumerDataService = new ConsumerDataService(ldapSearchDn, ldapFilterTxt, ldapDefaultRadiusClass, enableQueryLDAP, ldapConnectorV2,
      Settings.getString("eapsim.snmp.ldap.connection.down"),
      Settings.getString("eapsim.snmp.ldap.connection.up"))

    val authenticationVectorCache = tempAuthVectorCache
    val authVectorCacheManager = new CacheManagerImpl(authenticationVectorCache)
    val reAuthCtxCleanupManager = new ReAuthContextCleanerManagerImpl(reAuthContextRepository,
      reAuthCtxCleanupInterval, (reAuthCtxLifeTime/1000).toInt, maxExpiredCtxToBeLoad)
    val getAuthenticationHelper = new AuthenticationHelperUtilityImpl(authenticationVectorCache, keyManager,
      authVectorCount, sigtranResponseListener)



    if(!isInMemoryCacheEnabled.toBoolean) {
      reAuthCtxCleanupManager.init
    }
    authVectorCacheManager.init
    msisdnService.init()
    requestRepo.initTimeouter
  }


  def getLdapconfiguration(): LDAPConfiguration = {
    val config = new LDAPConfiguration
    config.setHostName(Settings.getString("eapsim.ldap.host"))
    config.setBindDN(Settings.getString("eapsim.ldap.auth.dn"))
    config.setBaseSearchDN(Settings.getString("eapsim.ldap.search.base.dn"))
    config.setBindPassword(Settings.getString("eapsim.ldap.auth.pwd"))
    config.setPort(Settings.getInt("eapsim.ldap.port"))
    config.setScope(Settings.getInt("eapsim.ldap.search.scope"))
    config.setMaxActive(Settings.getInt("eapsim.ldap.pool.max.active"))
    config.setMaxWait(Settings.getInt("eapsim.ldap.pool.max.wait"))
    config.setMaxIdle(Settings.getInt("eapsim.ldap.pool.max.idle"))
    config.setMinIdle(Settings.getInt("eapsim.ldap.pool.min.idle"))
    config.setExhaustedAction(Settings.getString("eapsim.ldap.pool.min.exhausted.action"))

    config
  }

  def loadConnectorList(clientManager: ClientManager): scala.collection.mutable.Map[String, Client] = {
    import scala.collection.mutable._
    val connectorCount = Settings.getInt("number.of.sigtran.connector")
    val hostList: Map[String, Client] = mutable.Map[String, Client]()
    for (i <- 0 until connectorCount) {
      val name = Settings.getString(s"sigtran.gateway.$i.name")
      val host = Settings.getString(s"sigtran.gateway.$i.host")
      val port = Settings.getInt(s"sigtran.gateway.$i.port")
      val maxConnections = Settings.getInt(s"sigtran.gateway.$i.max.connections")
      val reconnectDelayInSeconds = Settings.getInt(s"sigtran.gateway.$i.reconnect.delay.in.seconds")
      val priority = Settings.getInt(s"sigtran.gateway.$i.priority")
      val gwClientPoolSize = Settings.getInt(s"sigtran.gateway.$i.request.handler.thread.pool.size")

      val httpClient: AsyncHttpClient = new AsyncHttpClient(name, priority, host, port, new DefaultThreadFactory(s"SGW-C${i}"),
        gwClientPoolSize, maxConnections, reconnectDelayInSeconds, clientManager)
      hostList.put(name, httpClient)
    }
    validateSigtranClientConfigs(hostList)
    hostList
  }

  private def validateSigtranClientConfigs(hostList: scala.collection.mutable.Map[String, Client]) = {
    val priorities: List[Int] = hostList.map(entry => entry._2.getPriority).toList
    if(priorities.distinct.size != priorities.size) {
      val duplicatePriorities: Iterable[Int] = priorities.groupBy(identity).collect{ case (x, List(_,_,_*)) => x}
      throw new RuntimeException(s"Found duplicate priority elements [${duplicatePriorities.mkString(",")}]")
    }
    val nonPositivePriorities: List[Int] = priorities.filter(priority => priority <= 0)
    if(nonPositivePriorities.size > 0) {
      throw new RuntimeException(s"Found non positive priority elements [${nonPositivePriorities.mkString(",")}]")
    }
  }

  object RadiusRequestListener extends SimpleRadiusRequestListener with CoreRadiusRequestReceiverRegistry

  def createAccessAcceptDefaultParamList: List[AccessAcceptParameter[_]] = {

    val attrs = Settings.getIntOpt("eapsim.access.accept.default.Service-Type").map(v => AccessAcceptParameter[Int]("Service-Type", v)) ::
      Settings.getIntOpt("eapsim.access.accept.default.Session-Timeout").map(v => AccessAcceptParameter[Int]("Session-Timeout", v)) ::
      Settings.getIntOpt("eapsim.access.accept.default.Idle-Timeout").map(v => AccessAcceptParameter[Int]("Idle-Timeout", v)) ::
      Settings.getIntOpt("eapsim.access.accept.default.Acct-Interim-Interval").map(v => AccessAcceptParameter[Int]("Acct-Interim-Interval", v)) :: List()
    attrs.flatten
  }

  def ldapSnmpMsgMap: Map[String, String] = {
    Map("eapsim.snmp.ldap.connection.down" -> Settings.getString("eapsim.snmp.ldap.connection.down"),
      "eapsim.snmp.ldap.connection.up" -> Settings.getString("eapsim.snmp.ldap.connection.up"))
  }

  def loadRetriableErrorCode(): List[String] = {
    val errorCodes = Settings.getString("sigtran.gateway.retryable.error.code")
    errorCodes.split(",").toList
  }
}