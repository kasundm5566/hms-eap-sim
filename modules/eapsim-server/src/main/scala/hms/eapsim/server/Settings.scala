/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.server

import com.typesafe.config.{ConfigFactory, Config}

import scala.util.control.Exception._

/**
 * HOCONF Wrapper
 *
 * Note: This  is used for removing lift Props in core
 */

object Settings {

  private val config = ConfigFactory.load("eap-sim.conf")

  def getBoolean(path: String): Boolean = config.getBoolean(path)

  def getString(path: String): String = config.getString(path)

  def getSubConfig(key: String) = config.getConfig(key)

  def getStringOpt(path: String): Option[String] = {
    allCatch[String].opt(getString(path))
  }

  def getInt(path: String): Int = config.getInt(path)

  def getIntOpt(path: String): Option[Int] = {
    allCatch[Int].opt(getInt(path))
  }

  def getLong(path: String): Long = config.getLong(path)

  def getLongOpt(path: String): Option[Long] = {
    allCatch[Long].opt(getLong(path))
  }

  def getDouble(path: String): Double = config.getDouble(path)

  def getDoubleOpt(path: String): Option[Double] = {
    allCatch[Double].opt(getDouble(path))
  }


}
