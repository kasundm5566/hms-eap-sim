package hms.eapsim.mapgw.failover.state

import hms.eapsim.mapgw.{SaiResp, SaiReq}
import hms.eapsim.mapgw.failover.HttpConnector

class AllDisconnectedState extends State {
  override def sendSai(req: SaiReq): SaiResp= {
    throw new AllGWDownException("Can not delegate SAI request, since all the connectors are down")
  }

  override def name: String = "ALL-DISCONNECTED"

  override def getConnector: Option[HttpConnector] = None


  override def toString(): String = name
}
