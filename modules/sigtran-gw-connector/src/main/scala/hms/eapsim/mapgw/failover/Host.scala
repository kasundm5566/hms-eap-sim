package hms.eapsim.mapgw.failover

import java.util.Date
import hms.eapsim.mapgw.client.{HttpConnectorConfig, DefaultHttpClient}

case class Host(ip: String, port: Int, maxConnections: Int, isPrimary: Boolean, priority: Int) {
  override def equals(ob: Any): Boolean = {
    ob match {
      case ob: Host => this.ip.equals(ip) && ob.port == port
    }
  }
  override def hashCode(): Int =  31 * ip.hashCode + port

  override def toString = s"ip: $ip, port: $port, isPrimary: ${if(isPrimary) "Yes" else "No"}, priority: $priority"
}

case class HttpConnector(name: String, host: Host, timeout: Long) {
  val client = new DefaultHttpClient(HttpConnectorConfig(name,
    host.ip,
    host.port,
    host.maxConnections))

  override def equals(ob: Any): Boolean = {
    ob match {
      case con: HttpConnector => host.equals(con.host)
      case _ => false
    }
  }

  override def hashCode(): Int =  host.hashCode()

  override def toString = s"[$name( $host )]"
}

case class FaultyHost(connector: HttpConnector, failedTime: Date = new Date()) {

  override def equals(ob: Any): Boolean =  {
    ob match {
      case fh: FaultyHost => this.connector.equals(fh.connector)
      case _ => false
    }
  }

  override def hashCode(): Int =  connector.hashCode()

  override def toString = s"{connector: $connector, failedTime: $failedTime}"
}