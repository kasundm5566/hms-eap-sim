package hms.eapsim.mapgw.client

import java.io.IOException

import hms.eapsim.mapgw.client.CustomJsonProtocol._
import hms.eapsim.mapgw.{SaiReq, SaiResp}
import org.apache.http.HttpHost
import org.apache.http.client.HttpRequestRetryHandler
import org.apache.http.client.methods.{CloseableHttpResponse, HttpPost}
import org.apache.http.conn.routing.HttpRoute
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.{DefaultConnectionKeepAliveStrategy, CloseableHttpClient, HttpClientBuilder}
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.protocol.HttpContext
import org.apache.http.util.EntityUtils
import spray.json._

case class HttpConnectorConfig(connectorName: String, host: String, port: Int, maxConnections: Int) {
  require(connectorName.matches("""^[a-zA-Z0-9][a-zA-Z0-9-]*$"""))
}

trait HttpConnector {

  protected val config: HttpConnectorConfig
  private var client: CloseableHttpClient = null
  private var baseUrl: String = null

  def init {
    baseUrl = "http://" + config.host + ":" + config.port + "/"
    val connManager = new PoolingHttpClientConnectionManager
    connManager.setMaxTotal(config.maxConnections)
    connManager.setMaxPerRoute(new HttpRoute(new HttpHost(config.host, config.port)), config.maxConnections)
    connManager.setDefaultMaxPerRoute(config.maxConnections)
    val builder: HttpClientBuilder = HttpClientBuilder.create()
    client = builder
      .setConnectionManager(connManager)
      .setRetryHandler(new NoRetryRetryHandler)
      .setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy)
      .build()
  }

  class NoRetryRetryHandler extends HttpRequestRetryHandler {
    override def retryRequest(exception: IOException, executionCount: Int, context: HttpContext): Boolean = false
  }

  def doPost(path: String, input: SaiReq): SaiResp = {
    val httpPost: HttpPost = new HttpPost(baseUrl + path)
    val entity: StringEntity = new StringEntity(input.toJson.prettyPrint)
    entity.setContentType("application/json")
    httpPost.setEntity(entity)

    var response : CloseableHttpResponse = null
    try {
      response = client.execute(httpPost)
      val respStr: String = EntityUtils.toString(response.getEntity())
      val resp: SaiResp = respStr.parseJson.convertTo[SaiResp]
      resp
    } finally {
      if (response != null) {
        response.close()
      }
    }
  }
}