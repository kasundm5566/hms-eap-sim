package hms.eapsim.mapgw.failover.state

import hms.eapsim.mapgw.failover.{HttpConnector, STConnectionService}

trait State extends STConnectionService {
  def getConnector: Option[HttpConnector]
  def name: String
}
