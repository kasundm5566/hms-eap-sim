package hms.eapsim.mapgw.failover.state

case class AllGWDownException(message: String) extends Exception(message)