package hms.eapsim.mapgw

import spray.json.DefaultJsonProtocol

/**
 * Json formats for SDP NBL messages
 */
object SigtranGwJsonProtocol extends DefaultJsonProtocol {

  implicit val saiReqFmt = jsonFormat5(SaiReq)
  implicit val tripletFmt = jsonFormat3(Triplet)
  implicit val quintupletFmt = jsonFormat5(Quintuplet)
  implicit val saiRespFmt = jsonFormat6(SaiResp)
}
