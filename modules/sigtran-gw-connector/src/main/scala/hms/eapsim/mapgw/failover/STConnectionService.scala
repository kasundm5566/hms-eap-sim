package hms.eapsim.mapgw.failover

import hms.eapsim.mapgw.{SaiResp, SaiReq}

trait STConnectionService {
  def sendSai(req: SaiReq): SaiResp
}
