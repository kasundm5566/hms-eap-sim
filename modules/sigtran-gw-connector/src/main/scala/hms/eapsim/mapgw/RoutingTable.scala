/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.mapgw

import org.apache.logging.log4j.scala.Logging

/**
 *
 */
class RoutingTable(hlrRoutingConfiguration: String) extends Logging {

  /**
   * [IMSI_RANGE, (HLR_POINTCODE, HLR_GLOBAL_TITLE)]
   */
  private val RoutingTable: Map[String, (Int, String)] = loadRoutingTable(hlrRoutingConfiguration)

  /**
   * Return the HLR point code and GT for given IMSI
   * Return Option[(POINT_CODE, GT)]
   */
  def getHrlRoutingData(imsi: String) = RoutingTable.find(rt => imsi.startsWith(rt._1)).map(hlrRt => (hlrRt._2._1, hlrRt._2._2))

  /**
   * Process configuration string and generated HLR routing table.
   * Configuration string is in following format.
   * <IMSI_RANGE_1> -> <HLR_1_PC:HLR_1_GT>, <IMSI_RANGE_2> -> <HLR_2_PC:HLR_2_GT>
   * E.g: 52503437->HLR4, 525034740->HLR5, 525034741->HLR5
   */
  private def loadRoutingTable(routingStr: String) = {
    logger.debug(s"Loading IMSI-HLR Routing Table[${routingStr}]")
    val rt = routingStr.split(",").toList
      .map(ri => {
        ri.split("->").toList match {
          case (imsi: String) :: (hlr: String) :: Nil => {
            hlr.split(":").toList match {
              case (pc: String) :: (gt: String) :: Nil => (imsi.trim, (Integer.parseInt(pc.trim), gt.trim))
              case err => throw new IllegalArgumentException(s"Invalid IMSI-HLR_PC configuration[${ri}]")
            }

          }
          case err => throw new IllegalArgumentException(s"Invalid IMSI-HLR_PC configuration[${err}]")
        }
      })
      .toMap

    logger.info(s"Loaded IMSI-HLR Routing Table[${rt}]")
    rt
  }
}
