package hms.eapsim.mapgw.client

import hms.eapsim.mapgw._
import spray.json.DefaultJsonProtocol

object CustomJsonProtocol extends DefaultJsonProtocol {

  implicit val tripletFormat = jsonFormat3(Triplet.apply)
  implicit val quintupletFormat = jsonFormat5(Quintuplet.apply)
  implicit val saiReqFormat = jsonFormat5(SaiReq.apply)
  implicit val saiRespFormat = jsonFormat6(SaiResp.apply)
}
