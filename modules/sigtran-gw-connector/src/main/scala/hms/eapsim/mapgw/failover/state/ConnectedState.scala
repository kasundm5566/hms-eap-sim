package hms.eapsim.mapgw.failover.state

import hms.eapsim.mapgw.{SaiReq, SaiResp, _}
import hms.eapsim.mapgw.failover.HttpConnector

import scala.collection.mutable._

class ConnectedState(var connector: HttpConnector, uriMap: Map[String, String]) extends State {

  override def sendSai(req: SaiReq): SaiResp = {
    connector.client.doPost(uriMap(URIKey.SaiCallUrl), req)
  }

  override def name: String = "CONNECTED"

  override def getConnector: Option[HttpConnector] = Some(connector)

  override def toString(): String = s"$name-$connector"
}
