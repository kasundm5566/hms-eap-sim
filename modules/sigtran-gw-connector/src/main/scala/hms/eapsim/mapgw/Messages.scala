package hms.eapsim.mapgw


/**
  SAI Req:
  {clientId : "test-client-01", "calledPartyPc" : 1120, "imsi": "525036159999983", "correlationId": "222222224555"}
  */
case class SaiReq(clientId: String, calledPartyPc: Int, calledPartyGt: String, imsi: String, requestedVectors: Int)

case class Triplet(rand: String, kc: String, sres: String)

case class Quintuplet(rand: String, xres: String, ck: String, ik: String, autn: String)

/**
 * Link failure response
 * {"statusCode":"9000","statusDescription":"SCTP link not available","transactionId":"140541389249510000", "correlationId": "222222224555"}
 */
case class SaiResp(respondingSystemGt: Option[String],
                   transactionId: String,
                   statusCode: String,
                   statusDescription: String,
                   quinTuplets: Option[List[Quintuplet]],
                   gsmTriplets: Option[List[Triplet]])