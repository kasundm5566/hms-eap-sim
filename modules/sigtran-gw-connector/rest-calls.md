# Map Gateway REST Calls

## Restore Data

### Request

<pre>
curl http://127.0.0.1:4000/sigtran-api:restore-data/ --data-binary "{clientId : "test-client-01", "calledPartyPc" : 1120, "calledPartyGt" : "6596845976",  "imsi": "525036159999983"}"
</pre>

### Response

<pre>
{
   "statusCode":"1000",
   "statusDescription":"Success",
  "rdi": 8,
  "msisdn": "6598996183",
  "routingCat": 4338,
  "teleService": [
    17,
    33,
    34,
    98
  ]
}
</pre>



## SAI

### Request
<pre>
 curl http://127.0.0.1:4000/sigtran-api:sai/ --data-binary "{clientId : "test-client-01", "calledPartyPc" : 1120, "imsi": "525036159999983"}"
</pre>

### Response for GSM Sim
<pre>
{
   "statusCode":"1000",
   "statusDescription":"Success",
  "quinTuplets": [],
  "gsmTriplets": [
    {
      "rand": "642926a5cc2dcab8ed22a353d130b1b9",
      "kc": "170c151afe7b8000",
      "sres": "9679a093"
    },
    {
      "rand": "6399d79cc6d1286a141b71be763edfdc",
      "kc": "682e09529734d800",
      "sres": "d44e7632"
    },
    {
      "rand": "e3e3d1d64338c4fad9d6cd270b9b71c0",
      "kc": "3073f4404499f000",
      "sres": "b01d22e3"
    }
  ],
  "respondingSystemGt": "6594740131"
}
</pre>

### Response for 3G Sim
<pre>
{
   "statusCode":"1000",
   "statusDescription":"Success",
  "quinTuplets": [
    {
      "rand": "517391360bd0062f9bdee95dcff58df4",
      "xres": "cdf8b6a80b80cd59",
      "ck": "779d1e3050c82ab4b787f8c8726cbd4f",
      "ik": "b785000ca38d53925b482186eb899143",
      "autn": "55f63338f7ab00007ff6ab201684c8b1"
    }
  ],
  "gsmTriplets": [],
  "respondingSystemGt": "6594740131"
}
</pre>