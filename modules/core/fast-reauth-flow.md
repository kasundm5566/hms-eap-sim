# Fast Re-Authentication

## Eap-SIM Fast Re-Authentication

### Full Authentication flow changes

1. User name generator should be implemented to generate `NEXT_REAUTH_ID`.   - done

1. `AT_NEXT_REAUTH_ID` need to be sent with EAP-Request/SIM/Challenge  - done

1. When *EAP-Response/Identity* received need to check the availability in Fast Re-auth DB. If entry if found
should proceed with Fast Re-Authentication, else should use Full Authentication flow.   - done

1. NEXT_REAUTH_ID, Master Key, k_aut, k_encr, Imsi and Counter need to be kept in the DB. This will be saved at the
   end of the successful full authentication. - done
1. Authentication vectors shall be saved in a in-memory map & forget after sometime (configurable). SAI call is made
   only if no previous authentication vector found for that particular `IMSI`   - done

1. Need to maintain a counter in the DB and need to initialize it to `1` with every full authentication. When
validating for eligibility for Fast ReAuth this counter value should also be validated. - 1/2 done

1. Once *EAP-Response/Identity* received server shall check if the identity value exists in the db. If it is exist the
   request is routed to a new flow called 'Re-Authentication flow', which is described bellow, Otherwise the request should
   follow the full authentication flow. - done

### EAP-SIM Notifications
* . as per chapter 6

### Re-Authentication flow
1. There are two ways to pass next re-auth id to server
    * include in EAP-Response/Identity
    * include `AT_IDENTITY` attribute  in the EAP-Response/SIM/Start
1. If the re-auth identity is valid , then server send EAP- Request/AKA-Reauthentication
  include [`AT_ENCR_DATA` {*`AT_COUNTER`, *`AT_NONCE_S`, *`AT_NEXT_REAUTH_ID`}, `AT_IV`]. The counter < Max limt
1. Both  `AT_NEXT_REAUTH_ID` and  `AT_COUNTER` should be included in `AT_ENCR_DATA`  - done
1. Include encrypted `AT_NONCE_S` in *EAP-Request/SIM/Re-authentication*  - done
1. Generate a fresh Master Session Key, as specified in Section  7.  - done
1. `AT_MAC` must included in *EAP-Response/SIM/Re-authentication*. K_aut is used for encryption. (see 9.6)
    * MAC = SHA-1(EAP packet| NONCE_S), Here 16-byte NONCE_S value from the server's AT_NONCE_S is used.

### Falling back to full authentication

1. server does not recognize fast re auth id.
    * In that case, Permanent identity has to be decoded from full auth identity & server should initiate full authentication by issuing  EAP-Request/SIM/Start.
      Do not include `AT_ANY_ID_REQ` for this call.
    * If server failed to decode permanent id, then include `AT_FULLAUTH_ID_REQ` in the EAP-Request/SIM/Start.

### Key Generation

The Master Key is fed into a Pseudo-Random number Function (PRF)
which generates separate Transient EAP Keys (TEKs) for protecting
EAP-SIM packets, as well as a Master Session Key (MSK) for link layer
security, and an Extended Master Session Key (EMSK) for other
purposes.  On fast re-authentication, the same TEKs MUST be used for
protecting EAP packets, but a new MSK and a new EMSK MUST be derived
from the original MK and from new values exchanged in the fast
re-authentication.

1. The same Pseudo-Random Number generator (PRF) can be used to generate a new Master Session Key (KSK) and a new Extended Master Session Key (EMSK).
1. 160-bit (20 bytes) XKEY is taken from PRF(MK). The order resulting XKEY is as follows.
       * XKEY Full Auth => {TEKs [K_encr (128 bits, 16 bytes), K_aut (128 bits, 16 bytes)], MSK (64 bytes), EMSK (64 bytes)}
       * XKEY' = SHA1(Identity|counter|`NONCE_S`| MK)<br>
            - Identity - fast re-auth identity from `AT_IDENTITY` attribute [*EAP-Response/SIM/Start, EAP-Response/Identity*]<br>
            - counter - `AT_COUNTER` from *EAP-Response/SIM/Re-authentication*<br>
            - `NONCE_S` denotes the 16-byte `NONCE_S` value from `AT_NONCE_S` attribute used in *EAP-Request/SIM/Re-authentication*<br>
            - MK - master key from full authentication<br>
       * apply XKEY' and generate 320-bit random number. Then extract 64 byte MSK, 64 byte EMSK from key stream (x_0, x_1, ...).
       * first 32 byte from MSK can be used as Pairwise Master Key (PMK)<br>
            - MS-MPPE-RECV-KEY --> 32 bytes<br>
            - MS-MPPE-SEND-KEY --> 32 bytes

1. previous *K_aut* for `AT_MAC` & *K_encr* used in full authentication is used for `AT_ENCR_DATA` to encrypt
1. The same PRF can be used to generate a new Master Session Key and a new Extended Master Session Key (EMSK), Master Session Key (MSK).
   The seed value for the function is as follows,<br></br>
    XKEY' = SHA1(Identity|counter|`NONCE_S`| MK)
    * Identity - fast re-auth identity from `AT_IDENTITY` attribute [*EAP-Response/SIM/Start, EAP-Response/Identity*]
    * counter - `AT_COUNTER` from *EAP-Response/SIM/Re-authentication*
    * `NONCE_S` denotes the 16-byte `NONCE_S` value from `AT_NONCE_S` attribute used in *EAP-Request/SIM/Re-authentication*
    * MK - master key from full authentication

### Re-Auth Success flow

       Peer                                             Authenticator
          |                                                       |
          |                               EAP-Request/Identity    |
          |<------------------------------------------------------|
          |                                                       |
          | EAP-Response/Identity                                 |
          | (Includes a fast re-authentication identity)          |
          |------------------------------------------------------>|
          |                                                       |
          |                          +--------------------------------+
          |                          | Server recognizes the identity |
          |                          | and agrees to use fast         |
          |                          | re-authentication              |
          |                          +--------------------------------+
          |                                                       |
          :                                                       :
          :                                                       :
          :                                                       :
          :                                                       :
          |  EAP-Request/SIM/Re-authentication                    |
          |  (AT_IV, AT_ENCR_DATA, *AT_COUNTER,                   |
          |   *AT_NONCE_S, *AT_NEXT_REAUTH_ID, AT_MAC)            |
          |<------------------------------------------------------|
          |                                                       |
     +-----------------------------------------------+            |
     | Peer verifies AT_MAC and the freshness of     |            |
     | the counter. Peer MAY store the new fast re-  |            |
     | authentication identity for next re-auth.     |            |
     +-----------------------------------------------+            |
          |                                                       |
          | EAP-Response/SIM/Re-authentication                    |
          | (AT_IV, AT_ENCR_DATA, *AT_COUNTER with same value,    |
          |  AT_MAC)                                              |
          |------------------------------------------------------>|
          |                          +--------------------------------+
          |                          | Server verifies AT_MAC and     |
          |                          | the counter                    |
          |                          +--------------------------------+
          |                                                       |
          |                                          EAP-Success  |
          |<------------------------------------------------------|
          |                                                       |

                    Figure 8: Fast Re-authentication



### Simulator Modifications
1. Extract `AT_NEXT_REAUTH_ID` sent in EAP-Request/SIM/Challenge & use the extracted value in  EAP-Response/Identity for
   re-authentication
1. Introduce new command to initiate re-authentication, say `reAuth`
1. peer MUST verify that its counter <= counter from server
1. The AT_MAC attribute in the peer's response is calculated over NONCE_S to provide a challenge/response authentication
    * EAP packet| `NONCE_S`
1. `AT_ENCR_DATA` MUST be added for *EAP-Response/SIM/Re-authentication*. Nested attributes are as follows,
    * It MUST include the `AT_COUNTER`
    * `AT_COUNTER_TOO_SMALL` attribute MAY be included
1. If the validations are failed in the peer, then it MUST not use *EAP-Response/SIM/Re-authentication*
   but, *EAP-Response/SIM/Client-Error.*



## EAP-Aka Fast Re-Authentication

Aka re-authentication is similar to the Sim re-auth flow.

### Falling back to full authentication
1. server does not recognize the re-authentication id being sent. In that case, decode permanent id from re-auth id send request/AKA-challenge.
   If the decoding failed for some reason, then server should initiate full authentication by sending EAP-Request/AKA-Identity.
   This packet always start full authentication if server does not include `AT_ANY_ID_REQ`
1. counter > MAX
1.

### Changest to be done one Full authentication
1. add encrypted AT_NEXT_REAUTH_ID in EAP- Request/-AKA-Challenge

### Re-authentication Flow

1. There are two ways to pass next re-auth id to server
    * include in EAP-Response/Identity
    * include `AT_IDENTITY` attribute  in the EAP-Response/AKA-Identity
1. If the re-auth identity is valid , then server send EAP- Request/AKA-Reauthentication
  include [`AT_ENCR_DATA` {*`AT_COUNTER`, *`AT_NONCE_S`, *`AT_NEXT_REAUTH_ID`},`AT_IV`]. The counter < Max limt
1. identities are one time identities. How does identity build ?   TODO: ****************************************************




        Peer                                             Authenticator
          |                                                       |
          |                               EAP-Request/Identity    |
          |<------------------------------------------------------|
          |                                                       |
          | EAP-Response/Identity                                 |
          | (Includes a fast re-authentication identity)          |
          |------------------------------------------------------>|
          |                          +--------------------------------+
          |                          | Server recognizes the identity |
          |                          | and agrees on using fast       |
          |                          | re-authentication              |
          |                          +--------------------------------+
          |  EAP-Request/AKA-Reauthentication                     |
          |  (AT_IV, AT_ENCR_DATA, *AT_COUNTER,                   |
          |   *AT_NONCE_S, *AT_NEXT_REAUTH_ID, AT_MAC)            |
          |<------------------------------------------------------|
          |                                                       |
          :                                                       :
          :                                                       :


          :                                                       :
          :                                                       :
          |                                                       |
     +-----------------------------------------------+            |
     | Peer verifies AT_MAC and the freshness of     |            |
     | the counter. Peer MAY store the new re-       |            |
     | authentication identity for next re-auth.     |            |
     +-----------------------------------------------+            |
          |                                                       |
          | EAP-Response/AKA-Reauthentication                     |
          | (AT_IV, AT_ENCR_DATA, *AT_COUNTER with same value,    |
          |  AT_MAC)                                              |
          |------------------------------------------------------>|
          |                          +--------------------------------+
          |                          | Server verifies AT_MAC and     |
          |                          | the counter                    |
          |                          +--------------------------------+
          |                                          EAP-Success  |
          |<------------------------------------------------------|
          |                                                       |

                        Figure 10: Reauthentication



### EAP-Response/AKA/Re-authentication
1. AT_MAC calculated over AT_NONCE_S - same


### Key Generation

1. Full Auth
    * MK = SHA1(Identity|IK|CK) --> SHAR-1
    * XKEY = PRF(MK)
      then use that as a seed value for generating follows, <br>
      TEK [K_aut, K_encr], MSK, EMSK . Same TEK use for Re-auth
      usage of TEK as sim.
    * resulting 320-bit random numbers x_0, x_1, ..., x_m-1
      [K_encr (128 bits), K_aut (128 bits), MSK (64 bytes), EMSK (64 bytes)


1. Re-auth
    * XKEY' = SHA1(Identity|counter|NONCE_S| MK) - same
    * resulting 320-bit random numbers x_0, x_1, ..., x_m-1 concatenated and partitioned to build following keys
      MKS (64 byte) , EMSK (64 bytes)   - same as sim

        * MS-MPPE-RECV-KEY -> First 32 bytes of MSK
        * MS-MPPE-SEND-KEY -> Second 32 bytes of MSK
