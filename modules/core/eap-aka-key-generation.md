# Eap-AKA Key Generation Algorithms

Key generation is described in section 7 of the RFC 4187.

**Note:** In formulars described below the "|" character denotes concatenation.


## Master Key(MK) Generation

`MK = SHA1(Identity|IK|CK)`

SHA1 algorithm used here is  *SHA-1*

- Identity
 - Identity string taken from AT_IDENTITY attribute or from EAP-Response/Identity packets.

- IK
 - IK values of GSM quintuplet taken from the sending *Send Authentication Info(SAI)* request to HLR.

- CK
 - CK values of GSM quintuplet taken from the sending *Send Authentication Info(SAI)* request to HLR.


## Pseudo Random Key Generation

PRK is generation using FIPS 186-2 random number generation algorithm with some modifications. Exact algorithm can be found at *Appendix B.  Pseudo-Random Number Generator* of RFC 4186. Master key is fed to Psedo Random Generator and 160 bytes long PRK is generated. Then this key is fragmented in 4 parts to generate set of sub keys as shown below.

|16     | 16    | 64  | 64   |
|k_encr | k_aut | msk | emsk |


## MAC Generation

Server has to generate a MAC value and have to include it in the EAP-Request/AKA-Challenge sent to the client.

`EAP-Request/AKA-Challenge` EAP packet need to be create with AT_MAC attribute where value is set to all zeros.

```
DATA = EAP packet
MAC = HmacSHA1(k_aut, DATA)
```

MAC is calculated by encoding DATA bytes with HMAC-SHA1 using k_aut as the key.
Final `EAP-Request/AKA-Challenge` is generated include AT_MAC attribute with above calculated MAC as it value.


## Client MAC Validation

Client is sending AT_MAC attribute with `EAP-Response/AKA-Challenge` message. Server needs to validated this MAC value
to authenticate the client.

EAP packet is create using received `EAP-Response/AKA-Challenge` by replacing its AT_MAC attribute value with all zeros.

```
DATA = EAP packet
MAC = HmacSHA1(k_aut, DATA)
```

- EAP packet
 - EAP packet is create using received `EAP-Response/SIM/Challenge` by replacing its AT_MAC attribute value with all zeros.

- k_aut
 - k_aut value generated with pseudo random number generation

MAC is calculated by encoding DATA bytes with HMAC-SHA1 using k_aut as the key.

## Client RES Validation

Value of the AT_RES received from client with EAP-Response/AKA-Challenge should be equal to the xRes value received with the quintuplet received as the response to SAI call to HLR.





