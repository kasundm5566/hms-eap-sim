# Eap-SIM Key Generation Algorithms

Key generation is described in section 7 of the RFC 4186.

**Note:** In formulars described below the "|" character denotes concatenation.


## Master Key(MK) Generation

`MK = SHA1(Identity|n*Kc| NONCE_MT| Version List| Selected Version)`

SHA1 algorithm used here is  *SHA-1*

- Identity
 - Identity string taken from AT_IDENTITY attribute or from EAP-Response/Identity packets.

- n*Kc
 - Kc values of GSM triplets taken from the sending *Send Authentication Info(SAI)* request to HLR. Generally we get 3 triplets as SAI response. So value will be concatenation of received 3 kc values. eg: kc1|kc2|kc3

- NONCE_MT
 - NONCE_MT value extracted from received AT_NONCE_MT attribute. Just the value part of AT_NONCE_MT attribute.

- Version List
 - List of supported versions which we sent in AT_VERSION_LIST attribute.

- Selected Version
 - Value of AT_SELECTED_VERSION received from client.

## Pseudo Random Key Generation

PRK is generation using FIPS 186-2 random number generation algorithm with some modifications. Exact algorithm can be found at *Appendix B.  Pseudo-Random Number Generator* of RFC 4186. Master key is fed to the Psedo Random Generator and 160 bytes long PRK is generated. Then this key is fragmented in 4 parts to generate set of sub keys as shown below.

|16     | 16    | 64  | 64   |
|k_encr | k_aut | msk | emsk |


## MAC Generation

Server has to generate a MAC value and have to include it in the EAP-Request/SIM/Challenge sent to the client.

`EAP-Request/SIM/Challenge` EAP packet need to be create with AT_MAC attribute where value is set to all zeros.

```
DATA = EAP packet| NONCE_MT
MAC = HmacSHA1(k_aut, DATA)
```

MAC is calculated by encoding DATA bytes with HMAC-SHA1 using k_aut as the key.
Final `EAP-Request/SIM/Challenge` is generated include AT_MAC attribute with above calculated MAC as it value.


## Client MAC Validation

Client is sending AT_MAC attribute with `EAP-Response/SIM/Challenge` message. Server needs to validated this MAC value
to authenticate the client.

EAP packet is create using received `EAP-Response/SIM/Challenge` by replacing its AT_MAC attribute value with all zeros.

```
DATA = EAP packet| n*Sres
MAC = HmacSHA1(k_aut, DATA)
```

- EAP packet
 - EAP packet is create using received `EAP-Response/SIM/Challenge` by replacing its AT_MAC attribute value with all zeros.

- n*Sres
 - *sres* values of GSM triplets taken from the sending *Send Authentication Info(SAI)* request to HLR. This will be
         concatenated value of 3 sres values received.
         eg: sres1| sres2| sres3

MAC is calculated by encoding DATA bytes with HMAC-SHA1 using k_aut as the key.




