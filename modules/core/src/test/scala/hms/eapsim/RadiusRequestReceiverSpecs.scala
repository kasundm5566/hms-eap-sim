/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim

import hms.common.radius.{RadiusOutboundHandlerImpl, RequestContext}
import org.specs2.mutable._
import org.tinyradius.packet.AccessRequest
import org.tinyradius.dictionary.AttributeType
import org.tinyradius.attribute.RadiusAttribute
import hms.common.radius.util._
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.decoder.eap.EapMessageDecoder
import io.netty.buffer.Unpooled
import hms.eapsim.repo._
import hms.common.radius.decoder.eap.elements.Identity
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage
import hms.common.radius.decoder.eap.elements.EapCode

import scala.collection.JavaConversions._
import hms.eapsim.key._
import hms.eapsim.protocol.ProtocolHandlerComponent
import hms.eapsim.protocol.EapSimProtocolHandlerComponent
import hms.eapsim.protocol.EapAkaProtocolHandlerComponent
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype
import hms.eapsim.util._
import hms.eapsim.consumer.{ConsumerDataServiceComponent, MsisdnService, MsisdnServiceComponent}
import hms.eapsim.ldap.LdapConnectorComponent
import org.apache.directory.api.ldap.model.entry.Attribute
import hms.eapsim.ldap.LdapConnector
import org.apache.directory.api.ldap.model.entry.DefaultAttribute
import org.tinyradius.attribute.StringAttribute
import hms.eapsim.cache.{AuthenticationVectorCache, AuthenticationVectorCacheImpl, CacheComponent}
import hms.eapsim.repo.MysqlConnectorParams
import hms.eapsim.repo.TransLog
import java.security.MessageDigest

import hms.eapsim.keystore.{IdentityDecoderComponent, IdentityDecoderService, IdentityDecoderServiceImpl}

/**
 *
 */
class RadiusRequestReceiverSpecs extends Specification {

  val TransLogRepo = new TranslogRepository {
    def log(entry: TransLog) = {
      println("TL===>" + entry)
    }
  }

  val mysqlConnector = new MysqlConnectorImpl(MysqlConnectorParams(
    "eapsim",
    "eapsim"
  ))

  trait RepositoryComponentRegistry extends RepositoryComponent {
    val translogRepository: TranslogRepository = new MysqlTransLogRepository(mysqlConnector.pool, "", "", "", "")
    val reAuthContextRepository : ReAuthContextRepository = new ReAuthContextRepositoryImpl(mysqlConnector.pool, "", "")
    val msisdnStoreRepository : MsisdnStoreRepository = new MsisdnStoreRepositoryImpl(mysqlConnector.pool, "", "")
    val keystoreRepository: KeystoreRepository = new KeystoreRepositoryImpl(mysqlConnector.pool, "", "")
  }


  trait RRRDeps extends RadiusRequestReceiverComponent with ReceivedRequestRepositoryComponent with EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with Fips186RandomGeneratorComponent with ProtocolHandlerComponent with EapSimProtocolHandlerComponent with EapAkaProtocolHandlerComponent with ConsumerDataServiceComponent with MsisdnServiceComponent with LdapConnectorComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with RepositoryComponentRegistry with CacheComponent with AuthenticationHelperUtilityComponent with RadiusResponseSenderComponent with IdentityDecoderComponent{

    val messageResponder = new RadiusOutboundHandlerImpl(true)

    val Sha1Md = MessageDigest.getInstance("SHA-1")
    val eapsimAttributeBuilder = new EapsimAttributeBuilder
    val requestRepo = new SimpleRequestRepo(10000) with TranslogRepositoryComponent {
      val translogRepository = TransLogRepo
    }
    val identityDecoder: IdentityDecoderService = new IdentityDecoderServiceImpl(null)
    val keyGenerator: KeyGenerator = new KeyGenerator
    val keyManager: KeyManager = new DummyKeyManager
    val fips186Generator = new DummyFips186PRNGenerator
    val radiusRequestReceiver = new RadiusRequestReceiver
    val eapsimProtocolHandler = new EapSimProtocolHandler(List())
    val eapakaProtocolHandler = new EapAkaProtocolHandler(List())
    val msisdnService = new MsisdnService {
      def getMsisdn(imsi: String)(implicit correlationId: String): EapSimResult[String] = {
        Right("6512345678")
      }

      override def init(): Unit = ???
    }
    val authenticationVectorCache: AuthenticationVectorCache = new AuthenticationVectorCacheImpl()
    val getAuthenticationHelper = new AuthenticationHelperUtilityImpl(authenticationVectorCache, keyManager, 3, null)
    val consumerDataService = new ConsumerDataService("", "", "wsg_7168", false, null)
    val ldapConnector: LdapConnector = new LdapConnector {
      def search(baseDn: String, filter: String, requiredAttributes: List[String]): List[Attribute] = {
        List(new DefaultAttribute("radiusclass", "wsg_7168".getBytes()))
      }
    }
//    val translogRepository = TransLogRepo

    val errorCodeMapper: ErrorCodeMapper = new ErrorCodeMapper(){
      def getMsg(error: EapSimError): String = "Error Occurred"
    }
  }

  val rrc = new RRRDeps {}

  "RadiusRequestReceiver" should {

    "handle Aka-Identity" in {

      val eapMsg = Unpooled.copiedBuffer(HexCodec.hex2Byte("021e0040170500000e0e00333035323530333631353939393939323740776c616e2e6d6e633030332e6d63633532352e336770706e6574776f726b2e6f726700"))
      val decodedMsg = EapMessageDecoder.decode(eapMsg)

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REQUEST)
      rp.addAttribute(decodedMsg)
      rp.addAttribute(generateStateAttribute("232323232323235"))

      val result = rrc.radiusRequestReceiver.accessRequestReceived(new RequestContext(null, rp, null, null, 0, "test122", "", "", 0, 0, false, "", 0l))
    }

    def generateStateAttribute(correlationId: String): RadiusAttribute = {
      new StringAttribute(24, correlationId)
    }

//    "process EAP-Response/Identity for 2G" in {
//      val resp = rrc.radiusRequestReceiver.accessRequestReceived(accessReqWithIdentityFor2G)
//      resp must not beNull
//
//      val eap = resp.radiusResp.getAttribute(EapMessage.ATTRIBUTE_TYPE).asInstanceOf[EapRequestResponseMessage]
//      eap.getType.getTypeCode mustEqual EapTypeCode.EAP_SIM
//
//      val eapsim = eap.getType.asInstanceOf[EapSim]
//      eapsim.getAttributes must have size (2)
//      eapsim.getAttributes.map(_.getType) must contain(AT_VERSION_LIST, AT_FULLAUTH_ID_REQ).only
//    }
//
//    "process EAP-Response/Identity for 3G" in {
//      val resp = rrc.radiusRequestReceiver.accessRequestReceived(accessReqWithIdentityFor3G)
//      resp must not beNull
//
//      val eap = resp.radiusResp.getAttribute(EapMessage.ATTRIBUTE_TYPE).asInstanceOf[EapRequestResponseMessage]
//      eap.getType.getTypeCode mustEqual EapTypeCode.EAP_AKA
//
//      val eapsim = eap.getType.asInstanceOf[EapAka]
//      eapsim.getAttributes must have size (5)
//      eapsim.getAttributes.map(_.getType) must contain(AT_RAND, AT_AUTN, AT_MAC, AT_ENCR_DATA, AT_IV).only
//    }
//
//    "process EAP-Response/SIM/Start" in {
//
//      rrc.requestRepo.initSession("12345678", EapTypeCode.EAP_SIM, "3133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267", "username", "123456789", 1, null, Some(List(Triplet("202122232425262728292a2b2c2d2e2f", "e1e2e3e4", "b0b1b2b3b4b5b6b7"))))
//
//      val resp = rrc.radiusRequestReceiver.accessRequestReceived(accessReqWithEapSimStartResp(1, "12345678"))
//
//      println("======" + resp)
//
//      resp must not beNull
//
//      val eap = resp.radiusResp.getAttribute(EapMessage.ATTRIBUTE_TYPE).asInstanceOf[EapRequestResponseMessage]
//      eap.getType.getTypeCode mustEqual EapTypeCode.EAP_SIM
//
//      success
//    }
//
//
//
//    "process EAP-Response/AKA-Challenge" in {
//
//      val quintuplet = Quintuplet(rand = "83117e1f1d4f99c3770d793b77196c66",
//        xres = "5ca2563c3e43f478",
//        ck = "ff1a41dd89ac8c58dd65c305481b8f20",
//        ik = "a0f7a417d109b68189c7e6208d1d473c",
//        autn = "6bfbaf90cf9f0000b0a3674264974d4a")
//
//      val keys = PseudoRandomKeys(k_aut = "77f5525e53b1c2ec6d10d5bbaca6c34c",
//        k_encr = "6eea79f8039834715824c37b34d95d13",
//        msk = "795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96",
//        emsk = "7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24")
//
//      val session = rrc.requestRepo.initSession("45678910", EapTypeCode.EAP_AKA, "3133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267", "username", "123456789", 2, null, Some(List(quintuplet)))
//      val mk = generateMasterKeyEapAka(new String(HexCodec.hex2Byte(session.get.identity)), quintuplet.ik, quintuplet.ck)
//
//      rrc.requestRepo.updateSession("45678910", null, Some(keys), Some("34343"), Some(mk))
//
//      val resp = rrc.radiusRequestReceiver.accessRequestReceived(accessRespWithEapAkaChallenge(2, "45678910"))
//
//      println("======" + resp)
//
//      resp must not beNull
//
//      val eap = resp.radiusResp.getAttribute(EapMessage.ATTRIBUTE_TYPE).asInstanceOf[EapSuccessFailureMessage]
//      eap.getCode mustEqual EapCode.SUCCESS
//
//      success
//    }
  }

  private def accessReqWithIdentityFor2G: AccessRequest = {
    accessReqWithIdentity("3133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267")
  }

  private def accessReqWithIdentityFor3G: AccessRequest = {
    accessReqWithIdentity("3031323337323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267")
  }

  private def accessReqWithIdentity(idValue: String): AccessRequest = {
    val ar = new AccessRequest
    val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
    ar.setAuthProtocol(AccessRequest.AUTH_PAP);
    ar.addAttribute("NAS-Identifier", "this.is.my.nas-identifier.de");
    ar.addAttribute("NAS-IP-Address", "192.168.0.100");
    ar.addAttribute("Service-Type", "Login-User");
    ar.addAttribute("WISPr-Redirection-URL", "http://www.sourceforge.net/");
    ar.addAttribute("WISPr-Location-ID", "net.sourceforge.ap1");

    val identity = new Identity(Unpooled.copiedBuffer(HexCodec.hex2Byte(idValue)))
    val eapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, 0, identity)

    ar.addAttribute(eapMsg)
    ar
  }

  private def accessReqWithEapSimStartResp(simStartReqId: Byte, correlationId: String): AccessRequest = {
    val ar = new AccessRequest
    val eapAttrType: AttributeType = ar.getDictionary.getAttributeTypeByName("EAP-Message")
    //    val attribute: RadiusAttribute = RadiusAttribute.createRadiusAttribute(ar.getDictionary, eapAttrType.getVendorId, eapAttrType.getTypeCode)

    val respPacketId = HexCodec.byte2Hex(Array(simStartReqId))
    val eapSimStart = s"02${respPacketId}0058120a00000e0e00333133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267001001000107050000749ad02c4a257c20192738be0b40dc86";

    //    attribute.setAttributeData(HexCodec.hex2Byte(eapSimStart))

    ar.addAttribute(EapMessageDecoder.decode(Unpooled.copiedBuffer(HexCodec.hex2Byte(eapSimStart))));
    ar.addAttribute(new StringAttribute(24, correlationId))
    ar
  }

  private def accessRespWithEapAkaChallenge(simStartReqId: Byte, correlationId: String): AccessRequest = {
    val atRes = rrc.eapsimAttributeBuilder.buildAtRes(HexCodec.hex2Byte("5ca2563c3e43f478"))
    val atMac = rrc.eapsimAttributeBuilder.buildAtMac(HexCodec.hex2Byte("a17e3c94981a8b57ce31c1e5eea009d3"))
    val fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atRes, atMac))
    val fnlEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, simStartReqId, fnlAkaMsg)
    val ar = new AccessRequest
    ar.setAuthenticator(HexCodec.hex2Byte("418b13695d4cf917543edfaf93fe9864"))
    ar.addAttribute(fnlEapMsg)
    ar.addAttribute(new StringAttribute(24, correlationId))
    ar
  }

  def generateMasterKeyEapAka(identity: String, ik: String, ck: String): Array[Byte] = {
    val combined = identity + ik + ck
    rrc.Sha1Md.digest(HexCodec.hex2Byte(combined))
  }
}