/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import org.specs2.mutable.Specification


class VectorConversionUtilSpecs extends Specification {

  "VectorConversionUtil" should {

    "convert quintuplet to triplet" in {
      val qui = Quintuplet("f12ac2e620ccd40bb920f6b3b474306b", "3fe524d57101d202", "8bb41f73b559138cfab2b89bda709ee8", "bbdd21caaea33e90ccb827399649f189", "d749908734350000cd298d7429daed26")
      val x = VectorConversionUtil.umts2Gsm(qui)
      x.kc must beEqualTo("0663a11b57c3427d")
      x.rand must beEqualTo("f12ac2e620ccd40bb920f6b3b474306b")
      x.sres must beEqualTo("4ee4f6d7")
    }
  }
}
