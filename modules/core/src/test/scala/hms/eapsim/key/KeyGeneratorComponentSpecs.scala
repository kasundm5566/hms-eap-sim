/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import org.specs2.mutable.Specification
import hms.common.radius.util.HexCodec
import scala.collection.JavaConversions._

trait KGCEnv extends KeyGeneratorComponent with Fips186RandomGeneratorComponent {
  val fips186Generator = new DummyFips186PRNGenerator
  val keyGenerator = new KeyGenerator
}

class KeyGeneratorComponentSpecs extends Specification {

  val kgEnv = new KGCEnv {}

  "KeyGenerator" should {

    "generateMasterKey for EapSim" in {
      /*
       * Values for this test is taken from the example given in RFC4186
       * in "Appendix A.  Test Vectors"
       */
      val identity = "313234343037303130303030303030314065617073696d2e666f6f"
      val kc = List("a0a1a2a3a4a5a6a7", "b0b1b2b3b4b5b6b7", "c0c1c2c3c4c5c6c7")
      val nonceMt = "0123456789abcdeffedcba9876543210"
      val versionList = List("0001")
      val selectedVersion = "0001"
      val mk = kgEnv.keyGenerator.generateMasterKeyEapSimForFullAuth(identity, kc, nonceMt, versionList, selectedVersion)
      HexCodec.byte2Hex(mk) mustEqual "e576d5ca332e9930018bf1baee2763c795b3c712"
    }

    "generateMasterKey for EapAka" in {
      /*
       * Values for this test is taken from http://lists.frascone.com/pipermail/eap/msg03533.html
       */
      val identity = "616B6140646F742E636F6D"
      val ik = "F769BCD751044604127672711C6D3441"
      val ck = "B40BA9A3C58B2A05BBF0D987B21BF8CB"
      val mk = kgEnv.keyGenerator.generateMasterKeyEapAka(identity, ik, ck)
      HexCodec.byte2Hex(mk) mustEqual "C4834F21BEADF09E7A3BE817975ABA99DDB40C9A".toLowerCase
    }

    "generatePseudoRandomKeys" in {
      val randKeys = kgEnv.keyGenerator.generatePseudoRandomKeysForFullAuth(HexCodec.hex2Byte("e576d5ca332e9930018bf1baee2763c795b3c712"))
      randKeys must not beNull

      /*
       * Expected values are taken from RFC4186 section
       * A.5.  EAP-Request/SIM/Challenge page 82.
       */
      val K_encr = "536e5ebc4465582aa6a8ec9986ebb620"
      val K_aut = "25af1942efcbf4bc72b3943421f2a974"
      val MSK = "39d45aeaf4e30601983e972b6cfd46d1c363773365690d09cd44976b525f47d3a60a985e955c53b090b2e4b73719196a402542968fd14a888f46b9a7886e4488"
      val EMSK = "5949eab0fff69d52315c6c634fd14a7f0d52023d56f79698fa6596abeed4f93fbb48eb534d985414ceed0d9a8ed33c387c9dfdab92ffbdf240fcecf65a2c93b9"

      randKeys.k_aut mustEqual K_aut
      randKeys.k_encr mustEqual K_encr
      randKeys.msk mustEqual MSK
      randKeys.emsk mustEqual EMSK

    }

//    "generatePseudoRandomKeys 2" in {
//      val randKeys = kgEnv.keyGenerator.generatePseudoRandomKeys(HexCodec.hex2Byte("C4834F21BEADF09E7A3BE817975ABA99DDB40C9A".toLowerCase))
//      randKeys must not beNull
//
//      /*
//       * Expected values are taken from https://tools.ietf.org/html/draft-urien-eap-smartcard-22
//       * 18 Annex 7, EAP-AKA ISO7816 APDUs Trace (T=0 Protocol)
//       */
//      val K_encr = "28FF323842056B554B85A51116345AA4".toLowerCase
//      val K_aut = "B3080682488E686FAC3E1CF8248E7363".toLowerCase
//      val MSK = "BE1298C0B5338C91D6E11B33AE7D462DE29964640CF505FF26AED598822D41F920AF49FDCB77008C2AACDBA3A1AE7975208C25E540175D22D5480CDE88D79033".toLowerCase
//      val EMSK = "CD10C914BB54DC97AEE8960667F8C8591244DFE7BD4AC1B16E631B4DFA5DF6974A4C51F5D819FE68E7370F9E47439B43FD6E83CC357A01E71657F3BE6D264A2B".toLowerCase
//
//      println(HexCodec.byte2Hex(randKeys.k_encr.array))
//      println(HexCodec.byte2Hex(randKeys.k_aut.array))
//      println(HexCodec.byte2Hex(randKeys.msk.array))
//      println(HexCodec.byte2Hex(randKeys.emsk.array))
//
//      HexCodec.byte2Hex(randKeys.k_encr.array) mustEqual K_encr
//      HexCodec.byte2Hex(randKeys.k_aut.array) mustEqual K_aut
//      HexCodec.byte2Hex(randKeys.msk.array) mustEqual MSK
//      HexCodec.byte2Hex(randKeys.emsk.array) mustEqual EMSK
//
//    }

    "aesWithCbcEncode" in {
      val iv = HexCodec.hex2Byte("9e18b0c29a652263c06efb54dd00a895")
      val key = HexCodec.hex2Byte("536e5ebc4465582aa6a8ec9986ebb620")
      val secret = "1234567890123456"

      val encoded = kgEnv.keyGenerator.aesWithCbcEncode(iv, key, secret.getBytes)

      val decoded = kgEnv.keyGenerator.aesWithCbcDecode(iv, key, encoded)

      val decodedStr = decoded.foldLeft("")((a, b) => s"${a}${b.toChar}")
      decodedStr mustEqual secret
    }
  }
}

