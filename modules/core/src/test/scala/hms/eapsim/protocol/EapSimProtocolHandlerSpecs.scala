/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.protocol

import org.specs2.mutable.Specification
import hms.eapsim.util._
import hms.eapsim.repo._
import hms.eapsim._
import javax.crypto.KeyGenerator

import org.specs2.mock._
import hms.eapsim.key._
import hms.common.radius.util.HexCodec
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSim
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSimSubtype

import scala.collection.JavaConversions._
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage
import hms.common.radius.decoder.eap.elements.EapCode
import hms.eapsim.consumer.MsisdnServiceComponent
import hms.eapsim.consumer.ConsumerDataServiceComponent
import hms.eapsim.consumer.MsisdnService
import hms.eapsim.ldap.LdapConnectorComponent
import org.apache.directory.api.ldap.model.entry.Attribute
import hms.eapsim.ldap.LdapConnector
import hms.eapsim.cache.{AuthenticationVectorCache, AuthenticationVectorCacheImpl}
import hms.eapsim.repo.MysqlConnectorParams
import hms.eapsim.EapSimError
import hms.eapsim.repo.TransLog
import hms.eapsim.cache.{AuthenticationVectorCache, AuthenticationVectorCacheImpl, CacheComponent}
import hms.eapsim.repo.MysqlConnectorParams
import hms.eapsim.EapSimError
import hms.eapsim.repo.TransLog
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes._
import hms.eapsim.repo.MysqlConnectorParams
import hms.eapsim.EapSimError
import hms.eapsim.keystore.{IdentityDecoderComponent, IdentityDecoderService, IdentityDecoderServiceImpl}
import hms.eapsim.repo.TransLog

/**
 *
 */
class EapSimProtocolHandlerSpecs extends Specification {

  val TransLogRepo = new TranslogRepository {
    def log(entry: TransLog) = {
      println("TL===>" + entry)
    }
  }

  val mysqlConnector = new MysqlConnectorImpl(MysqlConnectorParams(
    "eapsim",
    "eapsim"
  ))

  trait RepositoryComponentRegistry extends RepositoryComponent {
    val translogRepository: TranslogRepository = new MysqlTransLogRepository(mysqlConnector.pool, null, null,  "", "")
    val reAuthContextRepository : ReAuthContextRepository = new ReAuthContextRepositoryImpl(mysqlConnector.pool, null, null)
    val authVectorRepository : AuthenticationVectorCache = new AuthenticationVectorCacheImpl
    val msisdnStoreRepository : MsisdnStoreRepository = new MsisdnStoreRepositoryImpl(mysqlConnector.pool, null, null)
    val keystoreRepository: KeystoreRepository = new KeystoreRepositoryImpl(mysqlConnector.pool, "", "")
  }

  trait SPHDeps extends ReceivedRequestRepositoryComponent with EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with Fips186RandomGeneratorComponent with ProtocolHandlerComponent with EapSimProtocolHandlerComponent with EapAkaProtocolHandlerComponent with ConsumerDataServiceComponent with MsisdnServiceComponent with LdapConnectorComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with  RepositoryComponentRegistry with AuthenticationHelperUtilityComponent with  IdentityDecoderComponent{
    val eapsimAttributeBuilder = new EapsimAttributeBuilder
    val requestRepo = new SimpleRequestRepo(10000) with TranslogRepositoryComponent {
      val translogRepository = TransLogRepo
    }
    val identityDecoder: IdentityDecoderService = new IdentityDecoderServiceImpl(null)
    val keyGenerator: KeyGenerator = new KeyGenerator
    val keyManager: KeyManager = new DummyKeyManager
    val fips186Generator = new DummyFips186PRNGenerator
    val eapsimProtocolHandler = new EapSimProtocolHandler(List())
    val eapakaProtocolHandler = new EapAkaProtocolHandler(List())
    val getAuthenticationHelper = new AuthenticationHelperUtilityImpl(null, keyManager, 3, null)
    val msisdnService = new MsisdnService {
      def getMsisdn(imsi: String)(implicit correlationId: String): EapSimResult[String] = {
        Right("6512345678")
      }

      override def init(): Unit = ???
    }
    val consumerDataService = new ConsumerDataService("", "", "wsg_7168", false, null)
    val ldapConnector: LdapConnector = new LdapConnector {
      def search(baseDn: String, filter: String, requiredAttributes: List[String]): List[Attribute] = List()
    }
//    val translogRepository = TransLogRepo

    val errorCodeMapper: ErrorCodeMapper = new ErrorCodeMapper(){
      def getMsg(error: EapSimError): String = "Error Occurred"
    }
  }

  val sph = new SPHDeps {}

  "EapSimProtocolHandler" should {

    //As per RFC 4168 - Page 87
    "generateXKEYDash'" in {
      val nextReAuthId = "Y24fNSrz8BP274jOJaF17WfxI8YO7QX00pMXk9XMMVOw7broaNhTczuFq53aEpOkk3L0dm@eapsim.foo"
      val counter = "0001"
      val nOnceS = "0123456789abcdeffedcba9876543210"
      val mk = "e576d5ca332e9930018bf1baee2763c795b3c712"

      val xkeyDash= sph.keyGenerator.generateXKeyAlternativeSeed(HexCodec.byte2Hex(nextReAuthId.getBytes), counter, nOnceS, mk)

      //20 bytes
      HexCodec.byte2Hex(xkeyDash) mustEqual "863dc12032e08343c1a2308db48377f6801f58d4"
    }


    //As per RFC 4168 - Page 86
    "validate re-auth data" in {
      val nextReAuthId = "uta0M0iyIsMwWp5TTdSdnOLvg2XDVf21OYt1vnfiMcs5dnIDHOIFVavIRzMRyzW6vFzdHW@eapsim.foo"
      val counterAsHex = "0001"
      val nOnceAsHex = "0123456789abcdeffedcba9876543210"
      val ivAsHex = "d585ac7786b90336657c77b46575b9c4"
      val kEncrAsHex = "536e5ebc4465582aa6a8ec9986ebb620"
      val kAutAsHex = "25af1942efcbf4bc72b3943421f2a974"

      val atNextReAuthId = sph.eapsimAttributeBuilder.buildAtNextReauthId(nextReAuthId.getBytes)
      println(s"next reauth id [${HexCodec.byte2Hex(atNextReAuthId.encode().array())}]")
      HexCodec.byte2Hex(atNextReAuthId.encode().array()) mustEqual "85160051757461304d30697949734d7757703554546453646e4f4c7667325844566632314f597431766e66694d637335646e4944484f494656617649527a4d52797a573676467a6448574065617073696d2e666f6f000000"

      val atCounter = sph.eapsimAttributeBuilder.buildAtCounter(1)
      HexCodec.byte2Hex(atCounter.encode().array()) mustEqual "13010001"

      val atNOnceS = sph.eapsimAttributeBuilder.buildAtNonceServer(HexCodec.hex2Byte(nOnceAsHex))
      HexCodec.byte2Hex(atNOnceS.encode().array()) mustEqual "150500000123456789abcdeffedcba9876543210"

      val atIv = sph.eapsimAttributeBuilder.buildAtIv(HexCodec.hex2Byte(ivAsHex))
      HexCodec.byte2Hex(atIv.encode().array()) mustEqual "81050000d585ac7786b90336657c77b46575b9c4"

      val atEncrData = sph.eapsimAttributeBuilder.buildAtEncrData(List(atCounter, atNOnceS,atNextReAuthId), HexCodec.hex2Byte(ivAsHex),
      HexCodec.hex2Byte(kEncrAsHex))
      HexCodec.byte2Hex(atEncrData.encode().array()) mustEqual "821d0000686291a9d2abc58caa3294b6e85b44846c44e5dcb2de8b9e80d69d49858a5db84cdc1c9bc95c01b96b6eca313474aea6d31416e19daa9df70f05008841ca8014964d3b30a49bcf43e4d3f18e86295a4a2b38d96c9705c2bbb05c4aace97d5eaff564046c8bd30bc39be5e17ace2b10a6"

      val atBlankMac = sph.eapsimAttributeBuilder.buildAtMac

      val tmpSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEncrData, atBlankMac))
      val tmpEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, 1, tmpSimMsg)
      val encodeEapMsg = tmpEapMsg.encode

      val mac = sph.eapsimProtocolHandler.calculateMacValue(encodeEapMsg.array(), HexCodec.hex2Byte(kAutAsHex))
      HexCodec.byte2Hex(mac) mustEqual("483a1799b83d7cd3d0a1e401d9ee4770")
    }

    "validate response re authentication" in {
      val ivAsHex = "cdf7ffa65de04c026b56c86b76b102ea"
      val kEncrAsHex = "536e5ebc4465582aa6a8ec9986ebb620"
      val kAutAsHex = "25af1942efcbf4bc72b3943421f2a974"
      val nOnceAsHex = "0123456789abcdeffedcba9876543210"

      val atIv = sph.eapsimAttributeBuilder.buildAtIv(HexCodec.hex2Byte(ivAsHex))
      val atCounter = sph.eapsimAttributeBuilder.buildAtCounter(1)
      val atEcnrData = sph.eapsimAttributeBuilder.buildAtEncrData(List(atCounter), HexCodec.hex2Byte(ivAsHex),
      HexCodec.hex2Byte(kEncrAsHex))

      HexCodec.byte2Hex(atEcnrData.encode().array()) mustEqual "82050000b6edd38279e2a1423c1afc5c455c7d56"

      val atBlankMac = sph.eapsimAttributeBuilder.buildAtMac

      val tmpSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEcnrData, atBlankMac))
      val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 1, tmpSimMsg)
      val encodeEapMsg = tmpEapMsg.encode

      val mac = sph.eapsimProtocolHandler.calculateMacValue(encodeEapMsg.array(), HexCodec.hex2Byte(nOnceAsHex),  HexCodec.hex2Byte(kAutAsHex))
      HexCodec.byte2Hex(mac) mustEqual("faf76b71fbe2d255b96a3566c915c617")

      val atMac = sph.eapsimAttributeBuilder.buildAtMac(mac)
      val finalSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEcnrData, atMac))
      val finalEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 1, finalSimMsg)

      val result = sph.eapsimProtocolHandler.validateClientMacForReAuth(finalEapMsg,
        HexCodec.hex2Byte(nOnceAsHex),
        HexCodec.hex2Byte(kAutAsHex))

      result.isRight mustEqual true
    }

    "calculateMacValue" in {
      val packet = HexCodec.hex2Byte("01020118120b0000010d0000101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f810500009e18b0c29a652263c06efb54dd00a895822d000055f2939bbdb1b19ea1b47fc0b3e0be4cab2cf7372d98e3023c6bb92415723d58bad66ce084e101b60f5358354bd4218278aea7bf2cbace33106aeddc625b0c1d5aa67a41739ae5b57950973fc7ff8301073c6f953150fc303ea152d1e10a2d1f4f5226daa1ee9005472252bdb3b71d6f0c3a3490316c46929871bd45cdfdbca6112f07f8be717990d25f6dd7f2b7b320bf4d5a992e880331d729945aec75ae5d43c8eda5fe6233fcac494ee67a0d504d0b05000000000000000000000000000000000000")
      val nonce = HexCodec.hex2Byte("0123456789abcdeffedcba9876543210")
      val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
      val mac = sph.eapsimProtocolHandler.calculateMacValue(packet, nonce, key)
      println(HexCodec.byte2Hex(mac))
      /*
       * Expected value is taken from RFC 4186 - A.5.  EAP-Request/SIM/Challenge Page 84.
       * It is the calculated MAC value.
       */
      val expected = "fef324ac3962b59f3bd78253ae4dcb6a"
      HexCodec.byte2Hex(mac) mustEqual expected

      val text1 = HexCodec.hex2Byte("01050050120b0000010d0000724a0db1cb2858d529d6a54e91b75474724a0db1cb2858d529d6a54e91b75474724a0db1cb2858d529d6a54e91b754740b05000000000000000000000000000000000000");
      val nonce1 = HexCodec.hex2Byte("7446928cd8820ad19baf7179a59bebed")
      val key1 = HexCodec.hex2Byte("84629ff222bcede51de7b08f1e4c6735");

      println(HexCodec.byte2Hex(sph.eapsimProtocolHandler.calculateMacValue(text1, nonce1, key1)))
      success
    }

    "validateClientMac" in {

      "successfull match" in {
        /*
       * Expected value is taken from RFC 4186 - A.5.  EAP-Request/SIM/Challenge Page 85.
       * It is the calculated MAC value.
       */
        val atMac = sph.eapsimAttributeBuilder.buildAtMac(HexCodec.hex2Byte("f56d6433e68ed2976ac11937fc3d1154"));
        val tmpSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List(atMac))
        val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 2, tmpSimMsg)
        val sres = HexCodec.hex2Byte("d1d2d3d4e1e2e3e4f1f2f3f4")
        //Key used here is the K_aut generated during random number generation
        val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
        val resp = sph.eapsimProtocolHandler.validateClientMac(tmpEapMsg, sres, key)
        resp must beRight.like {
          case x => x must beTrue
        }

      }

      "failed match" in {

        val atMac = sph.eapsimAttributeBuilder.buildAtMac(HexCodec.hex2Byte("f56d6433e68ed2976ac11937fc3d1111"));
        val tmpSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List(atMac))
        val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 2, tmpSimMsg)
        val sres = HexCodec.hex2Byte("d1d2d3d4e1e2e3e4f1f2f3f4")
        val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
        val resp = sph.eapsimProtocolHandler.validateClientMac(tmpEapMsg, sres, key)
        resp must beLeft.like {
          case x => x.statusCode.statusCode mustEqual MacValidationError.statusCode
        }

      }

      "resp without AT_MAC" in {

        val tmpSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List())
        val tmpEapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 2, tmpSimMsg)
        val sres = HexCodec.hex2Byte("d1d2d3d4e1e2e3e4f1f2f3f4")
        val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
        val resp = sph.eapsimProtocolHandler.validateClientMac(tmpEapMsg, sres, key)
        resp must beLeft.like {
          case x => x.statusCode.statusCode mustEqual EapAttributeNotFound.statusCode
        }

      }

    }
  }
}