/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.protocol

import org.specs2.mutable.Specification
import hms.eapsim.util._
import hms.eapsim.repo._
import hms.eapsim.key._
import hms.common.radius.util.HexCodec
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype

import scala.collection.JavaConversions._
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage
import hms.common.radius.decoder.eap.elements.EapCode
import hms.eapsim._
import io.netty.buffer.Unpooled
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute
import hms.eapsim.consumer.MsisdnServiceComponent
import hms.eapsim.consumer.ConsumerDataServiceComponent
import hms.eapsim.consumer.MsisdnService
import hms.eapsim.ldap.LdapConnectorComponent
import org.apache.directory.api.ldap.model.entry.Attribute
import hms.eapsim.ldap.LdapConnector
import hms.eapsim.repo.MysqlConnectorParams
import hms.eapsim.key.Quintuplet
import hms.eapsim.EapSimError
import hms.eapsim.repo.TransLog
import hms.eapsim.key.PseudoRandomKeys
import hms.eapsim.cache.{AuthenticationVectorCache, AuthenticationVectorCacheImpl}
import hms.eapsim.repo.MysqlConnectorParams
import hms.eapsim.key.Quintuplet
import hms.eapsim.EapSimError
import hms.eapsim.repo.TransLog
import hms.eapsim.key.PseudoRandomKeys
import hms.eapsim.keystore.{IdentityDecoderComponent, IdentityDecoderService, IdentityDecoderServiceImpl}

/**
 *
 */
class EapAkaProtocolHandlerSpecs extends Specification {

  val TransLogRepo = new TranslogRepository {
    def log(entry: TransLog) = {
      println("TL===>" + entry)
    }
  }

  val mysqlConnector = new MysqlConnectorImpl(MysqlConnectorParams(
    "eapsim",
    "eapsim"
  ))

  trait RepositoryComponentRegistry extends RepositoryComponent {
    val translogRepository: TranslogRepository = new MysqlTransLogRepository(mysqlConnector.pool, null, null,  "", "")
    val reAuthContextRepository : ReAuthContextRepository = new ReAuthContextRepositoryImpl(mysqlConnector.pool, null, null)
    val authVectorRepository : AuthenticationVectorCache = new AuthenticationVectorCacheImpl
    val msisdnStoreRepository : MsisdnStoreRepository = new MsisdnStoreRepositoryImpl(mysqlConnector.pool, null, null)
    val keystoreRepository: KeystoreRepository = new KeystoreRepositoryImpl(mysqlConnector.pool, "", "")
  }

  trait APHDeps extends ReceivedRequestRepositoryComponent with EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with Fips186RandomGeneratorComponent with ProtocolHandlerComponent with EapSimProtocolHandlerComponent with EapAkaProtocolHandlerComponent with ConsumerDataServiceComponent with MsisdnServiceComponent with LdapConnectorComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with RepositoryComponentRegistry with AuthenticationHelperUtilityComponent with  IdentityDecoderComponent {
    val eapsimAttributeBuilder = new EapsimAttributeBuilder
    val requestRepo = new SimpleRequestRepo(10000) with TranslogRepositoryComponent {
      val translogRepository = TransLogRepo
    }
    val identityDecoder: IdentityDecoderService = new IdentityDecoderServiceImpl(null)
    val keyGenerator: KeyGenerator = new KeyGenerator
    val keyManager: KeyManager = new DummyKeyManager
    val fips186Generator = new DummyFips186PRNGenerator
    val eapsimProtocolHandler = new EapSimProtocolHandler(List())
    val eapakaProtocolHandler = new EapAkaProtocolHandler(List())
    val msisdnService = new MsisdnService {
      def getMsisdn(imsi: String)(implicit correlationId: String): EapSimResult[String] = {
        Right("6512345678")
      }

      override def init(): Unit = ???
    }

    val getAuthenticationHelper = new AuthenticationHelperUtilityImpl(null, keyManager, 3, null)
    val consumerDataService = new ConsumerDataService("", "", "wsg_7168", false, null)
    val ldapConnector: LdapConnector = new LdapConnector {
      def search(baseDn: String, filter: String, requiredAttributes: List[String]): List[Attribute] = List()
    }
//    val translogRepository = TransLogRepo

    def errorCodeMapper: ErrorCodeMapper = new ErrorCodeMapper(){
      def getMsg(error: EapSimError): String = "Error Occurred"
    }
  }

  val aph = new APHDeps {}

  "EapAkaProtocolHandler" should {

    implicit val transLogEntry: TransLog = TransLog("", "", "", "", "", receivedTime = 0)

    "validateClientChallengeResp" in {

      "success - both RES and MAC are valid" in {
        /**
         * Data for this test was taken from the packet captures and logs of live test done with M1.
         * Logs and packet captures can be found at docs/m1-testing/3g/success
         */
        val atMac = aph.eapsimAttributeBuilder.buildAtMac(HexCodec.hex2Byte("bc1d2e2871e4a715095f87ada8788f1b"));
        val atRes = createAtRes(HexCodec.hex2Byte("00405ca2563c3e43f478"))
        val atCheckCode = aph.eapsimAttributeBuilder.buildAtCheckCode(Array())
        val akaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atCheckCode, atRes, atMac))
        val eapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 1, akaMsg)

        //rand='83117E1F1D4F99C3770D793B77196C66', xres='5CA2563C3E43F478', ck='FF1A41DD89AC8C58DD65C305481B8F20', ik='A0F7A417D109B68189C7E6208D1D473C', autn='6BFBAF90CF9F0000B0A3674264974D4A'
        val quintuplet = Quintuplet(rand = "83117e1f1d4f99c3770d793b77196c66",
          xres = "5ca2563c3e43f478",
          ck = "ff1a41dd89ac8c58dd65c305481b8f20",
          ik = "a0f7a417d109b68189c7e6208d1d473c",
          autn = "6bfbaf90cf9f0000b0a3674264974d4a")

        //Generated PRN k_encr[6eea79f8039834715824c37b34d95d13] k_aut[77f5525e53b1c2ec6d10d5bbaca6c34c] msk[795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96] emask[7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24]
        val keys = PseudoRandomKeys(k_aut = "77f5525e53b1c2ec6d10d5bbaca6c34c",
          k_encr = "6eea79f8039834715824c37b34d95d13",
          msk = "795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96",
          emsk = "7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24")

        val sres = HexCodec.hex2Byte("d1d2d3d4e1e2e3e4f1f2f3f4")
        val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
        val resp = aph.eapakaProtocolHandler.validateClientChallengeResp(eapMsg, quintuplet, keys)
        resp must beRight.like {
          case x => x must beTrue
        }

      }

      "failure - invalid RES" in {
        /**
         * Data for this test was taken from the packet captures and logs of live test done with M1.
         * Logs and packet captures can be found at docs/m1-testing/3g/success
         */
        val atMac = aph.eapsimAttributeBuilder.buildAtMac(HexCodec.hex2Byte("bc1d2e2871e4a715095f87ada8788f1b"));
        val atRes = createAtRes(HexCodec.hex2Byte("00405ca2563c3e43f488")) //RES is invalid
        val atCheckCode = aph.eapsimAttributeBuilder.buildAtCheckCode(Array())
        val akaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atCheckCode, atRes, atMac))
        val eapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 1, akaMsg)

        //rand='83117E1F1D4F99C3770D793B77196C66', xres='5CA2563C3E43F478', ck='FF1A41DD89AC8C58DD65C305481B8F20', ik='A0F7A417D109B68189C7E6208D1D473C', autn='6BFBAF90CF9F0000B0A3674264974D4A'
        val quintuplet = Quintuplet(rand = "83117e1f1d4f99c3770d793b77196c66",
          xres = "5ca2563c3e43f478",
          ck = "ff1a41dd89ac8c58dd65c305481b8f20",
          ik = "a0f7a417d109b68189c7e6208d1d473c",
          autn = "6bfbaf90cf9f0000b0a3674264974d4a")

        //Generated PRN k_encr[6eea79f8039834715824c37b34d95d13] k_aut[77f5525e53b1c2ec6d10d5bbaca6c34c] msk[795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96] emask[7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24]
        val keys = PseudoRandomKeys(k_aut = "77f5525e53b1c2ec6d10d5bbaca6c34c",
          k_encr = "6eea79f8039834715824c37b34d95d13",
          msk = "795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96",
          emsk = "7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24")

        val sres = HexCodec.hex2Byte("d1d2d3d4e1e2e3e4f1f2f3f4")
        val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
        val resp = aph.eapakaProtocolHandler.validateClientChallengeResp(eapMsg, quintuplet, keys)
        resp must beLeft.like {
          case x => x.statusCode.statusCode mustEqual ResValidationError.statusCode
        }

      }

      "failure - invalid MAC" in {
        /**
         * Data for this test was taken from the packet captures and logs of live test done with M1.
         * Logs and packet captures can be found at docs/m1-testing/3g/success
         */
        val atMac = aph.eapsimAttributeBuilder.buildAtMac(HexCodec.hex2Byte("bc1d2e2871e4a715095f87ada8788fbb")); //Invalid MAC
        val atRes = createAtRes(HexCodec.hex2Byte("00405ca2563c3e43f478"))
        val atCheckCode = aph.eapsimAttributeBuilder.buildAtCheckCode(Array())
        val akaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atCheckCode, atRes, atMac))
        val eapMsg = EapRequestResponseMessage.build(EapCode.RESPONSE, 1, akaMsg)

        //rand='83117E1F1D4F99C3770D793B77196C66', xres='5CA2563C3E43F478', ck='FF1A41DD89AC8C58DD65C305481B8F20', ik='A0F7A417D109B68189C7E6208D1D473C', autn='6BFBAF90CF9F0000B0A3674264974D4A'
        val quintuplet = Quintuplet(rand = "83117e1f1d4f99c3770d793b77196c66",
          xres = "5ca2563c3e43f478",
          ck = "ff1a41dd89ac8c58dd65c305481b8f20",
          ik = "a0f7a417d109b68189c7e6208d1d473c",
          autn = "6bfbaf90cf9f0000b0a3674264974d4a")

        //Generated PRN k_encr[6eea79f8039834715824c37b34d95d13] k_aut[77f5525e53b1c2ec6d10d5bbaca6c34c] msk[795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96] emask[7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24]
        val keys = PseudoRandomKeys(k_aut = "77f5525e53b1c2ec6d10d5bbaca6c34c",
          k_encr = "6eea79f8039834715824c37b34d95d13",
          msk = "795e0cdd8e7ce36984b833022b12dce5f2a410ae28bf951b19111691ce0807dd1ce94e259d277daaa2d2f6c63fbecb74454e9366b09d230dfbe19b6c8e75fa96",
          emsk = "7db6f9044af06a1578bd1936ca6a4e1a2344d633d39b6ab5f118a24efb32c5f6d49bee973e54c8778ca6dcd27ea3d8349cdd4ce455f17f9b2509e97a121bbd24")

        val sres = HexCodec.hex2Byte("d1d2d3d4e1e2e3e4f1f2f3f4")
        val key = HexCodec.hex2Byte("25af1942efcbf4bc72b3943421f2a974")
        val resp = aph.eapakaProtocolHandler.validateClientChallengeResp(eapMsg, quintuplet, keys)
        resp must beLeft.like {
          case x => x.statusCode.statusCode mustEqual MacValidationError.statusCode
        }

      }
    }

    def createAtRes(res: Array[Byte]) = {
      val data = Unpooled.buffer(res.length)
      data.writeBytes(res)
      new EapSimAkaAttribute(EapSimAkaAttributeTypes.AT_RES, data.readableBytes, data)
    }
  }

}