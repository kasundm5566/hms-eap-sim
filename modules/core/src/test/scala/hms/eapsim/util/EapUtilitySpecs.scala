/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

import org.specs2.mutable.Specification
import hms.eapsim.util.EapMsgUtil._
import hms.eapsim.repo.{SIM, AKA}

/**
 *
 */
class EapUtilitySpecs extends Specification {

  "EapUtility" should {

    "getEapMethod" in {
      getEapMethod("902rerererere@hms.com") mustEqual AKA
      getEapMethod("912rerererere@hms.com") mustEqual SIM
    }

    "getImsi" in {
//      extractImsi("90abdcdfdfd_013334343434566@some.com") mustEqual "13334343434566"
//      extractImsi("013334343434566@some.com") mustEqual "13334343434566"
    }
  }
}