package hms.eapsim.util;

import hms.eapsim.util.privacy.PrivacyUtil;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * Created by sampath on 11/9/18.
 */
public class PrivacyUtilTest {

    static String CERTIFICATE_TYPE = "X.509";
    static String CHAR_SET_TYPE = "UTF8";
    static String CYPHER = "RSA";

    @Test
    public void testMetoo() throws Exception {
        final URI uri = this.getClass().getClassLoader().getResource("core.iml").toURI();
        System.out.println(uri);

        File file = new File(".");
        System.out.println(file.getAbsolutePath());

    }

    @Test
    public void testMe() throws Exception {
        final String encrypted = PrivacyUtil.encrypt("/home/sampath/Desktop/keys/2048b-rsa-example-cert.der",
                "Super secret");
        System.out.println("======================");
        System.out.println(encrypted);
        System.out.println("Length : " + encrypted.length());

        System.out.println("======================");

        final String decrypt = decrypt("/home/sampath/Desktop/keys/pkcs8-2048b-rsa-example-keypair.der",
                encrypted.getBytes());
        System.out.println("Decrypted: " + decrypt);

    }

    private static String decrypt(String privateKeyFileFullPath, byte[] cipherText) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        PrivateKey privateKey = readPrivateKey(privateKeyFileFullPath);
        byte[] decodedBytes = decodeBase64(cipherText);
        byte[] recoveredMessageAsBytes = decryptByKey(privateKey, decodedBytes);
        return new String(recoveredMessageAsBytes, CHAR_SET_TYPE);

    }

    private static byte[] decryptByKey(PrivateKey key, byte[] ciphertext) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(CYPHER);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(ciphertext);
    }

    private static byte[] decodeBase64(byte[] encodedBytes) {
        return Base64.decodeBase64(encodedBytes);
    }


    private static PrivateKey readPrivateKey(String fullPathToPrivateKey) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(readFileBytes(fullPathToPrivateKey));
        KeyFactory keyFactory = KeyFactory.getInstance(CYPHER);
        return keyFactory.generatePrivate(keySpec);
    }


    private static byte[] readFileBytes(String filename) throws IOException {
        Path path = Paths.get(filename);
        byte[] bytes = Files.readAllBytes(path);
        System.out.println("Private key file read: [ length: " + bytes.length + "]");
        return bytes;
    }
}
