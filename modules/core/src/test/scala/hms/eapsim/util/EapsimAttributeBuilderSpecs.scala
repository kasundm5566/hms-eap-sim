/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

import org.specs2.mutable.Specification
import hms.eapsim.key.TempIdentityGenerator
import hms.common.radius.util.HexCodec
import hms.eapsim.key.KeyGeneratorComponent
import hms.eapsim.key.Fips186RandomGeneratorComponent
import hms.eapsim.key.DummyFips186PRNGenerator
import io.netty.buffer.Unpooled
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.{EapSimAkaAttribute, EapSimAkaAttributeTypes}
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.{EapSimSubtype, EapSim}
import hms.common.radius.decoder.eap.elements.{EapCode, EapRequestResponseMessage}

class EapsimAttributeBuilderSpecs extends Specification {

  val idGen = new TempIdentityGenerator

  val simAttrBuilder = new EapsimAttributeBuilderComponent with KeyGeneratorComponent with Fips186RandomGeneratorComponent {
    val fips186Generator = new DummyFips186PRNGenerator
    val keyGenerator = new KeyGenerator
    val eapsimAttributeBuilder = new EapsimAttributeBuilder
  }

  "EapsimAttributeBuilder" should {

    "decode encr data from client" in {  //as per spec RFC 4186
      val ivAsHex = "cdf7ffa65de04c026b56c86b76b102ea"
      val kEncrAsHex = "536e5ebc4465582aa6a8ec9986ebb620"
      val builder = simAttrBuilder.eapsimAttributeBuilder

      val atCounter = builder.buildAtCounter(1)
      val atEcnrData = builder.buildAtEncrData(List(atCounter), HexCodec.hex2Byte(ivAsHex),
      HexCodec.hex2Byte(kEncrAsHex))

      HexCodec.byte2Hex(atEcnrData.encode().array()) mustEqual "82050000b6edd38279e2a1423c1afc5c455c7d56"
      val decoded = builder.decordEncrData(HexCodec.hex2Byte(ivAsHex), HexCodec.hex2Byte(kEncrAsHex), atEcnrData)

      decoded.size mustEqual 2
      val counterList: List[EapSimAkaAttribute] = decoded.filter(att => {att.getType == EapSimAkaAttributeTypes.AT_COUNTER})
      counterList.size mustEqual 1
      val paddingList = decoded.filter(att => {att.getType == EapSimAkaAttributeTypes.AT_COUNTER})
      paddingList.size mustEqual 1

      counterList.foreach(att => HexCodec.byte2Hex(att.getAttributeData) mustEqual("0001"))
    }



    "buildAtNextPseudonym" in {
      val attr = simAttrBuilder.eapsimAttributeBuilder.buildAtNextPseudonym(idGen.generate("someone@somehost.com").pseudonym.getBytes)

      println(attr)

      val encoded = HexCodec.byte2Hex(attr.encode.array)
      val expected = "84130046773877343950657843617a574a2678434941526d78754d4b68743553317378524471585345464245673344635a50396349785465354a344f7949774e47567a78654a4f5531470000"

      encoded mustEqual expected

    }

    "buildAtNextReauthId" in {
      val reauthId = "Y24fNSrz8BP274jOJaF17WfxI8YO7QX00pMXk9XMMVOw7broaNhTczuFq53aEpOkk3L0dm@eapsim.foo".getBytes
      val attr = simAttrBuilder.eapsimAttributeBuilder.buildAtNextReauthId(reauthId)

      println(HexCodec.byte2Hex(idGen.generate("someone@somehost.com").fastReAuthId.getBytes))
      println(attr)

      val encoded = HexCodec.byte2Hex(attr.encode.array)
      val expected = "85160051593234664e53727a3842503237346a4f4a614631375766784938594f3751583030704d586b39584d4d564f773762726f614e6854637a75467135336145704f6b6b334c30646d4065617073696d2e666f6f000000"

      encoded mustEqual expected

    }

    "buildAtIv" in {
      val attr = simAttrBuilder.eapsimAttributeBuilder.buildAtIv(HexCodec.hex2Byte("9e18b0c29a652263c06efb54dd00a895"))
      println(attr)
      val encoded = HexCodec.byte2Hex(attr.encode.array)
      val expected = "810500009e18b0c29a652263c06efb54dd00a895"
      encoded mustEqual expected
    }

    "buildAtPadding" in {
      val attr = simAttrBuilder.eapsimAttributeBuilder.buildAtPadding(10)
      println(attr)
      val encoded = HexCodec.byte2Hex(attr.encode.array)
      val expected = "060300000000000000000000"
      encoded mustEqual expected
    }

    "buildAtEncrData" in {
      /*
       * Expected values are taken from RFC4186 section
       * A.5.  EAP-Request/SIM/Challenge page 82-84.
       */

      val pseudonym = simAttrBuilder.eapsimAttributeBuilder.buildAtNextPseudonym(idGen.generate("someone@somehost.com").pseudonym.getBytes)
      val reauthId = "Y24fNSrz8BP274jOJaF17WfxI8YO7QX00pMXk9XMMVOw7broaNhTczuFq53aEpOkk3L0dm@eapsim.foo".getBytes
      val reauth = simAttrBuilder.eapsimAttributeBuilder.buildAtNextReauthId(reauthId)

      val iv = HexCodec.hex2Byte("9e18b0c29a652263c06efb54dd00a895")
      val k_encr = HexCodec.hex2Byte("536e5ebc4465582aa6a8ec9986ebb620")
      val attr = simAttrBuilder.eapsimAttributeBuilder.buildAtEncrData(List(pseudonym, reauth), iv, k_encr)
      println(attr)
      val encoded = HexCodec.byte2Hex(attr.encode.array)
      val expected = "822d000055f2939bbdb1b19ea1b47fc0b3e0be4cab2cf7372d98e3023c6bb92415723d58bad66ce084e101b60f5358354bd4218278aea7bf2cbace33106aeddc625b0c1d5aa67a41739ae5b57950973fc7ff8301073c6f953150fc303ea152d1e10a2d1f4f5226daa1ee9005472252bdb3b71d6f0c3a3490316c46929871bd45cdfdbca6112f07f8be717990d25f6dd7f2b7b320bf4d5a992e880331d729945aec75ae5d43c8eda5fe6233fcac494ee67a0d504d"
      encoded mustEqual expected

      val attList = simAttrBuilder.eapsimAttributeBuilder.decordEncrData(iv, k_encr, attr)
      attList.size mustEqual 3
      attList.filter(att => att.getType == EapSimAkaAttributeTypes.AT_NEXT_REAUTH_ID).size mustEqual 1
      attList.filter(att => att.getType == EapSimAkaAttributeTypes.AT_NEXT_PSEUDONYM).size mustEqual 1
      attList.filter(att => att.getType == EapSimAkaAttributeTypes.AT_PADDING).size mustEqual 1
    }

    "buildAtRand" in {

      val attr = simAttrBuilder.eapsimAttributeBuilder.buildAtRand(List("101112131415161718191a1b1c1d1e1f",
        "202122232425262728292a2b2c2d2e2f",
        "303132333435363738393a3b3c3d3e3f").map(HexCodec.hex2Byte))
      println(attr)
      val encoded = HexCodec.byte2Hex(attr.encode.array)
      val expected = "010d0000101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f"

      val a = Unpooled.buffer(2)
      a.writeByte(79);
      a.writeByte(302);

      println(HexCodec.byte2Hex(a.array))

      encoded mustEqual expected
    }

  }
}