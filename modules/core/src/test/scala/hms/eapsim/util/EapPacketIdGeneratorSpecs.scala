/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

import org.specs2.mutable.Specification

/**
 *
 */
class EapPacketIdGeneratorSpecs extends Specification {

  "EapPacketIdGenerator" should {

    "next" in {
      EapPacketIdGenerator.reset
      EapPacketIdGenerator.next mustEqual 1
      EapPacketIdGenerator.next mustEqual 2

      for (a <- 3 to 255) {
        EapPacketIdGenerator.next
      }

      EapPacketIdGenerator.next mustEqual 1
    }
  }
}