/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.repo

import org.specs2.mutable.Specification
import io.netty.buffer.Unpooled._
import hms.common.radius.util.HexCodec
import scala.collection.JavaConversions._
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage
import hms.common.radius.decoder.eap.elements.EapCode
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.AtVersionList
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSim
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSimSubtype
import hms.common.radius.decoder.eap.elements.EapTypeCode
import hms.eapsim._

/**
 *
 */
class SimpleRequestRepoSpecs extends Specification {

  val repo = new SimpleRequestRepo(10000) with TranslogRepositoryComponent{
      val translogRepository = new TranslogRepository {
        def log(entry: TransLog)= {
          println("TL===>" + entry)
        }
      }
    }

  "SimpleRequestRepo" should {
    "save" in {

      val versionList = AtVersionList.build("0001")
      val fullAuth = new EapSimAkaAttribute(EapSimAkaAttributeTypes.AT_FULLAUTH_ID_REQ, 1,
        copiedBuffer(HexCodec.hex2Byte("0100")))
      val simMsg = new EapSim(EapSimSubtype.START, (List(versionList, fullAuth)))

      val eapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, 1, simMsg)

      val session = repo.initSession("12345678", EapTypeCode.EAP_SIM, "3133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267", "username", "123456789", 1, None, null)
    }
  }
}