package hms.eapsim.repo

import org.specs2.mutable.Specification
import java.util.{Calendar, Date}
import hms.eapsim.EapSimResult

class MsisdnStoreRepositoryImplSpec extends Specification{
  val mysqlConnector = new MysqlConnectorImpl(MysqlConnectorParams(
    "user",
    "password"
  ))
  val msisdnStoreRepository = new MsisdnStoreRepositoryImpl(mysqlConnector.datasouce, null, null)

  private def cleanup = {
    msisdnStoreRepository.deleteAllExpired(1)
  }

    "saving msisdn entry" in {
      cleanup
      val msisdnEntry = new MsisdnEntry(null, "121212121", "222222222222223")
      msisdnStoreRepository.save(msisdnEntry)
      val saved = msisdnStoreRepository.getBy(msisdnEntry.imsi)
      require(saved.isRight)

      val restult =  saved.right.foreach(
        op => op match {
          case Some(x) => x.msisdn mustEqual msisdnEntry.msisdn
          case None => require(false, "Saved entry expected")
        }
      )
    }


  "check get by" in {
    cleanup
    val someDay = Calendar.getInstance()
    val today = someDay.getTime
    msisdnStoreRepository.save(new MsisdnEntry(null, "1212121211", "22222222222221", today))
    msisdnStoreRepository.save(new MsisdnEntry(null, "1212121212", "22222222222222", today))
    msisdnStoreRepository.save(new MsisdnEntry(null, "1212121213", "22222222222223", today))

    someDay.add(Calendar.DAY_OF_MONTH, -1)
    val oneDayBefore = someDay.getTime
    msisdnStoreRepository.save(new MsisdnEntry(null, "1212121214", "22222222222224", oneDayBefore))

    someDay.add(Calendar.DAY_OF_MONTH, -1)
    val twoDayBefore = someDay.getTime
    msisdnStoreRepository.save(new MsisdnEntry(null, "1212121215", "22222222222225", twoDayBefore))

    msisdnStoreRepository.deleteAllExpired(60 * 60 * 24)

    validate("22222222222221", msisdnStoreRepository.getBy("22222222222221"))
    validate("22222222222222", msisdnStoreRepository.getBy("22222222222222"))
    validate("22222222222223", msisdnStoreRepository.getBy("22222222222223"))

    def validate(ref: String, result: EapSimResult[Option[MsisdnEntry]]) = {
      result.fold(
        fa => require(false, "Incorrect response received")
        ,
        fb => {
          fb match {
            case Some(x: MsisdnEntry) => ref mustEqual x.imsi
            case _ => "No value received" mustEqual "Any"
          }
        }
      )
    }
  }
}
