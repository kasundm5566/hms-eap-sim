package hms.eapsim.repo

import org.specs2.mutable.Specification
import hms.eapsim.key.AuthVector
import hms.eapsim.cache.{AuthenticationVectorCacheImpl, AuthenticationVectorCache}

import java.util.Calendar

class ReAuthContextRepositorySpec  extends Specification{

  val mysqlConnector = new MysqlConnectorImpl(MysqlConnectorParams(
    "user",
    "password"
  ))

  trait RepositoryComponentRegistry extends RepositoryComponent {
    val translogRepository: TranslogRepository = new MysqlTransLogRepository(mysqlConnector.pool, "", "",  "", "")
    val reAuthContextRepository : ReAuthContextRepository = new ReAuthContextRepositoryImpl(mysqlConnector.pool, "", "")
    def authVectorRepository() : AuthenticationVectorCache = new AuthenticationVectorCacheImpl
    val msisdnStoreRepository : MsisdnStoreRepository = new MsisdnStoreRepositoryImpl(mysqlConnector.pool, "", "")
    val keystoreRepository: KeystoreRepository = new KeystoreRepositoryImpl(mysqlConnector.pool, "", "")
  }

//
//  trait EapSim extends RepositoryComponentRegistry
//
//  val v = new EapSim{}
//
//  "re-auth-repo-test" in {
//    val next_authId = System.currentTimeMillis()
//    v.reAuthContextRepository.save(new ReAuthContext(10000l, "2323ere234", "344jkl;", "34344dfda", "sam", next_authId.toString, "23232"))
//
//    val one = v.reAuthContextRepository.findByNextReAuthId(next_authId.toString)
//    println(one)
//    require(one != null)
//  }

//  "find-expired-one" in {
//    val next_authId = System.currentTimeMillis()
//    v.reAuthContextRepository.save(new ReAuthContext("100003", "2323ere234", "344jkl;", "34344dfda", "sam", next_authId.toString, "232323"))
//
//    val one = v.reAuthContextRepository.loadExpired(1, 100)
//  }
}
