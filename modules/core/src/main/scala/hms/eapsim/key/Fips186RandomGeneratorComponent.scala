/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import hms.common.radius.util.HexCodec
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled

/**
 *
 */
trait Fips186RandomGeneratorComponent {
  def fips186Generator: Fips186PRNGenerator
}

trait Fips186PRNGenerator {
  def generateRandom(masterKey: Array[Byte]): ByteBuf
}

/**
 * Used to return dummy FIPS 186 Pseudo Random Number
 */
class DummyFips186PRNGenerator extends Fips186PRNGenerator {

  /**
   * Return a dummy Fips186 key.
   * This random key is taken from RFC4186 section A.5.  EAP-Request/SIM/Challenge page 82.
   */
  def generateRandom(masterKey: Array[Byte]): ByteBuf = {
    val rand = "536e5ebc4465582aa6a8ec9986ebb62025af1942efcbf4bc72b3943421f2a97439d45aeaf4e30601983e972b6cfd46d1c363773365690d09cd44976b525f47d3a60a985e955c53b090b2e4b73719196a402542968fd14a888f46b9a7886e44885949eab0fff69d52315c6c634fd14a7f0d52023d56f79698fa6596abeed4f93fbb48eb534d985414ceed0d9a8ed33c387c9dfdab92ffbdf240fcecf65a2c93b9"
    Unpooled.copiedBuffer(HexCodec.hex2Byte(rand))
  }
}