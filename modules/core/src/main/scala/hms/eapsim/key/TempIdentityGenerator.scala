/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import java.util.UUID

/**
 *
 */

case class TempIdentity(pseudonym: String, fastReAuthId: String)

class TempIdentityGenerator {

  @Deprecated
  def generate(userName : String): TempIdentity = {
    TempIdentity("w8w49PexCazWJ&xCIARmxuMKht5S1sxRDqXSEFBEg3DcZP9cIxTe5J4OyIwNGVzxeJOU1G",
      UUID.randomUUID().toString + "_" + userName)
  }

  /**
   * Generate pseudo random & next re authentication id
   * @param userNameWithRealm current username as per NAI, username@realm
   * @param flowType 0 for 3G and 1 for 2G need to be used
   * @return pseudo random & next re authentication id
   */
  def generateNEW(userNameWithRealm : String, flowType: String): TempIdentity = {
    TempIdentity("w8w49PexCazWJ&xCIARmxuMKht5S1sxRDqXSEFBEg3DcZP9cIxTe5J4OyIwNGVzxeJOU1G",
    "9" + flowType + UUID.randomUUID().toString + userNameWithRealm.substring(userNameWithRealm.indexOf("@")))
  }
}