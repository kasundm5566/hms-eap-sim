/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import hms.common.radius.util.HexCodec
import io.netty.buffer.Unpooled._

/**
 * Convert UMTS Quintuplet to GSM Triplet
 */
object VectorConversionUtil {

  private val logger: Logger = LogManager.getLogger(VectorConversionUtil.this)

  type ByteArray = Array[Byte]

  /**
   * Calculates GSM Triplet from UMTS Quintuplet.
   *
   * Logic is taken from http://books.google.com.sg/books?id=JEQn-RF86x4C&pg=PA227&lpg=PA227&dq=derive+gsm+triplet&source=bl&ots=QOTA1225BU&sig=iaWuSUKaXz1tc-mMl0QcT2KXbZU&hl=en&sa=X&ei=Prr9Uou5CIKNrgevy4CQCQ&ved=0CDoQ6AEwAw#v=onepage&q=derive%20gsm%20triplet&f=false
   *
   * @param quintuplet
   * @return
   */
  def umts2Gsm(quintuplet: Quintuplet): Triplet = {

    val gRand = quintuplet.rand
    val sres = calculateSres(quintuplet.xres)
    val kc = calculateKc(quintuplet.ck, quintuplet.ik)

    Triplet(gRand, sres, kc)
  }

  private def calculateSres(xresHex: String): String = {
    val xresSize = xresHex.length / 2

    val xres = buffer(16)
    xres.writeBytes(HexCodec.hex2Byte(xresHex))

    if (xresSize < 16) {
      val padSize: Int = 16 - xresSize
      xres.writeBytes(pad(padSize))
    }

    val divided = (xres.readBytes(4).array,
      xres.readBytes(4).array,
      xres.readBytes(4).array,
      xres.readBytes(4).array)

    val sres = xor(xor(xor(divided._1, divided._2), divided._3), divided._4)

    printCalculation(divided, sres)

    val sresStr = HexCodec.byte2Hex(sres)
    logger.debug(s"Calculated SRES[${sresStr}] from XRES[${xresHex}]")
    sresStr
  }

  private def calculateKc(ckStr: String, ikStr: String): String = {
    val ck = buffer(16).writeBytes(HexCodec.hex2Byte(ckStr))
    val ik = buffer(16).writeBytes(HexCodec.hex2Byte(ikStr))

    val divided = (ck.readBytes(8).array, ck.readBytes(8).array,
      ik.readBytes(8).array, ik.readBytes(8).array)

    val kc = xor(xor(xor(divided._1, divided._2), divided._3), divided._4)

    printCalculation(divided, kc)

    val kcStr = HexCodec.byte2Hex(kc)
    logger.debug(s"Calculated Kc[${kcStr}] from ck[${ckStr}], ik[${ikStr}]")

    kcStr
  }

  private def pad(size: Int) = {
    val pad = buffer(size)
    for (i <- 0 until size)
      pad.writeByte(0)

    pad
  }

  private def xor(a: ByteArray, b: ByteArray): ByteArray = {
    (a.toList zip b.toList).map(ele => (ele._1 ^ ele._2).toByte).toArray
  }

  private def printCalculation(source: Tuple4[ByteArray, ByteArray, ByteArray, ByteArray], result: ByteArray) {
    if (logger.isTraceEnabled) {
      printBinary(source._1)
      printBinary(source._2)
      printBinary(source._3)
      printBinary(source._4)
      logger.trace("--------------------------------------")
      printBinary(result)
    }
  }

  private def printBinary(a: ByteArray) {
    val sb = new StringBuilder
    for (x <- a) {
      sb.append(String.format("%8s", Integer.toBinaryString(x & 0xFF)).replace(' ', '0'))

    }
    logger.trace(sb.toString)
  }

}
