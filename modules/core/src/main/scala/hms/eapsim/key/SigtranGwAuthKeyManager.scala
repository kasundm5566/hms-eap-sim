package hms.eapsim.key

import hms.eapsim._
import hms.eapsim.async.http.client.connector.SigtranConnector
import hms.eapsim.gw.integration.SigtranGwRequestContext
import hms.eapsim.mapgw.{RoutingTable, SaiReq, SaiResp, URIKey}
import org.apache.logging.log4j.scala.Logging
import hms.eapsim.mapgw.client.CustomJsonProtocol._
import spray.json._

/**
 * Auth key manager using Map Gateway
 */
//todo : remove timeout arg
class SigtranGwAuthKeyManager(mapGwConnector: SigtranConnector, clientId: String, routingTable: RoutingTable,
                              uriMap: scala.collection.mutable.Map[String, String]) extends KeyManager with Logging {

  override def getAuthenticationSet(imsi: String, requestedVectors: Int, sigtranGwRequestContext: SigtranGwRequestContext)(implicit correlationId: String): EapSimResult[List[AuthVector]] = {

    logger.debug(s"Fetching auth vectors for Imsi [${imsi}]")
    def loadAuthVectors(hlrGt: String, hlrPc: Int): EapSimResult[List[AuthVector]] = {

      def getAuthVectors(saiResp: SaiResp):List[AuthVector] = {
        val triplets: List[AuthVector] = saiResp.gsmTriplets match {
          case Some(t) if(t != null && !t.isEmpty) => {
            t.map(ti => {
              val rand = ti.rand
              val sres = ti.sres
              val kc = ti.kc
              Triplet(rand, sres, kc)
            })
          }
          case _ => List.empty
        }

        val quintuplets: List[AuthVector] = saiResp.quinTuplets match {
          case Some(q) if(q!= null && !q.isEmpty) => {
            q.map(qi => {
              val rand = qi.rand
              val xres = qi.xres
              val ck = qi.ck
              val ik = qi.ik
              val autn = qi.autn
              Quintuplet(rand, xres, ck, ik, autn)
            })
          }

          case _ => List.empty
        }

        if(!triplets.isEmpty) triplets
        else if(!quintuplets.isEmpty) quintuplets
        else {
          logger.error("Neither Triplet Nor Quintuplet received with the response")
          List.empty
        }
      }

      val response = mapGwConnector.doPost(correlationId, (SaiReq(clientId, hlrPc, hlrGt, imsi, requestedVectors)).toJson.prettyPrint,
        uriMap(URIKey.SaiCallUrl), sigtranGwRequestContext)
      logger.debug(s"Response received from sigtranGw[$response]")
      Left(AsynchronousResponse.eapSimError("Response will send asynchronously"))

//      response.fold(
//        error => Left(SigtranGwErrorResp.eapSimError(s"Error occurred in sigtran gateway when calling SAI [ message: ${error.getMessage}]")),
//      resp => Right(getAuthVectors(resp)))

    }

    routingTable.getHrlRoutingData(imsi)
      .map(hlrRt => loadAuthVectors(hlrRt._2, hlrRt._1))
      .toRight(HlrRoutingDataNotSet.eapSimError(s"HLR Routing data not found for IMSI[${imsi}]"))
      .joinRight

  }
}
