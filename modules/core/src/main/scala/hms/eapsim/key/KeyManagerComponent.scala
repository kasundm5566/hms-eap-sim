/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import hms.eapsim.EapSimResult
import hms.eapsim.gw.integration.SigtranGwRequestContext

/**
 *
 */
trait KeyManagerComponent {
  def keyManager: KeyManager
}

sealed trait AuthVector extends Serializable

@SerialVersionUID(105l)
case class Triplet(rand: String, sres: String, kc: String) extends AuthVector
@SerialVersionUID(105l)
case class Quintuplet(rand: String, xres: String, ck: String, ik: String, autn: String) extends AuthVector

/**
 * Uses to get the GSM triplets from HLR
 */
trait KeyManager {

  def getAuthenticationSet(imsi: String, requestedVectors: Int, sigtranGwRequestContext: SigtranGwRequestContext)(implicit correlationId: String): EapSimResult[List[AuthVector]]
}

/**
 * Dummy implementation which sends hard-coded GSM triplet values for testing/simulations.
 */
class DummyKeyManager extends KeyManager {
  val triplets = List(Triplet("101112131415161718191a1b1c1d1e1f", "d1d2d3d4", "a0a1a2a3a4a5a6a7"),
    Triplet("202122232425262728292a2b2c2d2e2f", "e1e2e3e4", "b0b1b2b3b4b5b6b7"),
    Triplet("303132333435363738393a3b3c3d3e3f", "f1f2f3f4", "c0c1c2c3c4c5c6c7"))

  val quintuplets = List(Quintuplet("f12ac2e620ccd40bb920f6b3b474306b", "3fe524d57101d202", "8bb41f73b559138cfab2b89bda709ee8", "bbdd21caaea33e90ccb827399649f189", "d749908734350000cd298d7429daed26"))

  override def getAuthenticationSet(imsi: String, requestedVectors: Int, sigtranGwRequestContext: SigtranGwRequestContext)(implicit correlationId: String): EapSimResult[List[AuthVector]] = {
    if (imsi.startsWith("123")) {
      Right(quintuplets)
    } else {
      Right(triplets)
    }
  }

}