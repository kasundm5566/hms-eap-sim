/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.key

import java.security.MessageDigest
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}
import javax.crypto.{Cipher, Mac, KeyGenerator => JKeyGenerator}

import org.apache.logging.log4j.scala.Logging
import hms.common.radius.util.HexCodec
import io.netty.buffer.ByteBuf

@SerialVersionUID(100l)
case class PseudoRandomKeys(k_aut: String, k_encr: String, msk: String, emsk: String) extends Serializable {
//  override lazy val toString: String = s"PseudoRandomKeys[k_aut=${HexCodec.byte2Hex(k_aut.array)}, k_encr=${HexCodec.byte2Hex(k_encr.array)}, msk=${HexCodec.byte2Hex(msk.array)}, emsk=${HexCodec.byte2Hex(emsk.array)}]"
  override lazy val toString: String = s"PseudoRandomKeys[k_aut=${k_aut}, k_encr=${k_encr}, msk=${msk}, emsk=${emsk}]"
}

@SerialVersionUID(101l)
case class ReAuthPsudoRandomKeys(msk: String, emsk: String) extends Serializable {
//  override lazy val toString: String = s"ReAuthPsudoRandomKeys[msk=${HexCodec.byte2Hex(msk.array)}, emsk=${HexCodec.byte2Hex(emsk.array)}]"
  override lazy val toString: String = s"ReAuthPsudoRandomKeys[msk=${msk}, emsk=${emsk}"
}

/**
 *
 */
trait KeyGeneratorComponent {
  this: Fips186RandomGeneratorComponent =>

  def keyGenerator: KeyGenerator

  class KeyGenerator extends Logging {

    def getSHA1Instance: MessageDigest = MessageDigest.getInstance("SHA-1")

    /**
     * Encrypt using HMAC-SHA1
     */
    def hmacSha1Encode(key: Array[Byte], value: ByteBuf): Array[Byte] = {
      val keySpec = new SecretKeySpec(key, "HmacSHA1")
      val mac: Mac = Mac.getInstance("HmacSHA1")
      mac.init(keySpec)
      mac.doFinal(value.array)
    }

    /**
     * Encrypt given plain-text data with AES in CBC mode with an IV.
     */
    def aesWithCbcEncode(iv: Array[Byte], key: Array[Byte], plainText: Array[Byte]): Array[Byte] = {
      if (iv.length != 16) throw new IllegalArgumentException("Invalid IV, should be 16 bytes long.")

      val keygen: JKeyGenerator = JKeyGenerator.getInstance("AES");
      keygen.init(128);
      val skeySpec: SecretKeySpec = new SecretKeySpec(key, "AES");

      val ivspec: IvParameterSpec = new IvParameterSpec(iv);

      val cipher: Cipher = Cipher.getInstance("AES/CBC/NoPadding");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);

      val encrypted: Array[Byte] = cipher.doFinal(plainText);

      //      // reinitialize the cipher for decryption
      //      cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
      //
      //      // decrypt the message
      //      val decrypted: Array[Byte] = cipher.doFinal(encrypted);
      //      println("Plaintext: " + new String(decrypted) + "\n");
      //      println("=================================================")

      encrypted
    }

    def aesWithCbcDecode(iv: Array[Byte], key: Array[Byte], encrypted: Array[Byte]): Array[Byte] = {
      if (iv.length != 16) throw new IllegalArgumentException("Invalid IV, should be 16 bytes long.")

      val keygen: JKeyGenerator = JKeyGenerator.getInstance("AES")
      keygen.init(128);
      val skeySpec: SecretKeySpec = new SecretKeySpec(key, "AES")

      val ivspec: IvParameterSpec = new IvParameterSpec(iv)

      val cipher: Cipher = Cipher.getInstance("AES/CBC/NoPadding")
      // reinitialize the cipher for decryption
      cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec)
      // decrypt the message
      cipher.doFinal(encrypted)
    }

    /**
     * Generate the Master Key(MK) for EAP-SIM.
     * Refer section "7.  Key Generation" in RFC4186 for more information on MK generation.
     *
     * All input parameters should be HEX strings.
     */
    def generateMasterKeyEapSimForFullAuth(identity: String, kc: List[String], nonceMt: String,
      versionList: List[String], selectedVersion: String): Array[Byte] = {
      logger.debug(s"Generating Eap-SIM MasterKey from identity[${identity}], kc[${kc.mkString(",")}], nonceMt[${nonceMt}], versionList[${versionList.mkString(",")}], selectedVersion[${selectedVersion}]")

      val combined = identity :: (kc :+ nonceMt) ++: (versionList :+ selectedVersion)

      getSHA1Instance.digest(HexCodec.hex2Byte(combined.mkString("")))
    }

    /**
     * This will generate Master key for fast re-authentication. Which will be used later as a seed value for generating pseudo random number (Master Key).
     * On fast re-authentication, the same pseudo-random number generator can be used to generate a new Master Session Key and a new Extended
     *  Master Session Key.  The seed value XKEY' is calculated as follows:  <br><br>
     *  @see RFC 4186 page 43 for more information
     *
     *  XKEY' = SHA1(Identity|counter|NONCE_S| MK)   <br><br>
     *
     * @param identity fast re-auth identity from AT_IDENTITY attribute [EAP-Response/SIM/Start, EAP-Response/Identity]
     * @param counterAsNetworkByteOrder AT_COUNTER from EAP-Response/SIM/Re-authentication
     * @param nOnceSAsHex denotes the 16-byte NONCE_S value from AT_NONCE_S attribute used in EAP-Request/SIM/Re-authentication
     * @param mk master key from full authentication
     * @return SHA-1 20 bytes
     */
    def generateXKeyAlternativeSeed(identity: String, counterAsNetworkByteOrder: String, nOnceSAsHex: String, mk: String): Array[Byte] = {
      logger.debug(s"Generating Eap-SIM MasterKey for Re-Auth from [ identity$identity, counter: $counterAsNetworkByteOrder, nOnceS: $nOnceSAsHex, mk: $mk]")
      getSHA1Instance.digest(HexCodec.hex2Byte(identity + counterAsNetworkByteOrder + nOnceSAsHex + mk))
    }


    /**
     * Generate the Master Key(MK) for EAP-AKA.
     * Refer section "7.  Key Generation" in RFC4187 for more information on MK generation.
     *
     * All input parameters should be HEX strings.
     */
    def generateMasterKeyEapAka(identity: String, ik: String, ck: String): Array[Byte] = {
      logger.debug(s"Generating Eap-AKA MasterKey from identity[${identity}], ik[${ik}], ck[${ck}]")

      val combined = identity + ik + ck
      getSHA1Instance.digest(HexCodec.hex2Byte(combined))
    }

    /**
     * Generate the random keys required for the generation of SIM Challenge
     */
    def generatePseudoRandomKeysForFullAuth(masterKey: Array[Byte]): PseudoRandomKeys = {
      val randomKey = fips186Generator.generateRandom(masterKey)
      logger.debug(s"Received generated pseudo random number[${HexCodec.byte2Hex(randomKey.array)}]")

      val k_encr = randomKey.readBytes(16)
      val k_aut = randomKey.readBytes(16)
      val msk = randomKey.readBytes(64)
      val emsk = randomKey.readBytes(64)
      logger.debug(s"Generated PRN k_encr[${HexCodec.byte2Hex(k_encr.array)}] k_aut[${HexCodec.byte2Hex(k_aut.array)}] msk[${HexCodec.byte2Hex(msk.array)}] emask[${HexCodec.byte2Hex(emsk.array)}]")

      PseudoRandomKeys(k_aut = HexCodec.byte2Hex(k_aut.array), k_encr = HexCodec.byte2Hex(k_encr.array), msk = HexCodec.byte2Hex(msk.array), emsk = HexCodec.byte2Hex(emsk.array))
    }

    /**
     * Generate the random keys required for the generation of SIM re authentication
     */
    def generatePseudoRandomKeysForReAuth(masterKey: Array[Byte]): ReAuthPsudoRandomKeys = {
      val randomKey = fips186Generator.generateRandom(masterKey)
      logger.debug(s"Received generated pseudo random number for Re-Authentication [length: ${randomKey.readableBytes()}][${HexCodec.byte2Hex(randomKey.array)}]")
      val msk = randomKey.readBytes(64).slice()
      val emsk = randomKey.readBytes(64).slice()
      logger.debug(s"Generated PRN msk[${HexCodec.byte2Hex(msk.array)}] emask[${HexCodec.byte2Hex(emsk.array)}]")

      ReAuthPsudoRandomKeys(msk = HexCodec.byte2Hex(msk.array), emsk = HexCodec.byte2Hex(emsk.array))
    }
  }
}





