package hms.eapsim.serializer

/**
 * Created by sampath on 4/24/17.
 */
object RequestOperationCode {

  val REQUEST_UPDATE: Byte = 1
  val REQUEST_REMOVE:Byte = 2
  val REQUEST_GET:Byte = 3

}
