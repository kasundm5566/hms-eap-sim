package hms.eapsim.serializer

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.{Input, Output}
import hms.eapsim.repo.AuthenticationSession
import org.apache.logging.log4j.scala.Logging
import org.objenesis.strategy.StdInstantiatorStrategy

class AuthenticationSessionSerializer extends Logging {

  private val kryoThreadLocal = new ThreadLocal[Kryo] {

    override def initialValue(): Kryo = {
      val kryo: Kryo = new Kryo
      kryo.setInstantiatorStrategy(new StdInstantiatorStrategy)
      kryo.register(classOf[AuthenticationSession])

      return kryo
    }
  }

  def serialize(session: AuthenticationSession): Array[Byte] = {
    val outputStream: ByteArrayOutputStream = new ByteArrayOutputStream()
    val output: Output = new Output(outputStream)

    kryoThreadLocal.get().writeObject(output, session)

    output.flush()
    output.close()
    outputStream.toByteArray
  }

  def deserialize(array: Array[Byte]): AuthenticationSession = {
    val inputStream = new ByteArrayInputStream(array)
    val input = new Input(inputStream)

    val ob: AuthenticationSession = kryoThreadLocal.get().readObject(input, classOf[AuthenticationSession])
    input.close()
    ob
  }
}