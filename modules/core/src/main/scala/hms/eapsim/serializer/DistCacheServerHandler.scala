package hms.eapsim.serializer

import java.util
import java.util.concurrent.{ConcurrentMap, ExecutorService, Executors}

import hms.eapsim.repo.AuthenticationSession
import io.netty.buffer.{ByteBuf, ByteBufInputStream, Unpooled}
import io.netty.channel.socket.DatagramPacket
import io.netty.channel.{Channel, ChannelHandlerContext, ChannelInboundMessageHandlerAdapter}
import org.apache.logging.log4j.scala.Logging


class DistCacheServerHandler(cache: ConcurrentMap[String, AuthenticationSession]) extends ChannelInboundMessageHandlerAdapter[DatagramPacket] with Logging {
  val serializer = new AuthenticationSessionSerializer
  val channelList = new util.ArrayList[Channel]()


  override def channelActive(ctx: ChannelHandlerContext): Unit = {
    channelList.add(ctx.channel())
  }

  override def messageReceived(ctx: ChannelHandlerContext, msg: DatagramPacket): Unit = {
    try {

      val data: ByteBuf = msg.data()
      val inputStream = new ByteBufInputStream(data)

      val messageType: Byte = inputStream.readByte()
      val size: Int = inputStream.readInt()

      val dataContainer = new Array[Byte](size)
      inputStream.read(dataContainer)


      if (messageType == RequestOperationCode.REQUEST_UPDATE) {
        val session: AuthenticationSession = serializer.deserialize(dataContainer)
        cache.put(session.correlationId, session)
      } else if (messageType == RequestOperationCode.REQUEST_REMOVE) {
        val correlationId = new String(dataContainer)
        cache.remove(correlationId)
      } else if (messageType == RequestOperationCode.REQUEST_GET) {
        val correlationId = new String(dataContainer)
        logger.info(s"A1 [REQUEST_GET] received from [${correlationId}]")
        val removed = cache.remove(correlationId)

        //PROTOCOL
        //Length correlationId (1Byte) | correlationId| length of session data (2Byte) | session data
        if (removed == null) {
          val buffer = Unpooled.buffer(3 + dataContainer.length)
          buffer.writeByte(dataContainer.length)
          buffer.writeBytes(dataContainer)
          buffer.writeShort(0)
          logger.info(s"A1 Session not available [${correlationId}]")
        } else {
          val sessionData = serializer.serialize(removed)
          logger.info(s"A1 Session found [${removed}]")
          val buffer = Unpooled.buffer(3 + dataContainer.length + sessionData.length)
          buffer.writeByte(dataContainer.length)
          buffer.writeBytes(dataContainer)
          buffer.writeShort(sessionData.length)
          buffer.writeBytes(sessionData)

          val sessionGetResponse: DatagramPacket = new DatagramPacket(buffer, msg.remoteAddress())

          ctx.channel.write(sessionGetResponse)
          ctx.channel.flush()
        }
      }
    } catch {
      case e: Exception => logger.error("A1 : Error while handling message", e)
    }

  }
}