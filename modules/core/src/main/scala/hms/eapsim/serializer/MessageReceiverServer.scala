package hms.eapsim.serializer

import java.net.NetworkInterface
import java.util.concurrent.ConcurrentMap

import hms.common.radius.util.DefaultThreadFactory
import hms.eapsim.repo.AuthenticationSession
import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioDatagramChannel

/**
 * Created by sampath on 4/24/17.
 */
class MessageReceiverServer(host: String, port: Int, ioPoolSize: Int, cache: ConcurrentMap[String, AuthenticationSession]) {
  var serverBootstrap: Bootstrap = new Bootstrap
  val group = new NioEventLoopGroup(ioPoolSize, new DefaultThreadFactory("Dist-cache-server-IO-Pool"))
  val starterThread = new Thread(new Runnable {
    override def run(): Unit = bind()
  })
  start()


  def start() = {
    serverBootstrap.option(ChannelOption.SO_SNDBUF, Int.box(1048576))
    serverBootstrap.option(ChannelOption.SO_RCVBUF, Int.box(1048576))

    serverBootstrap.group(group).channel(classOf[NioDatagramChannel])
      .handler(new DistCacheServerHandler(cache))

    starterThread.start()
  }

  def bind() = {
    serverBootstrap.bind(host, port).sync.channel.closeFuture.await
  }

}
