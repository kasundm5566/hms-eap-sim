package hms.eapsim.serializer

import java.net.InetSocketAddress
import java.util.concurrent.atomic.AtomicInteger

import io.netty.bootstrap.Bootstrap
import io.netty.channel.Channel
import org.apache.logging.log4j.scala.Logging

import scala.collection.mutable.ListBuffer

/**
 * Created by sampath on 4/25/17.
 */
case class EndPoint(host: String, port: Int, numberOfConnection: Int) extends Logging {
  private val channelList = new ListBuffer[Channel]
  private val nextChannel = new AtomicInteger(0)

  override def equals(o: scala.Any): Boolean = {
    if (this == o) return true

    if (!(o.isInstanceOf[EndPoint])) return false

    val endPoint: EndPoint = o.asInstanceOf[EndPoint]

    if (port != endPoint.port) return false

    if (!(host == endPoint.host)) return false
    true
  }

  def connect(bootsrap: Bootstrap): Unit = {
    for (index <- 1 to numberOfConnection) {
      if (channelList.size < numberOfConnection) {
        logger.info(s"A1: Connecting to [host: ${host}], [port: ${port}]")
        bootsrap.connect(new InetSocketAddress(host, port))
      }
    }
  }

  def isConnectionRequired(): Boolean = {
    channelList.size < numberOfConnection
  }

  def add(channel: Channel) = {
    if (channelList.size < numberOfConnection) {
      channelList += channel
      logger.info(s"A1: Adding channel [${channel}] to end point [${this}}]")
    } else {
      logger.info(s"A1: Did not added the channel [${channel}] to end point [${this}}]")
    }
  }

  def remove(channel: Channel) = {
    if (channelList.size > 0) {
      channelList -= channel
      logger.info(s"A1: removing channel [${channel}] from [$this]")
    }
  }

  def getNextChannel(): Option[Channel] = {
    if (channelList.size > 0) {
      Some(channelList(nextChannel.incrementAndGet() % channelList.size))
    } else {
      None
    }
  }

  override def hashCode: Int = {
    val result = host.hashCode
    31 * result + port
  }
}