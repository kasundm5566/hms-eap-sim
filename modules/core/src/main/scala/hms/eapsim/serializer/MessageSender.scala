package hms.eapsim.serializer

import java.net.InetSocketAddress
import java.util.concurrent.locks.ReentrantLock
import java.util.concurrent.{ConcurrentHashMap, TimeUnit}

import hms.common.radius.util.DefaultThreadFactory
import hms.eapsim.repo.{AuthenticationSession, RequestSessionContext}
import io.netty.bootstrap.Bootstrap
import io.netty.buffer.Unpooled
import io.netty.channel._
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.DatagramPacket
import io.netty.channel.socket.nio.NioDatagramChannel
import org.apache.logging.log4j.scala.Logging

import scala.collection.mutable.ListBuffer

class MessageSender(connectingEndPointList: List[String], ioPoolSize: Int, reconnectDelayInMilliSec: Long = 3000
                    , requestTimeoutDelayInMillSec: Long = 200, numberOfConnection: Int = 3) extends Logging {

  val serializer = new AuthenticationSessionSerializer

  val clientBootstrap: Bootstrap = new Bootstrap

  var group: EventLoopGroup = new NioEventLoopGroup(ioPoolSize, new DefaultThreadFactory("Dist-Cache-IO-Pool"))

  val lock = new ReentrantLock()

  val endPointList = new ListBuffer[EndPoint]()

  val reconnector = new Thread(new ConnectionManager, "Dist-cache-Connection-Mgr")

  val requestContextMap = new ConcurrentHashMap[String, RequestSessionContext]()

  start()

  def start(): Unit = {
    clientBootstrap.option(ChannelOption.SO_SNDBUF, Int.box(1048576))
    clientBootstrap.option(ChannelOption.SO_RCVBUF, Int.box(1048576))
    clientBootstrap.group(group).channel(classOf[NioDatagramChannel]).handler(new DistCacheClientHandler(this, requestContextMap))

    for (endPoint <- connectingEndPointList) {
      val endPointInfo: Array[String] = endPoint.split(":", 0)
      endPointList += EndPoint(endPointInfo(0).trim, endPointInfo(1).trim.toInt, numberOfConnection)
    }
    reconnector.start()
  }

  def connect(endPoint: EndPoint): Unit = {
    endPoint.connect(clientBootstrap)
  }

  def removeChannel(channel: Channel) = {
    lock.lock()
    try {
      val endPoint = getEndPoint(channel)
      endPoint.map(ep => {
        ep.remove(channel)
      })
    } finally {
      lock.unlock()
    }
  }

  def addChannel(channel: Channel) = {
    lock.lock()
    try {
      val endPoint = getEndPoint(channel)

      endPoint.map(ep => if (ep.isConnectionRequired()) {
        ep.add(channel)
      })
    } finally {
      lock.unlock()
    }
  }

  def getEndPoint(channel: Channel): Option[EndPoint] = {
    val channelInetAddress = channel.remoteAddress().asInstanceOf[InetSocketAddress]
    val channelAddress = channelInetAddress.getAddress

    val list = endPointList.filter(ep => {
      val endPointInetAddress = new InetSocketAddress(ep.host, ep.port)
      val endPointAddress = endPointInetAddress.getAddress

      endPointAddress.getHostAddress.equals(channelAddress.getHostAddress) && channelInetAddress.getPort == endPointInetAddress.getPort
    })

    if (list.size == 1) {
      val endPoint: EndPoint = list(0)
      logger.info(s"sA1: [$endPoint] found for channel [$channel]")
      Some(endPoint)
    } else {
      logger.info(s"sA1: EndPoint Not Found for channel [$channel]")
      None
    }
  }

  def getSession(correlationId: String): Option[AuthenticationSession] = {
    for (endPoint <- endPointList) {
      val session = doGetSession(correlationId, endPoint)

      if (!session.isEmpty) {
        return session
      }
    }
    None
  }

  def doGetSession(correlationId: String, endPoint: EndPoint): Option[AuthenticationSession] = {
    val data: Array[Byte] = correlationId.getBytes
    val buffer = Unpooled.buffer(5 + data.length)
    buffer.writeByte(3)
    buffer.writeInt(data.length)
    buffer.writeBytes(data)

    val context: RequestSessionContext = new RequestSessionContext(correlationId)
    requestContextMap.put(correlationId, context)

    endPoint.getNextChannel().map(channel => {
      val sessionRemoveRequest: DatagramPacket = new DatagramPacket(buffer, channel.remoteAddress().asInstanceOf[InetSocketAddress])
      channel.write(sessionRemoveRequest)
      channel.flush()
      logger.info(s"S20: Sending request to [$channel}}]")
    })
    context.lock.lock()
    try {
      if (context.response.isEmpty) {
        context.noData.await(requestTimeoutDelayInMillSec, TimeUnit.MILLISECONDS)
      }
    } finally {
      requestContextMap.remove(correlationId)
      context.lock.unlock()
    }
    context.response
  }



  class ConnectionManager extends Runnable {

    override def run(): Unit = {
      for (endPoint <- endPointList) {
        connect(endPoint)
      }
    }
  }

}

