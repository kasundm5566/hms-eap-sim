package hms.eapsim.serializer

import java.util.concurrent.{ConcurrentHashMap, Executors}

import hms.eapsim.repo.{AuthenticationSession, RequestSessionContext}
import hms.eapsim.toOption
import io.netty.buffer.ByteBufInputStream
import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.socket.DatagramPacket
import io.netty.channel.{ChannelHandlerContext, ChannelInboundMessageHandlerAdapter}
import org.apache.logging.log4j.scala.Logging

@Sharable
class DistCacheClientHandler(messageSender: MessageSender, requestContextMap: ConcurrentHashMap[String, RequestSessionContext]) extends ChannelInboundMessageHandlerAdapter[DatagramPacket] with Logging {

  val serializer = new AuthenticationSessionSerializer


  override def messageReceived(ctx: ChannelHandlerContext, msg: DatagramPacket): Unit = {
    try {
      val inputStream = new ByteBufInputStream(msg.data())

      val sizeOfId = inputStream.read()
      val correlationIdData = new Array[Byte](sizeOfId)
      inputStream.read(correlationIdData)
      val correlationId = new String(correlationIdData)

      if (correlationId != null && !correlationId.isEmpty) {
        val context = toOption(requestContextMap.get(correlationId))
        context.map(ctx => {
          val dataSize = inputStream.readShort()

          if (dataSize > 0) {
            val sessionData = new Array[Byte](dataSize)
            inputStream.read(sessionData)
            val session: AuthenticationSession = serializer.deserialize(sessionData)
            ctx.response = Some(session)
          }
          ctx.lock.lock()
          try {
            ctx.noData.signalAll()
          } finally {
            ctx.lock.unlock()
          }
        })
      }
    } catch {
      case e: Exception => logger.error("A1 Error while receiving message", e)
    }
  }

  override def channelActive(ctx: ChannelHandlerContext): Unit = {
    logger.info("A1: channel activated")
    messageSender.addChannel(ctx.channel())
    logger.info("A1: setting channel on sender")
  }

  override def channelUnregistered(ctx: ChannelHandlerContext): Unit = {
    messageSender.removeChannel(ctx.channel())
    logger.info("A1: channel unregistered")
  }

  override def exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable): Unit = {
    messageSender.removeChannel(ctx.channel())
    logger.error("Error occurred in client handler", cause)
  }
}