/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

import hms.common.radius.RequestContext
import hms.common.radius.decoder.eap.elements.{EapCode, EapSuccessFailureMessage, Identity, EapMessage}
import org.tinyradius.dictionary.{AttributeType, Dictionary}
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.{AbstractEapSimAka, EapSimAkaAttributeTypes, EapSimAkaAttribute}
import hms.eapsim.{EapSimError, DataMissingInMessage, EapSimResult, EapAttributeNotFound}
import org.apache.logging.log4j.scala.Logging
import io.netty.buffer.Unpooled._
import scala.util.control.Exception._
import hms.eapsim.repo._
import org.tinyradius.attribute.{StringAttribute, RadiusAttribute}
import hms.common.radius.util.HexCodec
import io.netty.buffer.Unpooled
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.{EapAkaSubtype, EapAka}
import hms.eapsim.repo.TransLog
import scala.Some

/**
 *
 */
object EapMsgUtil extends Logging {
  def getEapMessage(requestContext: RequestContext): Option[EapMessage] = {
    val attr = requestContext.getReceived.getAttribute(79)
    attr match {
      case eap: EapMessage => Some(eap)
      case _ => None
    }
  }

  def toHex(value: Short): String =  HexCodec.byte2Hex(Unpooled.buffer(2).writeShort(value).array())

  def toHex(value: Int): String =  HexCodec.byte2Hex(Unpooled.buffer(2).writeShort(value).array())

  def getImsi(attribute: EapSimAkaAttribute)(implicit transLog: TransLog): EapSimResult[String] = {

    attribute.getType match {
      case EapSimAkaAttributeTypes.AT_IDENTITY => {
        val data = copiedBuffer(attribute.getAttributeData)
        val idLength = data.readShort
        val nai = new String(data.readBytes(idLength).array)
        logger.debug(s"Received Identity[${nai}]")
        getImsiFromNAI(nai)
      }
      case at => Left(EapAttributeNotFound.eapSimError(s"Cannot extract IMSI from [${at}]"))

    }
  }

  def transform(aka: EapAka) : Option[Identity] = {
    logger.info("Converting Aka-Identity ---> Identity to initialize authentication")
    val atIdentity = aka.getAttribute(EapSimAkaAttributeTypes.AT_IDENTITY)

    atIdentity match {
      case att: EapSimAkaAttribute => {
        val data = copiedBuffer(atIdentity.getAttributeData)
        val idLength = data.readShort
        val identity = new Identity(data.readBytes(idLength))
        logger.info(s"Identity created from AT_IDENTITY flag [ identity: ${identity}]")
        Some(identity)
      }
      case _ => {
        logger.info("No AT_IDENTITY flag found in Aka-Identity")
        None
      }
    }
  }



  @Deprecated
  def getAtIdentity(eapMsg: AbstractEapSimAka): EapSimResult[String] = {
    val atIdentity = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_IDENTITY)
    if (atIdentity != null) {
      val data = copiedBuffer(atIdentity.getAttributeData)
      val idLength = data.readShort
      val identity = new String(data.readBytes(idLength).array())
      logger.debug(s"Identity extracted from AT_IDENTITY flag [ Identity: ${identity}]")
      Right(identity)
    } else {
      Left(DataMissingInMessage.eapSimError("Required AT_IDENTITY flag missing in Aka-Identity"))
    }
  }

  def logRemoveSession(removed: Option[AuthenticationSession]) = {
    removed.map(session => logger.debug(s"Removed session for the imsi[${removed.get.imsi}]"))
//    removed match {
//      case Some(r) => logger.info(s"Removed session for the imsi[${removed.get.imsi}]")
//      case None => logger.warn(s"No saved session found to remove]")
//    }
  }

  /**
   * Extract IMSI form NAI
   */
  def getImsiFromNAI(nai: String)(implicit transLog: TransLog): EapSimResult[String] = {
    val imsi = catching(classOf[Throwable]) opt { nai.substring(1, nai.indexOf('@')) }
    logger.debug(s"Extracted IMSI[${imsi}]")
    imsi.foreach(x => transLog.imsi = x)
    imsi.toRight(EapAttributeNotFound.eapSimError("IMSI not found"))
  }

  def extactEmsiFromIdentity(identity: String) : String = {
    if (identity.indexOf('@') > 0) {
      identity.substring(1, identity.indexOf('@'))
    } else {
      identity
    }
  }

  /**
   * 3G permanent ids start with 0, and re-auth id start 90
   *
   * @param unAtRealm user's userName@realm
   * @return true if the given userName@realm belongs to a 3G identity
   */
  def getEapMethod(unAtRealm: String): EapProtocol = {
    if (unAtRealm.startsWith("90") || unAtRealm.startsWith("0")) {
      AKA
    } else {
      SIM
    }
  }

  def getFlowType(unAndRealm: String) : FlowType = {
    if (unAndRealm.startsWith("9")) {
      RE_AUTHENTICATION
    } else {
      FULL_AUTHENTICATION
    }
  }

  def getEapMethod(eapMsg: AbstractEapSimAka): EapProtocol = {
    eapMsg.getEapSubType match {
      case aka: EapAkaSubtype => AKA
      case _ => SIM
    }
  }

  def isReAuthIdentity(unAndRealm: String): Boolean = {
    unAndRealm.startsWith("9")
  }

  def extractUsernameFromNAI(unAndRealm: String): String = {
    if (isReAuthIdentity(unAndRealm)) {
      unAndRealm.substring(2, unAndRealm.indexOf('@'))
    } else {
      unAndRealm.substring(1, unAndRealm.indexOf('@'))
    }
  }

  /**
   * Check whether we can use EapSIM authentication.
   * If the first charactor of NAI is "1" we can use EapSIM instead of AKA.
   *
   * See section 4.2.1.6 in RFC 4186.
   * @param nai
   * @return
   */
  def canUseEapSim(nai: String): Boolean = {
    val ch1 = catching(classOf[Throwable]) opt { nai.substring(0, 1) }
    ch1.map(_.equalsIgnoreCase("1")).getOrElse(false)
  }

  /**
   * Extract value of Radius State attribute.
   */
  def getStateAttributeValue(requestContext: RequestContext): Option[String] = {
    val state: RadiusAttribute = requestContext.getReceived.getAttribute(24);
    if (state != null && state.getAttributeData != null) {
      val v = new String(state.getAttributeData)
      logger.debug(s"Found State attribute[${v}]")
      Some(v)
    } else {
      logger.error("Radius State attribute not found")
      None
    }

  }

  def generateReplyMessageAttribute(dict: Dictionary, msg: String): RadiusAttribute = {
    val eapAttrType: AttributeType = dict.getAttributeTypeByName("Reply-Message")
    val at = new StringAttribute(eapAttrType.getTypeCode, msg)
    at
  }

  def createDefaultEapError(error: EapSimError, request: RadiusPacket,
                            errorCodeMapper: ErrorCodeMapper) : Either[EapSimError, RadiusPacket] = {
    val erm: EapSuccessFailureMessage = new EapSuccessFailureMessage(EapCode.FAILURE, EapPacketIdGenerator.next, 1)
    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, request.getPacketIdentifier);
    rp.addAttribute(erm);
    rp.addAttribute(EapMsgUtil.generateReplyMessageAttribute(rp.getDictionary, errorCodeMapper.getMsg(error)))
    Right(rp)
  }

  def createDefaultRadiusError(request: RadiusPacket, errorMsg: Option[String]): RadiusPacket = {
    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, request.getPacketIdentifier);
    rp.addAttribute("Reply-Message", errorMsg.getOrElse("Unknown server error occurred."))
    rp
  }
}
