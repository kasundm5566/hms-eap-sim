/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

import io.netty.buffer.Unpooled._
import hms.common.radius.util.HexCodec
import hms.eapsim.key.KeyGeneratorComponent
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttribute
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes._
import scala.Some

trait EapsimAttributeBuilderComponent {
  this: KeyGeneratorComponent =>
  def eapsimAttributeBuilder: EapsimAttributeBuilder

  /**
   * Provide simple methods to build EapSim Attributes
   */
  class EapsimAttributeBuilder {

    /**
     *
    10.8.  AT_IDENTITY

   The format of the AT_IDENTITY attribute is shown below.

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | AT_IDENTITY   | Length        | Actual Identity Length        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    .                       Identity (optional)                     .
    .                                                               .
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * @param identity user identity
     */
    def buildAtIdentity(identity: Array[Byte]) =
      buildAttributeWithActualLengthField(AT_IDENTITY, identity)


    /**
     * RFC 4186 - 10.10.  AT_NEXT_PSEUDONYM
     */
    def buildAtNextPseudonym(pseudonym: Array[Byte]): EapSimAkaAttribute =
      buildAttributeWithActualLengthField(AT_NEXT_PSEUDONYM, pseudonym)

    /**
     * RFC 4186 - 10.11.  AT_NEXT_REAUTH_ID
     *
     *
    0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | AT_NEXT_REAU..| Length        | Actual Re-Auth Identity Length|
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    .               Next Fast Re-authentication Username            .
    .                                                               .
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     Eg: - Sending of `2b9e06f9-6d95-4373-91af-4d759cdb8fd3_1320727710000010@wlan.mnc072.mcc320.3gppnetwork.org` will produce ,

    |85|17|0058|
    32623965303666392d366439352d343337332d393161662d3464373539636462386664335f3133323037323737313030303030313040776c616e2e6d6e633037322e6d63633332302e336770706e6574776f726b2e6f7267

     */
    def buildAtNextReauthId(reauthId: Array[Byte]): EapSimAkaAttribute =
      buildAttributeWithActualLengthField(AT_NEXT_REAUTH_ID, reauthId)

    /**
     * RFC 4186 - 10.15.  AT_COUNTER
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  AT_COUNTER   | Length = 1    |           Counter             |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       Eg: sending of counter= 2 will produce following,
           |13|01|0002
     */
    def buildAtCounter(counter: Int): EapSimAkaAttribute = {
      val data = buffer(2)
      data.writeShort(counter) //Two reserved bytes are set to zero
      new EapSimAkaAttribute(AT_COUNTER, data.readableBytes(), data)
    }


    def buildClientError(value: Int) = {
      val errorCode = buffer(2)
      errorCode.writeShort(value)
      new EapSimAkaAttribute(AT_CLIENT_ERROR_CODE, errorCode.readableBytes(), errorCode)
    }

    /**
    10.16.  AT_COUNTER_TOO_SMALL

   The format of the AT_COUNTER_TOO_SMALL attribute is shown below.

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  AT_COUNTER...| Length = 1    |           Reserved            |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    def buildAtCounterTooSmall(): EapSimAkaAttribute = {
      val data = buffer(2)
      data.writeShort(0) //Two reserved bytes are set to zero
      new EapSimAkaAttribute(AT_COUNTER_TOO_SMALL, data.readableBytes(), data)
    }

    /**
     * RFC 4186 - 10.17.  AT_NONCE_S
     *
    0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | AT_NONCE_S    | Length = 5    |           Reserved            |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    |                            NONCE_S                            |
    |                                                               |
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     Eg: sending 101 will produce following byte stream
         |15| 06| 0000
         00000000 00000000 00000000 00000065
     */
    def buildAtNonceServer(nonceS: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(18)
      data.writeShort(0) //Reserved two bytes are set to zero
      data.writeBytes(nonceS)

      new EapSimAkaAttribute(AT_NONCE_S, 18, data)
    }

    def getNonceS(nonceServer: Int): Array[Byte] = {
      val data = buffer(18)
      for (a <- 1 to 3) data.writeInt(0)
      data.writeInt(nonceServer)

      data.array()
    }

    /**
     * RFC 4186 - 10.12.  AT_IV
     */
    def buildAtIv(iv: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(18)
      data.writeShort(0) //Reserved two bytes are set to zero
      data.writeBytes(iv)

      new EapSimAkaAttribute(AT_IV, 18, data)
    }

    def extractIv(iv: EapSimAkaAttribute): Array[Byte] = {
      val buffer = copiedBuffer(iv.getAttributeData)
      buffer.readShort()
      val data = new Array[Byte](16)
      buffer.readBytes(data)
      data
    }

    /**
     * RFC 4186 - 10.12.  AT_PADDING
     */
    def buildAtPadding(paddingLength: Int): EapSimAkaAttribute = {
      if (paddingLength <= 0) throw new IllegalArgumentException("Invalid Padding Length")

      val data = buffer(paddingLength)
      for (i <- 0 until paddingLength)
        data.writeByte(0)

      new EapSimAkaAttribute(AT_PADDING, paddingLength, data)
    }

    /**
     * RFC 4186 - 10.12.  AT_ENCR_DATA

    0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | AT_ENCR_DATA  | Length        |           Reserved            |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    .                    Encrypted Data                             .
    .                                                               .
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     */
    def buildAtEncrData(simAttributes: List[EapSimAkaAttribute], iv: Array[Byte], k_encr: Array[Byte]): EapSimAkaAttribute = {

      val encoded = simAttributes.map(atr => atr.encode)
      val dataLength = encoded.foldLeft(0)((tl, b) => tl + b.readableBytes)

      val padLength = if (dataLength % 16 == 0) {
        0
      } else {
        16 - (dataLength % 16) - 2 /* Value two is taken out for header and length fields */
      }

      val plainTextData =
        copiedBuffer((if (padLength > 0) encoded :+ buildAtPadding(padLength).encode
        else encoded): _*)

      val encrData = keyGenerator.aesWithCbcEncode(iv, k_encr, plainTextData.array)

      val valBuf = buffer(encrData.length + 2)
      valBuf.writeShort(0) //Two reserved bytes are set to zero
      valBuf.writeBytes(encrData)

      new EapSimAkaAttribute(AT_ENCR_DATA, encrData.length + 2, valBuf)
    }

    def decordEncrData(iv: Array[Byte], key: Array[Byte], att: EapSimAkaAttribute): List[EapSimAkaAttribute] = {
      val data = Some(att.getAttributeData)
      data.map(b => {
        val PADDING_FIELD_FOUND = 6
        val ERROR_OCCURED = -1

        val encodedData = copiedBuffer(b)
        val decodeDataArray = keyGenerator.aesWithCbcDecode(iv, key, encodedData.copy(2, encodedData.capacity() - 2).array())
        val decodedBuffer = copiedBuffer(decodeDataArray)

        var attList: List[EapSimAkaAttribute] = Nil
        var field = -1
        do {
          try {
            field = decodedBuffer.readUnsignedByte()
            val markedLength = decodedBuffer.readUnsignedByte()
            val length = (markedLength * 4) - 2
            val attribute = new EapSimAkaAttribute(EapSimAkaAttributeTypes.getEnum(field), length, decodedBuffer.readBytes(length))
            attList = attribute :: attList
          }
          catch {
            case e: Exception => field = -1
          }
        } while (field != PADDING_FIELD_FOUND && field != ERROR_OCCURED)
        attList
      }).getOrElse(List.empty)
    }

    def extractAtIdenty(att: EapSimAkaAttribute): Array[Byte] = {
      val buffer = copiedBuffer(att.getAttributeData)
      val valueLength = buffer.readShort()
      buffer.copy(buffer.readerIndex(), valueLength.toInt).array()
    }

    def extractCounter(counterData: Option[Array[Byte]]): Option[Int] =  {
      counterData.map(array => {
        val buffer = copiedBuffer(array)
        buffer.resetReaderIndex()
        buffer.readShort()
      })
    }

    /**
     * RFC 4186 - 10.14.  AT_MAC
     * AT_MAC with value field set to zeros.
     */
    def buildAtMac: EapSimAkaAttribute = {
      buildAtMac(pad(16).array)
    }

    /**
     * RFC 4186 - 10.14.  AT_MAC
     * AT_MAC with MAC value being set
     */
    def buildAtMac(macVal: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(18)
      data.writeShort(0) //Two reserved bytes are set to zero
      data.writeBytes(macVal)
      new EapSimAkaAttribute(AT_MAC, 18, data)
    }

    /**
     * RFC 4186 - 10.9.  AT_RAND
     */
    def buildAtRand(rands: List[Array[Byte]]) = {
      val randLength = rands.foldLeft(0)((tl, r) => tl + r.length)
      val data = buffer(randLength + 2)
      data.writeShort(0) //Two reserved bytes are set to zero
      rands.foreach(r => data.writeBytes(r))
      new EapSimAkaAttribute(AT_RAND, randLength + 2, data)
    }

    def buildAtFullAuthIdReq: EapSimAkaAttribute = {
      new EapSimAkaAttribute(AT_FULLAUTH_ID_REQ, 1,
        copiedBuffer(HexCodec.hex2Byte("0100")))
    }

    def buildAtPermanentIdReq: EapSimAkaAttribute = {
      new EapSimAkaAttribute(AT_PERMANENT_ID_REQ, 1,
        copiedBuffer(HexCodec.hex2Byte("0000")))
    }

    def buildAtAnyAuthIdReq: EapSimAkaAttribute = {
      new EapSimAkaAttribute(AT_ANY_ID_REQ, 1,
        copiedBuffer(HexCodec.hex2Byte("0000")))
    }

    /**
     * RFC 4187 - 10.7.  AT_AUTN
     *
     * 0                   1                   2                   3
     * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |    AT_AUTN    | Length = 5    |           Reserved            |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                                                               |
     * |                        AUTN                                   |
     * |                                                               |
     * |                                                               |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    def buildAtAutn(autn: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(18)
      data.writeShort(0) //Two reserved bytes are set to zero
      data.writeBytes(autn)
      new EapSimAkaAttribute(AT_AUTN, 18, data)
    }

    /**
     * RFC 4187 - 10.8.  AT_RES
     *
     * 0                   1                   2                   3
     * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |     AT_RES    |    Length     |          RES Length           |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-|
     * |                                                               |
     * |                             RES                               |
     * |                                                               |
     * |                                                               |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    def buildAtRes(res: Array[Byte]): EapSimAkaAttribute =
      buildAttributeWithActualLengthField(AT_RES, res)

    /**
     * RFC 4187 - 10.13.  AT_CHECKCODE
     *
     * 0                   1                   2                   3
     * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * | AT_CHECKCODE  | Length        |           Reserved            |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                                                               |
     * |                     Checkcode (0 or 20 bytes)                 |
     * |                                                               |
     * |                                                               |
     * |                                                               |
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    def buildAtCheckCode(checkCode: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(checkCode.length + 2)
      data.writeShort(0) //Two reserved bytes are set to zero
      data.writeBytes(checkCode)
      new EapSimAkaAttribute(AT_CHECKCODE, data.readableBytes, data)
    }

    //-----------------------------

    private def buildAttributeWithActualLengthField(attrType: EapSimAkaAttributeTypes, attrValue: Array[Byte]): EapSimAkaAttribute = {
      val attrLength = attrValue.length

      val padBytes = if (attrLength % 4 == 0) {
        0
      } else {
        4 - (attrLength % 4)
      }
      val dataLength = 2 + attrLength + padBytes
      val valBuf = buffer(dataLength)
      valBuf.writeShort(attrLength)
      valBuf.writeBytes(attrValue)
      valBuf.writeBytes(pad(padBytes))

      new EapSimAkaAttribute(attrType, dataLength, valBuf)
    }

    private def pad(size: Int) = {
      val pad = buffer(size)
      for (i <- 0 until size)
        pad.writeByte(0)

      pad
    }

    /**
      * RFC 4186 - 10.3. AT_SELECTED_VERSION
      *
      * 0                   1                   2                   3
      * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      * | AT_SELECTED...| Length = 1    |    Selected Version           |
      * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      */
    def buildAtSelectedVersion(versionList: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(versionList.length)
      data.writeBytes(versionList)
      new EapSimAkaAttribute(AT_SELECTED_VERSION, data.readableBytes, data)
    }

    /**
      * RFC 4186 - 10.4. AT_NONCE_MT
      *
      * 0                   1                   2                   3
      * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      * |AT_NONCE_MT    | Length = 5    |           Reserved            |
      * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      * |                                                               |
      * |                           NONCE_MT                            |
      * |                                                               |
      * |                                                               |
      * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      */
    def buildAtNonceMt(nonceS: Array[Byte]): EapSimAkaAttribute = {
      val data = buffer(18)
      data.writeShort(0) //Reserved two bytes are set to zero
      data.writeBytes(nonceS)

      new EapSimAkaAttribute(AT_NONCE_MT, 18, data)
    }
  }
}