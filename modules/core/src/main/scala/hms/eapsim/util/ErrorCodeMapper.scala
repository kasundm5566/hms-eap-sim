/*
 *   (C) Copyright 2008-2014 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

import com.typesafe.config.Config
import com.typesafe.config._
import hms.eapsim.EapSimError
import scala.collection.JavaConversions._
import org.apache.logging.log4j.scala.Logging


trait ErrorCodeMapperComponent {

  def errorCodeMapper: ErrorCodeMapper


}

trait ErrorCodeMapper {
  def getMsg(error: EapSimError): String
}

class ConfigBasedErrorCodeMapper(config: Config) extends ErrorCodeMapper with Logging {

  val defaultErrorMsg: String = config.getString("default")
  val errorMap: Map[String, String] = config.entrySet
    .filterNot(en => en.getKey == "default")
    .map(x => {
    (x.getKey, x.getValue.unwrapped.toString)
  })
    .toMap

  logger.debug(s"Loaded error code map[${errorMap}}], default[${defaultErrorMsg}}]")

  def getMsg(error: EapSimError) = errorMap.get(error.statusCode.statusCode).getOrElse(defaultErrorMsg)

}
