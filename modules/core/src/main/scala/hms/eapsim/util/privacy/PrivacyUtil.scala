package hms.eapsim.util.privacy

import java.io._
import java.net.URI
import java.nio.file.{Files, Path, Paths}
import java.security.cert.{Certificate, CertificateFactory}
import java.security.spec.{InvalidKeySpecException, PKCS8EncodedKeySpec}
import java.security._
import javax.crypto.{BadPaddingException, Cipher, IllegalBlockSizeException, NoSuchPaddingException}

import org.apache.commons.codec.binary.Base64
import org.apache.logging.log4j.scala.Logging

object PrivacyUtil extends Logging {

  val CERTIFICATE_TYPE: String = "X.509"
  val ENCRYPTION_TYPE: String = "RSA"
  val CHAR_SET_TYPE: String = "UTF8"

  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  @throws[UnsupportedEncodingException]
  @throws[NoSuchPaddingException]
  @throws[InvalidKeyException]
  @throws[IllegalBlockSizeException]
  @throws[BadPaddingException]
  def encrypt(classPathToPathToFile: String, sensitiveData: String): String = {
    val publicKey: PublicKey = readPublicKey(classPathToPathToFile)
    val dataAsBytes: Array[Byte] = sensitiveData.getBytes(CHAR_SET_TYPE)
    val encryptedDataAsBytes: Array[Byte] = encryptByKey(publicKey, dataAsBytes)
    val encodedStr: String = encodeBase64(encryptedDataAsBytes)
    encodedStr
  }

  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  @throws[NoSuchPaddingException]
  @throws[InvalidKeyException]
  @throws[IllegalBlockSizeException]
  @throws[BadPaddingException]
  def decrypt(classPathTorivateKey: String, cipherText: Array[Byte]): String = {
    val privateKey: PrivateKey = readPrivateKey(classPathTorivateKey)
    val decodedBytes: Array[Byte] = decodeBase64(cipherText)
    val recoveredMessageAsBytes: Array[Byte] = decryptByKey(privateKey, decodedBytes)
    new String(recoveredMessageAsBytes, CHAR_SET_TYPE)
  }

  def decrypt(privateKey: PrivateKey, cipherText: Array[Byte]): String = {
    val decodedBytes: Array[Byte] = decodeBase64(cipherText)
    val recoveredMessageAsBytes: Array[Byte] = decryptByKey(privateKey, decodedBytes)
    new String(recoveredMessageAsBytes, CHAR_SET_TYPE)
  }

  @throws[UnsupportedEncodingException]
  private def encodeBase64(encryptedStr: Array[Byte]): String = {
    val encodedBytes: Array[Byte] = Base64.encodeBase64(encryptedStr)
    new String(encodedBytes, CHAR_SET_TYPE)
  }

  private def decodeBase64(encodedBytes: Array[Byte]): Array[Byte] = {
    Base64.decodeBase64(encodedBytes)
  }

  @throws[NoSuchAlgorithmException]
  @throws[NoSuchPaddingException]
  @throws[InvalidKeyException]
  @throws[IllegalBlockSizeException]
  @throws[BadPaddingException]
  private def encryptByKey(key: PublicKey, plaintext: Array[Byte]): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance(ENCRYPTION_TYPE)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    cipher.doFinal(plaintext)
  }

  @throws[NoSuchAlgorithmException]
  @throws[NoSuchPaddingException]
  @throws[InvalidKeyException]
  @throws[IllegalBlockSizeException]
  @throws[BadPaddingException]
  private def encryptByPrivateKey(key: PrivateKey, plaintext: Array[Byte]): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance(ENCRYPTION_TYPE)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    cipher.doFinal(plaintext)
  }

  @throws[NoSuchAlgorithmException]
  @throws[NoSuchPaddingException]
  @throws[InvalidKeyException]
  @throws[IllegalBlockSizeException]
  @throws[BadPaddingException]
  private def decryptByKey(key: PrivateKey, ciphertext: Array[Byte]): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance(ENCRYPTION_TYPE)
    cipher.init(Cipher.DECRYPT_MODE, key)
    cipher.doFinal(ciphertext)
  }

  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  private def readPublicKey(classPathToPathToFile: String): PublicKey = {

    val is: InputStream = new ByteArrayInputStream(readFileFromClassPath(classPathToPathToFile))
    val cf: CertificateFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE)
    val cert: Certificate = cf.generateCertificate(is)
    cert.getPublicKey
  }

  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  def readPrivateKey(classPathOfPrivateKey: String): PrivateKey = {
    val keySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(readFileFromClassPath(classPathOfPrivateKey))
    val keyFactory: KeyFactory = KeyFactory.getInstance(ENCRYPTION_TYPE)
    keyFactory.generatePrivate(keySpec)
  }

  def readPrivateKey(privateKeyData: Array[Byte]): PrivateKey = {
    val keySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKeyData)
    val keyFactory: KeyFactory = KeyFactory.getInstance(ENCRYPTION_TYPE)
    keyFactory.generatePrivate(keySpec)
  }

  @throws[IOException]
  private def readFileBytes(filename: String): Array[Byte] = {
    val path: Path = Paths.get(filename)
    val bytes: Array[Byte] = Files.readAllBytes(path)
    bytes
  }

  @throws[IOException]
  private def readFileFromClassPath(fileName: String): Array[Byte] = {
    val path: Path = Paths.get(getClass.getClassLoader.getResource(fileName).toURI)
    val bytes: Array[Byte] = Files.readAllBytes(path)
    bytes
  }
}
