package hms.eapsim.util

import hms.common.radius.RequestContext
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.AbstractEapSimAka
import hms.eapsim._
import hms.eapsim.cache.AuthenticationVectorCache
import hms.eapsim.gw.integration.{SigtranGwRequestContext, SigtranResponseListener}
import hms.eapsim.key._
import hms.eapsim.protocol.ProtocolHandler
import hms.eapsim.repo.{AuthenticationSession, TransLog}
import hms.eapsim.util.EapMsgUtil._
import org.apache.logging.log4j.scala.Logging
import org.slf4j.LoggerFactory

trait AuthenticationHelperUtilityComponent {
  def getAuthenticationHelper: AuthenticationHelperUtility
}

trait AuthenticationHelperUtility {
  def removeAuthVectorOnSuccess(imsi: String)
  def getAuthVectors(identity: String, correlationId: String, sigtranGwRequestContext: SigtranGwRequestContext)(implicit transLog: TransLog): EapSimResult[List[AuthVector]]
  def requestAuthVectorsAsync(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, protocolHandler: ProtocolHandler, requestContext: RequestContext)(implicit transLog: TransLog): Unit
}

class AuthenticationHelperUtilityImpl(authenticationVectorCache: AuthenticationVectorCache,
                                      keyManager: KeyManager,
                                      vectorCount: Int = 3, responseListner: SigtranResponseListener) extends AuthenticationHelperUtility  {

  val logger = LoggerFactory.getLogger("AVU")

  def getAuthVectors(identity: String, correlationId: String, sigtranGwRequestContext: SigtranGwRequestContext)(implicit transLog: TransLog): EapSimResult[List[AuthVector]] = {
    val imsi = extactEmsiFromIdentity(identity)
    val previousVectors = authenticationVectorCache.get(imsi)
    previousVectors match {
      case Some(x) => {
        logger.info(s"Authentication vector found in cache for [ imsi: ${imsi}]")
        responseListner.handleResponseSync(sigtranGwRequestContext.protocolHandler, x, sigtranGwRequestContext.loginSession,
        sigtranGwRequestContext.eapMsg, sigtranGwRequestContext.requestContext)
        Right(x)
      }
      case _ =>
        logger.debug(s"No authentication vectors available in the cache.Going to request them for [ imsi: ${imsi}]")
        val requestedVectorCount = if (canUseEapSim(identity)) vectorCount else 1
        val value: EapSimResult[List[AuthVector]] = getImsiFromNAI(identity)
          .right
          .map(imsi => keyManager.getAuthenticationSet(imsi, requestedVectorCount, sigtranGwRequestContext)(correlationId = correlationId))
          .joinRight
        value match {
          case Right(li) => if (li.isEmpty) {
            val ec = UnsupportedAuthenticationSet.eapSimError("Neither Triplet not Quintuplet was received")
            transLog.setStatus(ec)
            Left(ec)
          } else {
            logger.debug(s"Received Auth Vectors[${li}}]")
            authenticationVectorCache.add(extactEmsiFromIdentity(identity), li)
            Right(li)
          }
          case Left(e) => {
            transLog.setStatus(e)
            Left(e)
          }
        }
    }
  }

  override def removeAuthVectorOnSuccess(imsi: String): Unit = {
    authenticationVectorCache.remove(imsi)
  }

  override def requestAuthVectorsAsync(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka,
                                   protocolHandler: ProtocolHandler,
                                   requestContext: RequestContext)(implicit transLog: TransLog): Unit = {

    getAuthVectors(loginSession.identity, loginSession.correlationId, SigtranGwRequestContext(loginSession, eapMsg, protocolHandler, requestContext, transLog))
  }


}
