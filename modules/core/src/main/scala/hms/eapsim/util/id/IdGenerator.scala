
/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util.id

import java.util.Date
import java.text.{ DecimalFormat, SimpleDateFormat }
import java.util.concurrent.atomic.{AtomicInteger, AtomicLong}

/**
 * Generate a unique id in following format
 *
 *  [prefix|datetime|cyclic counter value]
 *
 * Note that, generated id's max length is equal to MaxTrxIdLength
 *
 *  for example
 *  prefix = 1 and
 *  dateFormatPrefix = yyMMddHHmmss and
 *  cyclic counter max value is 999
 *
 *  generated id shall be  1120416114613001
 *
 */

trait IdGenerator {

  val maxTrxIdLength = 16

  val cyclicCounterDigitLimit = 3

  def idPrefix: String

  def dateTimeFormat: String

  private def counterDigitPatternMax(ch: Char): Stream[Char] = {
    Stream.continually(ch).take(cyclicCounterDigitLimit)
  }

//  protected val counter: CyclicCounter = new CyclicCounter(counterDigitPatternMax('9').mkString.toInt)
  protected val counter = new AtomicInteger(0)

  protected val TrxIdFormatter = new ThreadLocal[SimpleDateFormat] {
    protected override def initialValue: SimpleDateFormat = {
      new SimpleDateFormat(dateTimeFormat)
    }
  }

  protected val TrxIdCounterFormatter = new ThreadLocal[DecimalFormat] {
    protected override def initialValue: DecimalFormat = {
      new DecimalFormat(counterDigitPatternMax('0').mkString)
    }
  }

  /**
   * Creates a unique long value prefixed by core instance id.
   * Returned value is unique across all core instances.
   */
  def nextId: String
}



