/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.eapsim.util.id

import java.util.concurrent.locks.ReentrantLock

final class CyclicCounter(val end: Int) {

  private val incLock = new ReentrantLock(true)
  private val counterStream = Stream.continually((1 to end).toStream).flatten.iterator

  def incrementAndGet: Int = {
    incLock.lock()
    try {
      counterStream.next()
    } finally {
      incLock.unlock()
    }
  }
}
