/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util

/**
 * Generate single byte Id for Eap-Message
 */
object EapPacketIdGenerator {
  private var idGen = 0

  def next = {
    this.synchronized {
      if (idGen >= 255)
        idGen = 1
      else
        idGen = idGen + 1

      idGen
    }
  }

  def reset = {
    idGen = 0
  }
}