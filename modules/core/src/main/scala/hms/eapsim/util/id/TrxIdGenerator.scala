/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.util.id

import org.apache.commons.lang.StringUtils

class TrxIdGenerator(nodeId: String) extends IdGenerator {

  val nodeIdLength = 1

  validate()

  private def validate() {
    if (idPrefix == null) throw new NullPointerException("TrxIdGenerator NodeId should not be a null value")

    if (idPrefix.length != nodeIdLength) throw new IllegalArgumentException("Illegal nodeId [" + idPrefix + "], the length should be equal to 1")

    if (idPrefix.charAt(0) == '0') throw new IllegalArgumentException("Illegal nodeId [" + idPrefix + "], nodeId should not start with 0")

    assert(maxTrxIdLength >= (nodeIdLength + dateTimeFormat.length() + cyclicCounterDigitLimit))
  }

  def idPrefix = nodeId

  def dateTimeFormat = "yyMMddHHmmss"

  def nextId: String = {
    val current: Int = counter.incrementAndGet

    if (current >= 9999) counter.getAndSet(0)
    new StringBuilder().append(nodeId).append(System.currentTimeMillis()).append(StringUtils.leftPad(String.valueOf(current), 4, '0')).toString()
  }
}