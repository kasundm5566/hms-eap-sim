package hms.eapsim

import hms.common.radius.RequestContext
import hms.common.radius.connector.serialise.ContextDumpUtility
import hms.common.radius.connector.serialise.ContextDumpUtility.serialize
import hms.common.radius.connector.{ContextMessageListener, MessageType}
import hms.common.radius.connector.tcp.client.ClientManager
import hms.common.radius.server.ChannelHolder
import hms.eapsim.util.EapMsgUtil
import org.apache.logging.log4j.scala.Logging
import org.tinyradius.packet.RadiusPacket

/**
 * Created by sampath on 5/3/17.
 */
trait RadiusRequestDelegateComponent {

  this:RadiusRequestReceiverComponent with RadiusResponseSenderComponent =>

  def getServerMessageListener: ServerMessageHandler

  class ServerMessageHandler(clientManager: ClientManager) extends ContextMessageListener with Logging {

    override def onRequestMessage(data: Array[Byte]): Unit = {
      val context: RequestContext = ContextDumpUtility.deserialize(data)
      toOption(context).map(context => {
        val response: Either[EapSimError, RadiusPacket] = radiusRequestReceiver.accessRequestReceived(context)

        toOption(clientManager.getEndPoint(context.getServerConnectClientId)).map(ep => {
          response match {
            case Right(rp: RadiusPacket) =>  {
              context.setResponse(rp)
              logger.debug(s"Request context before serializing in RadiusRequestDelegateComponent with Radius packet [${context}]")
              ep.getMessageSender.send(MessageType.RADIUS_RES, context.getCorrelationId.toLong, serialize(context))
            }
            case Left(e : EapSimError) => {
              if (e.statusCode == AsynchronousResponse) {
                logger.debug("[Delegated Request] Response will be received asynchronously")
              } else {
                logger.error(s"Error occurred while processing request [${e}}]")
                val original = context.getReceived
                val errorResponse: Either[EapSimError, RadiusPacket] = EapMsgUtil.createDefaultEapError(e, original, errorCodeMapper)
                val rp: RadiusPacket = errorResponse.fold(
                  e => EapMsgUtil.createDefaultRadiusError(original, Some(e.description)),
                  rp => rp
                )
                context.setResponse(rp)
                logger.debug(s"Request context before serializing in RadiusRequestDelegateComponent with an error [${context}]")
                ep.getMessageSender.send(MessageType.RADIUS_RES, context.getCorrelationId.toLong, serialize(context))
              }
            }
          }
        })
      })
    }

    override def onResponseMessage(data: Array[Byte]): Unit = {
      val context: RequestContext = ContextDumpUtility.deserialize(data)
      toOption(context).map(context => {
        messageResponder.handleResponse(context.getResponse, context)
      })
    }
  }

}
