/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms

/**
 *
 */
import hms.common.radius.util.HexCodec
import io.netty.buffer.Unpooled
import org.tinyradius.packet.RadiusPacket
package object eapsim {

  type EapSimResult[+A] = Either[EapSimError, A]

  def toOption[T](x: T): Option[T] = if (x == null) None else Some(x)

  implicit def hexStr2ByteBuf(hex: String) = Unpooled.copiedBuffer(HexCodec.hex2Byte(hex))

  case class ServerResponse(correlationId: String, radiusResp: RadiusPacket)

//  case class ServerResponse2(radiusResp: Either[(RadiusPacket, String), EapSimError])
}
