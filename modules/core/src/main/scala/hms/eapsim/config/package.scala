/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim

/**
 *
 */
import hms.eapsim.repo.SimpleRequestRepo
import hms.eapsim.repo.ReceivedRequestRepositoryComponent
import hms.eapsim.key.KeyManagerComponent
import hms.eapsim.key.KeyManager
import hms.eapsim.key.DummyKeyManager
import hms.eapsim.key.KeyGeneratorComponent


package object config {

//  trait ReceivedRequestRepositoryRegistry extends ReceivedRequestRepositoryComponent {
//    val requestRepo = new SimpleRequestRepo
//  }

//  trait KeyGeneratorComponentRegistry extends KeyGeneratorComponent {
//	  val keyGenerator: KeyGenerator = new KeyGenerator
//  }
//
//  trait KeyManagerComponentRegistry extends KeyManagerComponent {
//    //TOODO: Use real Sigtran integration here
//    val keyManager: KeyManager = new DummyKeyManager
//  }
//
//  trait CoreRadiusRequestReceiverRegistry extends RadiusRequestReceiverComponent {
//    val radiusRequestReceiver = new RadiusRequestReceiver with ReceivedRequestRepositoryRegistry with KeyGeneratorComponentRegistry with KeyManagerComponentRegistry
//  }
}