/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.protocol

import hms.common.radius.RequestContext
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.decoder.eap.elements._
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes._
import hms.eapsim.util._
import hms.eapsim.repo._
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSim
import hms.eapsim.key._

import scala.collection.JavaConversions._
import org.apache.logging.log4j.scala.Logging
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSimSubtype
import hms.common.radius.util.HexCodec
import java.util.Arrays

import io.netty.buffer.Unpooled._
import hms.eapsim._

import scalaz._
import Scalaz._
import hms.eapsim.util.EapMsgUtil._
import hms.eapsim.consumer.ConsumerDataServiceComponent
import hms.eapsim.consumer.MsisdnServiceComponent
import hms.common.radius.decoder.eap.elements.Identity
import io.netty.buffer.ByteBuf

import scala.Some
import hms.eapsim.repo.AuthenticationSession
import hms.eapsim.repo.ReAuthContext
import hms.eapsim.repo.TransLog
import java.util.concurrent.ThreadLocalRandom

import hms.eapsim.keystore.{IdentityDecoderComponent, IdentityDecoderService}
import org.slf4j.LoggerFactory

/**
 *
 */
trait EapSimProtocolHandlerComponent {
  this: ReceivedRequestRepositoryComponent with EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with ConsumerDataServiceComponent with MsisdnServiceComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with RepositoryComponent with AuthenticationHelperUtilityComponent with IdentityDecoderComponent =>

  def eapsimProtocolHandler: EapSimProtocolHandler
  def errorCodeMapper: ErrorCodeMapper

  class EapSimProtocolHandler(accessAcceptDefaults: List[AccessAcceptParameter[_]], ignoreClientMacValidation: Boolean = false,
                              includeMsMppeKeys: Boolean = true, maxReAuthCounter: Int = 3) extends ProtocolHandler {

    accessAcceptDefaultParms = accessAcceptDefaults

    override val log = LoggerFactory.getLogger("SIM")
    val SupportedVersion: String = "0001"
    val iv = HexCodec.hex2Byte("9e18b0c29a652263c06efb54dd00a895")

    def initiateAuthentication(correlationId: String, requestContext: RequestContext, identity: Identity)(implicit transLog: TransLog): Either[EapSimError, RadiusPacket] = {

      log.debug("Generating EAP-Request/SIM/start with AT_ANY_ID_REQ flag")

      val eapId = EapPacketIdGenerator.next
      requestRepo.initSession(correlationId, EapTypeCode.EAP_SIM, "", "", transLog.imsi, eapId, None, requestContext)
      log.info("Session created")

      val atAnyAuth = eapsimAttributeBuilder.buildAtAnyAuthIdReq

      val atVersionList = AtVersionList.build(SupportedVersion)
      val simMsg = new EapSim(EapSimSubtype.START, (List(atVersionList, atAnyAuth)))
      val eapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, simMsg)

      log.debug(s"Received PacketIdentifier[${requestContext.getReceived.getPacketIdentifier}]")

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, requestContext.getReceived.getPacketIdentifier)
      rp.addAttribute(eapMsg)
      rp.addAttribute(generateStateAttribute(correlationId))

      val respRadiusMsg = Right(rp)

      transLog.protocol = Some(SIM)
      transLog.responseType = simMsg.getEapSubType.toString

      respRadiusMsg

    }

    def generateNextReAuthRequest(context: ReAuthContext, authSession: AuthenticationSession, requestContext: RequestContext)(implicit trasLog: TransLog): EapSimResult[RadiusPacket] = {
      log.debug(s"Generating Re Authentication request [EAP-Request/SIM/Re-authentication] for EAP-SIM [nextReAuthId: ${context.nextReAuthId} ]")

      if (context.counter < maxReAuthCounter) {
        trasLog.protocol = Some(SIM)
        val idGen = new TempIdentityGenerator
        val eapId = EapPacketIdGenerator.next

        val nextCount = context.counter + 1
        val nOnceAsByteArray = new Array[Byte](16)
        ThreadLocalRandom.current().nextBytes(nOnceAsByteArray)
        val nOnceSAsHex = HexCodec.byte2Hex(nOnceAsByteArray)

        val xKeySeed = keyGenerator.generateXKeyAlternativeSeed(HexCodec.byte2Hex(context.nextReAuthId.getBytes), toHex(nextCount), nOnceSAsHex , context.masterKey)
        log.debug(s"XKEY' Generated for Re-Authentication [ ${HexCodec.byte2Hex(xKeySeed)} ]")

        val randomKeys = keyGenerator.generatePseudoRandomKeysForReAuth(xKeySeed)
        val reAuthId = idGen.generateNEW(context.identity, "1").fastReAuthId
        val atReAuthId = eapsimAttributeBuilder.buildAtNextReauthId(reAuthId.getBytes)
        val atCounter = eapsimAttributeBuilder.buildAtCounter(nextCount)
        val atNonceS = eapsimAttributeBuilder.buildAtNonceServer(nOnceAsByteArray)

        log.debug(s"Encrypting server values [ counter: $nextCount, nOnceS: $nOnceSAsHex]")

        val atIv = eapsimAttributeBuilder.buildAtIv(iv)
        val atEncrData = eapsimAttributeBuilder.buildAtEncrData(List(/*atPseudonym,*/ atCounter, atNonceS, atReAuthId), iv, HexCodec.hex2Byte(context.kEncr))
        val atBlankMac = eapsimAttributeBuilder.buildAtMac

        val tmpSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEncrData, atBlankMac))
        val tmpEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, tmpSimMsg)
        val encodeEapMsg = tmpEapMsg.encode

        val mac = calculateMacValue(encodeEapMsg.array, HexCodec.hex2Byte(context.kAut))
        val atMac = eapsimAttributeBuilder.buildAtMac(mac)

        val finalSimMsg = new EapSim(EapSimSubtype.RE_AUTHENTICATE, List(atIv, atEncrData, atMac))
        val finalEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, finalSimMsg)


        log.debug(s"Received PacketIdentifier[${requestContext.getReceived.getPacketIdentifier}]")
        val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, requestContext.getReceived.getPacketIdentifier)
        rp.addAttribute(finalEapMsg)
        rp.addAttribute(generateStateAttribute(authSession.correlationId))
        val respRadiusMsg = Right(rp)

        trasLog.responseType = finalSimMsg.getEapSubType.toString
        trasLog.flowType = RE_AUTHENTICATION
        requestRepo.updateSession(authSession.correlationId, Some(new PseudoRandomKeys(context.kAut, context.kEncr, randomKeys.msk, randomKeys.emsk)), Some(reAuthId), Some(xKeySeed), None, Some(authSession))
        reAuthContextRepository.update(reAuthId, nextCount, context.imsi, nOnceSAsHex)
        respRadiusMsg
      } else {
        log.info(s"Re-authentication counter exceed its max. Falling back to full authentication [ counter: ${context.counter}]. Context will be deleted")
        reAuthContextRepository.delete(context.imsi)
        triggerFullAuth(requestContext, authSession.correlationId)
      }
    }

    def triggerFullAuth(requestContext: RequestContext, correlationId: String) (implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      log.info(s"Triggering full auth procedure for EAP-SIM")

      val eapId = EapPacketIdGenerator.next
      transLog.protocol = Some(SIM)

      val versionList = AtVersionList.build(SupportedVersion)
      val atPermanentId = eapsimAttributeBuilder.buildAtPermanentIdReq

      val simMsg = new EapSim(EapSimSubtype.START, (List(versionList, atPermanentId)))
      val eapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, simMsg)

      log.debug(s"Received PacketIdentifier[${requestContext.getReceived.getPacketIdentifier}]")

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, requestContext.getReceived.getPacketIdentifier)
      rp.addAttribute(eapMsg)
      rp.addAttribute(generateStateAttribute(correlationId))
      transLog.responseType = simMsg.getEapSubType.toString

      val respRadiusMsg = Right(rp)
      transLog.responseType = simMsg.getEapSubType.toString
      respRadiusMsg
    }

    def requestAuthenticationVector(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit transLog: TransLog): Unit = {
      log.debug(s"Requesting auth vector [ Response/SIM/start ], [ attributes: ${eapMsg.getAttributes}, IMSI: ${loginSession.imsi}]")
      getAtIdentity(eapMsg, identityDecoder).right.map((identity: String) => {
        loginSession.identity = identity
        getAuthenticationHelper.requestAuthVectorsAsync(loginSession, eapMsg, this, requestContext)
      })
    }

    def handleAuthenticationVectorResponse(avList: List[AuthVector], loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit trasLog: TransLog): EapSimResult[RadiusPacket] = {
      val res = for {
        transformedVector <- transform(loginSession, avList)
        responsePacket <- generateChallengeResponse(loginSession, eapMsg, requestContext, transformedVector)
      } yield responsePacket
      res
    }

    def transform(loginSession: AuthenticationSession, authVectorList: List[AuthVector]): EapSimResult[List[AuthVector]] = {
      val transformedVector =
        authVectorList match {
          case (qp1: Quintuplet) :: rest => {
            if (EapMsgUtil.canUseEapSim(loginSession.identity)) {
              Right(authVectorList.map(_.asInstanceOf[Quintuplet]).map(VectorConversionUtil.umts2Gsm))
            } else {
              Right(authVectorList)
            }
          }
          case _ => Right(authVectorList)
        }
      transformedVector
    }

    private def processReAuthenticationResponse(eapMsg: AbstractEapSimAka, eapPacket: Option[EapMessage], loginSession: AuthenticationSession, requestContext: RequestContext)(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      log.debug(s"Received EAP-Response/SIM/Re-authentication [ imsi: ${loginSession.imsi}]")
      transLog.imsi = loginSession.imsi

      val atEncrData = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA)

      val reAuthContext = reAuthContextRepository.findByImsi(loginSession.imsi.toLong)
      reAuthContext.map(ctx => {
        val atIv = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_IV)
        log.debug(s"Re-authentication context found. ${reAuthContext} Hence User fall into re authentication acceptance flow [ imsi: ${loginSession.imsi}]")
        val decodedAttList = eapsimAttributeBuilder.decordEncrData(eapsimAttributeBuilder.extractIv(atIv), HexCodec.hex2Byte(ctx.kEncr), atEncrData)

        val counter = getCounter(decodedAttList).getOrElse(-99999)
        val eapRRM = eapPacket.get.asInstanceOf[EapRequestResponseMessage]

        val macValidationResult = validateClientMacForReAuth(eapRRM, HexCodec.hex2Byte(ctx.nonceSAsHex), HexCodec.hex2Byte(ctx.kAut))
        val counterValidationResult = counter >= ctx.counter
        val isCouterUpdateRequired = counter > ctx.counter

        log.debug(s"Counter validation result [ status: ${counterValidationResult}, clientCounter: ${counter}, contextCounter: ${ctx.counter}, counterMax: ${maxReAuthCounter}]")

        if (isCounterTooSmall(decodedAttList)) {
          reAuthContextRepository.delete(ctx.imsi)
          if (counterValidationResult && macValidationResult.isRight) {
            log.info(s"Mac Validation Success. Counter too small. Fall back to full auth")

            triggerFullAuth(requestContext, loginSession.correlationId)
          } else {
            log.info(s"Counter too small and validation failed for [ IMSI: ${ctx.imsi}]. EAP-Request/SIM/Notification will be sent")
            Left(ValidationError.eapSimError(s"Counter too small & validation failed [ IMSI: ${ctx.imsi}, couterValidationResult: ${counterValidationResult}, MAC-ValidationResult: ${macValidationResult}]"))
          }
        } else {
          log.info(s"MAC & Counter Validation Success")
          if (counterValidationResult && macValidationResult.isRight) {
            if (isCouterUpdateRequired) {
              log.info("Counter from mobile has higher value than context. Updating context")
              reAuthContextRepository.update(ctx.nextReAuthId, counter +1, ctx.imsi, ctx.nonceSAsHex)
            }

            val result = for {
              consumerData <- consumerDataService.getConsumerData(loginSession.correlationId, ctx.imsi.toString)
              accessAcept <- generateAccessAccept(requestContext.getSharedSecret, requestContext, loginSession, requestContext.getReceived.getPacketIdentifier, EapPacketIdGenerator.next, consumerData, includeMsMppeKeys)
            } yield (accessAcept)

            transLog.flowType = RE_AUTHENTICATION
            logRemoveSession(requestRepo.removeSession(loginSession.correlationId))
            result
          } else {
            log.info(s"Mac & Counter validation failed. Fall back to Full Auth")
            reAuthContextRepository.delete(ctx.imsi)
            triggerFullAuth(requestContext, loginSession.correlationId)
          }
        }
      }).getOrElse(Left(ReAuthenticationContextNotFound.eapSimError(s"ReAuthentication context not found for [ umsi: ${loginSession.imsi}")))
    }

    private def processChallengeResponse(eapMsg: AbstractEapSimAka, eapPacket: Option[EapMessage], loginSession: AuthenticationSession, requestContext: RequestContext)(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      log.debug(s"Received [EAP-Response/SIM/Challenge], available attributes [${eapMsg.getAttributes}]")
      transLog.imsi = loginSession.imsi

      val validationResult = (eapPacket |@| loginSession.generatedKeys |@| loginSession.authVector).tupled.map(v => {
        val clientResp = v._1.asInstanceOf[EapRequestResponseMessage]
        val sres = v._3.map(_.asInstanceOf[Triplet]).map(_.sres).mkString
        val key = v._2.k_aut.array

        validateClientMac(clientResp, HexCodec.hex2Byte(sres), key)
      }).toRight(ValidationError.eapSimError(s"Error validating received client MAC. Session[${loginSession}], Packet[${eapPacket}]")).joinRight

      val resp = validationResult.fold(e => {
        log.error(s"Error during Client MAC validation[${e}]")
        generateAccessRejectNEW(e, requestContext.getReceived.getPacketIdentifier, EapPacketIdGenerator.next, errorCodeMapper, loginSession)
      }, r => {
        log.info("MAC validation success")
        for {
          consumerData <- consumerDataService.getConsumerData(loginSession.correlationId, loginSession.imsi)
          accessAcept <- generateAccessAccept(requestContext.getSharedSecret, requestContext, loginSession, requestContext.getReceived.getPacketIdentifier, EapPacketIdGenerator.next, consumerData, includeMsMppeKeys)
          saveStatus <- reAuthContextRepository.save(
            new ReAuthContext(loginSession.imsi.toLong, loginSession.mk.get,
              HexCodec.byte2Hex(loginSession.generatedKeys.get.k_aut.array()), HexCodec.byte2Hex(loginSession.generatedKeys.get.k_encr.array()),
              loginSession.identity, loginSession.nextReAuthId.get, loginSession.correlationId, SIM, counter = 1))
        } yield (accessAcept)
      })
      logRemoveSession(requestRepo.removeSession(loginSession.correlationId))
      resp.left.foreach(transLog.setStatus)
      resp
    }

    def generateNextRequest(eapMsg: AbstractEapSimAka, requestContext: RequestContext, loginSession: AuthenticationSession)(implicit transLog: TransLog): Either[EapSimError, RadiusPacket] = {
      transLog.protocol = Some(SIM)
      val eapPacket = getEapMessage(requestContext)
      val eapsim = eapMsg.asInstanceOf[EapSim]
      transLog.requestType = eapsim.getEapSubType.toString

      val codeSubType = getResponseIdentifier(eapMsg, eapPacket)

      codeSubType match {
        case Some((EapCode.RESPONSE, EapSimSubtype.START, eapId)) =>
          log.debug("Start processing EAP-Response/SIM/Start")
          processResponseHasAtIdentity(eapMsg, requestContext, loginSession, eapPacket, reAuthContextRepository, requestRepo, identityDecoder)

        case Some((EapCode.RESPONSE, EapSimSubtype.CHALLENGE, eapId)) =>
          log.debug("Start processing EAP-Response/SIM/Challenge")
          processChallengeResponse(eapMsg, eapPacket, loginSession, requestContext)

        case Some((EapCode.RESPONSE, EapSimSubtype.RE_AUTHENTICATE, eapId)) =>
          log.debug("Start processing EAP-Response/SIM/Re-authentication")
          processReAuthenticationResponse(eapMsg, eapPacket, loginSession, requestContext)

        case Some((EapCode.RESPONSE, EapSimSubtype.CLIENT_ERROR, eapId)) =>
          log.debug("Start processing EAP-Response/SIM/Client-Error")
          processClientErrorResponse(eapMsg, loginSession)

        case x => {
          log.warn(s"Unsupported Radius message[${x}]. Server don't know how to respond to this message at this moment.")
          new RadiusPacket(RadiusPacket.ACCESS_REJECT, requestContext.getReceived.getPacketIdentifier)
          Left(MessageTypeNotSupported.eapSimError("Unsupported Radius message[${x}]. Server don't know how to respond to this message at this moment."))
        }
      }
    }

    private def getCounter(attList: List[EapSimAkaAttribute]): Option[Int] = {
     val atCounterAtt = attList.filter(att => att.getType.getValue == EapSimAkaAttributeTypes.AT_COUNTER.getValue)
     if (atCounterAtt.isEmpty)
       Some(-9999)
     else
       eapsimAttributeBuilder.extractCounter(Some(atCounterAtt.get(0).getAttributeData.array))
    }

    private def isCounterTooSmall(attrList: List[EapSimAkaAttribute]): Boolean = {
      attrList.exists(att => att.getType == EapSimAkaAttributeTypes.AT_COUNTER_TOO_SMALL)
    }



    private def generateChallengeResponse(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext, authVector: List[AuthVector])(implicit transLog: TransLog): Either[EapSimError, RadiusPacket] = {
      import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.EapSimAkaAttributeTypes._

      loginSession.authVector = Some(authVector)
      val atNonceMt = toOption(eapMsg.getAttribute(AT_NONCE_MT)).map(getNonceMtValue)
      val atSelectedVersion = toOption(eapMsg.getAttribute(AT_SELECTED_VERSION))
      val someTriplets = loginSession.authVector.map(l => l.map(_.asInstanceOf[Triplet]))

      someTriplets.map(triplets => {
        val kc = triplets.map(_.kc)
        val versionList = List(SupportedVersion)

        val mk = keyGenerator.generateMasterKeyEapSimForFullAuth(HexCodec.byte2Hex(loginSession.identity.getBytes), kc, HexCodec.byte2Hex(atNonceMt.get), versionList, HexCodec.byte2Hex(atSelectedVersion.get.getAttributeData))
        log.debug(s"Generated MasterKey[${HexCodec.byte2Hex(mk)}]")

        val randomKeys = keyGenerator.generatePseudoRandomKeysForFullAuth(mk)

        val idGen = new TempIdentityGenerator
        val reAuthId = idGen.generateNEW(loginSession.identity, "1").fastReAuthId

        //        val atPseudonym = eapsimAttributeBuilder.buildAtNextPseudonym(idGen.generate.pseudonym.getBytes)
        val atReauthId = eapsimAttributeBuilder.buildAtNextReauthId(reAuthId.getBytes)

        val atIv = eapsimAttributeBuilder.buildAtIv(iv)
        val atEncrData = eapsimAttributeBuilder.buildAtEncrData(List(/*atPseudonym, */atReauthId), iv, randomKeys.k_encr.array)
        val atRand = eapsimAttributeBuilder.buildAtRand(triplets.map(_.rand).map(HexCodec.hex2Byte))
        val atBlankMac = eapsimAttributeBuilder.buildAtMac
        val newEapId = EapPacketIdGenerator.next
        requestRepo.updateSession(loginSession.correlationId, Some(randomKeys), Some(reAuthId), Some(mk), toOption(authVector), Some(loginSession))

        val tmpSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List(atRand, atIv, atEncrData, atBlankMac))
        val tmpEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, newEapId, tmpSimMsg)
        val encodeEapMsg = tmpEapMsg.encode

        log.debug(s"EncodedEapMsg[${HexCodec.byte2Hex(encodeEapMsg.array)}]")

        val mac = calculateMacValue(encodeEapMsg.array, atNonceMt.get, randomKeys.k_aut.array)
        val atMac = eapsimAttributeBuilder.buildAtMac(mac)
        val fnlSimMsg = new EapSim(EapSimSubtype.CHALLENGE, List(atRand, atIv, atEncrData, atMac))
        val fnlEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, newEapId, fnlSimMsg)

        log.debug(s"Final EapMessage [$fnlEapMsg]")
        log.debug(s"Final EapMessage encoded [${HexCodec.byte2Hex(fnlEapMsg.encode.array)}]")


        log.debug(s"Received PacketIdentifier[${requestContext.getReceived.getPacketIdentifier}]")

        val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, requestContext.getReceived.getPacketIdentifier)

        rp.addAttribute(fnlEapMsg)
        rp.addAttribute(generateStateAttribute(loginSession.correlationId))

        transLog.setStatus(hms.eapsim.Success.eapSimError)
        transLog.responseType = fnlSimMsg.getEapSubType.toString
        Right(rp)
      }).getOrElse(Left(NoAuthenticationVectorFound.eapSimError("No authentication vector found")))
    }



    private def getNonceMtValue(atNonceMt: EapSimAkaAttribute): Array[Byte] = {
      val d = copiedBuffer(atNonceMt.getAttributeData)
      d.readShort //Read through 2 reserved bytes
      d.readBytes(16).array
    }

    /**
     * Calculate the MAC value for EAP-SIM
     */
    def calculateMacValue(packetData: Array[Byte], nonce: Array[Byte], key: Array[Byte]): Array[Byte] = {
      val data = copiedBuffer(packetData, nonce)
      log.debug(s"Generating MAC for EAP-SIM, EAP-Packet[${HexCodec.byte2Hex(packetData)}] nonce[${HexCodec.byte2Hex(nonce)}] key[${HexCodec.byte2Hex(key)}] totalLength[${data.readableBytes}]")

      generateMac(data, key)
    }

    def calculateMacValue(packetData: Array[Byte], key: Array[Byte]): Array[Byte] = {
      val data = copiedBuffer(packetData)
      log.debug(s"Generating MAC for EAP-SIM Request/SIM/Re-Authentication, EAP-Packet[${HexCodec.byte2Hex(packetData)}, key[${HexCodec.byte2Hex(key)}] totalLength[${data.readableBytes}]")

      generateMac(data, key)
    }

    private def generateMac(data: ByteBuf, key: Array[Byte]): Array[Byte] = {
      val msgSha1 = keyGenerator.hmacSha1Encode(key, data)

      log.debug(s"Calculated Hmac-SHA1[${HexCodec.byte2Hex(msgSha1)}]")

      val mac = Arrays.copyOf(msgSha1, 16)

      log.debug(s"Selected MAC value [${HexCodec.byte2Hex(mac)}]")
      mac
    }

    def validateClientMac(receivedResp: EapRequestResponseMessage, sres: Array[Byte], key: Array[Byte]): EapSimResult[Boolean] = {
      import EapSimAkaAttributeTypes._
      val eapSim = receivedResp.getType.asInstanceOf[EapSim]
      if (ignoreClientMacValidation) {
        log.info("Eap-Sim client MAC validation disabled...")
        Right(true)
      } else {
        toOption(eapSim.removeAttribute(AT_MAC)).map(atMac => {
          val blankMac = eapsimAttributeBuilder.buildAtMac
          eapSim.getAttributes.add(blankMac)
          val encoded = receivedResp.encode
          val calcedMac = HexCodec.byte2Hex(calculateMacValue(encoded.array, sres, key))
          val receivedMac = HexCodec.byte2Hex(atMac.getAttributeData.drop(2))
          log.debug(s"Doing MAC validation of client response. Received[${receivedMac}], Calculated[${calcedMac}]")

          if (calcedMac.equalsIgnoreCase(receivedMac)) Right(true)
          else Left(MacValidationError.eapSimError("Received MAC do not match with calculate MAC"))
        })
          .toRight(EapAttributeNotFound.eapSimError("Required AT_MAC attribute not found with request"))
          .joinRight
      }

    }

    def validateClientMacForReAuth(receivedResp: EapRequestResponseMessage, nonceS: Array[Byte], key: Array[Byte]): EapSimResult[Boolean] = {
      import EapSimAkaAttributeTypes._
      log.debug("Validating MAC for received re-Auth response")
      val eapSim = receivedResp.getType.asInstanceOf[EapSim]

      if (ignoreClientMacValidation) {
        log.info("Eap-Sim client MAC validation disabled for re-Auth...")
        Right(true)
      } else {
        toOption(eapSim.removeAttribute(AT_MAC)).map(atMac => {
          val blankMac = eapsimAttributeBuilder.buildAtMac
          eapSim.getAttributes.add(blankMac)
          val encoded = receivedResp.encode
          val calcedMac = HexCodec.byte2Hex(calculateMacValue(encoded.array, nonceS, key))
          val receivedMac = HexCodec.byte2Hex(atMac.getAttributeData.drop(2))
          log.debug(s"Doing MAC validation of client re-Auth response . Received[$receivedMac], Calculated[$calcedMac]")

          if (calcedMac.equalsIgnoreCase(receivedMac)) Right(true)
          else Left(MacValidationError.eapSimError("Received MAC do not match with calculate MAC"))
        })
          .toRight(EapAttributeNotFound.eapSimError("Required AT_MAC attribute not found with request"))
          .joinRight
      }

    }

  }

}