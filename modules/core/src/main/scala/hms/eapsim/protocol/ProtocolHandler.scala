/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.protocol

import hms.common.radius.RequestContext
import hms.eapsim.key.AuthVector
import hms.eapsim.repo._
import io.netty.buffer.Unpooled
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.decoder.eap.elements.EapMessage
import hms.common.radius.decoder.eap.elements.EapTypeCode
import hms.eapsim._

import scala.util.Left
import scalaz._
import Scalaz._
import hms.common.radius.decoder.eap.elements.Identity
import org.apache.logging.log4j.scala.Logging
import hms.common.radius.util.HexCodec
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.{AbstractEapSimAka, EapSimAkaAttributeTypes}
import hms.common.radius.decoder.eap.elements.EapSuccessFailureMessage
import hms.common.radius.decoder.eap.elements.EapCode
import org.tinyradius.attribute.RadiusAttribute
import org.tinyradius.dictionary.AttributeType
import org.tinyradius.dictionary.Dictionary
import org.tinyradius.attribute.IntegerAttribute
import org.tinyradius.attribute.StringAttribute
import hms.common.radius.util.RadiusUtil
import org.tinyradius.attribute.IpAttribute
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapSubType
import hms.eapsim.util.EapMsgUtil._
import hms.eapsim.util.ErrorCodeMapper
import io.netty.buffer.Unpooled._

import scala.util.control.Exception._
import hms.eapsim.EapSimError
import hms.eapsim.keystore.IdentityDecoderService
import hms.eapsim.repo.AuthenticationSession
import hms.eapsim.repo.ReAuthContext
import hms.eapsim.repo.TransLog
import org.slf4j.LoggerFactory

/**
 *
 */
trait ProtocolHandlerComponent {
  this: EapSimProtocolHandlerComponent with EapAkaProtocolHandlerComponent =>

  def protocolHandler(eapType: EapTypeCode): ProtocolHandler = eapType match {
    case EapTypeCode.EAP_SIM => eapsimProtocolHandler
    case EapTypeCode.EAP_AKA => eapakaProtocolHandler
    case _ => throw new IllegalArgumentException(s"Invalid EapTypeCode[${eapType}]")
  }

}

case class AccessAcceptParameter[T](name: String, value: T)

case class IpAddress(addr: String)

trait ProtocolHandler extends Logging {

  val log = LoggerFactory.getLogger("PH")
  private val FramedUser = 2

  var accessAcceptDefaultParms: List[AccessAcceptParameter[_]] = _

  def initiateAuthentication(correlationId: String, requestContext: RequestContext, identity: Identity)(implicit trasLog: TransLog): Either[EapSimError, RadiusPacket]

  def generateNextReAuthRequest(context: ReAuthContext, authSession: AuthenticationSession, requestContext: RequestContext)(implicit trasLog: TransLog): EapSimResult[RadiusPacket]

  def triggerFullAuth(requestContext: RequestContext, correlationId: String) (implicit trasLog: TransLog): EapSimResult[RadiusPacket]

  def requestAuthenticationVector(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit trasLog: TransLog): Unit
  
  def handleAuthenticationVectorResponse(avList: List[AuthVector], loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit trasLog: TransLog): Either[EapSimError, RadiusPacket]

  def generateNextRequest(eapMsg: AbstractEapSimAka, requestContext: RequestContext, loginSession: AuthenticationSession)(implicit trasLog: TransLog): Either[EapSimError, RadiusPacket]

  protected def processClientErrorResponse(eapMsg: AbstractEapSimAka, loginSession: AuthenticationSession) (implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
    transLog.imsi = loginSession.imsi
    val atClientErrorCode = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_CLIENT_ERROR_CODE)
    log.error(s"Error code received from client [ error: $atClientErrorCode]")

    Left(ClientErrorCodeReceived.eapSimError("Client error code received"))
  }

  def generateAccessAccept(sharedSecret: String, requestContext: RequestContext, authSession: AuthenticationSession, radiusReqPacketIdentifier: Int, eapId: Int, consumerData: Map[String, Object], includeMsMppeKeys: Boolean)(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
    transLog.responseType = "ACCESS_ACCEPT"
    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_ACCEPT, radiusReqPacketIdentifier);
    val eapsuccess = new EapSuccessFailureMessage(EapCode.SUCCESS, eapId, 1)

    getClassAttribute(rp.getDictionary, consumerData)
      .right.map(classAtr => {
      rp.addAttribute(classAtr)
      rp.addAttribute(eapsuccess)

      val defaultAttrs = createDefaultAttributes(accessAcceptDefaultParms, rp.getDictionary)
      log.debug(s"Created default attributes list[${defaultAttrs.mkString(",")}]")
      defaultAttrs.foreach(da => {
        rp.addAttribute(da)
      })

      if (includeMsMppeKeys) {
        getMsMppeKeys(sharedSecret, rp.getDictionary, requestContext, authSession).foreach(rp.addAttribute)
      }

      log.debug(s"Generated Access_Accept [${rp}]")
      rp
    })
  }

  def processResponseHasAtIdentity(eapMsg: AbstractEapSimAka, requestContext: RequestContext,
                                        loginSession: AuthenticationSession,
                                        eapPacket: Option[EapMessage],
                                        repository: ReAuthContextRepository,
                                        requestRepo: ReceivedRequestRepository,
                                        decoderService: IdentityDecoderService
                                  )(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
    log.debug("Received identity response with AT_IDENTITY flag")
    val result = for {
      identity <- getAtIdentity(eapMsg, decoderService)
      imsi <- loadIMSI(identity, repository)
      previousContext <- repository.findByImsiNEW(imsi.toLong)
      sessionUpdated <- updateSession(imsi, identity, loginSession, requestRepo)
      transLogUpdated <- updateTransLog(transLog, imsi, identity)
      routeResult <- processValidatedIdentityResponse(previousContext, eapMsg, requestContext, loginSession, repository)

    } yield routeResult
    result
  }

  def getResponseIdentifier(eapMsg: AbstractEapSimAka, eapPacket: Option[EapMessage]): Option[(EapCode, EapSubType, Int)] = {
    val codeSubType = for {
      code <- eapPacket.map(_.getCode)
      eapSimSubType <- Some(eapMsg.getEapSubType)
      id <- eapPacket.map(_.getIdentifier)
    } yield (code, eapSimSubType, id)
    codeSubType
  }

  def updateTransLog(transLog: TransLog, imsi: String, identity: String): EapSimResult[Boolean]= {
    transLog.imsi = imsi
    transLog.flowType = FULL_AUTHENTICATION
    Right(true)
  }

  def updateSession(imsi: String, identity: String, authSession: AuthenticationSession, requestRepo: ReceivedRequestRepository) : EapSimResult[Boolean] = {
    authSession.imsi = imsi
    authSession.identity = identity
    Right(true)
  }

  def loadIMSI(nai: String, reAuthRepo: ReAuthContextRepository): EapSimResult[String] = {
    if (isReAuthIdentity(nai)) {
      allCatch.either(doLoadIMSI(nai, reAuthRepo)).left.map(e => DataMissingInMessage.eapSimError("Error while loading IMSI from NAI"))
    } else {
      Right(nai.substring(1, nai.indexOf('@')))
    }
  }

  def doLoadIMSI(nai: String, reAuthRepo: ReAuthContextRepository): String = {
    logger.debug(s"Loading imsi for [ NAI : ${nai}] received via AT_IDENTITY")

    val reAuthContext = reAuthRepo.findByNextReAuthId(nai)

    reAuthContext match {
      case Some(ctx) => ctx.imsi.toString
      case _ => throw new RuntimeException(s"ReAuth context is not available for ${nai}")
    }
  }

  private def getIdentifire(identity : String) : Option[String] = {
    allCatch.opt(identity.split(",", 0)(1).split("=")(1))
  }

  private def getRealm(identity : String) : Option[String] = {
    allCatch.opt({
      val first = identity.split(",", 0)(0)
      first.substring(first.indexOf("@"))
    })
  }

  def getAtIdentity(eapMsg: AbstractEapSimAka, decoderService: IdentityDecoderService): EapSimResult[String] = {
    val atIdentity = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_IDENTITY)
    if (atIdentity != null) {
      val data = copiedBuffer(atIdentity.getAttributeData)
      val idLength = data.readShort

      val first = data.readByte()
      if (first == '\u0000') {
        val identityData = data.readBytes(idLength - 1).array
        val identity = new String(identityData)
        logger.debug(s"Encoded identity [$identity]")

        val decoded = decoderService.decode(getIdentifire(identity).getOrElse("default"), identityData)

        decoded.right.map(un => {
          val decodedIdentity = s"$un${getRealm(identity).getOrElse("@someHost.com")}"
          logger.debug(s"Decoded Identity [$decodedIdentity]")
          decodedIdentity
        })
      } else {
        data.readerIndex(data.readerIndex() - 1)
        val identity = new String(data.readBytes(idLength).array())

        logger.debug(s"Identity extracted from AT_IDENTITY [$identity]")
        Right(identity)
      }
    } else {
      Left(DataMissingInMessage.eapSimError("Required AT_IDENTITY flag missing in Aka-Identity"))
    }
  }

  def processValidatedIdentityResponse(previousContext: Option[ReAuthContext],
                                       eapMsg: AbstractEapSimAka, requestContext: RequestContext,
                                       authSession: AuthenticationSession,
                                       repository: ReAuthContextRepository) (implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
    previousContext match {
      case Some(ctx) => {
        handleWithContext(ctx, eapMsg,  authSession, requestContext, repository)
      }
      case _ => {
        handleWithoutContext(eapMsg, requestContext, authSession)
      }
    }
  }

  def handleWithoutContext(eapMsg: AbstractEapSimAka, requestContext: RequestContext, authSession: AuthenticationSession)(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {

    if (isReAuthIdentity(authSession.identity)) {
      log.info("Continue with Full authentication since re-authentication context has timed out")
      triggerFullAuth(requestContext, authSession.correlationId)
    } else {
      log.debug("Delegating to generate next request")
      requestAuthenticationVector(authSession, eapMsg, requestContext)
      Left(AsynchronousResponse.eapSimError)
    }
  }

  def handleWithContext(previousContext: ReAuthContext, eapMsg: AbstractEapSimAka,
                        authSession: AuthenticationSession, requestContext: RequestContext, repository: ReAuthContextRepository) (implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
    log.debug(s"Received request for routing [ ctx: ${previousContext}, session: ${authSession}]")
    val requestedProtocolType = getEapMethod(eapMsg)

    if (previousContext.protocol == requestedProtocolType){
      if (isReAuthIdentity(authSession.identity)) {
        generateNextReAuthRequest(previousContext, authSession, requestContext)
      } else {
        log.info("Reauth ctx exist for full auth request")
        repository.delete(authSession.imsi.toLong)
        requestAuthenticationVector(authSession, eapMsg, requestContext)
        Left(AsynchronousResponse.eapSimError)
      }
    } else {
      log.info(s"Requested [ protocol: $requestedProtocolType] does not match that of saved [ context: $previousContext ]. Hence deleting previous context")
      repository.delete(authSession.imsi.toLong)
      requestAuthenticationVector(authSession, eapMsg, requestContext)
      Left(AsynchronousResponse.eapSimError)
    }
  }


  def generateStateAttribute(correlationId: String): RadiusAttribute = {
    new StringAttribute(24, correlationId)
  }

  def putResponseType2TransLog(translog: TransLog, respRadiusMsg: EapSimResult[RadiusPacket]) {
    respRadiusMsg.right.foreach(resp => {
      translog.responseType = processRight(resp)
    })

    respRadiusMsg.left.foreach(e => {
      translog.setStatus(e)
      translog.responseType = "ACCESS_REJECT"
    })

    def processRight(respPacket: RadiusPacket) = respPacket.getPacketType match {
      case RadiusPacket.ACCESS_ACCEPT => "ACCESS_ACCEPT"

      case RadiusPacket.ACCESS_REJECT => "ACCESS_REJECT"

      case RadiusPacket.ACCESS_CHALLENGE => {
        val eapMsg = respPacket.getAttribute(EapMessage.ATTRIBUTE_TYPE)
        if (eapMsg != null) {
          eapMsg match {
            case e: EapRequestResponseMessage => e.getType match {
              case sim: AbstractEapSimAka => sim.getEapSubType.toString
              case x => x.getTypeCode.name
            }

            case e: EapSuccessFailureMessage => e.getCode.name

            case e => {
              log.error(s"Unknown Eap message[${e}] found inside ACCESS_CHALLENGE")
              ""
            }
          }
        } else {
          log.error("No Eap attribute found inside ACCESS_CHALLENGE")
          ""
        }
      }

      case _ => respPacket.getPacketType.toString
    }
  }

  private def createDefaultAttributes(defaultParams: List[AccessAcceptParameter[_]], dic: Dictionary) = {
    defaultParams.map(dp => {
      dp.value match {
        case v: Int => radiusAttributeInt(dic, dp.name, v)
        case v: String => radiusAttributeString(dic, dp.name, v)
        case v: IpAddress => radiusAttributeIp(dic, dp.name, v.addr)
        case v => radiusAttributeString(dic, dp.name, v.toString)
      }
    })
  }

  private def getClassAttribute(dictionaty: Dictionary, consumerData: Map[String, Object]): EapSimResult[RadiusAttribute] = {
   val result = consumerData.find(p => p._1.equalsIgnoreCase("radiusclass"))
      .map(a => radiusAttributeOctate(dictionaty, "Class", a._2.asInstanceOf[Array[Byte]]))
      .toRight(LdapAttributesNotReceived.eapSimError("Required attribute 'radiusclass' not found"))
    result
  }

  def generateAccessReject(error: EapSimError, radiusReqPacketIdentifier: Int, eapId: Int, errorCodeMapper: ErrorCodeMapper)(implicit transLog: TransLog) = {
    transLog.responseType = "ACCESS_REJECT"
    transLog.setStatus(error)
    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, radiusReqPacketIdentifier);
    val eapFailure = new EapSuccessFailureMessage(EapCode.FAILURE, eapId, 1)
    rp.addAttribute(eapFailure)
    rp.addAttribute(generateReplyMessageAttribute(rp.getDictionary, errorCodeMapper.getMsg(error)))
    log.info(s"Generated Access_Reject [${rp}]")
    Right(rp)
  }

  def generateAccessRejectNEW(error: EapSimError, radiusReqPacketIdentifier: Int, eapId: Int, errorCodeMapper: ErrorCodeMapper, loginSession: AuthenticationSession)(implicit transLog: TransLog) = {
    transLog.responseType = "ACCESS_REJECT"
    transLog.setStatus(error)
    transLog.imsi = loginSession.imsi
    val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, radiusReqPacketIdentifier);
    val eapFailure = new EapSuccessFailureMessage(EapCode.FAILURE, eapId, 1)
    rp.addAttribute(eapFailure)
    rp.addAttribute(generateReplyMessageAttribute(rp.getDictionary, errorCodeMapper.getMsg(error)))
    log.info(s"Generated Access_Reject [${rp}]")
    Right(rp)
  }

  def generateReplyMessageAttribute(dict: Dictionary, msg: String): RadiusAttribute = {
    radiusAttributeString(dict, "Reply-Message", msg)
  }

  private def radiusAttributeString(dict: Dictionary, attributeName: String, value: String): RadiusAttribute = {
    val eapAttrType: AttributeType = dict.getAttributeTypeByName(attributeName)
    val at = new StringAttribute(eapAttrType.getTypeCode, value)
    at
  }

  private def radiusAttributeIp(dict: Dictionary, attributeName: String, value: String): RadiusAttribute = {
    val eapAttrType: AttributeType = dict.getAttributeTypeByName(attributeName)
    val at = new IpAttribute(eapAttrType.getTypeCode, value)
    at
  }

  private def radiusAttributeInt(dict: Dictionary, attributeName: String, value: Int): RadiusAttribute = {
    val eapAttrType: AttributeType = dict.getAttributeTypeByName(attributeName)
    val at = new IntegerAttribute(eapAttrType.getTypeCode, value)
    at
  }

  private def radiusAttributeOctate(dict: Dictionary, attributeName: String, value: Array[Byte]): RadiusAttribute = {
    val eapAttrType: AttributeType = dict.getAttributeTypeByName(attributeName)
    val at = new RadiusAttribute(eapAttrType.getTypeCode, value)
    at
  }

  private def getMsMppeSendKey(sharedSecret: String, dict: Dictionary, requestContext: RequestContext, key: Array[Byte]): RadiusAttribute = {
    log.debug(s"Encrypting attribute MS-MPPE-Send-Key [${HexCodec.byte2Hex(key)}], sharedSecret[${sharedSecret}], authenticator[${HexCodec.byte2Hex(requestContext.getReceived.getAuthenticator)}]")

    val encodedKey = RadiusUtil.generateEncryptedMPPEPassword(key, 1024, sharedSecret.getBytes, requestContext.getReceived.getAuthenticator)

    log.debug(s"Created encrypted key [${HexCodec.byte2Hex(encodedKey)}]")

    radiusAttributeVendoSpecificOctate(dict, "MS-MPPE-Send-Key", encodedKey)
  }

  private def getMsMppeRecvKey(sharedSecret: String, dict: Dictionary, requestContext: RequestContext, key: Array[Byte]): RadiusAttribute = {
    log.debug(s"Encrypting attribute MS-MPPE-Recv-Key [${HexCodec.byte2Hex(key)}], sharedSecret[${sharedSecret}], authenticator[${HexCodec.byte2Hex(requestContext.getReceived.getAuthenticator)}]")

    val encodedKey = RadiusUtil.generateEncryptedMPPEPassword(key, 1024, sharedSecret.getBytes, requestContext.getReceived.getAuthenticator)

    log.debug(s"Created encrypted key [${HexCodec.byte2Hex(encodedKey)}]")

    radiusAttributeVendoSpecificOctate(dict, "MS-MPPE-Recv-Key", encodedKey)
  }

  private def fixMppeEncryptIssue(encrypted: Array[Byte]) = {
    val originalHex = HexCodec.byte2Hex(encrypted)
    log.debug(s"Original Hex[${originalHex}]")
    val length = originalHex.length
    val byte2Flip = originalHex.substring(length - 2, length)
    val fliped = byte2Flip.reverse
    log.debug(s"Byte to flip[${byte2Flip}, flipped[${fliped}]]")
    val after = originalHex.substring(0, length - 2) + fliped;
    log.debug(s"Corrected key[${after}]")

    HexCodec.hex2Byte(after)

  }

  private def getMsMppeKeys(sharedSecret: String, dict: Dictionary, requestContext: RequestContext, authSession: AuthenticationSession): List[RadiusAttribute] = {
    authSession.generatedKeys.map(k => {
      log.debug(s"Found generated keys[${k}]")
//      val msk = HexCodec.byte2Hex(k.msk.slice().array)
      log.debug(s"msk found[${k.msk}] to generate MS-MPPE keys")
      val mskAsByteArray = Unpooled.copiedBuffer(HexCodec.hex2Byte(k.msk))
      val recvKey = mskAsByteArray.readBytes(32).array
      val sendKey = mskAsByteArray.readBytes(32).array
      log.debug(s"MS-MPPE-Recv-Key[${HexCodec.byte2Hex(recvKey)}], MS-MPPE-Send-Key[${HexCodec.byte2Hex(sendKey)}]")

      List(
        getMsMppeRecvKey(sharedSecret, dict, requestContext, recvKey),
        getMsMppeSendKey(sharedSecret, dict, requestContext, sendKey))
    }).getOrElse(List())

  }

  private def radiusAttributeVendoSpecificOctate(dict: Dictionary, attributeName: String, value: Array[Byte]): RadiusAttribute = {
    val eapAttrType: AttributeType = dict.getAttributeTypeByName(attributeName)
    val atr = RadiusAttribute.createRadiusAttribute(dict, eapAttrType.getVendorId, eapAttrType.getTypeCode)
    atr.setAttributeData(value)
    atr
  }

  def getUsername(packet: RadiusPacket): String = {
    import hms.eapsim.toOption
    val username = toOption(packet.getAttribute("User-Name"))

    username.map(at => {
      new String(at.getAttributeData())
    }).getOrElse("")
  }

}