/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.protocol

import org.tinyradius.packet.RadiusPacket
import org.apache.logging.log4j.scala.Logging
import hms.common.radius.decoder.eap.elements._
import hms.common.radius._
import hms.eapsim.util._
import hms.eapsim.repo._
import hms.eapsim.key._
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAkaSubtype

import scala.collection.JavaConversions._
import hms.common.radius.util.HexCodec
import io.netty.buffer.Unpooled._
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.{AbstractEapSimAka, EapSimAkaAttribute, EapSimAkaAttributeTypes}
import hms.eapsim._
import hms.eapsim.util.EapMsgUtil._

import scalaz._
import Scalaz._
import hms.eapsim.consumer.MsisdnServiceComponent
import hms.eapsim.consumer.ConsumerDataServiceComponent

import scala.util.Left
import hms.eapsim.key.Quintuplet

import scala.Some
import hms.eapsim.repo.AuthenticationSession
import hms.common.radius.decoder.eap.elements.Identity
import hms.eapsim.repo.ReAuthContext
import hms.eapsim.repo.TransLog
import hms.eapsim.key.PseudoRandomKeys
import java.util.concurrent.ThreadLocalRandom

import hms.eapsim.keystore.IdentityDecoderComponent
import org.slf4j.LoggerFactory

/**
 *
 */
trait EapAkaProtocolHandlerComponent {
  this: ReceivedRequestRepositoryComponent with EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with ConsumerDataServiceComponent with MsisdnServiceComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with RepositoryComponent with AuthenticationHelperUtilityComponent with IdentityDecoderComponent =>

  def eapakaProtocolHandler: EapAkaProtocolHandler
  def errorCodeMapper: ErrorCodeMapper

  class EapAkaProtocolHandler(accessAcceptDefaults: List[AccessAcceptParameter[_]], ignoreClientMacValidation: Boolean = false, includeMsMppeKeys: Boolean = true, maxReAuthCounter: Int = 3) extends ProtocolHandler with Logging {
    import java.util.{Arrays}

    override val log = LoggerFactory.getLogger("AKA")
    
    val iv = HexCodec.hex2Byte("9e18b0c29a652263c06efb54dd00a895")
    accessAcceptDefaultParms = accessAcceptDefaults

    def initiateAuthentication(correlationId: String, requestContext: RequestContext, identity: Identity)(implicit transLog: TransLog): Either[EapSimError, RadiusPacket] = {
      log.debug("Generating EAP-Request/AKA-Identity with AT_ANY_ID_REQ flag")

      val eapId = EapPacketIdGenerator.next
      requestRepo.initSession(correlationId, EapTypeCode.EAP_AKA, "", "", transLog.imsi, eapId, None, requestContext)
      log.info(s"Session created")

      val atAnyAuth = eapsimAttributeBuilder.buildAtAnyAuthIdReq

      val requestIdentityMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, List(atAnyAuth))
      val eapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, requestIdentityMsg)

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE , requestContext.getReceived

        .getPacketIdentifier)
      rp.addAttribute(eapMsg)
      rp.addAttribute(generateStateAttribute(correlationId))

      transLog.protocol = Some(AKA)
      transLog.responseType = requestIdentityMsg.getEapSubType.toString

      log.debug(s"Response message generated initiation request [ $rp ]")
      Right(rp)
    }

    def generateNextReAuthRequest(context: ReAuthContext, authSession: AuthenticationSession, requestContext: RequestContext)(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      log.debug(s"Generating Re Authentication request [EAP-Request/SIM/Re-authentication] for EAP-AKA [ nextReAuthId: ${context.nextReAuthId}] ]")
      transLog.protocol = Some(AKA)

      if (context.counter < maxReAuthCounter) {
        val idGen = new TempIdentityGenerator
        val eapId = EapPacketIdGenerator.next

        val nextCount = context.counter + 1
        val nOnceAsByteArray = new Array[Byte](16)
        ThreadLocalRandom.current().nextBytes(nOnceAsByteArray)
        val nOnceSAsHex = HexCodec.byte2Hex(nOnceAsByteArray)

        val xKeyDash = keyGenerator.generateXKeyAlternativeSeed(HexCodec.byte2Hex(context.nextReAuthId.getBytes), toHex(nextCount), nOnceSAsHex, context.masterKey)
        log.debug(s"Generated MasterKey for Re-Authentication[${HexCodec.byte2Hex(xKeyDash)}]")

        val randomKeys = keyGenerator.generatePseudoRandomKeysForReAuth(xKeyDash)
        val reAuthId = idGen.generateNEW(context.identity, "0").fastReAuthId
        val atReAuthId = eapsimAttributeBuilder.buildAtNextReauthId(reAuthId.getBytes)
        val atCounter = eapsimAttributeBuilder.buildAtCounter(nextCount)
        val atNonceS = eapsimAttributeBuilder.buildAtNonceServer(nOnceAsByteArray)

        log.debug(s"Encrypting server values [ counter: ${nextCount}, nOnceS: $nOnceSAsHex]")

        val atIv = eapsimAttributeBuilder.buildAtIv(iv)
        val atEncrData = eapsimAttributeBuilder.buildAtEncrData(List(/*atPseudonym,*/ atCounter, atNonceS, atReAuthId), iv, HexCodec.hex2Byte(context.kEncr))
        val atBlankMac = eapsimAttributeBuilder.buildAtMac

        val tmpSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, List(atIv, atEncrData, atBlankMac))
        val tmpEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, tmpSimMsg)
        val encodeEapMsg = tmpEapMsg.encode

        val mac = calculateMacForReAuth(encodeEapMsg.array, HexCodec.hex2Byte(context.kAut))
        val atMac = eapsimAttributeBuilder.buildAtMac(mac)

        val finalSimMsg = new EapAka(EapAkaSubtype.AKA_REAUTHENTICATION, List(atIv, atEncrData, atMac))
        val finalEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, finalSimMsg)


        log.debug(s"Received PacketIdentifier[${requestContext.getReceived.getPacketIdentifier}]")
        val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, requestContext.getReceived.getPacketIdentifier)
        rp.addAttribute(finalEapMsg)
        rp.addAttribute(generateStateAttribute(authSession.correlationId))
        val respRadiusMsg = Right(rp)

        transLog.responseType = finalSimMsg.getEapSubType.toString
        transLog.flowType = RE_AUTHENTICATION

        requestRepo.updateSession(authSession.correlationId, Some(new PseudoRandomKeys(context.kAut, context.kEncr, randomKeys.msk, randomKeys.emsk)), Some(reAuthId), Some(xKeyDash), None, Some(authSession))
        reAuthContextRepository.update(reAuthId, nextCount, context.imsi, nOnceSAsHex)
        respRadiusMsg
      } else {
        log.info(s"Re-authentication counter exceed its [ max: $maxReAuthCounter ]. Falling back to full authentication [ counter: ${context.counter}]. Context will be deleted")
        reAuthContextRepository.delete(context.imsi)
        triggerFullAuth(requestContext, authSession.correlationId)
      }
    }

    def triggerFullAuth(requestContext: RequestContext, correlationId: String)(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      log.info("Trigger full auth")
      val eapId = EapPacketIdGenerator.next

      val atPermanentIdReq = eapsimAttributeBuilder.buildAtPermanentIdReq
      val fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_IDENTITY, List(atPermanentIdReq))
      val fnlEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, fnlAkaMsg)

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE , requestContext.getReceived.getPacketIdentifier)

      transLog.responseType = fnlAkaMsg.getEapSubType.toString
      rp.addAttribute(fnlEapMsg)
      rp.addAttribute(generateStateAttribute(correlationId))

      log.debug(s"Response message generated for full authentication [ $rp ]")
      Right(rp)
    }

    private def generateChallengeResponse(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext, authVector: List[AuthVector])(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      transLog.protocol = Some(AKA)
      val eapId = EapPacketIdGenerator.next
      loginSession.authVector = Some(authVector)
      val identityValue = HexCodec.byte2Hex(loginSession.identity.getBytes)

      val quintuplet = authVector.get(0).asInstanceOf[Quintuplet]

      val idGen = new TempIdentityGenerator
      val reAuthId = idGen.generateNEW(loginSession.identity, "0").fastReAuthId

      val atReauthId = eapsimAttributeBuilder.buildAtNextReauthId(reAuthId.getBytes)
      val atIv = eapsimAttributeBuilder.buildAtIv(iv)
      val atAutn = eapsimAttributeBuilder.buildAtAutn(HexCodec.hex2Byte(quintuplet.autn))
      val atRand = eapsimAttributeBuilder.buildAtRand(List(HexCodec.hex2Byte(quintuplet.rand)))

      val mk = keyGenerator.generateMasterKeyEapAka(identityValue, quintuplet.ik, quintuplet.ck)
      log.debug(s"Generated MasterKey[${HexCodec.byte2Hex(mk)}]")
      val randomKeys = keyGenerator.generatePseudoRandomKeysForFullAuth(mk)
      log.debug(s"Generated RandomKes[${randomKeys}]")

      val atEncrData = eapsimAttributeBuilder.buildAtEncrData(List(/*atPseudonym, */atReauthId), iv, randomKeys.k_encr.array)
      val atBlankMac = eapsimAttributeBuilder.buildAtMac

      val tmpAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atRand, atAutn, atBlankMac, atEncrData, atIv))
      val tmpEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, tmpAkaMsg)
      val encodeEapMsg = tmpEapMsg.encode

      log.debug(s"EncodedEapMsg[${HexCodec.byte2Hex(encodeEapMsg.array)}]")
      requestRepo.updateSession(loginSession.correlationId, Some(randomKeys), Some(reAuthId), Some(mk), toOption(authVector), Some(loginSession))

      val mac = calculateMacValue(encodeEapMsg.array, randomKeys.k_aut.array)
      val atMac = eapsimAttributeBuilder.buildAtMac(mac)
      val fnlAkaMsg = new EapAka(EapAkaSubtype.AKA_CHALLENGE, List(atRand, atAutn, atMac, atEncrData, atIv))
      val fnlEapMsg = EapRequestResponseMessage.build(EapCode.REQUEST, eapId, fnlAkaMsg)

      log.debug(s"Received PacketIdentifier[${requestContext.getReceived.getPacketIdentifier}]")
      transLog.responseType = fnlAkaMsg.getEapSubType.toString

      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_CHALLENGE, requestContext.getReceived.getPacketIdentifier)
      rp.addAttribute(fnlEapMsg)
      rp.addAttribute(generateStateAttribute(loginSession.correlationId))

      val respRadiusMsg = Right(rp)

      transLog.setStatus(hms.eapsim.Success.eapSimError)
      putResponseType2TransLog(transLog, respRadiusMsg)
      respRadiusMsg
    }
    
    def requestAuthenticationVector(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit transLog: TransLog): Unit = {
      getAuthenticationHelper.requestAuthVectorsAsync(loginSession, eapMsg, this, requestContext)
    }


    def handleAuthenticationVectorResponse(avList: List[AuthVector], loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit trasLog: TransLog): Either[EapSimError, RadiusPacket] = {
      val res = for {
        validVector <- validateVector(loginSession, avList)
        challengeMsg <- generateChallengeResponse(loginSession, eapMsg, requestContext, avList)
      } yield challengeMsg
      res
    }

    private def validateVector(loginSession: AuthenticationSession, authVector: List[AuthVector]): EapSimResult[List[AuthVector]] = {
      authVector.head match {
        case vec: Quintuplet => Right(authVector)
        case any => Left(EapNakError.eapSimError(s"Invalid plugging used [ requestType: AKA, vectorTtpe: ${any.getClass}]"))
      }
    }

    def generateNextRequest(eapMsg: AbstractEapSimAka, requestContext: RequestContext, loginSession: AuthenticationSession)(implicit transLog: TransLog): Either[EapSimError, RadiusPacket] = {

      transLog.protocol = Some(AKA)

      val eapPacket = getEapMessage(requestContext)
      val codeSubType = getResponseIdentifier(eapMsg, eapPacket)

      codeSubType match {
        case Some((EapCode.RESPONSE, EapAkaSubtype.AKA_IDENTITY, eapId)) =>
          log.debug("Start processing EAP-Response/AKA-Identity")
          processResponseHasAtIdentity(eapMsg, requestContext, loginSession, eapPacket, reAuthContextRepository, requestRepo, identityDecoder)

        case Some((EapCode.RESPONSE, EapAkaSubtype.AKA_CHALLENGE, eapId)) =>
          log.debug("Start processing EAP-Response/AKA-Challenge")
          processChallengeResponse(eapMsg, requestContext, loginSession, eapPacket)

        case Some((EapCode.RESPONSE, EapAkaSubtype.AKA_REAUTHENTICATION, eapId)) =>
          log.debug("Start processing EAP-Response/AKA-ReAuthentication")
          processReAuthenticationResponse(eapMsg, loginSession, eapPacket, requestContext)

        case Some((EapCode.RESPONSE, EapAkaSubtype.AKA_CLIENT_ERROR, eapId)) =>
          log.debug("Start processing EAP-Response/AKA-Client-Error")
          processClientErrorResponse(eapMsg, loginSession)

        case _ => Left(MessageTypeNotSupported.eapSimError("Unsupported response type received"))
      }
    }

    private def processChallengeResponse(eapMsg: AbstractEapSimAka, requestContext: RequestContext, loginSession: AuthenticationSession, eapPacket: Option[EapMessage])(implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      log.debug(s"Received EAP-Response/AKA-Challenge [eapMessage: $eapMsg] [sesion: ${loginSession}]")

      val validationResult = (eapPacket |@| loginSession.generatedKeys |@| loginSession.authVector).tupled.map(v => {
        val clientResp = v._1.asInstanceOf[EapRequestResponseMessage]
        val quintuplet = v._3.map(_.asInstanceOf[Quintuplet])
        val keys = v._2

        validateClientChallengeResp(clientResp, quintuplet(0), keys)
      }).toRight(ValidationError.eapSimError(s"Error validating received client response. Session[$loginSession], Packet[$eapPacket]")).joinRight

      val resp = validationResult.fold(e => {
        log.error(s"Error during Client RES/MAC validation[${e}]")
        generateAccessRejectNEW(e, requestContext.getReceived.getPacketIdentifier, EapPacketIdGenerator.next, errorCodeMapper, loginSession)
      }, r => {
        log.info("MAC validation success")

        def saveContext(): EapSimResult[Boolean] = {
          reAuthContextRepository.save(
            new ReAuthContext(loginSession.imsi.toLong, loginSession.mk.get,
              HexCodec.byte2Hex(loginSession.generatedKeys.get.k_aut.array()), HexCodec.byte2Hex(loginSession.generatedKeys.get.k_encr.array()),
              loginSession.identity, loginSession.nextReAuthId.get, loginSession.correlationId, AKA, counter = 1))
        }

        val result = for {
          consumerData <- consumerDataService.getConsumerData(loginSession.correlationId, loginSession.imsi)
          accessAccept <- generateAccessAccept(requestContext.getSharedSecret, requestContext, loginSession, requestContext.getReceived.getPacketIdentifier, EapPacketIdGenerator.next, consumerData, includeMsMppeKeys)
          status <- saveContext()
        } yield accessAccept
        result
      })
      transLog.imsi = loginSession.imsi
      logRemoveSession(requestRepo.removeSession(loginSession.correlationId))
      resp.left.foreach(transLog.setStatus)
      resp
    }

    private def getCounter(attList: List[EapSimAkaAttribute]): Option[Int] = {
      val atCounterAtt = attList.filter(att => att.getType.getValue == EapSimAkaAttributeTypes.AT_COUNTER.getValue)
      if (atCounterAtt.isEmpty)
        Some(-9999)
      else
        eapsimAttributeBuilder.extractCounter(Some(atCounterAtt.get(0).getAttributeData.array))
    }

    private def isCounterTooSmall(attrList: List[EapSimAkaAttribute]): Boolean = {
      attrList.exists(att => att.getType == EapSimAkaAttributeTypes.AT_COUNTER_TOO_SMALL)
    }

    private def processReAuthenticationResponse(eapMsg: AbstractEapSimAka, loginSession: AuthenticationSession, eapPacket: Option[EapMessage], requestContext: RequestContext) (implicit transLog: TransLog): EapSimResult[RadiusPacket] = {
      val atEncrData = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_ENCR_DATA)

      log.debug(s"Received EAP-Response/AKA-Reauthentication [ imsi: ${loginSession.imsi}]")
      val reAuthContext = reAuthContextRepository.findByImsi(loginSession.imsi.toLong)
      reAuthContext.map(ctx => {
        val atIv = eapMsg.getAttribute(EapSimAkaAttributeTypes.AT_IV)

        log.debug(s"Re-authentication context found. $reAuthContext Hence User fall into re authentication acceptance flow [ imsi: ${loginSession.imsi}]")
        val decodedAttList = eapsimAttributeBuilder.decordEncrData(eapsimAttributeBuilder.extractIv(atIv), HexCodec.hex2Byte(ctx.kEncr), atEncrData)
        val counter = getCounter(decodedAttList).getOrElse(-99999)
        val eapRRM = eapPacket.get.asInstanceOf[EapRequestResponseMessage]

        val macValidationResult = validateClientMacForReAuth(eapRRM, HexCodec.hex2Byte(ctx.nonceSAsHex), HexCodec.hex2Byte(ctx.kAut))
        val counterValidationResult = counter >= ctx.counter
        val isCouterUpdateRequired = counter > ctx.counter

        log.debug(s"Counter validation result [ status: ${counterValidationResult}, clientCounter: ${counter}, contextCounter: ${ctx.counter}, counterMax: ${maxReAuthCounter}]")

        if (isCounterTooSmall(decodedAttList)) {
          reAuthContextRepository.delete(ctx.imsi)
          if (counterValidationResult && macValidationResult.isRight) {
            log.info("Mac validation success. Counter too small. Falling back to full auth")

            triggerFullAuth(requestContext, loginSession.correlationId)
          } else {
            log.info(s"Counter too small. Mac validation failed.")
            Left(ValidationError.eapSimError(s"Counter too small & validation failed [ IMSI: ${ctx.imsi}, counterValidationResult: $counterValidationResult, MAC-ValidationResult: $macValidationResult]"))
          }
        } else {
          if (counterValidationResult && macValidationResult.isRight) {
            if (isCouterUpdateRequired) {
              log.info("Counter from mobile has higher value than context. Updating context")
              reAuthContextRepository.update(ctx.nextReAuthId, counter +1, ctx.imsi, ctx.nonceSAsHex)
            }
            log.info(s"MAC and Counter validation success")
            val result = for {
              consumerData <- consumerDataService.getConsumerData(loginSession.correlationId, ctx.imsi.toString)
              accessAcept <- generateAccessAccept(requestContext.getSharedSecret, requestContext, loginSession, requestContext.getReceived.getPacketIdentifier, EapPacketIdGenerator.next, consumerData, includeMsMppeKeys)
            } yield accessAcept
            logRemoveSession(requestRepo.removeSession(loginSession.correlationId))

            transLog.imsi = loginSession.imsi
            transLog.flowType = RE_AUTHENTICATION

            result
          } else {
            log.info(s"Mac Counter validation failed. Falling back to full auth")
            reAuthContextRepository.delete(ctx.imsi)
            triggerFullAuth(requestContext, loginSession.correlationId)
          }

 }
      }).getOrElse(Left(ReAuthenticationContextNotFound.eapSimError(s"ReAuthentication context not found for [ umsi: ${loginSession.imsi}")))
    }


    /**
     * Calculate the MAC value for EAP-AKA
     */
    def calculateMacValue(packetData: Array[Byte], nonceMt: Array[Byte], key: Array[Byte]): Array[Byte] = {
      val data = copiedBuffer(packetData, nonceMt)
      log.debug(s"Generating MAC for EAP-AKA, EAP-Packet[${HexCodec.byte2Hex(packetData)}] nonceMt[${HexCodec.byte2Hex(nonceMt)}] key[${HexCodec.byte2Hex(key)}] totalLenth[${data.readableBytes}]")
      val msgSha1 = keyGenerator.hmacSha1Encode(key, data)

      log.debug(s"Calculated Hmac-SHA1[${HexCodec.byte2Hex(msgSha1)}]")

      val mac = Arrays.copyOf(msgSha1, 16)

      log.debug(s"Selected MAC value [${HexCodec.byte2Hex(mac)}]")
      mac
    }

    def validateClientMacForReAuth(receivedResp: EapRequestResponseMessage, nonceS: Array[Byte], key: Array[Byte]): EapSimResult[Boolean] = {
      import EapSimAkaAttributeTypes._
      log.debug("Validating MAC for received re-Auth response")
      val eapAka = receivedResp.getType.asInstanceOf[EapAka]

      if (ignoreClientMacValidation) {
        log.debug("Eap-Sim client MAC validation disabled for re-Auth...")
        Right(true)
      } else {
        toOption(eapAka.removeAttribute(AT_MAC)).map(atMac => {
          val blankMac = eapsimAttributeBuilder.buildAtMac
          eapAka.getAttributes.add(blankMac)
          val encoded = receivedResp.encode
          val calcedMac = HexCodec.byte2Hex(calculateMacValue(encoded.array, nonceS, key))
          val receivedMac = HexCodec.byte2Hex(atMac.getAttributeData.drop(2))
          log.debug(s"Doing MAC validation of client re-Auth response . Received[$receivedMac], Calculated[$calcedMac]")

          if (calcedMac.equalsIgnoreCase(receivedMac)) Right(true)
          else Left(MacValidationError.eapSimError("Received MAC do not match with calculate MAC"))
        })
          .toRight(EapAttributeNotFound.eapSimError("Required AT_MAC attribute not found with request"))
          .joinRight
      }
    }

    /**
     * Calculate the MAC value for EAP-SIM
     */
    def calculateMacForReAuth(packetData: Array[Byte], key: Array[Byte]): Array[Byte] = {
      val data = copiedBuffer(packetData)
      log.debug(s"Generating MAC for EAP-AKA, EAP-Packet[${HexCodec.byte2Hex(packetData)}, key[${HexCodec.byte2Hex(key)}] totalLength[${data.readableBytes}]")
      val msgSha1 = keyGenerator.hmacSha1Encode(key, data)

      log.debug(s"Calculated Hmac-SHA1[${HexCodec.byte2Hex(msgSha1)}]")

      val mac = Arrays.copyOf(msgSha1, 16)

      log.debug(s"Selected MAC value [${HexCodec.byte2Hex(mac)}]")
      mac
    }


    private def calculateMacValue(packetData: Array[Byte], key: Array[Byte]): Array[Byte] = {
      log.debug(s"Generating MAC for EAP-AKA, EAP-Packet[${HexCodec.byte2Hex(packetData)}] key[${HexCodec.byte2Hex(key)}] totalLenth[${packetData.length}]")

      val msgSha1 = keyGenerator.hmacSha1Encode(key, copiedBuffer(packetData))

      log.debug(s"Calculate SHA1 of EAP message [${HexCodec.byte2Hex(msgSha1)}]")

      val mac = Arrays.copyOf(msgSha1, 16)

      log.debug(s"Selected MAC value [${HexCodec.byte2Hex(mac)}]")
      mac
    }

    def validateClientChallengeResp(receivedResp: EapRequestResponseMessage, quintuplet: Quintuplet, randomKeys: PseudoRandomKeys)(implicit transLog: TransLog): EapSimResult[Boolean] = {
      import EapSimAkaAttributeTypes._
      val eapAka = receivedResp.getType.asInstanceOf[EapAka]
      transLog.requestType = eapAka.getEapSubType.toString

      def validateRes: EapSimResult[Boolean] = {
        toOption(eapAka.getAttribute(AT_RES)).map(atRes => {
          val res = HexCodec.byte2Hex(atRes.getAttributeData.drop(2))
          val xres = quintuplet.xres
          log.debug(s"Doing AT_RES validation. Received AT_RES[${res}], xRES[${xres}]")
          if (xres.equalsIgnoreCase(res)) Right(true)
          else Left(ResValidationError.eapSimError("Received RES do not match with XRES"))
        })
          .toRight(EapAttributeNotFound.eapSimError("Required attribute AT_RES not found with client response"))
          .joinRight
      }

      def validateMac: EapSimResult[Boolean] = {
        if (ignoreClientMacValidation) {
          log.info("Eap-AKA client MAC validation disabled...")
          Right(true)
        } else {
          toOption(eapAka.removeAttribute(AT_MAC)).map(atMac => {
            val blankMac = eapsimAttributeBuilder.buildAtMac
            eapAka.getAttributes.add(blankMac)
            val encoded = receivedResp.encode
            val calcedMac = HexCodec.byte2Hex(calculateMacValue(encoded.array, randomKeys.k_aut.array))
            val receivedMac = HexCodec.byte2Hex(atMac.getAttributeData.drop(2))
            log.debug(s"Doing MAC validation of client response. Received[${receivedMac}], Calculated[${calcedMac}]")

            if (calcedMac.equalsIgnoreCase(receivedMac)) Right(true)
            else Left(MacValidationError.eapSimError("Received MAC do not match with calculate MAC"))
          })
            .toRight(EapAttributeNotFound.eapSimError("Required AT_MAC attribute not found with request"))
            .joinRight
        }
      }

      for {
        res <- validateRes
        mac <- validateMac
      } yield (res & mac)

    }

  }

}