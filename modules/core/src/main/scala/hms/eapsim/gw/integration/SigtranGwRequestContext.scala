/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.gw.integration

import hms.common.radius.RequestContext
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.AbstractEapSimAka
import hms.eapsim.protocol.ProtocolHandler
import hms.eapsim.repo.{AuthenticationSession, TransLog}

/**
 *
 */
case class SigtranGwRequestContext(loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka,
                                   protocolHandler: ProtocolHandler, requestContext: RequestContext, transLog: TransLog)