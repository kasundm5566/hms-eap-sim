/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.gw.integration

import java.util.concurrent.{ExecutorService, Executors}

import hms.common.radius.connector.MessageType
import hms.common.radius.connector.serialise.ContextDumpUtility
import hms.common.radius.connector.tcp.client.ClientManager
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes.AbstractEapSimAka
import hms.common.radius.util.DefaultThreadFactory
import hms.common.radius.{RequestContext, RadiusOutboundHandler}
import hms.eapsim
import hms.eapsim.async.http.client.exception.{ErrorCode, SigtranConnectorException}
import hms.eapsim.cache.AuthenticationVectorCache
import hms.eapsim.protocol.ProtocolHandler
import hms.eapsim.repo._
import hms.eapsim._
import hms.eapsim.async.http.client.listner.ResponseListener
import hms.eapsim.key.{AuthVector, Quintuplet, Triplet}
import hms.eapsim.mapgw.SaiResp
import hms.eapsim.mapgw.client.CustomJsonProtocol._
import hms.eapsim.util.{EapMsgUtil, ErrorCodeMapper}
import org.apache.logging.log4j.scala.Logging
import org.tinyradius.packet.RadiusPacket
import spray.json._

/**
 *
 */
class SigtranResponseListener(saiPoolSize: Int, messageResponder: RadiusOutboundHandler, errorCodeMaper: ErrorCodeMapper,
                               transLogRepository: TranslogRepository, simpleRequestRepo: => SimpleRequestRepo,
                               authVectorCache: AuthenticationVectorCache, clientManager: ClientManager)
  extends ResponseListener with Logging {

  val responseWorker: ExecutorService = Executors.newFixedThreadPool(saiPoolSize, new DefaultThreadFactory("SGW-R"))

  override def onMessage(content: String, correlationId: String, context: Object): Unit = {
    val resp: SaiResp = content.parseJson.convertTo[SaiResp]
    val ctx: SigtranGwRequestContext = context.asInstanceOf[SigtranGwRequestContext]

    if (ctx != null) {
      responseWorker.submit(new Runnable {
        override def run(): Unit = handleResponseAsync(ctx, resp)
      })
    }
  }

  /**
   * This wll invoke when authetication vectors found in the vector cache.
   * Note : As name implied method call on the same thread
   * @param protocolHandler
   * @param authVectorList
   * @param loginSession
   * @param eapMsg
   * @param requestContext
   * @param transLog
   * @return
   */
  def handleResponseSync(protocolHandler: ProtocolHandler, authVectorList: List[AuthVector],
                                 loginSession: AuthenticationSession, eapMsg: AbstractEapSimAka, requestContext: RequestContext)(implicit transLog: TransLog): Unit = {
    val response: Either[EapSimError, RadiusPacket] = protocolHandler.handleAuthenticationVectorResponse(authVectorList, loginSession, eapMsg, requestContext)(transLog)

    response.right.map(rp => {
      messageResponder.handleResponse(rp, requestContext)
    })

  }

  private def handleResponseAsync(ctx: SigtranGwRequestContext, resp: SaiResp) = {
    val authVector: List[AuthVector] = getAuthVectors(resp)

    if (!authVector.isEmpty) {
      val response: Either[EapSimError, RadiusPacket] = ctx.protocolHandler.handleAuthenticationVectorResponse(authVector, ctx.loginSession, ctx.eapMsg, ctx.requestContext)(ctx.transLog)

      response.right.map(rp => {
        authVectorCache.add(ctx.loginSession.imsi, authVector)
        handleResponseForServer(rp, ctx.requestContext)
      })
      response.left.map(err => logger.error(s"Error while preparing AV response [$err]"))
    } else {
      logger.error("No authentication vector has received")
      val error: Either[EapSimError, RadiusPacket] = EapMsgUtil.createDefaultEapError(AuthVectorFetchError.eapSimError, ctx.requestContext.getReceived, errorCodeMaper)
      error.left.map(e => {
        ctx.transLog.status = e.statusCode.statusCode
        ctx.transLog.description = e.description
      })
      error.right.map(rp => {
        handleResponseForServer(rp, ctx.requestContext)
      })
    }

    def handleResponseForServer(radiusPacket: RadiusPacket, context: RequestContext): Unit = {
      if (ctx.requestContext.isRemote) {
        //send reply from remote server. So, we need to pass context data to that server
        ctx.requestContext.setResponse(radiusPacket)
        val endPoint = clientManager.getEndPoint(ctx.requestContext.getServerConnectClientId)
        toOption(endPoint).foreach(ep => {
          logger.debug(s"Request context before serializing in SigtranResponseListener [${ctx.requestContext}]")
          ep.getMessageSender.send(MessageType.RADIUS_RES, context.getCorrelationId.toLong, ContextDumpUtility.serialize(ctx.requestContext))
        })
      } else {
        //Reply from this server
        messageResponder.handleResponse(radiusPacket, ctx.requestContext)
      }
      transLogRepository.log(ctx.transLog)
    }
  }


  def getAuthVectors(saiResp: SaiResp):List[AuthVector] = {
    val triplets: List[AuthVector] = saiResp.gsmTriplets match {
      case Some(t) if(t != null && !t.isEmpty) => {
        t.map(ti => {
          val rand = ti.rand
          val sres = ti.sres
          val kc = ti.kc
          Triplet(rand, sres, kc)
        })
      }
      case _ => List.empty
    }

    val quintuplets: List[AuthVector] = saiResp.quinTuplets match {
      case Some(q) if(q!= null && !q.isEmpty) => {
        q.map(qi => {
          val rand = qi.rand
          val xres = qi.xres
          val ck = qi.ck
          val ik = qi.ik
          val autn = qi.autn
          Quintuplet(rand, xres, ck, ik, autn)
        })
      }

      case _ => List.empty
    }

    if(!triplets.isEmpty) triplets
    else if(!quintuplets.isEmpty) quintuplets
    else {
      logger.error("Neither Triplet Nor Quintuplet received with the response")
      List.empty
    }
  }

  override def onFailure(correlationId: String, sigtranGwRequestContext: Any, cause: Throwable): Unit = {

    def resolveStatusCode: StatusCode = {
      if (cause.isInstanceOf[SigtranConnectorException]) {
        cause.asInstanceOf[SigtranConnectorException].getErrorCode match {
          case ErrorCode.SIGTRAN_GW_ERROR_RESPONSE_RECEIVED => SigtranGwErrorResp
          case ErrorCode.SIGTRAN_GW_MAX_RETRIES_EXCEED => SigtranGwMaxRetryExceeds
          case ErrorCode.SIGTRAN_GW_ALL_CLIENTS_DOWN => AllSigtranConnectorsDown
          case _ => AllSigtranConnectorsDown
        }
      } else {
        AllSigtranConnectorsDown
      }
    }

    val ctx: SigtranGwRequestContext = sigtranGwRequestContext.asInstanceOf[SigtranGwRequestContext]
    val removed: Option[AuthenticationSession] = simpleRequestRepo.removeSession(correlationId)
    if(removed.isDefined) {
      val session: AuthenticationSession = removed.get
      val status: StatusCode = resolveStatusCode
      val log = TransLog(session.correlationId, "", session.imsi, status.statusCode, status.description,
        calledStationMac = ctx.requestContext.getCalledStationMac, ip = ctx.requestContext.getIp, receivedTime = ctx.requestContext.getReceivedTime, nasId = ctx.requestContext.getNasIdentifier)
      log.protocol = session.eapType.name match {
        case "EAP_SIM" => Some(SIM)
        case "EAP_AKA" => Some(AKA)
        case _ => None
      }
      val error: Either[EapSimError, RadiusPacket] = EapMsgUtil.createDefaultEapError(status.eapSimError,
        ctx.requestContext.getReceived, errorCodeMaper)
      error.right.map(rp => {
        messageResponder.handleResponse(rp, ctx.requestContext)
        transLogRepository.log(log)
      })
    } else {
      logger.info("No session found for the correlationId[" + correlationId + "]")
    }
  }

  override def onException(cause: Throwable): Unit = {
    logger.error(s"Error occurred during sending request", cause)
  }
}