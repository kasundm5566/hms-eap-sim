/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim

import hms.common.radius.RequestContext
import org.tinyradius.packet.RadiusPacket
import hms.common.radius.decoder.eap.elements.EapRequestResponseMessage
import hms.common.radius.decoder.eap.elements.EapSuccessFailureMessage
import org.apache.logging.log4j.scala.Logging
import hms.common.radius.decoder.eap.elements.Identity
import hms.common.radius.decoder.eap.elements.EapMessage
import hms.common.radius.decoder.eap.elements.EapCode
import hms.eapsim.repo._
import hms.eapsim.key._

import scalaz._
import Scalaz._
import hms.common.radius.decoder.eap.elements.eapsimaka.attributes._
import hms.eapsim.protocol.ProtocolHandlerComponent
import hms.common.radius.decoder.eap.elements.EapTypeCode
import hms.eapsim.util.EapMsgUtil._
import hms.eapsim.util._
import hms.eapsim.util.id.TrxIdGenerator
import org.apache.log4j.{Logger, NDC}
import hms.common.radius.decoder.eap.elements.EapNak
import org.tinyradius.dictionary.{AttributeType, Dictionary}
import org.tinyradius.attribute.{RadiusAttribute, StringAttribute}
import hms.eapsim.cache.CacheComponent

import scala.util.control.Exception._
import scala.Some
import hms.eapsim.repo.AuthenticationSession
import hms.eapsim.repo.TransLog
import org.slf4j.LoggerFactory

trait RadiusRequestReceiverComponent {
  this: ReceivedRequestRepositoryComponent with EapsimAttributeBuilderComponent with KeyGeneratorComponent with KeyManagerComponent with ProtocolHandlerComponent with TranslogRepositoryComponent with ErrorCodeMapperComponent with RepositoryComponent with CacheComponent with AuthenticationHelperUtilityComponent with RadiusResponseSenderComponent =>

  def radiusRequestReceiver: RadiusRequestReceiver

  def errorCodeMapper: ErrorCodeMapper

  /**
   * Receives Radius requests for processing.
   * This is the main starting point of EAP-SIM application.
   */
  class RadiusRequestReceiver(nodeId: Int = 1) {
    val logger = LoggerFactory.getLogger("RadReqRec")

    val trxIdGenerator = new TrxIdGenerator(nodeId.toString)

    /**
     * Process received Radius Access Request
     */
    def accessRequestReceived(requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {

      val eapMsg = getEapMessage(requestContext)

      val resp = eapMsg.map(m => processEapMessage(m, requestContext))

      resp.getOrElse(createDefaultRadiusError(requestContext.getReceived, Some("No EAP-Message attribute found.")))
    }

    private def processEapMessage(eapMsg: EapMessage, requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      val resp = try {
        eapMsg match {
          case rrm: EapRequestResponseMessage => handleRequestResponse(rrm, requestContext)
          case sfm: EapSuccessFailureMessage => handleFailureResponse(requestContext)
          case _ => {
            logger.error(s"Unsupported EAP message received[${requestContext.getReceived}]")
            Left(MessageTypeNotSupported.eapSimError(s"EAP Message Type[${requestContext.getReceived.getPacketType}] not supported"))
          }
        }
      } catch {
        case ex: Throwable => {
          logger.error(s"Error occurred while processing[${requestContext.getReceived}]", ex)
          Left(UnknownError.eapSimError(s"Unknown error occurred[${ex.getMessage}]"))
        }
      }
      resp
//      resp.fold(er => {
//        logger.error(s"Error occurred while processing request [${er}]")
//        createDefaultEapError(er, requestContext.getReceived)
//      }, rp => Right(rp))
    }

    private def handleRequestResponse(rrm: EapRequestResponseMessage, requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      logger.debug(s"Eap RequestResponseMessage received [${requestContext.getReceived}]")

      val resp = rrm.getType match {
        case id: Identity => requestAuthenticationId(id, requestContext)
        case simAka: AbstractEapSimAka => continueAuthentication(simAka, requestContext)
        case nak: EapNak => processNak(nak, requestContext)
        case ex => Left(MessageTypeNotSupported.eapSimError(s"EAP-Message type not supported [${ex}]"))
      }
      resp
    }

    private def handleFailureResponse(requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      logger.error(s"Received failure response[${requestContext.getReceived}]")
      val answer = new RadiusPacket(RadiusPacket.ACCESS_REJECT, requestContext.getReceived.getPacketIdentifier)
      Right(answer)
    }

    private def requestAuthenticationId(identity: Identity, requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      implicit val correlationId: String = requestContext.getCorrelationId

      for {
        userName <- validateIdentity(identity.getValue)
        response <- doRequestAuthenticationId(correlationId, userName, identity, requestContext)
      } yield response
    }

    private def doRequestAuthenticationId(correlationId: String, username: String, identity: Identity, requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      implicit val transLog: TransLog = TransLog(correlationId, "Identity", username , flowType = getFlowType(identity.getValue),
      calledStationMac = requestContext.getCalledStationMac, ip = requestContext.getIp, receivedTime = requestContext.getReceivedTime, nasId = requestContext.getNasIdentifier)
      NDC.push(correlationId)
      try {
        logger.debug(s"Identity Request [ $identity ]")
        val eapMethod = getEapMethod(identity.getValue)

        val respPacket = eapMethod match {
          case AKA => protocolHandler(EapTypeCode.EAP_AKA).initiateAuthentication(correlationId, requestContext, identity)

          case SIM => protocolHandler(EapTypeCode.EAP_SIM).initiateAuthentication(correlationId, requestContext, identity)

          case _ => Left(UnknownError.eapSimError("Unsupported eap method"))
        }
        respPacket.fold(
          e => {e.copy(trxId = correlationId)
            Left(e)
          },
          rp => Right(rp)
        )
      } finally {
        printTrasLog
        NDC.pop
      }
    }

    private def validateIdentity(nai: String): EapSimResult[String] = {
      allCatch.either(extractUsernameFromNAI(nai)).left.map(e => IdentityWithoutIMSI.eapSimError("No identity available"))
    }

    private def continueAuthentication(eapMsg: AbstractEapSimAka, requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      val eapPacket = getEapMessage(requestContext)
      val correlationInReq: String = toOption(requestContext.getCorrelationId).getOrElse(trxIdGenerator.nextId)

      implicit val transLogEntry: TransLog = TransLog(correlationInReq, "", "", calledStationMac = requestContext.getCalledStationMac, ip = requestContext.getIp,
        receivedTime = requestContext.getReceivedTime, nasId = requestContext.getNasIdentifier)
      eapPacket.foreach(transLogEntry.setProtocolData)

      def getPreviousReq: EapSimResult[AuthenticationSession] = {
        val previousReq = requestRepo.findSession(correlationInReq)

        logger.debug(s"Found AuthenticationSession - [${previousReq}]")

        val session = previousReq.toRight(LoginSessiongNotFound.eapSimError("Login session not found", correlationInReq))
        session.left.foreach(e => {
          transLogEntry.imsi = ""
          transLogEntry.setStatus(e)
          transLogEntry.responseType = "ACCESS_REJECT"
        })
        session
      }

      def processRequest(authSession: AuthenticationSession): Either[EapSimError, RadiusPacket] = {
        val respPacket = protocolHandler(authSession.eapType).generateNextRequest(eapMsg, requestContext, authSession)
        respPacket
          .right.map(rp => rp)
          .left.map(
            e => {
              if (e.statusCode != AsynchronousResponse) {
                transLogEntry.setStatus(e)
                transLogEntry.responseType = "ACCESS_REJECT"
                logRemoveSession(requestRepo.removeSession(authSession.correlationId))
              }
              e.copy(trxId = authSession.correlationId)
              e
            })
      }



      def handleContinueAuthentication(): Either[EapSimError, RadiusPacket] = {
        NDC.push(correlationInReq)
        try {
          getPreviousReq >>= processRequest
        } catch {
          case ex: Exception => {logger.error("Error while generating message", ex)
            Left(UnknownError.eapSimError("Error occurred while handling identity response"))
          }
        } finally {
          printTrasLog
          NDC.pop
        }
      }
      val response = handleContinueAuthentication()
      response
    }

    private def createDefaultEapError(error: EapSimError, request: RadiusPacket) : Either[EapSimError, RadiusPacket] = {
      val erm: EapSuccessFailureMessage = new EapSuccessFailureMessage(EapCode.FAILURE, EapPacketIdGenerator.next, 1)
      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, request.getPacketIdentifier);
      rp.addAttribute(erm);
      rp.addAttribute(generateReplyMessageAttribute(rp.getDictionary, errorCodeMapper.getMsg(error)))
      Right(rp)
    }

    def generateReplyMessageAttribute(dict: Dictionary, msg: String): RadiusAttribute = {
      val eapAttrType: AttributeType = dict.getAttributeTypeByName("Reply-Message")
      val at = new StringAttribute(eapAttrType.getTypeCode, msg)
      at
    }

    def createDefaultRadiusError(request: RadiusPacket, errorMsg: Option[String]): Either[EapSimError, RadiusPacket] = {
      val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, request.getPacketIdentifier);
      rp.addAttribute("Reply-Message", errorMsg.getOrElse("Unknown server error occurred."))
      Right(rp)
    }

    private def processNak(eapNak: EapNak, requestContext: RequestContext): Either[EapSimError, RadiusPacket] = {
      logger.info(s"Eap NAK message received[${eapNak}]")

      val eapPacket = getEapMessage(requestContext)

      def getPreviousReq: EapSimResult[AuthenticationSession] = {
        val previousReq = for {
          correl <- getStateAttributeValue(requestContext)
          previousReq <- requestRepo.removeSession(correl)
        } yield (previousReq)

        logger.debug(s"Found AuthenticationSession[${previousReq}]")

        previousReq.toRight(LoginSessiongNotFound.eapSimError("Login session not found"))
      }

      def processRequest(authSession: AuthenticationSession): Either[EapSimError, RadiusPacket] = {
        NDC.push(authSession.correlationId)
        implicit val transLogEntry: TransLog = TransLog(authSession.correlationId, "NAK", authSession.imsi, EapNakError.statusCode, EapNakError.description, calledStationMac = requestContext.getCalledStationMac, ip = requestContext.getIp,
        receivedTime = authSession.requestReceivedTime, nasId = authSession.nasId)
        transLogEntry.responseType = "ACCESS_REJECT"
        transLogEntry.protocol = authSession.eapType.name match {
          case "EAP_SIM" => Some(SIM)
          case "EAP_AKA" => Some(AKA)
          case _ => none
        }
        try {
          val rp: RadiusPacket = new RadiusPacket(RadiusPacket.ACCESS_REJECT, requestContext.getReceived.getPacketIdentifier);
          rp.addAttribute(EapMsgUtil.generateReplyMessageAttribute(rp.getDictionary, errorCodeMapper.getMsg(EapNakError.eapSimError)))
          Right(rp)
        } finally {
          printTrasLog
          NDC.pop
        }
      }

      getPreviousReq >>= processRequest
    }

  }

}

