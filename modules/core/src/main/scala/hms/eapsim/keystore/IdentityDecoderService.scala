package hms.eapsim.keystore

import java.security.PrivateKey
import java.util.concurrent.ConcurrentHashMap

import hms.eapsim._
import hms.eapsim.repo.{KeyInfo, KeystoreRepository}
import hms.eapsim.util.privacy.PrivacyUtil
import org.slf4j.LoggerFactory

import scalaz._
import Scalaz._
import scala.util.control.Exception._

trait IdentityDecoderComponent {
  def identityDecoder: IdentityDecoderService
}

trait IdentityDecoderService {
  /**
    * Responsible for extracting permanent identity from encrypted identity
    *
    * @param identifier    identifier for a private key which is sent from mobile
    * @param encryptedData encrypted identity
    * @return extracted IMSI
    */
  def decode(identifier: String, encryptedData: Array[Byte]): EapSimResult[String]
}

class IdentityDecoderServiceImpl(keystoreRepository: KeystoreRepository) extends IdentityDecoderService {
  val cache: ConcurrentHashMap[String, Array[Byte]] = new ConcurrentHashMap[String, Array[Byte]]
  val logger = LoggerFactory.getLogger("IdentityDecoderService")

  override def decode(identifier: String, encryptedData: Array[Byte]): EapSimResult[String] = {
//    val decryptedKey = PrivacyUtil.decrypt("pkcs8-2048b-rsa-example-keypair.der", encryptedData)
//    Right(decryptedKey)

    //todo: Load PrivateKey from cache instead of byteArray
    val result = for {
      privateKeyData <- loadPrivateKeyDate(identifier)
      privateKey <- convertToPrivateKey(privateKeyData)
      imsi <- doDecode(encryptedData, privateKey)
    } yield imsi

    result
  }

  private def loadPrivateKeyDate(identifier: String): EapSimResult[Array[Byte]] = {
    if (!cache.contains(identifier)) {
      val privateKey: EapSimResult[Option[KeyInfo]] = keystoreRepository.find(identifier)

      privateKey.right.map {
        case Some(keyInfo) => {
          cache.put(identifier, keyInfo.privateKey)
          Right(keyInfo.privateKey)
        }
        case _ => Left(UnKnownKeyIdentifier.eapSimError("Key identifier not found"))
      }
    }
    Right(cache.get(identifier))
  }

  private def convertToPrivateKey(dataOfPrivateKey: Array[Byte]): EapSimResult[PrivateKey] = {
    allCatch.either(PrivacyUtil.readPrivateKey(dataOfPrivateKey)).left.map(e => {
      logger.error("PrivateKey generation failed. Make sure your key is [algo: RSA, keySize:2048, Format: DER, spec:PKCS8]", e)

      KeyRetrievingFailed.eapSimError("Failed due to PrivateKey generate issue")
    })
  }

  private def doDecode(data: Array[Byte], privateKey: PrivateKey): EapSimResult[String] = {
    allCatch.either(PrivacyUtil.decrypt(privateKey, data)).left.map(e => {
      logger.error("Error while decoding identity", e)
      DecryptFailed.eapSimError("Decoding failed")
    }).right.map(identity => {
      logger.debug(s"Identity decoded successfully [identity: $identity]")
      identity
    })
  }
}



