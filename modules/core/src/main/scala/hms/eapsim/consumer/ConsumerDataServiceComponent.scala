/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.consumer

import java.util

import hms.commons.SnmpLogUtil
import org.apache.logging.log4j.scala.Logging
import sun.util.logging.resources.logging
import hms.eapsim.EapSimResult

import scalaz._
import Scalaz._
import hms.eapsim.ldap.{LDAPConnector, LdapConnectorComponent}
import hms.eapsim.LdapAttributesNotReceived

import scala.util.control.Exception._
import hms.eapsim.repo.TransLog
import org.slf4j.LoggerFactory

/**
 *
 */
trait ConsumerDataServiceComponent {
  this: MsisdnServiceComponent with LdapConnectorComponent =>

  def consumerDataService: ConsumerDataService

  /**
   * Eg:
   * baseDn = "ou=m1net-consumers,dc=m1net,dc=com"
   * filterTxt = "(uid=%s*)"
   * Here %s will be replaced with msisdn we need to query.
   */
  class ConsumerDataService(baseDn: String, filterTxt: String, defaultRadiusClass: String, enableQueryLdap: Boolean = false,
                            ldapConnector: LDAPConnector, ldapDownMessage: String = "ldap.down", ldapUpMessage: String = "ldap.up") {

    val requiredList = util.Arrays.asList("radiusclass")

    val logger = LoggerFactory.getLogger("CDS")


    def getConsumerData(correlationId: String, imsi: String)(implicit transLog: TransLog): EapSimResult[Map[String, Object]] = {
      val data = if (enableQueryLdap) {
        val result = msisdnService.getMsisdn(imsi)(correlationId).fold(
          error => {
            logger.warn("Error occurred while requesting msisdn from spml gateway and taking default class")
            Right(Map("radiusclass" -> defaultRadiusClass.getBytes))
          },
          msisdn => {
            getData(msisdn)
          }
        )
        logger.debug(s"Consumer data received[$result]")
        result
      } else {
        logger.debug(s"Quering ldap has disabled [ enableQueryLdap: ${enableQueryLdap} ]. Getting RadiusClass from file [ ${defaultRadiusClass}]")
        Right(Map("radiusclass" -> defaultRadiusClass.getBytes))
      }
      data
    }

    private def getData(msisdn: String)(implicit transLog: TransLog): EapSimResult[Map[String, Object]] = {
      logger.debug(s"Getting customer data for Msisdn[${msisdn}]")
      transLog.msisdn = Some(msisdn)
      val msisdnWithoutCountryCode = msisdn.substring(2)
      val filter = filterTxt.replaceAll("%s", msisdnWithoutCountryCode)

      allCatch.either(ldapConnector.search(baseDn, filter, requiredList)).fold(
        error => {
          logger.error(s"Error while finding radius class for [ msisdn: ${msisdn}}]", error)
          SnmpLogUtil.trap("eapServerToLDAPConnection", ldapDownMessage)
          Right(Map("radiusclass" -> defaultRadiusClass.getBytes))
        },
        searchResult => {
          SnmpLogUtil.clearTrap("eapServerToLDAPConnection", ldapUpMessage)
          if (searchResult.size() > 0) {
            Right(Map("radiusclass" -> searchResult.get("radiusclass")))
          } else {
            Right(Map("radiusclass" -> defaultRadiusClass.getBytes))
          }
        }
      )
    }
  }
}