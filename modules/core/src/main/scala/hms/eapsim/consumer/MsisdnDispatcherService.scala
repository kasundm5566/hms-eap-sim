package hms.eapsim.consumer

import java.util

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import hms.eapsim.mapgw.RoutingTable
import hms.eapsim._
import java.util.concurrent.{TimeUnit, Executors}
import java.util.Date
import hms.eapsim.repo.{MsisdnEntry, MsisdnStoreRepository}
import hms.eapsim.spml.SpmlGatewayConnectorComponent
import hms.smpl.connector.{CommandResponse, ExecutionContext}
import hms.smpl.connector.command.RequestTemplate

import scala.util.control.Exception._

/**
 * Get msisdn using Sigtran gateway
 */
class MsisdnDispatcherService(spmlGatewayConnector: SpmlGatewayConnectorComponent,
                             imsiKey: String, msisdnKey: String, msisdnResponseId: Int,
                             clientId: String, routingTable: RoutingTable,
                             cacheCleanUpInterval: Long, entryLifeTime: Long,
                             msisdnStoreRepository: MsisdnStoreRepository, isCacheEnabled :Boolean = true) extends MsisdnService {

  private val logger: Logger = LogManager.getLogger(MsisdnDispatcherService.this)
  private val cacheCleaner = Executors.newScheduledThreadPool(1)


  class MsisdnElement(val msisdn: String, val imsi: String) {
    val recordedTime: Date = new Date()

    override def toString() = s" MSISDN: ${msisdn}, IMSI: ${imsi}"
  }


  override def getMsisdn(imsi: String)(implicit correlationId: String): EapSimResult[String] = {
    logger.debug(s"Getting MSISDN from IMSI[${imsi}]")

    def retrieveMsisdnFromGW: EapSimResult[String] = {
      sendRequestSpml
    }

    def sendRequestSpml: EapSimResult[String] = {
      val executionContext: ExecutionContext = new ExecutionContext()
      executionContext.putString(imsiKey, imsi)

      val template: RequestTemplate = spmlGatewayConnector.spmlGWConnector.searchRequestTemplate
      val requestTemplates: util.ArrayList[RequestTemplate] = new util.ArrayList[RequestTemplate]()
      requestTemplates.add(template)

      val result = allCatch.either(retrieveMsisdn(requestTemplates, executionContext))
      result.fold(
        e => {
          logger.error("Error occurred while requesting msisdn from spml gateway", e.getCause)
          Left(SpmlGwErrorResp.eapSimError("Error occurred while requesting msisdn from spml gateway"))
        },
        result => result
      )
    }

    def retrieveMsisdn(requestTemplates: util.ArrayList[RequestTemplate], executionContext: ExecutionContext): EapSimResult[String] = {
      spmlGatewayConnector.spmlGWConnector.commandProcessor.process(requestTemplates, executionContext)
      val response: CommandResponse = executionContext.getCommandResponse

      val commandResp: util.Map[String, String] = response.getResponse(msisdnResponseId)
      if (commandResp != null && commandResp.containsKey(msisdnKey) && commandResp.get(msisdnKey) != null &&
        !commandResp.get(msisdnKey).isEmpty) {
        val msisdn = commandResp.get(msisdnKey)
        logger.info(s"Received MSISDN from SPML gateway: ${msisdn}")
        Right(msisdn)
      } else {
        logger.error("No MSISDN received with SPML GW response")
        Left(MsisdnNotReceivedSpml.eapSimError("No MSISDN received with SPML GW response"))
      }
    }

    //TRY CACHE
    val cached = msisdnStoreRepository.getBy(imsi)
    cached.fold(
      e => Left(DBFailure.eapSimError(s"Db failure occurred while retrieving cached msisdn for [ IMSI: ${imsi}]")),
      recodeFromDb => {
        recodeFromDb match {
          case Some(entry) => {
            logger.info(s"A record found in the msisdn cache [${entry}]")
            Right(entry.msisdn)
          }
          case _ => {
            logger.info(s"record for [ IMSI: ${imsi}] not available in the msisdn cache. Going to take from HLR")
            val msisdn = retrieveMsisdnFromGW
            msisdn.right.foreach(num => msisdnStoreRepository.save(new MsisdnEntry(null, num, imsi)))
            logger.info(s"Msisdn received from [ IMSI: ${imsi}], MSISDN: ${msisdn} ]")
            msisdn
          }
        }
      }
    )
  }

  override def init(): Unit = {
    if (isCacheEnabled) {
      val cacheManager = new Thread(new Runnable {
        override def run(): Unit = {
          logger.debug(s"Msisdn cache clean up job [ lifeTime: ${entryLifeTime} ms] - started")
          val status = msisdnStoreRepository.deleteAllExpired(entryLifeTime)

          status.fold(error => logger.error("Error occurred while deleting entries from MSISDN cache", error),
          count => {
            if (count > 0) {
              logger.info(s"Expired entries removed from MSISDN cache [ count: $count]")
            }
          })
        }
      })
      val cleanUpJobName = "MsisdnCacheManager"
      cacheManager.setName(cleanUpJobName)
      cacheCleaner.scheduleAtFixedRate(cacheManager, 10, cacheCleanUpInterval, TimeUnit.SECONDS)
      logger.info(s"Msisdn cache clean up job started [ job: ${cleanUpJobName}] with [ delay: ${cacheCleanUpInterval} ms, entryLifeTime: ${entryLifeTime} ms ]")

    } else {
      logger.info("Msisdn cache has been disabled")
    }

  }
}

case class CacheConfig(cacheCleanUpInterval: Long, entryLifeTime: Long, maxLimit: Long = 10000, isEnabled: Boolean = true)
