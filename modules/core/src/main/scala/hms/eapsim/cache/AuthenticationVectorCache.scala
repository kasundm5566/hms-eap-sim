package hms.eapsim.cache

import java.util.concurrent.ConcurrentHashMap
import java.util.Date
import hms.eapsim.consumer.CacheConfig
import org.apache.logging.log4j.scala.Logging
import scala.collection.JavaConversions._
import hms.eapsim.key.AuthVector


trait AuthenticationVectorCache {

  def getCurrentConfig(): Option[CacheConfig]

  def add(key: String, vector: List[AuthVector]): Unit

  def get(key: String): Option[List[AuthVector]]

  def remove(key: String):Unit

  def cleanUp: Unit
}

case class CacheElement(key: String, value: List[AuthVector]) {
  val lastUpdated: Date = new Date()
}

class AuthenticationVectorCacheImpl(val cacheConfig: Option[CacheConfig] = None) extends AuthenticationVectorCache with Logging {

  private val cache: ConcurrentHashMap[String, CacheElement] = new ConcurrentHashMap[String, CacheElement]()

  def get(key: String): Option[List[AuthVector]] = {
    val element = cache.get(key)
    if (element != null) Some(element.value) else None
  }

  def add(key: String, value: List[AuthVector]): Unit = {
    if (cacheConfig.getOrElse(new CacheConfig(0, 0, 0, false)).isEnabled){
      cache.put(key, new CacheElement(key, value))
      logger.debug(s"Adding entry for auth vector [ imsi: $key, value: $value,")
    }
  }

  override def cleanUp: Unit = {
    logger.debug(s"Auth vectors cache clean up job [ currentEntryCount: ${cache.size}, lifeTime: ${cacheConfig.get.entryLifeTime}] - started")
    if (cacheConfig.get.maxLimit < cache.size) {
      logger.warn(s"AUTH VECTORS CACHE HAS REACHED ITS MAX-LIMIT, ${cacheConfig.get.maxLimit}")
    }
    val now = System.currentTimeMillis()

    val filtered = cache.values().filter(ele => now - ele.lastUpdated.getTime > cacheConfig.get.entryLifeTime)
    filtered.foreach(ele => {
      cache.remove(ele.key)
    })
    if (filtered.size > 0) {
      logger.debug(s"Authentication vector cached cleaned up [ count: ${filtered.size}]")
    }
    logger.debug("Auth vectors cache clean up job - done")
  }

  override def getCurrentConfig(): Option[CacheConfig] = cacheConfig

  override def remove(key: String): Unit ={
    val removed = cache.remove(key)
    logger.info(s"Removing authentication vector for [ IMSI: $key, cached: $removed]")
  }
}


