package hms.eapsim.cache

import org.apache.logging.log4j.scala.Logging
import scala.util.control.Exception._
import hms.eapsim.repo.ReAuthContextRepository
import java.util.concurrent.{TimeUnit, Executors}


trait ReAuthContextCleanerManager {
  def init
}

class ReAuthContextCleanerManagerImpl(reAuthContextRepo: ReAuthContextRepository, intervalInMillSec: Long, lifeTimeInSecond: Int, maxRecordLimit: Int) extends ReAuthContextCleanerManager with Logging {

  private val cacheCleaner = Executors.newScheduledThreadPool(1)

  override def init: Unit = {
    val cleanUpJob = new Thread(new Runnable {
      override def run(): Unit = {
        logger.debug("Re-Auth Context cache cleanup job starting")

        val result = allCatch.either(reAuthContextRepo.loadExpired(lifeTimeInSecond, maxRecordLimit)) //.left.map(e => e)
        result.fold(err => logger.error("Error while loading expired context", err),
        expired => {
          expired.foreach(ctx => {
            reAuthContextRepo.delete(ctx.imsi)
          })
          if (expired.size > 0) {
            logger.info(s"Re-Authentication context cache cleaned up [ expiredCount: ${expired.size}]")
          }
        })
      }
    }, "Re-Auth-Context-CleanUpJob")
    cleanUpJob.setDaemon(true)

    logger.info("Re-Auth Context cache cleanup manager starting")

    cacheCleaner.scheduleAtFixedRate(cleanUpJob, 10, intervalInMillSec, TimeUnit.MILLISECONDS)
    logger.info(s"Re-Auth Context cache cleanup manager started [ reAuthCtxLifeTime: ${lifeTimeInSecond} sec, intervalInMillSec: ${intervalInMillSec}, maxRecord: ${maxRecordLimit}]")
  }
}