package hms.eapsim.cache

import java.util.concurrent.{Executors, TimeUnit}
import org.apache.logging.log4j.scala.Logging

trait CacheManager {
  def init

}

class CacheManagerImpl(cache: AuthenticationVectorCache) extends CacheManager with Logging {
  private val cacheCleaner = Executors.newScheduledThreadPool(1)

  override def init: Unit = {
    val cleanUpJob = new Thread(new Runnable {
      override def run(): Unit = cache.cleanUp
    }, "AuthVectorCache-CleanUpJob")

    val config = cache.getCurrentConfig().get
    if (config.isEnabled) {
      logger.info("Auth vector cache manager starting")

      cacheCleaner.scheduleAtFixedRate(cleanUpJob, 10, config.cacheCleanUpInterval, TimeUnit.MILLISECONDS)
      logger.info(s"Auth vector cache clean up job started [ job: AuthVectorCache-CleanUpJob ] with [ delay: ${config.cacheCleanUpInterval} ms, entryLifeTime: ${config.entryLifeTime} ms, maxEntry: ${config.maxLimit} ]")
    } else {
      logger.info("Auth vector cache has been disabled")
    }
  }
}
