package hms.eapsim.repo

trait RepositoryComponent {
  def translogRepository : TranslogRepository

  def reAuthContextRepository : ReAuthContextRepository

  def msisdnStoreRepository: MsisdnStoreRepository

  def keystoreRepository: KeystoreRepository
}
