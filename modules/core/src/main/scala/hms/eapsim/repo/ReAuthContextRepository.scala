package hms.eapsim.repo

import hms.commons.SnmpLogUtil

import scala.collection.JavaConversions._
import javax.sql.DataSource
import java.util.Date
import java.sql.{Connection, Timestamp}
import java.util
import java.util.concurrent.Executors

import org.apache.logging.log4j.scala.Logging

import scala.util.control.Exception._
import hms.eapsim._
import org.slf4j.LoggerFactory

trait ReAuthContextRepository {

  def save(context: ReAuthContext): EapSimResult[Boolean]

  def findByNextReAuthId(usernamePartOfNAI: String): Option[ReAuthContext]

  @Deprecated
  def findByImsi(imsi: Long): Option[ReAuthContext]

  def findByImsiNEW(imsi: Long): EapSimResult[Option[ReAuthContext]]

  def delete(imsi: Long)

  def loadExpired(lifeTime: Int, limit: Int): List[ReAuthContext]

  def update(nextReAuthId: String, counter: Int, imsi: Long, nonceSAsHex: String)

}

case class ReAuthContext(imsi: Long, masterKey: String, kAut: String,
                         kEncr: String, identity: String,
                         nextReAuthId: String, correlationId: String, protocol: EapProtocol = SIM,
                         counter: Int = 0, date: Date = null, nonceSAsHex: String = null) {

  override def toString = s"ReAuthContext - [ IMSI: ${imsi}, counter: ${counter}, nextReAuthId: ${nextReAuthId}, correlationId: ${correlationId}, protocol: ${protocol}, masterKey: ${masterKey}, kAut: ${kAut}, kEncr: ${kEncr}, date: ${date} ]"
}


class ReAuthContextRepositoryImpl(dataSource: DataSource, dbDownSnmpMessage: String, dbUpSnmpMessage: String,
                                  connectionSocketSOTimeoutInMilliSec: Int = 3000, soPoolSize: Int = 20
                                 ) extends ReAuthContextRepository  {

  val logger = LoggerFactory.getLogger("ReAuthCtxRepo")
  private val pool = Executors.newFixedThreadPool(soPoolSize)

  def save(context: ReAuthContext): EapSimResult[Boolean] = {
    allCatch.either(save(context, dataSource)).left.map(e => {
      logger.error("Error while saving", e)
      SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
      DBFailure.eapSimError(s"DB failure occurred while saving re-auth context record [ ctx:${context} ], error: ${e}")
    })
  }

  private def save(context: ReAuthContext, dataSource: DataSource): Boolean = {
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)

      val stmt = connection.prepareStatement("insert into re_auth_ctx " +
        "(imsi, master_key, k_aut, k_encr, identity, next_re_auth_id, correlation_id, protocol, created, counter) values " +
        "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY " +
        "UPDATE imsi=?, master_key=?, k_aut=?, k_encr=?, identity=?, next_re_auth_id=?, correlation_id=?, protocol=?, created=?, counter=?")
      stmt.setLong(1, context.imsi)
      stmt.setString(2, context.masterKey)
      stmt.setString(3, context.kAut)
      stmt.setString(4, context.kEncr)
      stmt.setString(5, context.identity)
      stmt.setString(6, context.nextReAuthId)
      stmt.setString(7, context.correlationId)
      stmt.setString(8, context.protocol.toString)
      stmt.setTimestamp(9, new Timestamp(new Date().getTime))
      stmt.setInt(10, context.counter)

      stmt.setLong(11, context.imsi)
      stmt.setString(12, context.masterKey)
      stmt.setString(13, context.kAut)
      stmt.setString(14, context.kEncr)
      stmt.setString(15, context.identity)
      stmt.setString(16, context.nextReAuthId)
      stmt.setString(17, context.correlationId)
      stmt.setString(18, context.protocol.toString)
      stmt.setTimestamp(19, new Timestamp(new Date().getTime))
      stmt.setInt(20, context.counter)
      stmt.execute()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)
      stmt.close()
      logger.debug(s"Context saved successfully ${context}")
      true
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  def findByNextReAuthId(usernamePartOfNAI: String): Option[ReAuthContext] = {
    logger.debug(s"Finding context by user part of NAI [ userName: ${usernamePartOfNAI}]")
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      var context: Option[ReAuthContext] = None
      val stmt = connection.prepareStatement("select * from re_auth_ctx where next_re_auth_id like ?")
      stmt.setString(1, "%" + usernamePartOfNAI + "%")
      val resultSet = stmt.executeQuery()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)


      if (resultSet.next()) {
        val imsi = resultSet.getLong("imsi")
        val createdTimeStamp = new Date(resultSet.getTimestamp("created").getTime)
        val identity = resultSet.getString("identity")
        val nextReAuthId = resultSet.getString("next_re_auth_id")
        val counter = resultSet.getInt("counter")
        val masterKey = resultSet.getString("master_key")
        val kAut = resultSet.getString("k_aut")
        val kEncr = resultSet.getString("k_encr")
        val correlationId = resultSet.getString("correlation_id")
        val nonceS = resultSet.getString("nonce_s")
        val protocol: EapProtocol = if (resultSet.getString("protocol").equals("SIM")) SIM else AKA

        context = Some(ReAuthContext(imsi, masterKey, kAut, kEncr, identity, nextReAuthId, correlationId, protocol, counter, createdTimeStamp, nonceS))
        logger.debug(s"Previous saved context found ${context}")
      } else {
        context = None
        logger.debug(s"No previous saved context found [ nextReAuthUserName: ${usernamePartOfNAI} ]")
      }
      resultSet.close()
      stmt.close()
      context
    } catch {
      case e : Exception => {
        logger.error("Error while finding", e)
        SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
        throw e;
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  override def update(nextReAuthId: String, counter: Int, imsi: Long, nonceSAsHex: String): Unit = {
    logger.debug(s"Updating context for [ IMSI: ${imsi}, counter: ${counter}, nextReAuthId: ${nextReAuthId}]")
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)

      val stmt = connection.prepareStatement("update re_auth_ctx set next_re_auth_id=?, counter=?, nonce_s=? where imsi=?")
      stmt.setString(1, nextReAuthId)
      stmt.setInt(2, counter)
      stmt.setString(3, nonceSAsHex)
      stmt.setLong(4, imsi)
      stmt.executeUpdate()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)

      stmt.close()
      logger.debug(s"Context updated successfully for [ IMSI: ${imsi}, counter: ${counter}, nextReAuthId: ${nextReAuthId}]")
    } catch {
      case e : Exception => {
        logger.error("Error while updating", e)
        SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
        throw e;
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  override def findByImsi(imsi: Long): Option[ReAuthContext] = {

    logger.debug(s"Finding context by [ IMSI: ${imsi}]")
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      var context: Option[ReAuthContext] = None
      val stmt = connection.prepareStatement("select * from re_auth_ctx where imsi=?")
      stmt.setLong(1, imsi)
      val resultSet = stmt.executeQuery()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)


      if (resultSet.next()) {
        val imsi = resultSet.getLong("imsi")
        val createdTimestamp = new Date(resultSet.getTimestamp("created").getTime)
        val identity = resultSet.getString("identity")
        val nextReAuthId = resultSet.getString("next_re_auth_id")
        val counter = resultSet.getInt("counter")
        val masterKey = resultSet.getString("master_key")
        val kAut = resultSet.getString("k_aut")
        val kEncr = resultSet.getString("k_encr")
        val correlationId = resultSet.getString("correlation_id")
        val nonceS = resultSet.getString("nonce_s")
        val protocol: EapProtocol = if (resultSet.getString("protocol").equals("SIM")) SIM else AKA

        context = Some(ReAuthContext(imsi, masterKey, kAut, kEncr, identity, nextReAuthId, correlationId, protocol, counter, createdTimestamp, nonceS))
        logger.debug(s"Previous context found ${context}")
      } else {
        context = None
        logger.info(s"No previous context found for [ IMSI: ${imsi}]")
      }
      resultSet.close()
      stmt.close()
      context
    } catch {
      case e : Exception => {
        logger.error("Error while finding by imsi", e)
        SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
        throw e;
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }


  override def delete(imsi: Long): Unit = {
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      val stmt = connection.prepareStatement("delete from re_auth_ctx where imsi=?")
      stmt.setLong(1, imsi)
      stmt.executeUpdate()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)
      stmt.close()
    } catch {
      case e : Exception => {
        logger.error("Error while delete by imis", e)
        SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
        throw e;
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  override def loadExpired(lifetime: Int, limit: Int): List[ReAuthContext] = {
    logger.debug(s"Loading expired contexts [ lifetime: ${lifetime}, limit: ${limit}]")
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      val expiredList = new util.ArrayList[ReAuthContext]()

      val stmt = connection.prepareStatement("select * from re_auth_ctx where ADDDATE(created, INTERVAL ? SECOND) < now() limit ?")
      stmt.setInt(1, lifetime)
      stmt.setInt(2, limit)
      val resultSet = stmt.executeQuery()

      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)


      while (resultSet.next()) {
        val imsi = resultSet.getLong("imsi")
        val createdTimestamp = new Date(resultSet.getTimestamp("created").getTime)
        val identity = resultSet.getString("identity")
        val nextReAuthId = resultSet.getString("next_re_auth_id")
        val counter = resultSet.getInt("counter")
        val masterKey = resultSet.getString("master_key")
        val kAut = resultSet.getString("k_aut")
        val kEncr = resultSet.getString("k_encr")
        val correlationId = resultSet.getString("correlation_id")
        val nonceS = resultSet.getString("nonce_s")
        val protocol: EapProtocol = if (resultSet.getString("protocol").equals("SIM")) SIM else AKA
        expiredList.add(ReAuthContext(imsi, masterKey, kAut, kEncr, identity, nextReAuthId, correlationId, protocol, counter, createdTimestamp, nonceS))
      }
      resultSet.close()
      stmt.close()
      logger.debug(s"Loaded expired context count [${expiredList.size()}]")
      expiredList.toList
    } catch {
      case e : Exception => {
        SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
        throw e;
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  override def findByImsiNEW(imsi: Long): EapSimResult[Option[ReAuthContext]] = {
    allCatch.either(findByImsi(imsi, dataSource)).left.map(e => {
      logger.error("Error while finding ctx by imsi", e)
      SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
      DBFailure.eapSimError(s"Error occurred while getting context by [ IMSI: $imsi]")
    })
  }

  private def findByImsi(imsi: Long, dataSource: DataSource): Option[ReAuthContext] = {
    logger.debug(s"Finding context by [ IMSI: ${imsi}]")
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      val stmt = connection.prepareStatement("select * from re_auth_ctx where imsi=?")
      stmt.setLong(1, imsi)
      val resultSet = stmt.executeQuery()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)


      if (resultSet.next()) {
        val imsi = resultSet.getLong("imsi")
        val createdTimestamp = new Date(resultSet.getTimestamp("created").getTime)
        val identity = resultSet.getString("identity")
        val nextReAuthId = resultSet.getString("next_re_auth_id")
        val counter = resultSet.getInt("counter")
        val masterKey = resultSet.getString("master_key")
        val kAut = resultSet.getString("k_aut")
        val kEncr = resultSet.getString("k_encr")
        val correlationId = resultSet.getString("correlation_id")
        val nonceS = resultSet.getString("nonce_s")
        val protocol: EapProtocol = if (resultSet.getString("protocol").equals("SIM")) SIM else AKA

        val context = Some(ReAuthContext(imsi, masterKey, kAut, kEncr, identity, nextReAuthId, correlationId, protocol, counter, createdTimestamp, nonceS))
        logger.debug(s"Previous context found ${context}")

        resultSet.close()
        stmt.close()
        context
      } else {
        logger.info(s"No previous context found for [ IMSI: ${imsi}]")
        None
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }
}

