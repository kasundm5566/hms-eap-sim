/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.repo

import hms.eapsim.EapSimError
import hms.common.radius.decoder.eap.elements.{EapRequestResponseMessage, EapMessage, EapTypeCode}
import hms.common.radius.decoder.eap.elements.eapsimaka.sim.EapSim
import hms.common.radius.decoder.eap.elements.eapsimaka.aka.EapAka
import java.util.Date

/**
 *
 */
trait TranslogRepositoryComponent {
  def translogRepository: TranslogRepository

  def printTrasLog(implicit logEntry: TransLog) {
    translogRepository.log(logEntry)
  }

}

trait FlowType
case object RE_AUTHENTICATION extends FlowType {
  override def toString = "RE_AUTHENTICATION"
}
case object FULL_AUTHENTICATION extends FlowType {
  override def toString = "FULL_AUTHENTICATION"
}

//case object INIT extends FlowType {
//  override def toString = "INIT"
//}

trait EapProtocol
case object SIM extends EapProtocol
case object AKA extends EapProtocol
case class TransLog(correlationId: String,
  var requestType: String,
  var imsi: String,
  var status: String = "S1000",
  var description: String = "Success",
  var msisdn: Option[String] = None,
  time: Date = new Date,
  var protocol: Option[EapProtocol] = None,
  var responseType: String = "",
  var responseAttributes: Map[String, String] = Map(),
  var flowType: FlowType = FULL_AUTHENTICATION,
                     var calledStationMac: String = "", var ip: String = "", var nasId: String = "", var receivedTime: Long) {

  def setStatus(ec: EapSimError) {
    status = ec.statusCode.statusCode
    description = ec.statusCode.description
  }

  def setProtocolData(eapMsg : EapMessage) {
    eapMsg match {
      case e: EapRequestResponseMessage => e.getType.getTypeCode match {
        case EapTypeCode.EAP_SIM =>
          val msg = e.getType.asInstanceOf[EapSim]
          protocol = Some(SIM)
          requestType = msg.getEapSubType.toString

        case EapTypeCode.EAP_AKA =>
          val msg = e.getType.asInstanceOf[EapAka]
          protocol = Some(AKA)
          requestType = msg.getEapSubType.toString

        case _ => protocol = None
      }
      case _ => protocol = None
    }
  }
}
