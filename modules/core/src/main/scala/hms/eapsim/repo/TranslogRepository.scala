package hms.eapsim.repo

import java.sql.{Connection, Timestamp}
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.atomic.AtomicInteger

import com.mchange.v2.c3p0.ComboPooledDataSource
import hms.commons.SnmpLogUtil
import hms.eapsim.AsynchronousResponse
import org.apache.logging.log4j.scala.Logging
import org.slf4j.{Logger, LoggerFactory}

trait TranslogRepository {
  def log(entry: TransLog)
}

class MysqlTransLogRepository(datasource: ComboPooledDataSource, dbDownSnmpMessage: String, dbUpSnmpMessage: String, 
                              translogSeparator: String = "|", serverId: String) extends TranslogRepository with Logging {

  val TRANS_LOGGER: Logger = LoggerFactory.getLogger("translog")


  def insert(log: TransLog): Boolean = {
    var connection : Connection = null
    try {
      connection = datasource.getConnection
      val stmt = connection.prepareStatement(" insert into trans_log (request_type, imsi, status, description, msisdn, created, protocol, response_type, correlation_id, flow_type) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
      stmt.setString(1, log.requestType)
      stmt.setString(2, log.imsi)
      stmt.setString(3, log.status)
      stmt.setString(4, log.description)
      stmt.setString(5, log.msisdn.getOrElse(""))
      stmt.setTimestamp(6, new Timestamp(log.time.getTime))
      stmt.setString(7, log.protocol.getOrElse(None).toString)
      stmt.setString(8, log.responseType)
      stmt.setString(9, log.correlationId)
      stmt.setString(10, log.flowType.toString)
      stmt.execute()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)
      stmt.close()
      true
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  override def log(entry: TransLog) = {
    printTransLog(entry)
  }

  private def printTransLog(entry: TransLog) = {
    if (!entry.status.equals(AsynchronousResponse.statusCode)) {
      printTxtLogEntry(entry)
      increaseTPSProperties(entry)
    }
  }

  private def increaseTPSProperties(transLog: TransLog): Unit = {
    if(transLog.description.equals("Success")) {
      transLog.protocol.get match {
        case AKA =>
          transLog.flowType match {
            case FULL_AUTHENTICATION =>
              TpsLog.akaFullAuthSuccess.incrementAndGet()
            case RE_AUTHENTICATION =>
              TpsLog.akaReAuthSuccess.incrementAndGet()
          }
        case SIM =>
          transLog.flowType match {
            case FULL_AUTHENTICATION =>
              TpsLog.simFullAuthSuccess.incrementAndGet()
            case RE_AUTHENTICATION =>
              TpsLog.simReAuthSuccess.incrementAndGet()
          }
      }
    } else {
      transLog.protocol.get match {
        case AKA =>
          transLog.flowType match {
            case FULL_AUTHENTICATION =>
              TpsLog.akaFullAuthFail.incrementAndGet()
            case RE_AUTHENTICATION =>
              TpsLog.akaReAuthFail.incrementAndGet()
          }
        case SIM =>
          transLog.flowType match {
            case FULL_AUTHENTICATION =>
              TpsLog.simFullAuthFail.incrementAndGet()
            case RE_AUTHENTICATION =>
              TpsLog.simReAuthFail.incrementAndGet()
          }
      }
    }
  }

  private def printTxtLogEntry(entry: TransLog) {
    val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss.SSS")

    val logEntry = s"${System.currentTimeMillis() - entry.receivedTime}$translogSeparator${entry.ip}$translogSeparator${entry.calledStationMac}$translogSeparator${entry.nasId}$translogSeparator$serverId$translogSeparator${entry.correlationId}$translogSeparator${entry.protocol.getOrElse(None).toString}$translogSeparator${entry.flowType.toString}$translogSeparator${entry.requestType}$translogSeparator${entry.responseType}$translogSeparator${entry.imsi}$translogSeparator${entry.msisdn.getOrElse("")}$translogSeparator${entry.status}$translogSeparator${entry.description}$translogSeparator${dateFormat.format(entry.time)}"
    TRANS_LOGGER.info(logEntry)
  }
}

object TpsLog {

  val akaFullAuthSuccess: AtomicInteger = new AtomicInteger(0)
  val akaFullAuthFail: AtomicInteger = new AtomicInteger(0)
  val akaReAuthSuccess: AtomicInteger = new AtomicInteger(0)
  val akaReAuthFail: AtomicInteger = new AtomicInteger(0)
  val simFullAuthSuccess: AtomicInteger = new AtomicInteger(0)
  val simFullAuthFail: AtomicInteger = new AtomicInteger(0)
  val simReAuthSuccess: AtomicInteger = new AtomicInteger(0)
  val simReAuthFail: AtomicInteger = new AtomicInteger(0)

  var tempAkaFullAuthSuccess : Int = 0
  var tempAkaFullAuthFail : Int = 0
  var tempAkaReAuthSuccess : Int = 0
  var tempAkaReAuthFail : Int = 0
  var tempSimFullAuthSuccess : Int = 0
  var tempSimFullAuthFail : Int = 0
  var tempSimReAuthSuccess : Int = 0
  var tempSimReAuthFail : Int = 0

  val TPS_LOGGER: Logger = LoggerFactory.getLogger("tpslog")
  val DATE_FORMATTER : SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss")
  val tpsLogSeparator : String = "|"

  def updateTPS(): Unit = {
    tempAkaFullAuthSuccess = akaFullAuthSuccess.getAndSet(0)
    tempAkaFullAuthFail = akaFullAuthFail.getAndSet(0)
    tempAkaReAuthSuccess = akaReAuthSuccess.getAndSet(0)
    tempAkaReAuthFail = akaReAuthFail.getAndSet(0)
    tempSimFullAuthSuccess = simFullAuthSuccess.getAndSet(0)
    tempSimFullAuthFail = simFullAuthFail.getAndSet(0)
    tempSimReAuthSuccess = simReAuthSuccess.getAndSet(0)
    tempSimReAuthFail = simReAuthFail.getAndSet(0)
  }

  def addTPSLogs(): Unit = {

    val currentDateTime = new Date()
    val tempAkaSuccess: Int = tempAkaFullAuthSuccess + tempAkaReAuthSuccess
    val tempSimSuccess: Int = tempSimFullAuthSuccess + tempSimReAuthSuccess
    val tempAkaFail: Int = tempAkaFullAuthFail + tempAkaReAuthFail
    val tempSimFail: Int = tempSimFullAuthFail + tempSimReAuthFail
    val totalSuccessCount = tempAkaSuccess + tempSimSuccess
    val totalFailCount = tempAkaFail + tempSimFail

    TPS_LOGGER.info(DATE_FORMATTER.format(currentDateTime) + tpsLogSeparator
      + "AKA" + tpsLogSeparator + tempAkaSuccess + tpsLogSeparator + tempAkaFail + tpsLogSeparator
      + "SIM" + tpsLogSeparator + tempSimSuccess + tpsLogSeparator + tempSimFail + tpsLogSeparator
      + "AKA-FA" + tpsLogSeparator + tempAkaFullAuthSuccess + tpsLogSeparator + tempAkaFullAuthFail + tpsLogSeparator
      + "AKA-RA" + tpsLogSeparator + tempAkaReAuthSuccess + tpsLogSeparator + tempAkaReAuthFail + tpsLogSeparator
      + "SIM-FA" + tpsLogSeparator + tempSimFullAuthSuccess + tpsLogSeparator + tempSimFullAuthFail + tpsLogSeparator
      + "SIM-RA" + tpsLogSeparator + tempSimReAuthSuccess + tpsLogSeparator + tempSimReAuthFail + tpsLogSeparator
      + "Total" + tpsLogSeparator + totalSuccessCount + tpsLogSeparator + totalFailCount)
  }

  def executeTPSLogging(): Unit = {
    updateTPS()
    addTPSLogs()
  }
}
