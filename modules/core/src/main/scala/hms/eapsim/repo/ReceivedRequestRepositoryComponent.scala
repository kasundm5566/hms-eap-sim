/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim.repo

import java.util.concurrent._
import hms.common.radius.RequestContext
import hms.common.radius.decoder.eap.elements.EapTypeCode
import hms.common.radius.util.HexCodec
import hms.eapsim.key.{AuthVector, PseudoRandomKeys}
import hms.eapsim.{Timeout, toOption}
import org.apache.logging.log4j.scala.Logging

/**
 *
 */
trait ReceivedRequestRepositoryComponent {
  def requestRepo: ReceivedRequestRepository
}

trait ReceivedRequestRepository {

  def initTimeouter

  def initSession(correlationId: String, eapType: EapTypeCode, identity: String, username: String, imsi: String, eapId: Int, authVector: Option[List[AuthVector]], requestCtx: RequestContext): Option[AuthenticationSession]

  def updateSession(correlationId: String, generatedKeys: Option[PseudoRandomKeys] = None,
                    nextReAuthId: Option[String], mk: Option[Array[Byte]], authVector: Option[List[AuthVector]] = None, current: Option[AuthenticationSession])


  def findSession(correlationId: String): Option[AuthenticationSession]

  def removeSession(correlationId: String): Option[AuthenticationSession]
}

case class AuthenticationSession(val correlationId: String,
                                 val eapType: EapTypeCode,
                                 var identity: String,
                                 var username: String,
                                 var imsi: String,
                                 var eapId: Int,
                                 var lastAccessTime: Long = System.currentTimeMillis,
                                 var authVector: Option[List[AuthVector]],
                                 var generatedKeys: Option[PseudoRandomKeys],
                                 var nextReAuthId: Option[String] = None,
                                 var mk: Option[String] = None,
                                 clientNodeId: String = "",
                                 ip: String = "",
                                 macAddress: String = "",
                                 nasId:String = "",
                                 var requestReceivedTime: Long) extends Serializable

class SimpleRequestRepo(sessionTimeoutInMilliSec: Long, timeCheckIntervalInMilliSec: Long = 1000) extends ReceivedRequestRepository with Logging {
  this: TranslogRepositoryComponent =>

  logger.info(s"Creating in-memory session cache")

  val db = new ConcurrentHashMap[String, AuthenticationSession]()
  val timeOuter = Executors.newScheduledThreadPool(1)

  def initTimeouter = {
    timeOuter.scheduleWithFixedDelay(new Runnable() {
      def run {
        timeout
      }
    }, 1000, timeCheckIntervalInMilliSec, TimeUnit.MILLISECONDS)
  }

  def initSession(correlationId: String, eapType: EapTypeCode, identity: String, username: String, imsi: String, eapId: Int, authVector: Option[List[AuthVector]], requestCtx: RequestContext): Option[AuthenticationSession] =  {
    val aus = AuthenticationSession(correlationId = correlationId,
      eapType = eapType,
      identity = identity,
      username = username,
      imsi = imsi,
      eapId = eapId,
      authVector = authVector,
      generatedKeys = None,
      ip = requestCtx.getIp,
      macAddress = requestCtx.getCalledStationMac,
      requestReceivedTime = requestCtx.getReceivedTime,
      nasId = requestCtx.getNasIdentifier
    )
    db.put(correlationId, aus)
    logger.debug(s"F11 New session created for request, [ session: ${aus} ]")
    Some(aus)
  }

  def updateSession(correlationId: String, generatedKeys: Option[PseudoRandomKeys] = None
                    , nextReAuthId: Option[String], mk: Option[Array[Byte]], authVector: Option[List[AuthVector]] = None, inProgress: Option[AuthenticationSession]) {
    inProgress.map(ip => {
      ip.generatedKeys = generatedKeys
      ip.lastAccessTime = System.currentTimeMillis
      ip.nextReAuthId = nextReAuthId
      mk.map(masterKey => ip.mk = Some(HexCodec.byte2Hex(mk.get)))
      authVector.map(av => ip.authVector = authVector) // This will make sure av not updated for NONE

      db.put(correlationId, ip)
    })
    logger.debug(s"F11 Updated existing AuthenticationSession[${inProgress}]")
  }


  def findSession(correlationId: String): Option[AuthenticationSession] = {
    val session = toOption(db.get(correlationId))
    logger.debug(s"S12Finding saved session in for correlationId[${correlationId}] [${session}}]")
    session
  }

  def removeSession(correlationId: String): Option[AuthenticationSession] = {
    val removed = toOption(db.remove(correlationId))
    logger.debug(s"F11 Authentication session removed [${removed}]")
    removed
  }

  private def timeout {
    import scala.collection.JavaConversions._
    logger.trace(s"Running session time-outer sessions older than[${sessionTimeoutInMilliSec}ms] [ entrycount: ${db.size()}}]")
    try {
      val now = System.currentTimeMillis

      val toEligibal = db.filter(as => (now - as._2.lastAccessTime) > sessionTimeoutInMilliSec)
      toEligibal.foreach(as => {
        val removed = removeSession(as._2.correlationId)
        removed.foreach(r => {
          val log = TransLog(r.correlationId, "", r.imsi, Timeout.statusCode, Timeout.description,
          calledStationMac = r.macAddress, ip = r.ip, receivedTime = r.requestReceivedTime, nasId = r.nasId)
          log.protocol = r.eapType.name match {
            case "EAP_SIM" => Some(SIM)
            case "EAP_AKA" => Some(AKA)
            case _ => None
          }

          printTrasLog(log)
        })
      })
      if (toEligibal.size > 0) {
        logger.debug(s"Removed expired sessions [ count: ${toEligibal.size}]")
      }
    } catch {
      case ex: Throwable => logger.error("Error while running session timeouter", ex)
    }
  }

}