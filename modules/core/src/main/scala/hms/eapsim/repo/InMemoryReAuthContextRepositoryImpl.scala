package hms.eapsim.repo

import java.util.concurrent.TimeUnit

import com.google.common.cache.{Cache, CacheBuilder}
import hms.eapsim._
import org.apache.logging.log4j.scala.Logging

import scala.util.control.Exception._

class InMemoryReAuthContextRepositoryImpl(cacheMaxSize: Long, expirePeriodInSeconds: Long) extends ReAuthContextRepository with Logging {

  private val cache: Cache[Long, ReAuthContext] = CacheBuilder.newBuilder().maximumSize(cacheMaxSize).expireAfterWrite(expirePeriodInSeconds, TimeUnit.SECONDS).build().asInstanceOf[Cache[Long, ReAuthContext]]
  private val secondCache: Cache[String, ReAuthContext] = CacheBuilder.newBuilder().maximumSize(cacheMaxSize).expireAfterWrite(expirePeriodInSeconds, TimeUnit.SECONDS).build().asInstanceOf[Cache[String, ReAuthContext]]

  override def save(context: ReAuthContext): EapSimResult[Boolean] = {
    allCatch.either(saveInCache(context)).left.map(e => {
      logger.error("Error while saving re-auth context", e)
      ReAuthCacheFailure.eapSimError(s"Cache failure occurred while saving re-auth context record [ ctx:${context} ], error: ${e}")
    })
  }

  private def saveInCache(context: ReAuthContext): Boolean = {
    toOption(cache.getIfPresent(context.imsi)).foreach(ctx => {
      secondCache.invalidate(context.nextReAuthId)
    })
    cache.put(context.imsi, context)
    secondCache.put(context.nextReAuthId, context)
    logger.info(s"Context saved successfully ${context}")
    true
  }

  override def findByNextReAuthId(usernamePartOfNAI: String): Option[ReAuthContext] = {
    logger.debug(s"Finding context by user part of NAI [ userName: ${usernamePartOfNAI}]")

    toOption(secondCache.getIfPresent(usernamePartOfNAI))
  }

  @Deprecated
  override def findByImsi(imsi: Long): Option[ReAuthContext] = {
    logger.info(s"Finding context by [ IMSI: ${imsi}]")
    try {
      val reAuthContext: ReAuthContext = cache.getIfPresent(imsi)
      if(reAuthContext != null) {
        logger.info(s"Previous context found ${reAuthContext}")
        Some(reAuthContext)
      } else {
        logger.info(s"No previous context found for [ IMSI: ${imsi}]")
        None
      }
    } catch {
      case e : Exception => {
        logger.error("Error while finding re-auth context", e)
        throw e
      }
    }
  }

  override def findByImsiNEW(imsi: Long): EapSimResult[Option[ReAuthContext]] = {
    allCatch.either(findByImsi(imsi)).left.map(e => {
      logger.error("Error while finding re-auth context by IMSI", e)
      ReAuthCacheFailure.eapSimError(s"Error occurred while getting context by [ IMSI: $imsi]")
    })
  }

  override def delete(imsi: Long): Unit = {
    try {
      cache.invalidate(imsi)
    } catch {
      case e : Exception => {
        logger.error("Error while deleting re-auth context", e)
        throw e
      }
    }
  }

  override def loadExpired(lifeTime: Int, limit: Int): List[ReAuthContext] = List.empty

  override def update(nextReAuthId: String, counter: Int, imsi: Long, nonceSAsHex: String): Unit = {
    logger.debug(s"Updating context for [IMSI: ${imsi}, counter: ${counter}, nextReAuthId: ${nextReAuthId}]")
    try {
      val reAuthContext: ReAuthContext = cache.getIfPresent(imsi)
      if(reAuthContext != null) {
        reAuthContext.synchronized {
          val copy: ReAuthContext = reAuthContext.copy(nextReAuthId = nextReAuthId, counter = counter, nonceSAsHex = nonceSAsHex)
          cache.put(copy.imsi, copy)
        }
        logger.info(s"Context updated successfully for [IMSI: ${imsi}, counter: ${counter}, nextReAuthId: ${nextReAuthId}]")
      } else {
        logger.error(s"Re-auth context not found for [IMSI: ${imsi}]")
      }
    } catch {
      case e : Exception => {
        logger.error("Error while updating re-auth context", e)
        throw e
      }
    }
  }
}
