package hms.eapsim.repo

import java.sql.Connection
import java.util.concurrent.Executors
import javax.sql.DataSource

import hms.commons.SnmpLogUtil
import hms.eapsim._
import hms.eapsim.util.privacy.PrivacyUtil
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._
import scala.util.control.Exception._

trait KeystoreRepository {
  /**
    * Find a KeyInfo for a given identifier
    * @param identifier Unique identity for private key
    * @return Some(KeyInfo) if identifier is available in db, None otherwise
    */
  def find(identifier: String): EapSimResult[Option[KeyInfo]]
}

case class KeyInfo(identifier: String, privateKey: Array[Byte]) {
  override def toString: String = s"[ id: $identifier]"
}

//class FileKeyStoreRepository extends KeystoreRepository{
//  val logger = LoggerFactory.getLogger("KeyStoreFileRepository")
//  /**
//    * Find a KeyInfo for a given identifier
//    *
//    * @param identifier Unique identity for private key
//    * @return Some(KeyInfo) if identifier is available in db, None otherwise
//    */
//  override def find(identifier: String): EapSimResult[Option[KeyInfo]] = {
//    allCatch.either(loadPrivateKeyFromFile(identifier)).left.map(e => {
//      logger.error("Error while loading private key", e)
//      KeyRetrievingFailed.eapSimError(s"Error occurred while loading file from classpath")
//    })
//  }
//
//  private def loadPrivateKeyFromFile(identifier: String) : Option[KeyInfo] = {
//    allCatch.either(() => {
//      val privateKey = PrivacyUtil.readPrivateKey(s"conf/$identifier.der")
//      new KeyInfo(identifier, privateKey.getEncoded)
//
//    }).fold(
//      e => None,
//    )
//  }
//}


class KeystoreRepositoryImpl(dataSource: DataSource, dbDownSnmpMessage: String, dbUpSnmpMessage: String,
                             connectionSocketSOTimeoutInMilliSec: Int = 3000, soPoolSize: Int = 20
                            ) extends KeystoreRepository {

  val logger = LoggerFactory.getLogger("KeystoreRepository")
  private val pool = Executors.newFixedThreadPool(soPoolSize)

  override def find(keyIdentifier: String): EapSimResult[Option[KeyInfo]] = {
    allCatch.either(findBy(keyIdentifier, dataSource)).left.map(e => {
      logger.error("Error while finding Keystore using identifier", e)
      SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)

      KeyRetrievingFailed.eapSimError(s"Error occurred while getting PrivateKey using [ keyIdentifier: $keyIdentifier]")
    })
  }

  private def findBy(keyIdentifier: String, dataSource: DataSource): Option[KeyInfo] = {
    logger.debug(s"Finding Keystore by [ identifier: ${keyIdentifier}]")
    var connection: Connection = null
    try {
      connection = dataSource.getConnection
//      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      val stmt = connection.prepareStatement("select * from keystore")
//      stmt.setString(1, keyIdentifier)
      val resultSet = stmt.executeQuery()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)

      if (resultSet.next()) {
        val privateKeyData = resultSet.getBlob("private_key")

        val bytes = privateKeyData.getBytes(1, privateKeyData.length().toInt)
        logger.info(s"Private key loaded from DB [ length: ${bytes.length}]")
        val keystore = new KeyInfo(keyIdentifier, bytes)

        resultSet.close()
        stmt.close()

        if(keystore.privateKey.length == 0) {
          None
        } else {
          Some(keystore)
        }
      } else {
        logger.info(s"No private key found for given [ keyIdentifier: ${keyIdentifier}]")
        None
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }
}

