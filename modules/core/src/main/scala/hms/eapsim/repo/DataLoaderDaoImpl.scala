package hms.eapsim.repo


import hms.common.radius.service.DataLoaderDao
import org.apache.logging.log4j.scala.Logging

class DataLoaderDaoImpl(reAuthRepo: ReAuthContextRepository) extends DataLoaderDao with Logging {

  override def find(usernamePartOfNAI: String): String = {
    val reAuthCtx: Option[ReAuthContext] = reAuthRepo.findByNextReAuthId(usernamePartOfNAI)

   reAuthCtx match {
      case Some(re) => re.imsi.toString
      case _ => ""
    }
  }
}
