package hms.eapsim.repo

import hms.commons.SnmpLogUtil
import hms.eapsim.{DBFailure, EapSimResult}
import javax.sql.DataSource

import org.apache.logging.log4j.scala.Logging

import scala.util.control.Exception._
import java.sql.{Connection, ResultSet, Timestamp}
import java.util.Date
import java.util.concurrent.Executors

trait MsisdnStoreRepository {
  def save(entry: MsisdnEntry): EapSimResult[Boolean]

  def getBy(imsi: String): EapSimResult[Option[MsisdnEntry]]

  def deleteAllExpired(ageInDaysInSecond: Long): EapSimResult[Int]
}

case class MsisdnEntry(id: BigInt, msisdn: String, imsi: String, created: java.util.Date = new java.util.Date())

class MsisdnStoreRepositoryImpl(dataSource: DataSource, dbDownSnmpMessage: String, dbUpSnmpMessage: String,
                                connectionSocketSOTimeoutInMilliSec: Int = 3000, soPoolSize: Int = 20) extends MsisdnStoreRepository with Logging {

  private val pool = Executors.newFixedThreadPool(soPoolSize)

  override def deleteAllExpired(ageInDaysInSecond: Long): EapSimResult[Int] = {
    val result = allCatch.either(delete(ageInDaysInSecond, dataSource)).left.map(e => {
      logger.error("Error while deleting all expired", e)
      SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
      DBFailure.eapSimError(s"Error occurred while deleting all expired record, cause: ${e}")
    })
    result
  }

  private def delete(ageInDaysInSecond: Long, dataSource: DataSource): Int = {
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)

      val stmt = connection.prepareStatement("delete from msisdn_cache where TIMESTAMPDIFF(SECOND, created, now()) >= ?")
      stmt.setLong(1, ageInDaysInSecond)
      val updatedCount = stmt.executeUpdate()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)
      logger.debug(s"Deletion executed for entries older than $ageInDaysInSecond seconds")
      updatedCount
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  override def getBy(imsi: String): EapSimResult[Option[MsisdnEntry]] = {
    allCatch.either(findBy(imsi, dataSource)).left.map(e => {
      logger.error("Error while getting msisdnEntry", e)
      SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
      DBFailure.eapSimError(s"Error occurred while getting msisdn using [ IMSI: $imsi], cause: ${e}")
    })
  }

  private def findBy(imsi: String, dataSource: DataSource): Option[MsisdnEntry] = {
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)
      val stmt = connection.prepareStatement("select * from msisdn_cache where imsi=?")
      stmt.setString(1, imsi)
      val result = stmt.executeQuery()

      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)
      if (result.next()) {
        createMsisdnEntry(result)
      } else {
        logger.info(s"No msisdn entry is available in the cache for [ $imsi ]")
        None
      }
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }

  private def createMsisdnEntry (result: ResultSet) = {
    val msisdn = result.getString("msisdn")
    val imsi = result.getString("imsi")
    val date = new Date(result.getTimestamp("created").getTime)
    val id = result.getLong("id")

    val cached = new MsisdnEntry(id, msisdn, imsi, date)
    logger.debug(s"Msisd found for [ imsi : $imsi, $cached")
    Some(cached)
  }

  override def save(entry: MsisdnEntry): EapSimResult[Boolean] = {
    allCatch.either(save(entry, dataSource)).left.map(e => {
      logger.error("Error while saving", e)
      SnmpLogUtil.trap("DBSnmpTrap", dbDownSnmpMessage)
      DBFailure.eapSimError(s"Error occurred while saving msisdn entry, cause: ${e}")
    })
  }

  private def save(cacheEntry: MsisdnEntry, dataSource: DataSource): Boolean = {
    var connection : Connection = null
    try {
      connection = dataSource.getConnection
      connection.setNetworkTimeout(pool, connectionSocketSOTimeoutInMilliSec)

      val stmt = connection.prepareStatement("insert into msisdn_cache " +
        " (msisdn, imsi, created) values (?, ?, ?)")
      stmt.setString(1, cacheEntry.msisdn)
      stmt.setString(2, cacheEntry.imsi)
      val timestap = new Timestamp(cacheEntry.created.getTime)
      stmt.setTimestamp(3, timestap)
      stmt.executeUpdate()
      SnmpLogUtil.clearTrap("DBSnmpTrap", dbUpSnmpMessage)
      stmt.close()
      logger.debug(s"Msisdn saved successfully ${}")
      true
    } finally {
      if (connection != null) {
        connection.close()
      }
    }
  }
}

