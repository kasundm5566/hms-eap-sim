package hms.eapsim.repo

import java.util.concurrent.locks.ReentrantLock

class RequestSessionContext(correlationId: String, var response: Option[AuthenticationSession] = None) {
  val requestSentTime = System.currentTimeMillis()
  val lock = new ReentrantLock()
  val noData = lock.newCondition()
}
