package hms.eapsim.repo

import com.mchange.v2.c3p0.ComboPooledDataSource

class MysqlConnectorImpl(parms: MysqlConnectorParams) {

  val pool = new ComboPooledDataSource()

  pool.setDriverClass(parms.driverClass)
  pool.setJdbcUrl(parms.url)
  pool.setUser(parms.user)
  pool.setPassword(parms.password)

  pool.setMinPoolSize(parms.minPoolSize)
  pool.setMaxPoolSize(parms.maxPoolSize)
  pool.setAcquireIncrement(parms.acquireIncrement)
  pool.setInitialPoolSize(parms.initialPoolSize)
  pool.setMaxIdleTime(parms.maxIdleTime)
  pool.setCheckoutTimeout(parms.checkoutTimeout)
  pool.setUnreturnedConnectionTimeout(parms.unReturnedConnectionTimeout)
  pool.setDebugUnreturnedConnectionStackTraces(parms.debugUnreturnedConnectionStackTraces)



  def destroy(): Unit = pool.close()

  def datasouce: ComboPooledDataSource = pool
}

case class MysqlConnectorParams(user: String,
                                password: String,
                                url: String = "jdbc:mysql://localhost:3306/eapsim?autoReconnect=true",
                                driverClass: String = "com.mysql.jdbc.Driver",
                                minPoolSize: Int = 1,
                                initialPoolSize: Int= 5,
                                maxPoolSize: Int = 10,
                                acquireIncrement: Int = 1,
                                maxIdleTime: Int = 300 /*Default non-interactive timeout = 5 minutes,
                                                         this should be less than mysql wait_timeout */,
                                checkoutTimeout : Int = 2000,
                                unReturnedConnectionTimeout: Int = 3,
                                debugUnreturnedConnectionStackTraces: Boolean = false

                                 )