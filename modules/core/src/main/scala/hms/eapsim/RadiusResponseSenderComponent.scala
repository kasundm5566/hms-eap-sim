package hms.eapsim

import hms.common.radius.RadiusOutboundHandler

/**
 * Created by sampath on 2/22/17.
 */
trait RadiusResponseSenderComponent {
  def messageResponder : RadiusOutboundHandler

}
