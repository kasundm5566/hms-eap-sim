/*
 *   (C) Copyright 2008-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.eapsim

case class EapSimError(statusCode: StatusCode, description: String = "", exception: Option[Throwable] = None, trxId: String = "-1")

case class StatusCode(statusCode: String, description: String = "") {

  def eapSimError(desc: String): EapSimError = EapSimError(this, desc)

  def eapSimError(desc: String, trxId: String): EapSimError = EapSimError(this, desc, None, trxId)

  def eapSimError: EapSimError = EapSimError(this, description)

}

object Success extends StatusCode("S1000", "Success")
object UnknownError extends StatusCode("E1000", "Unknown server error")
object LoginSessiongNotFound extends StatusCode("E1001", "Previous login-session not found")

object EapAttributeNotFound extends StatusCode("E2000", "EapSim attribute not found")
object MessageTypeNotSupported extends StatusCode("E2001", "Message cannot be processed.")
object ValidationError extends StatusCode("E2002", "Common validation error")
object MacValidationError extends StatusCode("E2003", "MAC validation error")
object ResValidationError extends StatusCode("E2004", "EapAka AT_RES validation error")

object AuthVectorFetchError extends StatusCode("E3000", "Error while fetching Authentication vectors(Triplet/Quintuplet)")
object UnsupportedAuthenticationSet extends StatusCode("E3001", "Unsupported AuthenticationSet")
object HlrRoutingDataNotSet extends StatusCode("E3002", "HLR Routing data not found")
object MsisdnNotReceived extends StatusCode("E3003", "Msisdn not received from HLR")
object LdapAttributesNotReceived extends StatusCode("E3004", "Required data not received from LDAP Server")
object MsisdnNotReceivedSpml extends StatusCode("E3005", "Msisdn not received with SPML GW response")
object SpmlGwErrorResp extends StatusCode("E3006", "Error response received from SPML GW")
object DBFailure extends StatusCode("E3007", "DB failure occurred")
object DataMissingInMessage extends StatusCode("E3008", "Required data missing")
object NoAuthenticationVectorFound extends StatusCode("E3009", "Authentication vector not found")
object ClientErrorCodeReceived extends StatusCode("E3010", "Client Error Code received from mobile")
object IdentityWithoutIMSI extends StatusCode("E3011", "No User name available in Identity request")
object ReAuthCacheFailure extends StatusCode("E3012", "Re-auth cache failure occurred")
object ReAuthenticationContextNotFound extends StatusCode("E3013", "Re-Authentication context not found")
object ReAuthenticationContextTimeOut extends StatusCode("E3014", "Re-Authentication context timeout")
object AsynchronousResponse extends StatusCode("E3015", "Response will send asynchronously")
object SigtranGwErrorResp extends StatusCode("E3016", "Error response received from SigtranGw")
object SigtranGwMaxRetryExceeds extends StatusCode("E3017", "Max retries exceeds on Sigtran Gw")
object AllSigtranConnectorsDown extends StatusCode("E3018", "All the Sigtran connectors down")

object PseudonymContextTimedOut extends StatusCode("E3020", "Pseudonym context timeout")
object PseudonymCacheFailure extends StatusCode("E3021", "Pseudonym cache failure occurred")
object PseudonymContextNotFound extends StatusCode("E3022", "Pseudonym context not found")
object DecryptFailed extends StatusCode("E3023", "Failure to decrypt the encrypted identity")
object UnKnownKeyIdentifier extends StatusCode("E3024", "Private key not found for the given key identifier")
object KeyRetrievingFailed extends StatusCode("E3025", "Private Key retrieval failed")

object EapNakError extends StatusCode("E4001", "Legacy NAK - Invalid EAP plugin")
object Timeout extends StatusCode("E4002", "Timeout")