package hms.eapsim.spml

import java.util

import org.apache.logging.log4j.scala.Logging
import hms.smpl.connector.command.{RequestTemplate, CommandExecutor}
import hms.smpl.connector.factory.impl.CommandFactoryImpl
import hms.smpl.connector.processor.impl.CommandProcessorImpl
import hms.smpl.connector.service.PropertyBasedErrorCodeMapServiceImpl
import hms.smpl.connector.xml.TemplateRequestProcessor
import hms.smpl.connector.xml.xpath.{XPathResponseProcessorImpl, DataExtractPoint}

trait SpmlGatewayConnectorComponent {

  def spmlGWConnector: SpmlGWConnector

  class SpmlGWConnector(templateRelativePath: String, searchTemplateName: String, imsiKey: String, msisdnKey: String,
                        msisdnXpath: String, msisdnResponseId: Int,
                         spmlDownMessage: String, spmlUpMessage: String) extends Logging {

    private val searchRespMsisdn: DataExtractPoint = new DataExtractPoint(msisdnKey, msisdnXpath)
    private val mandatoryDataExtractPoints: util.ArrayList[DataExtractPoint] = new util.ArrayList[DataExtractPoint]()
    mandatoryDataExtractPoints.add(searchRespMsisdn)
    private val defaultDataExtractPoints: util.ArrayList[DataExtractPoint] = new util.ArrayList[DataExtractPoint]()
    defaultDataExtractPoints.add(searchRespMsisdn)

    private val errorCodeMapService: PropertyBasedErrorCodeMapServiceImpl = new PropertyBasedErrorCodeMapServiceImpl
    private val templateRequestProcessor: TemplateRequestProcessor = new TemplateRequestProcessor
    private val xpathResponseProcessor: XPathResponseProcessorImpl = new XPathResponseProcessorImpl
    private val commandExecutor: CommandExecutor = new CommandExecutor

    private val commandFactory: CommandFactoryImpl = new CommandFactoryImpl
    commandFactory.setRequestProcessor(templateRequestProcessor)
    commandFactory.setResponseProcessor(xpathResponseProcessor)
    commandFactory.setExecutor(commandExecutor)
    commandFactory.setErrorCodeMapService(errorCodeMapService)
    commandFactory.setRelativeTemplatePath(templateRelativePath)
    commandFactory.setDefaultUpdateInfoList(defaultDataExtractPoints)
    commandFactory.setSpmlDownMessage(spmlDownMessage)
    commandFactory.setSpmlUpMessage(spmlUpMessage)

    val commandProcessor: CommandProcessorImpl = new CommandProcessorImpl
    commandProcessor.setFactory(commandFactory)

    val searchRequestTemplate: RequestTemplate = new RequestTemplate
    searchRequestTemplate.setRequestFileName(searchTemplateName)
    searchRequestTemplate.setMandatoryDataExtractPointList(mandatoryDataExtractPoints)

    def init() {
      errorCodeMapService.init()
    }

  }

}
