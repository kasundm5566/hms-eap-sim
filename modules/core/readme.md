# Eap Authentication with Eap-SIM and Eap-AKA

Eap-SIM and Eap-AKA are protocols used to handle SIM based authentication in 2G/3G networks. Protocols are described in following RFCs.

- Eap-SIM - RFC4186
- Eap-AKA - RFC4187

These RFC documents can be found at `docs/RFC` folder.

Eap-SIM is used for authentication of 2G SIM and Eap-AKA is used for 3G SIM.

Basic message flow for Eap-SIM can be found in section *3.  Overview* of RFC4186
Basic message flow for Eap-AKA can be found in section *3. Protocol Overview* of RFC4187

## Key Generation Formulars

Refer to following documents.

- Eap-SIM - eap-sim-key-generation.md
- Eap-AKA - eap-aka-key-generation.md