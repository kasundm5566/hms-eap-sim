package hms.eapsim.apiserver.repository.impl;

import hms.eapsim.apiserver.domain.CertificateStatus;
import hms.eapsim.apiserver.domain.Keystore;
import hms.eapsim.apiserver.repository.CertificateRepository;
import hms.eapsim.apiserver.repository.KeystoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by kasun on 9/28/18.
 */
@Component
public class CertificateRepositoryImpl implements CertificateRepository {

    @Autowired
    private KeystoreRepository keystoreRepository;

    @Override
    public Keystore retrieveCertificate(String keyIdentifier) {
        return keystoreRepository.findCertificate(keyIdentifier, CertificateStatus.ENABLED.toString());
    }

    @Override
    public Keystore retrieveCertificateIgnoreStatus(String keyIdentifier) {
        return keystoreRepository.findOne(keyIdentifier);
    }

    @Override
    public int addCertificate(Keystore keystore) {
        if (!keystoreRepository.exists(keystore.getKeyIdentifier())) {
            keystoreRepository.save(keystore);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int updateCertificateStatus(String keyIdentifier, CertificateStatus status, String user) {
        return keystoreRepository.updateCertificateStatus(keyIdentifier, status.toString(), user);
    }

    @Override
    public void updateCertificate(Keystore keystore) {
        keystoreRepository.save(keystore);
    }

    @Override
    public int deleteCertificate(String keyIdentifier) {
        if (keystoreRepository.exists(keyIdentifier)) {
            keystoreRepository.delete(keyIdentifier);
            return 1;
        } else {
            return 0;
        }
    }
}
