package hms.eapsim.apiserver.domain;

/**
 * Created by kasun on 10/11/18.
 */
public enum EventSource {
    RETRIEVE_CERTIFICATE,
    ADD_CERTIFICATE,
    UPDATE_CERTIFICATE,
    UPDATE_CERTIFICATE_STATUS,
    DELETE_CERTIFICATE
}
