package hms.eapsim.apiserver.repository;

import hms.eapsim.apiserver.domain.CertificateStatus;
import hms.eapsim.apiserver.domain.Keystore;

/**
 * Created by kasun on 9/28/18.
 */
public interface CertificateRepository {

    Keystore retrieveCertificate(String keyIdentifier);

    Keystore retrieveCertificateIgnoreStatus(String keyIdentifier);

    int addCertificate(Keystore keystore);

    int updateCertificateStatus(String keyIdentifier, CertificateStatus status, String user);

    void updateCertificate(Keystore keystore);

    int deleteCertificate(String keyIdentifier);
}
