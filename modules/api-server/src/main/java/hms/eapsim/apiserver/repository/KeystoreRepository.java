package hms.eapsim.apiserver.repository;

import hms.eapsim.apiserver.domain.Keystore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by kasun on 9/28/18.
 */
@Component
public interface KeystoreRepository extends JpaRepository<Keystore, String> {

    @Query(value = "SELECT * FROM keystore WHERE key_identifier = ?1 AND status = ?2", nativeQuery = true)
    Keystore findCertificate(String keyIdentifier, String status);

    @Modifying
    @Transactional
    @Query(value = "UPDATE keystore k SET k.status = ?2, k.user = ?3 WHERE k.key_identifier = ?1", nativeQuery = true)
    int updateCertificateStatus(String keyIdentifier, String status, String user);
}
