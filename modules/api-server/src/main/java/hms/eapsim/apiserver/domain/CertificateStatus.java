package hms.eapsim.apiserver.domain;

/**
 * Created by kasun on 10/1/18.
 */
public enum CertificateStatus {
    ENABLED, DISABLED
}
