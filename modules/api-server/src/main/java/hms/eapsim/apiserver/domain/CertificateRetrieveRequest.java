package hms.eapsim.apiserver.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kasun on 10/1/18.
 */
public class CertificateRetrieveRequest {
    private String keyIdentifier;

    @JsonProperty("key-identifier")
    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public void setKeyIdentifier(String keyIdentifier) {
        this.keyIdentifier = keyIdentifier;
    }

    public String toString() {
        return "{ key-identifier: " + keyIdentifier + " }";
    }
}
