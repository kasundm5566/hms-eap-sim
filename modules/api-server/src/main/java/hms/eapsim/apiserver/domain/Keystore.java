package hms.eapsim.apiserver.domain;

import org.springframework.util.Base64Utils;

import javax.persistence.*;

/**
 * Created by kasun on 9/28/18.
 */
@Entity
@Table(name = "keystore")
public class Keystore {
    @Id
    @Column(name = "key_identifier")
    private String keyIdentifier;
    @Lob
    @Column(name = "certificate")
    private byte[] certificate;
    @Lob
    @Column(name = "private_key")
    private byte[] privateKey;
    @Column(name = "status")
    private String status;
    @Column(name = "algorithm")
    private String algorithm;
    @Column(name = "key_size")
    private int keySize;
    @Column(name = "format")
    private String format;
    @Column(name = "user")
    private String user;

    public Keystore() {
    }

    public Keystore(String keyIdentifier, byte[] certificate, byte[] privateKey, String status, String algorithm,
                    int keySize, String format, String user) {
        this.keyIdentifier = keyIdentifier;
        this.certificate = certificate;
        this.privateKey = privateKey;
        this.status = status;
        this.algorithm = algorithm;
        this.keySize = keySize;
        this.format = format;
        this.user = user;
    }

    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public void setKeyIdentifier(String keyIdentifier) {
        this.keyIdentifier = keyIdentifier;
    }

    public byte[] getCertificate() {
        return certificate;
    }

    public void setCertificate(byte[] certificate) {
        this.certificate = certificate;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(byte[] privateKey) {
        this.privateKey = privateKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) {
        this.keySize = keySize;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String toString() {
        return "{ key_identifier: " + keyIdentifier + ", public_key: " + Base64Utils.encodeToString(certificate) +
                ", private_key: " + Base64Utils.encodeToString(privateKey) + ", status: " + status + ", algorithm: " + algorithm +
                ", key-size" + keySize + ", format: " + format + ", user: " + user + " }";
    }
}
