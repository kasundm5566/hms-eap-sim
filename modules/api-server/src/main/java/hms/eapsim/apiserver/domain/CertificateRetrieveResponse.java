package hms.eapsim.apiserver.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by kasun on 9/28/18.
 */
@JsonPropertyOrder({"key-identifier", "certificate"})
public class CertificateRetrieveResponse {
    private String keyIdentifier;
    private String certificate;

    public CertificateRetrieveResponse(String keyIdentifier, String certificate) {
        this.keyIdentifier = keyIdentifier;
        this.certificate = certificate;
    }

    @JsonProperty("key-identifier")
    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public void setKeyIdentifier(String keyIdentifier) {
        this.keyIdentifier = keyIdentifier;
    }

    @JsonProperty("certificate")
    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String toString() {
        return "{ key-identifier: " + keyIdentifier + ", certificate: " + certificate + " }";
    }
}
