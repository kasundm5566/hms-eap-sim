package hms.eapsim.apiserver.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kasun on 10/12/18.
 */
public class CertificateDeleteRequest {

    private String keyIdentifier;
    private String user;

    @JsonProperty("key-identifier")
    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public void setKeyIdentifier(String keyIdentifier) {
        this.keyIdentifier = keyIdentifier;
    }

    @JsonProperty("user")
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String toString() {
        return "{ key-identifier: " + keyIdentifier + ", user: " + user + " }";
    }
}
