package hms.eapsim.apiserver.service;

import hms.eapsim.apiserver.domain.CertificateDeleteRequest;
import hms.eapsim.apiserver.domain.CertificateRetrieveRequest;
import hms.eapsim.apiserver.domain.UpdateCertificateStatusRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by kasun on 10/1/18.
 */
public interface CertificateService {

    ResponseEntity<Object> retrieveCertificate(@RequestBody CertificateRetrieveRequest request);

    ResponseEntity<Object> addCertificate(@RequestPart(value = "key-identifier") String keyIdentifier,
                                          @RequestPart(value = "certificate") MultipartFile certificateFile,
                                          @RequestPart(value = "private-key") MultipartFile privateKeyFile,
                                          @RequestPart(value = "algorithm") String algorithm,
                                          @RequestParam(value = "key-size") int keySize,
                                          @RequestPart(value = "format") String format,
                                          @RequestPart(value = "user") String user);

    ResponseEntity<Object> updateCertificateStatus(@RequestBody UpdateCertificateStatusRequest request);

    ResponseEntity<Object> updateCertificate(@RequestPart(value = "key-identifier") String keyIdentifier,
                                             @RequestPart(value = "certificate") MultipartFile certificateFile,
                                             @RequestPart(value = "private-key") MultipartFile privateKeyFile,
                                             @RequestPart(value = "algorithm", required = false) String algorithm,
                                             @RequestParam(value = "key-size", required = false) Integer keySize,
                                             @RequestPart(value = "format", required = false) String format,
                                             @RequestPart(value = "status", required = false) String status,
                                             @RequestPart(value = "user") String user);

    ResponseEntity<Object> deleteCertificate(@RequestBody CertificateDeleteRequest request);
}