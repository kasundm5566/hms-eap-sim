package hms.eapsim.apiserver.service.impl;

import hms.eapsim.apiserver.domain.*;
import hms.eapsim.apiserver.repository.CertificateRepository;
import hms.eapsim.apiserver.service.CertificateService;
import hms.eapsim.apiserver.util.TransLogWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * Created by kasun on 9/28/18.
 */
@Controller
@RequestMapping(value = "/certificate")
public class CertificateServiceImpl implements CertificateService {

    private static final Logger logger = LogManager.getLogger(CertificateServiceImpl.class);

    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private TransLogWriter transLogWriter;

    @RequestMapping(value = "/retrieve", method = RequestMethod.POST, produces = {"application/json"},
            consumes = {"application/json"})
    @ResponseBody
    @Override
    public ResponseEntity<Object> retrieveCertificate(@RequestBody CertificateRetrieveRequest request) {
        logger.info("Retrieve certificate request: [{}]", request);
        long requestId = new Date().getTime();
        try {
            Keystore keystore = certificateRepository.retrieveCertificate(request.getKeyIdentifier());
            CertificateRetrieveResponse certificateRetrieveResponse;
            if (keystore != null) {
                certificateRetrieveResponse = new CertificateRetrieveResponse(keystore.getKeyIdentifier(),
                        Base64Utils.encodeToString(keystore.getCertificate()));
            } else {
                certificateRetrieveResponse = new CertificateRetrieveResponse(request.getKeyIdentifier(), "");
            }
            logger.info("Retrieve certificate response: [{}]", certificateRetrieveResponse);
            transLogWriter.writeLogEntry(requestId, EventSource.RETRIEVE_CERTIFICATE, HttpStatus.OK.getReasonPhrase(), "", request.toString() + " " + certificateRetrieveResponse.toString());
            return new ResponseEntity<Object>(certificateRetrieveResponse, HttpStatus.OK);
        } catch (Exception ex) {
            GenericServerResponse errorResponse = new GenericServerResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
            logger.error("Retrieve certificate response: [{}]", errorResponse);
            logger.error("Error in retrieve certificate: ", ex);
            transLogWriter.writeLogEntry(requestId, EventSource.RETRIEVE_CERTIFICATE, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), "", request.toString() + " " + ex.getMessage());
            return new ResponseEntity<Object>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json"},
            consumes = {"multipart/form-data"})
    @ResponseBody
    @Override
    public ResponseEntity<Object> addCertificate(@RequestPart(value = "key-identifier") String keyIdentifier,
                                                 @RequestPart(value = "certificate") MultipartFile certificateFile,
                                                 @RequestPart(value = "private-key") MultipartFile privateKeyFile,
                                                 @RequestPart(value = "algorithm") String algorithm,
                                                 @RequestParam(value = "key-size") int keySize,
                                                 @RequestPart(value = "format") String format,
                                                 @RequestPart(value = "user") String user) {
        logger.info("Add new keys to the key-identifier: [{}]", keyIdentifier);
        long requestId = new Date().getTime();
        try {
            Keystore keystore = new Keystore();
            keystore.setKeyIdentifier(keyIdentifier);
            keystore.setAlgorithm(algorithm);
            keystore.setKeySize(keySize);
            keystore.setFormat(format);
            keystore.setCertificate(certificateFile.getBytes());
            keystore.setPrivateKey(privateKeyFile.getBytes());
            keystore.setStatus(CertificateStatus.ENABLED.toString());
            keystore.setUser(user);
            int status = certificateRepository.addCertificate(keystore);
            if (status == 0) {
                GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.CONFLICT.value(), "Cannot add new keys. Key-identifier already exists.");
                logger.info("Add new keys response: [{}]", successResponse);
                transLogWriter.writeLogEntry(requestId, EventSource.ADD_CERTIFICATE, HttpStatus.CONFLICT.getReasonPhrase(), user, keystore.toString() + " " + successResponse.toString());
                return new ResponseEntity<Object>(successResponse, HttpStatus.CONFLICT);
            }
            GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.OK.value(), "New keys added successfully.");
            logger.info("Add new keys response: [{}]", successResponse);
            transLogWriter.writeLogEntry(requestId, EventSource.ADD_CERTIFICATE, HttpStatus.OK.getReasonPhrase(), user, keystore.toString() + " " + successResponse.toString());
            return new ResponseEntity<Object>(successResponse, HttpStatus.OK);
        } catch (Exception ex) {
            GenericServerResponse errorResponse = new GenericServerResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
            logger.error("Add new keys response: [{}]", errorResponse);
            logger.error("Error in adding new keys: ", ex);
            transLogWriter.writeLogEntry(requestId, EventSource.ADD_CERTIFICATE, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), user, ex.getMessage());
            return new ResponseEntity<Object>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/update-status", method = RequestMethod.PUT, produces = {"application/json"},
            consumes = {"application/json"})
    @ResponseBody
    @Override
    public ResponseEntity<Object> updateCertificateStatus(@RequestBody UpdateCertificateStatusRequest request) {
        logger.info("Update the certificate status of the key-identifier: [{}] to [{}]", request.getKeyIdentifier(), request.getCertificateStatus());
        long requestId = new Date().getTime();
        try {
            if (request.getUser() != null) {
                int status = certificateRepository.updateCertificateStatus(request.getKeyIdentifier(), request.getCertificateStatus(), request.getUser());
                if (status == 1) {
                    GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.OK.value(), "Certificate status updated successfully.");
                    logger.info("Update the certificate status response: [{}]", successResponse);
                    transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE_STATUS, HttpStatus.OK.getReasonPhrase(), request.getUser(), request.toString() + " " + successResponse.toString());
                    return new ResponseEntity<Object>(successResponse, HttpStatus.OK);
                }
                GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.BAD_REQUEST.value(), "Cannot update certificate status. Key-identifier may not be available.");
                logger.info("Update the certificate status response: [{}]", successResponse);
                transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE_STATUS, HttpStatus.BAD_REQUEST.getReasonPhrase(), request.getUser(), request.toString() + " " + successResponse.toString());
                return new ResponseEntity<Object>(successResponse, HttpStatus.BAD_REQUEST);
            } else {
                GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.BAD_REQUEST.value(), "Required request param 'user' is not present.");
                logger.info("Update the certificate status response: [{}]", successResponse);
                transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE_STATUS, HttpStatus.BAD_REQUEST.getReasonPhrase(), request.getUser(), request.toString() + " " + successResponse.toString());
                return new ResponseEntity<Object>(successResponse, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            GenericServerResponse errorResponse = new GenericServerResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
            logger.error("Update the certificate status response: [{}]", errorResponse);
            logger.error("Error update the certificate status ", ex);
            transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE_STATUS, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), request.getUser(), request.toString() + " " + errorResponse.toString());
            return new ResponseEntity<Object>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = {"application/json"},
            consumes = {"multipart/form-data"})
    @ResponseBody
    @Override
    public ResponseEntity<Object> updateCertificate(@RequestPart(value = "key-identifier") String keyIdentifier,
                                                    @RequestPart(value = "certificate") MultipartFile certificateFile,
                                                    @RequestPart(value = "private-key") MultipartFile privateKeyFile,
                                                    @RequestPart(value = "algorithm", required = false) String algorithm,
                                                    @RequestParam(value = "key-size", required = false) Integer keySize,
                                                    @RequestPart(value = "format", required = false) String format,
                                                    @RequestPart(value = "status", required = false) String status,
                                                    @RequestPart(value = "user") String user) {
        logger.info("Update the details of the key-identifier: [{}]", keyIdentifier);
        long requestId = new Date().getTime();
        try {
            Keystore keystore = certificateRepository.retrieveCertificateIgnoreStatus(keyIdentifier);
            if (keystore != null) {
                keystore.setCertificate(certificateFile.getBytes());
                keystore.setPrivateKey(privateKeyFile.getBytes());
                keystore.setUser(user);
                if (algorithm != null) {
                    keystore.setAlgorithm(algorithm);
                }
                if (keySize != null) {
                    keystore.setKeySize(keySize);
                }
                if (format != null) {
                    keystore.setFormat(format);
                }
                if (status != null) {
                    keystore.setStatus(status);
                }
                certificateRepository.updateCertificate(keystore);
                GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.OK.value(), "Keys updated successfully.");
                logger.info("Update keys response: [{}]", successResponse);
                transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE, HttpStatus.OK.getReasonPhrase(), user, keystore.toString() + " " + successResponse.toString());
                return new ResponseEntity<Object>(successResponse, HttpStatus.OK);
            }
            GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.BAD_REQUEST.value(), "Cannot update keys. Key-identifier does not exists.");
            logger.info("Update keys response: [{}]", successResponse);
            transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE, HttpStatus.BAD_REQUEST.getReasonPhrase(), user, keystore.toString() + " " + successResponse.toString());
            return new ResponseEntity<Object>(successResponse, HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            GenericServerResponse errorResponse = new GenericServerResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
            logger.error("Update keys response: [{}]", errorResponse);
            logger.error("Error in updating keys: ", ex);
            transLogWriter.writeLogEntry(requestId, EventSource.UPDATE_CERTIFICATE, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), user, errorResponse.toString());
            return new ResponseEntity<Object>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = {"application/json"},
            consumes = {"application/json"})
    @ResponseBody
    @Override
    public ResponseEntity<Object> deleteCertificate(@RequestBody CertificateDeleteRequest request) {
        logger.info("Delete the certificate of the key-identifier: [{}]", request.getKeyIdentifier());
        long requestId = new Date().getTime();
        try {
            if (request.getUser() != null) {
                int status = certificateRepository.deleteCertificate(request.getKeyIdentifier());
                if (status == 0) {
                    GenericServerResponse response = new GenericServerResponse(HttpStatus.BAD_REQUEST.value(), "Cannot delete keys. Key-identifier does not exists.");
                    logger.info("Delete keys response: [{}]", response);
                    transLogWriter.writeLogEntry(requestId, EventSource.DELETE_CERTIFICATE, HttpStatus.BAD_REQUEST.getReasonPhrase(), request.getUser(), request.toString() + " " + response.toString());
                    return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
                }
                GenericServerResponse successResponse = new GenericServerResponse(HttpStatus.OK.value(), "Keys deleted successfully.");
                logger.info("Delete keys response: [{}]", successResponse);
                transLogWriter.writeLogEntry(requestId, EventSource.DELETE_CERTIFICATE, HttpStatus.OK.getReasonPhrase(), request.getUser(), request.toString() + " " + successResponse.toString());
                return new ResponseEntity<Object>(successResponse, HttpStatus.OK);
            } else {
                GenericServerResponse response = new GenericServerResponse(HttpStatus.BAD_REQUEST.value(), "Required request param 'user' is not present.");
                logger.info("Delete keys response: [{}]", response);
                transLogWriter.writeLogEntry(requestId, EventSource.DELETE_CERTIFICATE, HttpStatus.BAD_REQUEST.getReasonPhrase(), request.getUser(), request.toString() + " " + response.toString());
                return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            GenericServerResponse errorResponse = new GenericServerResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
            logger.error("Delete keys response: [{}]", errorResponse);
            logger.error("Error in deleting keys: ", ex);
            transLogWriter.writeLogEntry(requestId, EventSource.DELETE_CERTIFICATE, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), request.getUser(), request.toString() + " " + errorResponse.toString());
            return new ResponseEntity<Object>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
