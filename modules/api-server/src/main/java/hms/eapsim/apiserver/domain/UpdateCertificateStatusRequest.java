package hms.eapsim.apiserver.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kasun on 10/3/18.
 */
public class UpdateCertificateStatusRequest {

    private String keyIdentifier;
    private CertificateStatus certificateStatus;
    private String user;

    public UpdateCertificateStatusRequest() {
    }

    public UpdateCertificateStatusRequest(String keyIdentifier, CertificateStatus certificateStatus, String user) {
        this.keyIdentifier = keyIdentifier;
        this.certificateStatus = certificateStatus;
        this.user = user;
    }

    @JsonProperty("key-identifier")
    public String getKeyIdentifier() {
        return keyIdentifier;
    }

    public void setKeyIdentifier(String keyIdentifier) {
        this.keyIdentifier = keyIdentifier;
    }

    @JsonProperty("status")
    public CertificateStatus getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(CertificateStatus certificateStatus) {
        this.certificateStatus = certificateStatus;
    }

    @JsonProperty("user")
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String toString() {
        return "{ key-identifier: " + keyIdentifier + ", status: " + certificateStatus + ", user: " + user + " }";
    }
}
