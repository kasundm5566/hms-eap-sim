package hms.eapsim.apiserver.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by kasun on 11/2/18.
 */
@Component
@ConfigurationProperties("translog")
public class TransLogConfigurations {
    private String dateFormat;

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }
}
