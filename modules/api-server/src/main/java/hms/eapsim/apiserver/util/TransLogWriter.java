package hms.eapsim.apiserver.util;

import hms.eapsim.apiserver.config.TransLogConfigurations;
import hms.eapsim.apiserver.domain.EventSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kasun on 10/11/18.
 */
@Component
public class TransLogWriter {

    private static final Logger logger = LoggerFactory.getLogger("TransLogger");

    @Autowired
    private TransLogConfigurations transLogConfigurations;

    public void writeLogEntry(Long requestId,EventSource eventSource, String status, String user, String requestResposeDetails) {
        DateFormat dateFormat = new SimpleDateFormat(transLogConfigurations.getDateFormat());
        Date date = new Date();
        logger.info("{}|{}|{}|{}|{}|{}", dateFormat.format(date), requestId, eventSource.toString(), status, user, requestResposeDetails);
    }
}
