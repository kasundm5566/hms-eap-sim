package hms.eapsim.apiserver.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by kasun on 10/1/18.
 */
@JsonPropertyOrder({"status-code", "description"})
public class GenericServerResponse {

    private int statusCode;
    private String description;

    public GenericServerResponse() {
    }

    public GenericServerResponse(int statusCode, String description) {
        this.statusCode = statusCode;
        this.description = description;
    }

    @JsonProperty("status-code")
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return "{ status-code: " + statusCode + ", description: " + description + " }";
    }
}
