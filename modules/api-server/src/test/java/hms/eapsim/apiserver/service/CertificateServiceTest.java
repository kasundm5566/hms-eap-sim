package hms.eapsim.apiserver.service;

import hms.eapsim.apiserver.domain.*;
import hms.eapsim.apiserver.repository.KeystoreRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by kasun on 10/9/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CertificateService.class)
@EnableJpaRepositories(basePackageClasses = KeystoreRepository.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ComponentScan({"hms.eapsim.apiserver"})
public class CertificateServiceTest {

    @Autowired
    private CertificateService certificateService;

    private String certificateString = "MIIC2jCCAkMCAg38MA0GCSqGSIb3DQEBBQUAMIGbMQswCQYDVQQGEwJKUDEOMAwGA1UECBMFVG9reW8xEDAOBgNVBAcTB0NodW8ta3UxETAPBgNVBAoTCEZyYW5rNEREMRgwFgYDVQQLEw9XZWJDZXJ0IFN1cHBvcnQxGDAWBgNVBAMTD0ZyYW5rNEREIFdlYiBDQTEjMCEGCSqGSIb3DQEJARYUc3VwcG9ydEBmcmFuazRkZC5jb20wHhcNMTIwODIyMDUyNzQxWhcNMTcwODIxMDUyNzQxWjBKMQswCQYDVQQGEwJKUDEOMAwGA1UECAwFVG9reW8xETAPBgNVBAoMCEZyYW5rNEREMRgwFgYDVQQDDA93d3cuZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC0z9FeMynsC8+udvX+LciZxnh5uRj4C9S6tNeeAlIGCfQYk0zUcNFCoCkTknNQd/YEiawDLNbxBqutbMDZ1aarys1a0lYmUeVLCIqvzBkPJTSQsCopQQ9V8WuT252zzNzs68dVGNdCJd5JNRQykpwexmnjPPv0mvj7i8XgG379TyW6P+WWV5okeUkXJ9eJS2ouDYdR2SM9BoVW+FgxDu6BmXhozW5EfsnajFp7HL8kQClI0QOc79yuKl3492rH6bzFsFn2lfwWy9ic7cP8EpCTeFp1tFaD+vxBhPZkeTQ1HKx6hQ5zeHIB5ySJJZ7af2W8r4eTGYzbdRW24DDHCPhZAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAQMv+BFvGdMVzkQaQ3/+2noVz/uAKbzpEL8xTcxYyP3lkOeh4FoxiSWqy5pGFALdPONoDuYFpLhjJSZaEwuvjI/TrrGhLV1pRG9frwDFshqD2Vaj4ENBCBh6UpeBop5+285zQ4SI7q4U9oSebUDJiuOx6+tZ9KynmrbJpTSi0+BMK";

    @Before
    public void beforeTest() throws IOException {
        ResponseEntity<Object> responseEntity = certificateService.addCertificate("beforetest",
                new MockMultipartFile("certificate", new FileInputStream(new ClassPathResource("2048b-rsa-example-cert.der").getFile())),
                new MockMultipartFile("privateKey", new FileInputStream(new ClassPathResource("2048b-rsa-example-keypair.pem").getFile())),
                "RSA", 2048, "DER", "user");
        Assert.assertEquals("200", responseEntity.getStatusCode().toString());
    }

    @Test
    public void addCertificate() throws IOException {
        ResponseEntity<Object> responseEntity = certificateService.addCertificate("test",
                new MockMultipartFile("certificate", new FileInputStream(new ClassPathResource("2048b-rsa-example-cert.der").getFile())),
                new MockMultipartFile("privateKey", new FileInputStream(new ClassPathResource("2048b-rsa-example-keypair.pem").getFile())),
                "RSA", 2048, "DER", "user");
        Assert.assertEquals("200", responseEntity.getStatusCode().toString());
    }

    @Test
    public void addDuplicateCertificate() throws IOException {
        ResponseEntity<Object> responseEntity = certificateService.addCertificate("beforetest",
                new MockMultipartFile("certificate", new FileInputStream(new ClassPathResource("2048b-rsa-example-cert.der").getFile())),
                new MockMultipartFile("privateKey", new FileInputStream(new ClassPathResource("2048b-rsa-example-keypair.pem").getFile())),
                "RSA", 2048, "DER", "user");
        Assert.assertEquals("409", responseEntity.getStatusCode().toString());
    }

    @Test
    public void retrieveCertificate() throws NoSuchFieldException {
        CertificateRetrieveRequest request = new CertificateRetrieveRequest();
        request.setKeyIdentifier("beforetest");
        ResponseEntity<Object> responseEntity = certificateService.retrieveCertificate(request);
        Assert.assertEquals("beforetest", ((CertificateRetrieveResponse) responseEntity.getBody()).getKeyIdentifier());
    }

    @Test
    public void retrieveCertificateEncodedString() throws NoSuchFieldException {
        CertificateRetrieveRequest request = new CertificateRetrieveRequest();
        request.setKeyIdentifier("beforetest");
        ResponseEntity<Object> responseEntity = certificateService.retrieveCertificate(request);
        String encodedCertificate = ((CertificateRetrieveResponse) responseEntity.getBody()).getCertificate();
        Assert.assertEquals(certificateString, encodedCertificate);
    }

    @Test
    public void disableCertificate(){
        UpdateCertificateStatusRequest request = new UpdateCertificateStatusRequest();
        request.setKeyIdentifier("beforetest");
        request.setCertificateStatus(CertificateStatus.DISABLED);
        request.setUser("user");
        ResponseEntity<Object> responseEntity = certificateService.updateCertificateStatus(request);
        Assert.assertEquals("200", responseEntity.getStatusCode().toString());
    }

    @Test
    public void disableCertificateByInvalidKeyId(){
        UpdateCertificateStatusRequest request = new UpdateCertificateStatusRequest();
        request.setKeyIdentifier("beforetes");
        request.setCertificateStatus(CertificateStatus.DISABLED);
        request.setUser("user");
        ResponseEntity<Object> responseEntity = certificateService.updateCertificateStatus(request);
        Assert.assertEquals("400", responseEntity.getStatusCode().toString());
    }

    @Test
    public void enableCertificate(){
        UpdateCertificateStatusRequest request = new UpdateCertificateStatusRequest();
        request.setKeyIdentifier("beforetest");
        request.setCertificateStatus(CertificateStatus.ENABLED);
        request.setUser("user");
        ResponseEntity<Object> responseEntity = certificateService.updateCertificateStatus(request);
        Assert.assertEquals("200", responseEntity.getStatusCode().toString());
    }

    @Test
    public void deleteCertificate() {
        CertificateDeleteRequest request = new CertificateDeleteRequest();
        request.setKeyIdentifier("test");
        request.setUser("user");
        ResponseEntity<Object> responseEntity = certificateService.deleteCertificate(request);
        Assert.assertEquals("200", responseEntity.getStatusCode().toString());
    }

    @Test
    public void deleteCertificateByInvalidKeyId() {
        CertificateDeleteRequest request = new CertificateDeleteRequest();
        request.setKeyIdentifier("tes");
        request.setUser("user");
        ResponseEntity<Object> responseEntity = certificateService.deleteCertificate(request);
        Assert.assertEquals("400", responseEntity.getStatusCode().toString());
    }

    @After
    public void afterTests() {
        CertificateDeleteRequest request = new CertificateDeleteRequest();
        request.setKeyIdentifier("beforetest");
        request.setUser("user");
        certificateService.deleteCertificate(request);
    }
}
