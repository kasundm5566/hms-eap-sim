package hms.eapsim.apiserver.boot;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import wiremock.org.apache.http.HttpResponse;
import wiremock.org.apache.http.client.methods.HttpPost;
import wiremock.org.apache.http.entity.StringEntity;
import wiremock.org.apache.http.impl.client.CloseableHttpClient;
import wiremock.org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by kasun on 10/9/18.
 */
@RunWith(SpringRunner.class)
public class WiremockTest {

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(8000);

    @Rule
    public WireMockClassRule instanceRule = wireMockRule;

    @Test
    public void retrieveCertificate() {
        stubFor(post(urlEqualTo("/api-server/v1/certificate/retrieve"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(containing("{\n" +
                        "  \"key-identifier\":\"testkey\"\n" +
                        "}"))
                .willReturn(aResponse()
                        .withBody("{" +
                                "\"key-identifier\":\"testkey\"," +
                                "\"certificate\":\"MIIC2jCCAkMCAg38MA0GCSqGSIb3DQEBBQUAMIGbMQswCQYDVQQGEwJKUDEOMAwGA1UECBMFVG9reW8xEDAOBgNVBAcTB0NodW8ta3UxETAPBgNVBAoTCEZyYW5rNEREMRgwFgYDVQQLEw9XZWJDZXJ0IFN1cHBvcnQxGDAWBgNVBAMTD0ZyYW5rNEREIFdlYiBDQTEjMCEGCSqGSIb3DQEJARYUc3VwcG9ydEBmcmFuazRkZC5jb20wHhcNMTIwODIyMDUyNzQxWhcNMTcwODIxMDUyNzQxWjBKMQswCQYDVQQGEwJKUDEOMAwGA1UECAwFVG9reW8xETAPBgNVBAoMCEZyYW5rNEREMRgwFgYDVQQDDA93d3cuZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC0z9FeMynsC8+udvX+LciZxnh5uRj4C9S6tNeeAlIGCfQYk0zUcNFCoCkTknNQd/YEiawDLNbxBqutbMDZ1aarys1a0lYmUeVLCIqvzBkPJTSQsCopQQ9V8WuT252zzNzs68dVGNdCJd5JNRQykpwexmnjPPv0mvj7i8XgG379TyW6P+WWV5okeUkXJ9eJS2ouDYdR2SM9BoVW+FgxDu6BmXhozW5EfsnajFp7HL8kQClI0QOc79yuKl3492rH6bzFsFn2lfwWy9ic7cP8EpCTeFp1tFaD+vxBhPZkeTQ1HKx6hQ5zeHIB5ySJJZ7af2W8r4eTGYzbdRW24DDHCPhZAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAQMv+BFvGdMVzkQaQ3/+2noVz/uAKbzpEL8xTcxYyP3lkOeh4FoxiSWqy5pGFALdPONoDuYFpLhjJSZaEwuvjI/TrrGhLV1pRG9frwDFshqD2Vaj4ENBCBh6UpeBop5+285zQ4SI7q4U9oSebUDJiuOx6+tZ9KynmrbJpTSi0+BMK\"" +
                                "}")
                        .withStatus(200)));
        try {
            StringEntity entity = generateRequestEntityFromJsonFile("retrieve_certificate_request.json");
            HttpResponse response = sendPostRequest("http://localhost:8000/api-server/v1/certificate/retrieve",
                    "application/json", entity);
            assertEquals("{\"key-identifier\":\"testkey\"," +
                    "\"certificate\":\"MIIC2jCCAkMCAg38MA0GCSqGSIb3DQEBBQUAMIGbMQswCQYDVQQGEwJKUDEOMAwGA1UECBMFVG9reW8" +
                    "xEDAOBgNVBAcTB0NodW8ta3UxETAPBgNVBAoTCEZyYW5rNEREMRgwFgYDVQQLEw9XZWJDZXJ0IFN1cHBvcnQxGDAWBgNVBAMT" +
                    "D0ZyYW5rNEREIFdlYiBDQTEjMCEGCSqGSIb3DQEJARYUc3VwcG9ydEBmcmFuazRkZC5jb20wHhcNMTIwODIyMDUyNzQxWhcNM" +
                    "TcwODIxMDUyNzQxWjBKMQswCQYDVQQGEwJKUDEOMAwGA1UECAwFVG9reW8xETAPBgNVBAoMCEZyYW5rNEREMRgwFgYDVQQDDA" +
                    "93d3cuZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC0z9FeMynsC8+udvX+LciZxnh5uRj4C9S" +
                    "6tNeeAlIGCfQYk0zUcNFCoCkTknNQd/YEiawDLNbxBqutbMDZ1aarys1a0lYmUeVLCIqvzBkPJTSQsCopQQ9V8WuT252zzNzs" +
                    "68dVGNdCJd5JNRQykpwexmnjPPv0mvj7i8XgG379TyW6P+WWV5okeUkXJ9eJS2ouDYdR2SM9BoVW+FgxDu6BmXhozW5Efsnaj" +
                    "Fp7HL8kQClI0QOc79yuKl3492rH6bzFsFn2lfwWy9ic7cP8EpCTeFp1tFaD+vxBhPZkeTQ1HKx6hQ5zeHIB5ySJJZ7af2W8r4" +
                    "eTGYzbdRW24DDHCPhZAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAQMv+BFvGdMVzkQaQ3/+2noVz/uAKbzpEL8xTcxYyP3lkOeh" +
                    "4FoxiSWqy5pGFALdPONoDuYFpLhjJSZaEwuvjI/TrrGhLV1pRG9frwDFshqD2Vaj4ENBCBh6UpeBop5+285zQ4SI7q4U9oSeb" +
                    "UDJiuOx6+tZ9KynmrbJpTSi0+BMK\"}", convertHttpResponseToString(response));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteCertificate() {
        stubFor(post(urlEqualTo("/api-server/v1/certificate/delete"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(containing("{\n" +
                        "  \"key-identifier\":\"testkey\",\n" +
                        "  \"user\":\"user\"\n" +
                        "}"))
                .willReturn(aResponse()
                        .withBody("{" +
                                "\"status-code\":200," +
                                "\"description\":\"Keys deleted successfully.\"" +
                                "}")
                        .withStatus(200)));
        try {
            StringEntity entity = generateRequestEntityFromJsonFile("delete_certificate_request.json");
            HttpResponse response = sendPostRequest("http://localhost:8000/api-server/v1/certificate/delete",
                    "application/json", entity);
            assertEquals("{\"status-code\":200,\"description\":\"Keys deleted successfully.\"}",
                    convertHttpResponseToString(response));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void updateCertificateStatus() {
        stubFor(post(urlEqualTo("/api-server/v1/certificate/update-status"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(containing("{\n" +
                        "  \"key-identifier\":\"1234\",\n" +
                        "  \"status\":\"DISABLED\",\n" +
                        "  \"user\":\"user\"\n" +
                        "}"))
                .willReturn(aResponse()
                        .withBody("{" +
                                "\"status-code\":200," +
                                "\"description\":\"Certificate status updated successfully.\"" +
                                "}")
                        .withStatus(200)));
        try {
            StringEntity entity = generateRequestEntityFromJsonFile("update_certificate_status_request.json");
            HttpResponse response = sendPostRequest("http://localhost:8000/api-server/v1/certificate/update-status",
                    "application/json", entity);
            assertEquals("{\"status-code\":200,\"description\":\"Certificate status updated successfully.\"}",
                    convertHttpResponseToString(response));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private StringEntity generateRequestEntityFromJsonFile(String fileName) throws UnsupportedEncodingException {
        InputStream jsonInputStream
                = this.getClass().getClassLoader().getResourceAsStream(fileName);
        String jsonString = convertInputStreamToString(jsonInputStream);
        StringEntity entity = new StringEntity(jsonString);
        return entity;
    }

    private HttpResponse sendPostRequest(String url, String contentType, StringEntity entity) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(url);
        request.addHeader("Content-Type", contentType);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        return response;
    }

    private String convertHttpResponseToString(HttpResponse httpResponse) throws IOException {
        InputStream inputStream = httpResponse.getEntity().getContent();
        return convertInputStreamToString(inputStream);
    }

    private String convertInputStreamToString(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        String string = scanner.useDelimiter("\\Z").next();
        scanner.close();
        return string;
    }
}
