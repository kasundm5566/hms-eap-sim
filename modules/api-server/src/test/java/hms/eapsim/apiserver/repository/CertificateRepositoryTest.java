package hms.eapsim.apiserver.repository;

import hms.eapsim.apiserver.domain.CertificateStatus;
import hms.eapsim.apiserver.domain.Keystore;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by kasun on 10/4/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CertificateRepository.class)
@EnableJpaRepositories(basePackageClasses = KeystoreRepository.class)
@EntityScan(basePackageClasses = Keystore.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ComponentScan({"hms.eapsim.apiserver"})
public class CertificateRepositoryTest {

    @Autowired
    private CertificateRepository certificateRepository;

    @Before
    public void beforeTests() {
        Keystore keystore = new Keystore();
        keystore.setKeyIdentifier("testbefore");
        keystore.setCertificate("testbefore123".getBytes());
        keystore.setPrivateKey("testbefore123".getBytes());
        keystore.setAlgorithm("RSA");
        keystore.setStatus(CertificateStatus.ENABLED.toString());
        keystore.setKeySize(2048);
        keystore.setFormat("DER");
        certificateRepository.addCertificate(keystore);
    }

    @Test
    public void addCertificate() {
        Keystore keystore = new Keystore();
        keystore.setKeyIdentifier("test");
        keystore.setCertificate("test123".getBytes());
        keystore.setPrivateKey("test123".getBytes());
        keystore.setAlgorithm("RSA");
        keystore.setStatus(CertificateStatus.ENABLED.toString());
        keystore.setKeySize(2048);
        keystore.setFormat("DER");
        int status = certificateRepository.addCertificate(keystore);
        Assert.assertEquals(1, status);
    }

    @Test
    public void retrieveCertificate() {
        Keystore keystore = certificateRepository.retrieveCertificate("testbefore");
        Assert.assertNotNull(keystore);
    }

    @Test
    public void retrieveCertificateFormat() {
        Keystore keystore = certificateRepository.retrieveCertificate("testbefore");
        Assert.assertEquals("DER", keystore.getFormat());
    }

    @Test
    public void retrieveCertificateStatus() {
        Keystore keystore = certificateRepository.retrieveCertificate("testbefore");
        Assert.assertEquals(CertificateStatus.ENABLED.toString(), keystore.getStatus());
    }

    @Test
    public void retrieveCertificateAlgorithm() {
        Keystore keystore = certificateRepository.retrieveCertificate("testbefore");
        Assert.assertEquals("RSA", keystore.getAlgorithm());
    }

    @Test
    public void retrieveCertificateKeySize() {
        Keystore keystore = certificateRepository.retrieveCertificate("testbefore");
        Assert.assertEquals(2048, keystore.getKeySize());
    }

    @Test
    public void retrieveDisabledCertificate() {
        certificateRepository.updateCertificateStatus("testbefore", CertificateStatus.DISABLED, "testuser");
        Keystore keystore = certificateRepository.retrieveCertificate("testbefore");
        Assert.assertNull(keystore);
        certificateRepository.updateCertificateStatus("testbefore", CertificateStatus.ENABLED, "testuser");
    }

    @Test
    public void enableCertificate() {
        int status = certificateRepository.updateCertificateStatus("testbefore", CertificateStatus.ENABLED, "testuser");
        Assert.assertEquals(1, status);
    }

    @Test
    public void disableCertificate() {
        int status = certificateRepository.updateCertificateStatus("testbefore", CertificateStatus.DISABLED, "testuser");
        Assert.assertEquals(1, status);
    }

    @Test
    public void deleteCertificate() {
        int status = certificateRepository.deleteCertificate("test");
        Assert.assertEquals(1, status);
    }

    @Test
    public void deleteCertificateByInvalidKeyId() {
        int status = certificateRepository.deleteCertificate("tes");
        Assert.assertEquals(0, status);
    }

    @After
    public void afterTests() {
        certificateRepository.deleteCertificate("testbefore");
    }
}