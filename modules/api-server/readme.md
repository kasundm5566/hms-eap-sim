# Api-Server: Public Key Management Web Portal
---

Provide a RESTful API to support the key management.

#### Pre-requisites
* JDK 1.7.0_80
* Mysql 5.7.23
* Maven 3.5.0 or later

#### Setup MySQL database
1. It is required to have a MySQL user with username 'user' and password 'password'.

2. Check modules/dbscripts/create-table.sql and add `keystore` table into DB

#### How to build
Run following command.
`$ mvn clean install -DskipTests -Dhttps.protocols=TLSv1.2`

#### Execute the program
1. Goto the target directory and extract the tar file(`api-server-<version>.tar.gz`).

1. Goto the bin directory and execute the following command.
`$ cd <extracted-directory>/bin`</br>

1. Run the following command to start the api-server.
`$ ./api-server start`</br>

#### API calls
Use the postman collection(api-server.postman_collection.json) in the following location to test API requests.
`<project-root>/modules/api-server/docs`