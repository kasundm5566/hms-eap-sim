package hms.eapsim.async.http.client.exception;

public enum ErrorCode {

    SIGTRAN_GW_ERROR_RESPONSE_RECEIVED,
    SIGTRAN_GW_MAX_RETRIES_EXCEED,
    SIGTRAN_GW_ALL_CLIENTS_DOWN

}
