package hms.eapsim.async.http.client.connector.impl;

import hms.common.radius.util.DefaultThreadFactory;
import hms.commons.SnmpLogUtil;
import hms.eapsim.async.http.client.client.ClientManager;
import hms.eapsim.async.http.client.connector.RequestContext;
import hms.eapsim.async.http.client.connector.SigtranConnector;
import hms.eapsim.async.http.client.exception.ErrorCode;
import hms.eapsim.async.http.client.exception.SigtranConnectorException;
import hms.eapsim.async.http.client.listner.ResponseListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class SigtranConnectorImpl implements SigtranConnector {

    private Map<String, RequestContext> requestStore;
    private ClientManager clientManager;
    private ResponseListener responseListener;
    private String successErrorCode;
    private List<String> retriableErrorCodes;
    private int maxRetryCount;

    private int requestTimeoutInMillis;
    private int timeoutSchedulerPeriodInMillis;
    private ScheduledExecutorService timeoutTriggeringScheduler;
    private ExecutorService timeoutExecutorService;
    private int timeoutPoolSize;

    private String sigtranUpSnmpMessage;
    private String sigtranDownSnmpMessage;

    private static final String SUCCESS_STATUS_CODE_KEY = "statusCode";

    private static final Logger logger = LogManager.getLogger(SigtranConnectorImpl.class);

    public SigtranConnectorImpl(ClientManager clientManager, ResponseListener responseListener, String successErrorCode,
                                List<String> retriableErrorCodes, int maxRetryCount, int requestTimeoutInMillis,
                                int timeoutSchedulerPeriodInMillis, int timeoutPoolSize,
                                String sigtranUpSnmpMessage, String sigtranDownSnmpMessage) {
        this.clientManager = clientManager;
        this.responseListener = responseListener;
        this.successErrorCode = successErrorCode;
        this.retriableErrorCodes = retriableErrorCodes;
        this.maxRetryCount = maxRetryCount;
        this.requestTimeoutInMillis = requestTimeoutInMillis;
        this.timeoutSchedulerPeriodInMillis = timeoutSchedulerPeriodInMillis;
        this.timeoutPoolSize = timeoutPoolSize;
        this.sigtranUpSnmpMessage = sigtranUpSnmpMessage;
        this.sigtranDownSnmpMessage = sigtranDownSnmpMessage;
    }

    public void init() {
        requestStore = new ConcurrentHashMap<>();
        timeoutExecutorService = Executors.newFixedThreadPool(timeoutPoolSize, new DefaultThreadFactory("Sigtran-client-timeout"));
        timeoutTriggeringScheduler = Executors.newScheduledThreadPool(1);
        timeoutTriggeringScheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                List<String> timeOutedContexts = new ArrayList<>();
                for(final RequestContext requestContext : requestStore.values()) {
                    long lastUpdatedTime = requestContext.getLastUpdatedTime();
                    if(currentTime > lastUpdatedTime + requestTimeoutInMillis) {
                        timeOutedContexts.add(requestContext.getCorrelationId());
                    }
                }
                logger.trace("Request store, Total count [{}], Time-Outed count [{}]", requestStore.size(), timeOutedContexts.size());
                for(final String correlationId: timeOutedContexts) {
                    timeoutExecutorService.submit(new Runnable() {
                        @Override
                        public void run() {
                            logger.info("Sigtran request has timeout and retry if possible, correlationId[{}]", correlationId);
                            retry(correlationId, requestStore.get(correlationId).getLastSentClientId());
                        }
                    });
                }
            }
        }, 5000, timeoutSchedulerPeriodInMillis, TimeUnit.MILLISECONDS);
    }

    @Override
    public void doPost(String correlationId, String json, String uri, Object sigtranGwRequestContext) {
        RequestContext requestContext = new RequestContext(correlationId, json, uri, sigtranGwRequestContext);
        requestStore.put(correlationId, requestContext);
        if(requestContext.isProcessing().compareAndSet(false, true)) {
            try {
                String clientId = clientManager.doPost(correlationId, json, uri);
                requestContext.setLastSentClientId(clientId);
            } catch (Exception ex) {
                logger.error("Exception occurred while sending request", ex);
                remove(correlationId);
                responseListener.onFailure(correlationId, sigtranGwRequestContext, ex);
            } finally {
                requestContext.isProcessing().set(false);
            }
        } else {
            logger.info("Request is already in progress and skipping retrying, correlationId [{}]", correlationId);
        }
    }

    @Override
    public void retry(String correlationId, String clientId) {
        clientManager.removeActiveClient(clientId);
        if(requestStore.containsKey(correlationId)) {
            RequestContext requestContext = requestStore.get(correlationId);
            int retryCount = requestContext.getRetryCount();
            if(retryCount >= maxRetryCount) {
                logger.error("Max retry counts reached for the correlationId [{}]", correlationId);
                remove(correlationId);
                SnmpLogUtil.trap("SigtranSNMPTrapSAI", sigtranDownSnmpMessage);
                responseListener.onFailure(correlationId, requestContext.getSigtranGwRequestContext(),
                        new SigtranConnectorException("Max retry count reached for the request, correlationId[" + correlationId + "]", ErrorCode.SIGTRAN_GW_MAX_RETRIES_EXCEED));
            } else {
                if(requestContext.isProcessing().compareAndSet(false, true)) {
                    logger.info("Retrying SigtranGWReq - SAI [request: {}, retryCount: {}]", requestContext.getJson(), retryCount);
                    requestContext.setLastUpdatedTime(System.currentTimeMillis());
                    requestContext.setRetryCount(retryCount + 1);
                    try {
                        String lastSentClientId = clientManager.doPost(correlationId, requestContext.getJson(), requestContext.getUri());
                        requestContext.setLastSentClientId(lastSentClientId);
                    } catch (Exception ex) {
                        logger.error("Exception occurred while retrying", ex);
                        remove(correlationId);
                        SnmpLogUtil.trap("SigtranSNMPTrapSAI", sigtranDownSnmpMessage);
                        responseListener.onFailure(correlationId, requestContext.getSigtranGwRequestContext(), ex);
                    } finally {
                        requestContext.isProcessing().set(false);
                    }
                } else {
                    logger.info("Request is already in progress and skipping retrying, correlationId [{}]", correlationId);
                }
            }
        } else {
            logger.error("Expected request can not find in the store for the correlationId [{}]", correlationId);
        }
    }

    @Override
    public void resetProcessing(String correlationId) {
        if(requestStore.containsKey(correlationId)) {
            RequestContext requestContext = requestStore.get(correlationId);
            requestContext.isProcessing().set(false);
        }
    }

    @Override
    public RequestContext remove(String correlationId) {
        RequestContext remove = null;
        if(requestStore.containsKey(correlationId)) {
            remove = requestStore.remove(correlationId);
        } else {
            logger.debug("Expected request context can not find in the store, for the correlationId [{}]", correlationId);
        }
        return remove;
    }

    @Override
    public void onMessage(String correlationId, String content, String clientId) {
        String statusCode = extractStatusCode(content);
        if (successErrorCode.equals(statusCode)) {
            RequestContext removedContext = remove(correlationId);
            SnmpLogUtil.clearTrap("SigtranSNMPTrapSAI", sigtranUpSnmpMessage);
            if(removedContext != null) {
                responseListener.onMessage(content, correlationId, removedContext.getSigtranGwRequestContext());
            } else {
                logger.warn("Flow might be already processed by an another Sigtran connector response or already timeout, " +
                        "for the correlationId [{}] and ignore current Sigtran response", correlationId);
            }
        } else if (retriableErrorCodes.contains(statusCode)) {
            logger.info("Error code status is [{}] in SAI Request, Going to retry for next connector [ res: {}]", statusCode, content);
            retry(correlationId, clientId);
        } else {
            RequestContext removedContext = remove(correlationId);
            responseListener.onFailure(correlationId, removedContext.getSigtranGwRequestContext(),
                    new SigtranConnectorException("Sigtran gateway returned a error code [" + statusCode + "]", ErrorCode.SIGTRAN_GW_ERROR_RESPONSE_RECEIVED));
        }
    }

    @Override
    public void onException(Throwable cause) {
        responseListener.onException(cause);
    }

    private String extractStatusCode(String content) {
        String[] entries = content.split(",");
        for(String entry : entries) {
            if(entry.contains(SUCCESS_STATUS_CODE_KEY)) {
                return entry.split("\\:")[1].replaceAll("\"", "");
            }
        }
        return "";
    }

}
