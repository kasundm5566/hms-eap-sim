package hms.eapsim.async.http.client.client;

public class FaultyClientContext {
    
    private Client faultyClient;
    private long lastUpdatedTime;

    public FaultyClientContext(Client faultyClient) {
        this.faultyClient = faultyClient;
        this.lastUpdatedTime = System.currentTimeMillis();
    }

    public Client getFaultyClient() {
        return faultyClient;
    }

    public long getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @Override
    public String toString() {
        return "FaultyClientContext{" +
                "faultyClient=" + faultyClient +
                ", lastUpdatedTime=" + lastUpdatedTime +
                '}';
    }
}
