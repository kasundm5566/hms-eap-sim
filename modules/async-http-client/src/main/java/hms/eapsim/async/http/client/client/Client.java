package hms.eapsim.async.http.client.client;

import io.netty.channel.Channel;

public interface Client extends Comparable<Client> {

    void start();

    void stop();

    void reconnect();

    void doPost(String correlationId, String json, String uri);

    boolean isAllChannelsInactive();

    void notifyChannelActive(Channel channel);

    void notifyChannelInactive(Channel channel);

    String getClientId();

    String getHost();

    int getPort();

    int getPriority();

}
