package hms.eapsim.async.http.client.connector;

import java.util.concurrent.atomic.AtomicBoolean;

public class RequestContext {

    private String correlationId;
    private String json;
    private String uri;
    private int retryCount;
    private long lastUpdatedTime;
    private AtomicBoolean isProcessing;
    private String lastSentClientId;
    private Object sigtranGwRequestContext;

    public RequestContext(String correlationId, String json, String uri, Object sigtranGwRequestContext) {
        this.correlationId = correlationId;
        this.json = json;
        this.uri = uri;
        this.retryCount = 1;
        this.lastUpdatedTime = System.currentTimeMillis();
        this.isProcessing = new AtomicBoolean(false);
        this.sigtranGwRequestContext = sigtranGwRequestContext;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public String getJson() {
        return json;
    }

    public String getUri() {
        return uri;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public long getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public AtomicBoolean isProcessing() {
        return isProcessing;
    }

    public String getLastSentClientId() {
        return lastSentClientId;
    }

    public void setLastSentClientId(String lastSentClientId) {
        this.lastSentClientId = lastSentClientId;
    }

    public Object getSigtranGwRequestContext() {
        return sigtranGwRequestContext;
    }

    @Override
    public String toString() {
        return "RequestContext{" +
                "correlationId='" + correlationId + '\'' +
                ", json='" + json + '\'' +
                ", uri='" + uri + '\'' +
                ", retryCount=" + retryCount +
                ", lastUpdatedTime=" + lastUpdatedTime +
                ", isProcessing=" + isProcessing +
                ", sigtranGwRequestContext=" + sigtranGwRequestContext +
                '}';
    }
}
