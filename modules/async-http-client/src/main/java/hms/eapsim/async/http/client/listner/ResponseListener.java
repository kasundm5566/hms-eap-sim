package hms.eapsim.async.http.client.listner;

/**
 * This would not notified when ever message is receiving via channel
 */
public interface ResponseListener {

    /**
     * On message received this will be notified with  the message
     *
     * @param content response that received
     * @param correlationId correlationId of the request
     * @param sigtranGwRequestContext application level request context
     */
    void onMessage(String content, String correlationId, Object sigtranGwRequestContext);

    /**
     * On failure occurred during the message sending, this will be called with respective correlationId
     *
     * @param correlationId correlationId of the request
     * @param sigtranGwRequestContext application level request context
     * @param cause cause of the error
     */
    void onFailure(String correlationId, Object sigtranGwRequestContext, Throwable cause);

    /**
     * On exception occurred during the message sending, this will be called
     *
     * @param cause cause of the error
     */
    void onException(Throwable cause);
}
