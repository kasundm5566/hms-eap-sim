package hms.eapsim.async.http.client.client.impl;

import hms.common.radius.util.DefaultThreadFactory;
import hms.eapsim.async.http.client.client.Client;
import hms.eapsim.async.http.client.client.ClientAttributeKeys;
import hms.eapsim.async.http.client.client.ClientManager;
import hms.eapsim.async.http.client.listner.ConnectionListener;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AsyncHttpClient implements Client {

    private String clientId;
    private int priority;
    private String host;
    private int port;
    private DefaultThreadFactory threadFactory;
    private int numberOfClientWorker = 10;
    private int maxConnections;

    private ClientManager clientManager;

    private ScheduledExecutorService scheduler;
    private int reconnectDelayInSeconds = 10;

    private Bootstrap bootstrap;
    private List<Channel> channels;
    private ReadWriteLock lock;

    private final int SEQUENCE_NUM_MAX = Integer.MAX_VALUE;
    private final int SEQUENCE_NUM_MIN = 1;
    private final AtomicInteger GENERATOR = new AtomicInteger(SEQUENCE_NUM_MIN);

    private static final Logger logger = LogManager.getLogger(AsyncHttpClient.class);

    public AsyncHttpClient(String clientId, int priority, String host, int port, DefaultThreadFactory threadFactory,
                           int numberOfClientWorker, int maxConnections, int reconnectDelayInSeconds, ClientManager clientManager) {
        this.clientId = clientId;
        this.priority = priority;
        this.host = host;
        this.port = port;
        this.threadFactory = threadFactory;
        this.numberOfClientWorker = numberOfClientWorker;
        this.maxConnections = maxConnections;
        this.reconnectDelayInSeconds = reconnectDelayInSeconds;
        this.clientManager = clientManager;
    }

    @Override
    public void start() {
        scheduler = Executors.newScheduledThreadPool(1);

        bootstrap = new Bootstrap();
        bootstrap.group(new NioEventLoopGroup(numberOfClientWorker, threadFactory));
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.handler(new HttpClientChannelInitializer());
        channels = new ArrayList<>();
        lock = new ReentrantReadWriteLock();

        for(int i = 0; i < maxConnections; i++) {
            ChannelFuture connect = bootstrap.connect(host, port);
            connect.addListener(new ConnectionListener(this));
            try {
                connect.sync().channel();
            } catch (Exception ex) {
                logger.error("Failed to start the channel [{}] of client [{}]", i, clientId);
            }
        }
    }

    @Override
    public void reconnect() {
        logger.info("Scheduling reconnect for the client [{}]", clientId);
        final AsyncHttpClient httpClient = this;
        scheduler.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    logger.debug("Reconnecting channel from the client [{}]", clientId);
                    ChannelFuture connect = bootstrap.connect(host, port);
                    connect.addListener(new ConnectionListener(httpClient));
                    connect.sync().channel();
                } catch (InterruptedException e) {
                    logger.error("Exception occurred while reconnecting from the client[{}]", clientId);
                }
            }
        }, reconnectDelayInSeconds, TimeUnit.SECONDS);
    }

    @Override
    public void stop() {
        bootstrap.shutdown();
    }

    /**
     * send a post request
     *
     * @param json the request being passed
     */
    @Override
    public void doPost(final String correlationId, String json, String uri) {
        logger.info("Sending SAI [clientId: {}]", clientId);

        ByteBuf content = Unpooled.copiedBuffer(json, StandardCharsets.UTF_8);

        FullHttpRequest request = new DefaultFullHttpRequest(
                HttpVersion.HTTP_1_1, HttpMethod.POST, uri, content);

        request.headers().set(HttpHeaders.Names.HOST, host);
        request.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE); // or HttpHeaders.Values.CLOSE
        request.headers().set(HttpHeaders.Names.ACCEPT_ENCODING, HttpHeaders.Values.GZIP);
        request.headers().add(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        request.headers().set(HttpHeaders.Names.CONTENT_LENGTH, content.readableBytes());
        request.headers().set("correlationId", correlationId);

        Channel nextActiveChannel;
        final int nextIndex;

        Lock readLock = lock.readLock();
        try {
            readLock.lock();
            if(channels.size() > 0) {
                nextIndex = getNextIndex() % channels.size();
                nextActiveChannel = channels.get(nextIndex);
            } else {
                nextIndex = 0;
                nextActiveChannel = null;
            }
        } finally {
            readLock.unlock();
        }

        if(nextActiveChannel != null) {
            final ChannelFuture channelFuture = nextActiveChannel.write(request);
            channelFuture.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        logger.debug("Sigtran message write operation successful through client [{}] channel [{}]", clientId, nextIndex);
                    } else {
                        logger.error("Operation has failed. Notifying client manager for retry.");
                        clientManager.onRequestFailure(correlationId, clientId);
                    }
                }
            });
        } else {
            logger.info("Client has no channels and forwarded to client manager.");
            clientManager.onRequestFailure(correlationId, clientId);
        }
    }

    @Override
    public boolean isAllChannelsInactive() {
        boolean isEmpty;
        Lock readLock = lock.readLock();
        try {
            readLock.lock();
            isEmpty = channels.isEmpty();
        } finally {
            readLock.unlock();
        }
        return isEmpty;
    }

    @Override
    public void notifyChannelActive(Channel channel) {
        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            channels.add(channel);
        } finally {
            writeLock.unlock();
        }
        channel.attr(ClientAttributeKeys.CLIENT_ID_ATTR_KEY).set(clientId);
    }

    @Override
    public void notifyChannelInactive(Channel channel) {
        Integer channelId;
        Lock readLock = lock.readLock();
        try {
            readLock.lock();
            channelId = channels.indexOf(channel);
        } finally {
            readLock.unlock();
        }
        logger.debug("Channel going to remove clientId [{}], channelId [{}]", clientId, channelId);
        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            channels.remove(channel);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public int compareTo(Client client) {
        return priority - client.getPriority();
    }

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    private int getNextIndex() {
        int currentValue;
        int nextValue;
        do {
            currentValue = GENERATOR.get();
            nextValue = (currentValue + 1) == SEQUENCE_NUM_MAX ? SEQUENCE_NUM_MIN : (currentValue + 1);
        } while (!GENERATOR.compareAndSet(currentValue, nextValue));
        return nextValue;
    }

    @Override
    public String toString() {
        return "AsyncHttpClient{" +
                "clientId='" + clientId + '\'' +
                ", priority=" + priority +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", numberOfClientWorker=" + numberOfClientWorker +
                ", maxConnections=" + maxConnections +
                '}';
    }

    private class HttpClientChannelInitializer extends ChannelInitializer<io.netty.channel.socket.SocketChannel> {

        @Override
        protected void initChannel(io.netty.channel.socket.SocketChannel ch) throws Exception {
            ChannelPipeline pipeline = ch.pipeline();
            pipeline.addLast("log", new LoggingHandler(LogLevel.INFO));

            pipeline.addLast("codec", new HttpClientCodec());

            // Remove the following line if you don't want automatic content decompression.
            pipeline.addLast("inflater", new HttpContentDecompressor());

            // Uncomment the following line if you don't want to handle HttpChunks.
            pipeline.addLast("aggregator", new HttpObjectAggregator(1048576));

            HttpClientHandler handler = new HttpClientHandler();
            handler.setClientManager(clientManager);
            pipeline.addLast("handler", handler);
        }
    }

}
