package hms.eapsim.async.http.client.connector;

public interface SigtranConnector {

    void doPost(String correlationId, String json, String uri, Object sigtranGwRequestContext);

    void retry(String correlationId, String clientId);

    void resetProcessing(String correlationId);

    RequestContext remove(String correlationId);

    void onMessage(String correlationId, String content, String clientId);

    void onException(Throwable cause);

}
