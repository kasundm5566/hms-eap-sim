package hms.eapsim.async.http.client.client;

import io.netty.util.AttributeKey;

public class ClientAttributeKeys {

    public static final AttributeKey<String> CLIENT_ID_ATTR_KEY = new AttributeKey<>("clientId");

}
