package hms.eapsim.async.http.client.client.impl;

import hms.eapsim.async.http.client.client.Client;
import hms.eapsim.async.http.client.client.ClientManager;
import hms.eapsim.async.http.client.client.FaultyClientContext;
import hms.eapsim.async.http.client.connector.SigtranConnector;
import hms.eapsim.async.http.client.exception.ErrorCode;
import hms.eapsim.async.http.client.exception.SigtranConnectorException;
import io.netty.channel.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AsyncHttpClientManager implements ClientManager {

    private SigtranConnector sigtranConnector;
    private Map<String, Client> allClients;

    private ReadWriteLock lockOnActiveClients;
    private ReadWriteLock lockOnFaultyClients;
    private TreeSet<Client> activeClients;
    private Set<FaultyClientContext> faultyClients;
    private AtomicReference<Client> activeClient;

    private ScheduledExecutorService scheduledExecutorService;
    private long faultyConnectionValidityCheckPeriodInMillis;
    private long faultyConnectionIdlePeriodInMillis;

    private static final Logger logger = LogManager.getLogger(AsyncHttpClientManager.class);

    @Override
    public void init() throws Exception {
        lockOnActiveClients = new ReentrantReadWriteLock();
        lockOnFaultyClients = new ReentrantReadWriteLock();
        activeClient = new AtomicReference<>();
        faultyClients = new HashSet<>();
        activeClients = new TreeSet<>();
        for(Client client: allClients.values()) {
            client.start();
            addActiveClient(client);
        }

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                logger.trace("Faulty client check scheduler started. Faulty clients [{}]", faultyClients);
                long now = System.currentTimeMillis();
                List<FaultyClientContext> toBeActivatedClients = new ArrayList<>();
                Lock writeLock = lockOnFaultyClients.writeLock();
                try {
                    writeLock.lock();
                    for(FaultyClientContext faultyClientContext : faultyClients) {
                        if(now - faultyClientContext.getLastUpdatedTime() > faultyConnectionIdlePeriodInMillis) {
                            toBeActivatedClients.add(faultyClientContext);
                        }
                    }
                    faultyClients.removeAll(toBeActivatedClients);
                } finally {
                    writeLock.unlock();
                }
                for(FaultyClientContext toBeActivatedClient : toBeActivatedClients) {
                    logger.info("Client [{}] will be moved from faulty to active set", toBeActivatedClient.getFaultyClient().getClientId());
                    addActiveClient(toBeActivatedClient.getFaultyClient());
                }
            }
        }, 1000, faultyConnectionValidityCheckPeriodInMillis, TimeUnit.MILLISECONDS);
    }

    @Override
    public String doPost(String correlationId, String json, String uri) throws Exception {
        Client currentActiveClient;
        Lock readLock = lockOnActiveClients.readLock();
        try {
            readLock.lock();
            currentActiveClient = activeClient.get();
        } finally {
            readLock.unlock();
        }
        if(currentActiveClient != null) {
            currentActiveClient.doPost(correlationId, json, uri);
            return currentActiveClient.getClientId();
        } else {
            throw new SigtranConnectorException("No active clients found to send the request [" + correlationId + "]", ErrorCode.SIGTRAN_GW_ALL_CLIENTS_DOWN);
        }
    }

    @Override
    public void onMessage(String correlationId, String content, String clientId) {
        sigtranConnector.onMessage(correlationId, content, clientId);
    }

    @Override
    public void reconnect(String clientId) {
        Client currentClient = allClients.get(clientId);
        handleChannelReconnect(currentClient);
    }

    @Override
    public void channelActive(Channel channel) throws Exception {
        InetSocketAddress socketAddress = (InetSocketAddress)channel.remoteAddress();
        Client currentClient = findClientFromAddress(socketAddress);
        currentClient.notifyChannelActive(channel);
    }

    @Override
    public void channelInactive(String clientId, Channel channel) throws Exception {
        Client currentClient = allClients.get(clientId);
        currentClient.notifyChannelInactive(channel);
        handleChannelReconnect(currentClient);
    }

    @Override
    public void onResponseFailure(String correlationId, String clientId) {
        logger.error("Error occurred while requesting SAI for the correlationId [{}]", correlationId);
        sigtranConnector.retry(correlationId, clientId);
    }

    @Override
    public void onRequestFailure(String correlationId, String clientId) {
        logger.error("Error occurred while requesting SAI for the correlationId [{}]", correlationId);
        sigtranConnector.resetProcessing(correlationId);
        sigtranConnector.retry(correlationId, clientId);
    }

    @Override
    public void onException(Throwable cause) {
        sigtranConnector.onException(cause);
    }

    private void handleChannelReconnect(Client client) {
        client.reconnect();
    }

    private Client findClientFromAddress(InetSocketAddress address) throws Exception {
        for (Client client : allClients.values()) {
            if(client.getHost().equals(address.getAddress().getHostAddress()) && client.getPort() == address.getPort()) {
                return client;
            }
        }
        logger.error("Cannot find respective client");
        throw new Exception("Cannot find respective client for the address [" + address.getHostName() + ":" + address.getPort() + "]");
    }

    @Override
    public void removeActiveClient(String clientId) {
        Client currentClient = allClients.get(clientId);
        removeActiveClient(currentClient);
    }

    private void removeActiveClient(Client client) {
        boolean isRemoved = false;
        Lock writeLock = lockOnActiveClients.writeLock();
        try {
            writeLock.lock();
            isRemoved = activeClients.remove(client);
            if(isRemoved) {
                logger.info("Removed client [{}] from the active set and going to put into faulty set.", client.getClientId());
                changeActiveConnectorIfRequired();
                Lock writeLockOnFaultyClients = lockOnFaultyClients.writeLock();
                try {
                    writeLockOnFaultyClients.lock();
                    faultyClients.add(new FaultyClientContext(client));
                } finally {
                    writeLockOnFaultyClients.unlock();
                }
            }
        } finally {
            writeLock.unlock();
        }
    }

    private void addActiveClient(Client client) {
        boolean isAdded = false;
        Lock writeLock = lockOnActiveClients.writeLock();
        try {
            writeLock.lock();
            isAdded = activeClients.add(client);
        } finally {
            writeLock.unlock();
        }
        if(isAdded) {
            logger.info("Added client to the active set [{}]", client.getClientId());
            changeActiveConnectorIfRequired();
        }
    }

    private void changeActiveConnectorIfRequired() {
        Lock readLock = lockOnActiveClients.readLock();
        try {
            readLock.lock();
            if (activeClients.size() > 0) {
                Client currentClient = activeClient.get();
                Client newClient = activeClients.first();
                if(currentClient == null || !currentClient.getClientId().equals(newClient.getClientId())) {
                    logger.info("Changing active client from [{}] to [{}]", currentClient, newClient);
                    activeClient.set(newClient);
                } else {
                    logger.debug("Client is not changing");
                }
            } else {
                logger.info("No active clients found, Changing active client from [{}] to null", activeClient.get());
                activeClient.set(null);
            }
        } finally {
            readLock.unlock();
        }
    }

    public void setSigtranConnector(SigtranConnector sigtranConnector) {
        this.sigtranConnector = sigtranConnector;
    }

    public void setAllClients(Map<String, Client> allClients) {
        this.allClients = allClients;
    }

    public void setFaultyConnectionIdlePeriodInMillis(long faultyConnectionIdlePeriodInMillis) {
        this.faultyConnectionIdlePeriodInMillis = faultyConnectionIdlePeriodInMillis;
    }

    public void setFaultyConnectionValidityCheckPeriodInMillis(long faultyConnectionValidityCheckPeriodInMillis) {
        this.faultyConnectionValidityCheckPeriodInMillis = faultyConnectionValidityCheckPeriodInMillis;
    }
}
