package hms.eapsim.async.http.client.client.impl;

import hms.eapsim.async.http.client.client.ClientAttributeKeys;
import hms.eapsim.async.http.client.client.ClientManager;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.util.CharsetUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.net.ConnectException;

public class HttpClientHandler extends ChannelInboundMessageHandlerAdapter<Object> {

    private ClientManager clientManager;

    private static final String CORRELATION_ID_KEY = "correlationId";

    private static final Logger logger = LogManager.getLogger(HttpClientHandler.class);

    @Override
    public void messageReceived(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.debug("Message received [{}]", msg);
        if (msg instanceof HttpResponse) {
            HttpResponse response = (HttpResponse) msg;
            String correlationId = response.headers().get(CORRELATION_ID_KEY);
            String clientId = extractClientId(ctx.channel());
            HttpResponseStatus httpStatus = response.getStatus();
            try {
                ThreadContext.push(correlationId);
                if (httpStatus.equals(HttpResponseStatus.OK)) {
                    if (msg instanceof HttpContent) {
                        handleSuccess(correlationId, (HttpContent) msg, clientId);
                    } else {
                        logger.error("Content is not a http content [{}]", msg);
                        handleFailure(correlationId, httpStatus, clientId);
                    }
                } else {
                    logger.error("Http error code not success one [{}]", httpStatus);
                    handleFailure(correlationId, httpStatus, clientId);
                }
            } finally {
                ThreadContext.pop();
            }
        } else {
            logger.error("Received message is not a http response [{}]", msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("Exception caught on client handler [{}]", cause);
        if(cause instanceof ConnectException) {
            Channel currentChannel = ctx.channel();
            String clientId = extractClientId(currentChannel);
            clientManager.reconnect(clientId);
        } else {
            clientManager.onException(cause);
        }
        ctx.close();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("Channel activation detected [{}]", ctx);
        Channel currentChannel = ctx.channel();
        clientManager.channelActive(currentChannel);
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.info("Channel deactivation detected [{}]", ctx);
        Channel currentChannel = ctx.channel();
        String clientId = extractClientId(currentChannel);
        clientManager.channelInactive(clientId, currentChannel);
        super.channelInactive(ctx);
    }

    private void handleSuccess(String correlationId, HttpContent content, String clientId) {
        String message = content.data().toString(CharsetUtil.UTF_8);
        logger.info("success SAI response received");
        clientManager.onMessage(correlationId, message, clientId);
    }

    private void handleFailure(String correlationId, HttpResponseStatus httpStatus, String clientId) {
        if(correlationId != null && !correlationId.isEmpty()) {
            clientManager.onResponseFailure(correlationId, clientId);
        } else {
            clientManager.onException(new Exception("Sigtran gateway response error [" + httpStatus + "]"));
        }
    }

    private String extractClientId(Channel currentChannel) {
        return currentChannel.attr(ClientAttributeKeys.CLIENT_ID_ATTR_KEY).get();
    }

    public void setClientManager(ClientManager clientManager) {
        this.clientManager = clientManager;
    }
}