package hms.eapsim.async.http.client.listner;

import hms.eapsim.async.http.client.client.Client;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConnectionListener implements ChannelFutureListener {

    private static final Logger logger = LogManager.getLogger(ConnectionListener.class);

    private Client client;

    public ConnectionListener(Client client) {
        this.client = client;
    }

    @Override
    public void operationComplete(ChannelFuture channelFuture) throws Exception {
        if (channelFuture.isSuccess()) {
            logger.debug("Connection has successful for the client [{}]", client.getClientId());
        } else {
            logger.info("Connection has not successful for the client [{}]", client.getClientId());
            client.reconnect();
        }
    }

}
