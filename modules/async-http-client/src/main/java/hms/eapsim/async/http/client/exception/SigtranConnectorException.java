package hms.eapsim.async.http.client.exception;

public class SigtranConnectorException extends Exception {

    private ErrorCode errorCode;

    public SigtranConnectorException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public SigtranConnectorException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public SigtranConnectorException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public SigtranConnectorException(Throwable cause, ErrorCode errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public SigtranConnectorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ErrorCode errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
