package hms.eapsim.async.http.client.client;

import io.netty.channel.Channel;

public interface ClientManager {

    void init() throws Exception;

    String doPost(String correlationId, String json, String uri) throws Exception;

    void onMessage(String correlationId, String content, String clientId);

    void reconnect(String clientId);

    void channelActive(Channel channel) throws Exception;

    void channelInactive(String clientId, Channel channel) throws Exception;

    void removeActiveClient(String clientId);

    void onResponseFailure(String correlationId, String clientId);

    void onRequestFailure(String correlationId, String clientId);

    void onException(Throwable cause);

}
